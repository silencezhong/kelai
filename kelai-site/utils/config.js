/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

module.exports = {
    title: '科来客流',
    contextPath: '/',
    viewPrefix: 'view',
    publicPath: "",
    mode: "",
    mock: false,
    cdn: '//cdn.bootcss.com',
    // cdn: '',
    production: false,
    imagePath:'',
    apiServerUrl: 'http://127.0.0.1:9111',

    apiFilePath: '/../apiConfig.json',
    i18n : {
        locales: ['en', 'zh', 'tw'],
        directory: 'utils\\local',
        defaultLocale : 'zh',
        extension: '.json'
    }
};
