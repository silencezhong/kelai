'use strict';
const api = require('../utils/api');
let serverSocket = require('dgram').createSocket('udp4');

serverSocket.on('message', function(msg, rinfo){
    console.log('recv %s(%d bytes) from client %s:%d\n', msg.toString('hex'), msg.length, rinfo.address, rinfo.port);
    //echo to client
    //2430201612070000000049310000012300000000000000104931060010390000001523
    global.tool.send({
        method: 'post',
        uri: api.get('579e72dde170433ea0b4b333e5113842'),
        json: {
            "cmd":"video",
            "data":msg.toString('hex')
        }
    }, function (error, response, body) {

    });

    serverSocket.send(msg, 0, msg.length, rinfo.port, rinfo.address);
});
//    err - Error object, https://nodejs.org/api/errors.html
serverSocket.on('error', function(err){
    console.log('error, msg - %s, stack - %s\n', err.message, err.stack);
});

serverSocket.on('listening', function(){
    console.log("echo server is listening on port 9019.");
});
serverSocket.bind(9109);
console.log('udp server bind on port: 9109');
module.exports = serverSocket;