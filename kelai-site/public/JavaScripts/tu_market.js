/**
 * Created by Administrator on 2016/11/10.
 */
//====================================================================================================
var myChart = echarts.init(document.getElementById('main6'));
option6 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['客流量','销售额','客单量','转化率'],
        top:'-1%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['周一','周二','周三','周四','周五','周六','周日']
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"活动分析",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'客流量',
            type:'line',
            data:[11, 11, 15, 7, 12, 13, 6],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'销售额',
            type:'line',
            data:[10, 20, 15, 4, 30, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        },
        {
            name:'客单量',
            type:'line',
            data:[10, 9, 20, 50, 35, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        },
        {
            name:'转化率',
            type:'line',
            data:[7, 20, 20, 5, 30, 15, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        }
    ]
};

myChart.setOption(option6);
//==========================777777777777777777777777777777777777777777777
//
var myChart = echarts.init(document.getElementById('main9'));
option9 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['客流量','销售额','客单量','转化率'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['周一','周二','周三','周四','周五','周六','周日'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"天气分析",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'客流量',
            type:'line',
            data:[11, 11, 15, 7, 12, 13, 6],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'销售额',
            type:'line',
            data:[10, 20, 15, 4, 30, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        },
        {
            name:'客单量',
            type:'line',
            data:[10, 9, 20, 50, 35, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        },
        {
            name:'转化率',
            type:'line',
            data:[7, 20, 20, 5, 30, 15, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option9);
//
//=============================11,12,13
var myChart = echarts.init(document.getElementById('main12'));
option12 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['客流量','销售额','客单量','转化率'],
        top:'-1%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['周一','周二','周三','周四','周五','周六','周日'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"节假日分析",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'客流量',
            type:'line',
            data:[11, 11, 15, 7, 12, 13, 6],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'销售额',
            type:'line',
            data:[10, 20, 15, 4, 30, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        },
        {
            name:'客单量',
            type:'line',
            data:[10, 9, 20, 50, 35, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        },
        {
            name:'转化率',
            type:'line',
            data:[7, 20, 20, 5, 30, 15, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option12);
////======================================================================14,15,16
var myChart = echarts.init(document.getElementById('main15'));
option15 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['客流量','销售额','客单量','转化率'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['周一','周二','周三','周四','周五','周六','周日'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"商圈分析",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'客流量',
            type:'line',
            data:[11, 11, 15, 7, 12, 13, 6],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'销售额',
            type:'line',
            data:[10, 20, 15, 4, 30, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        },
        {
            name:'客单量',
            type:'line',
            data:[10, 9, 20, 50, 35, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        },
        {
            name:'转化率',
            type:'line',
            data:[7, 20, 20, 5, 30, 15, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option15);