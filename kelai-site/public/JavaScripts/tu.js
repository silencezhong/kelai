/**
 * Created by Administrator on 2016/11/9.
 */
// 基于准备好的dom，初始化echarts实例
var myChart = echarts.init(document.getElementById('main'));

// 指定图表的配置项和数据
//客流量
option = {
    title: {
        x: 'center',
        text: '客流量',
        subtext: 'Passenger flow statistics',
        textStyle:{
            // color:'red',
            fontSize:14,
            fontWeight:400
        }
    },
    tooltip: {
        trigger: 'item'
    },
    toolbox: {
        show: false,
        feature: {
            dataView: {show: true, readOnly: false},
            restore: {show: true},
            saveAsImage: {show: true}
        }
    },
    calculable: true,
    grid: {
        borderWidth: 0,
        y: 80,
        y2: 60
    },
    xAxis: [
        {
            type: 'category',
            show: false,
            data: ['昨日', '今日']
        }
    ],
    yAxis: [
        {
            type: 'value',
            show: false
        }
    ],
    series: [
        {
            name: '客流量',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: function(params) {
                        // build a color map as your need.
                        var colorList = [
                            '#B5C334','#C1232B'
                        ];
                        return colorList[params.dataIndex]
                    },
                    label: {
                        show: true,
                        position: 'top',
                        formatter: '{b}\n{c}'
                    }
                }
            },
            data: [5000,6000],
            markPoint: {
                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0)',
                    formatter: function(params){
                        return '<img src="'
                            + params.data.symbol.replace('image://', '')
                            + '"/>';
                    }
                },
                data: [
                    {xAxis:0, y: 350, name:'昨日', symbolSize:20, symbol: 'image://../asset/ico/折线图.png'},
                    {xAxis:1, y: 350, name:'今日', symbolSize:20, symbol: 'image://../asset/ico/柱状图.png'},

                ]
            }
        }
    ]
};



// 使用刚指定的配置项和数据显示图表。
myChart.setOption(option);

var myChart = echarts.init(document.getElementById('main2'));
option2 = {
    title: {
        x: 'center',
        text: '销售额',
        subtext: 'Sales volume',
        textStyle:{
            // color:'red',
            fontSize:14,
            fontWeight:400
        }
    },
    tooltip: {
        trigger: 'item'
    },
    toolbox: {
        show: false,
        feature: {
            dataView: {show: true, readOnly: false},
            restore: {show: true},
            saveAsImage: {show: true}
        }
    },
    calculable: true,
    grid: {
        borderWidth: 0,
        y: 80,
        y2: 60
    },
    xAxis: [
        {
            type: 'category',
            show: false,
            data: ['昨日', '今日']
        }
    ],
    yAxis: [
        {
            type: 'value',
            show: false
        }
    ],
    series: [
        {
            name: '销售额',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: function(params) {
                        // build a color map as your need.
                        var colorList = [
                            '#B5C334','#C1232B'
                        ];
                        return colorList[params.dataIndex]
                    },
                    label: {
                        show: true,
                        position: 'top',
                        formatter: '{b}\n{c}'
                    }
                }
            },
            data: [50000,60000],
            markPoint: {
                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0)',
                    formatter: function(params){
                        return '<img src="'
                            + params.data.symbol.replace('image://', '')
                            + '"/>';
                    }
                },
                data: [
                    {xAxis:0, y: 350, name:'昨日', symbolSize:20, symbol: 'image://../asset/ico/折线图.png'},
                    {xAxis:1, y: 350, name:'今日', symbolSize:20, symbol: 'image://../asset/ico/柱状图.png'},

                ]
            }
        }
    ]
};
myChart.setOption(option2);
var myChart = echarts.init(document.getElementById('main3'));
option3 = {
    title: {
        x: 'center',
        text: '客单量',
        subtext: 'Guest single quantity',
        textStyle:{
            // color:'red',
            fontSize:14,
            fontWeight:400
        }
    },
    tooltip: {
        trigger: 'item'
    },
    toolbox: {
        show: false,
        feature: {
            dataView: {show: true, readOnly: false},
            restore: {show: true},
            saveAsImage: {show: true}
        }
    },
    calculable: true,
    grid: {
        borderWidth: 0,
        y: 80,
        y2: 60
    },
    xAxis: [
        {
            type: 'category',
            show: false,
            data: ['昨日', '今日']
        }
    ],
    yAxis: [
        {
            type: 'value',
            show: false
        }
    ],
    series: [
        {
            name: '客单量',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: function(params) {
                        // build a color map as your need.
                        var colorList = [
                            '#B5C334','#C1232B'
                        ];
                        return colorList[params.dataIndex]
                    },
                    label: {
                        show: true,
                        position: 'top',
                        formatter: '{b}\n{c}'
                    }
                }
            },
            data: [50,60],
            markPoint: {
                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0)',
                    formatter: function(params){
                        return '<img src="'
                            + params.data.symbol.replace('image://', '')
                            + '"/>';
                    }
                },
                data: [
                    {xAxis:0, y: 350, name:'昨日', symbolSize:20, symbol: 'image://../asset/ico/折线图.png'},
                    {xAxis:1, y: 350, name:'今日', symbolSize:20, symbol: 'image://../asset/ico/柱状图.png'},

                ]
            }
        }
    ]
};
myChart.setOption(option3);
var myChart = echarts.init(document.getElementById('main4'));
option4 = {
    title: {
        x: 'center',
        text: '转化率',
        subtext: 'Conversion rate',
        textStyle:{
            // color:'red',
            fontSize:14,
            fontWeight:400
        }
    },
    tooltip: {
        trigger: 'item'
    },
    toolbox: {
        show: false,
        feature: {
            dataView: {show: true, readOnly: false},
            restore: {show: true},
            saveAsImage: {show: true}
        }
    },
    calculable: true,
    grid: {
        borderWidth: 0,
        y: 80,
        y2: 60
    },
    xAxis: [
        {
            type: 'category',
            show: false,
            data: ['昨日', '今日']
        }
    ],
    yAxis: [
        {
            type: 'value',
            show: false
        }
    ],
    series: [
        {
            name: '转化率',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: function(params) {
                        // build a color map as your need.
                        var colorList = [
                            '#B5C334','#C1232B'
                        ];
                        return colorList[params.dataIndex]
                    },
                    label: {
                        show: true,
                        position: 'top',
                        formatter: '{b}\n{c}'
                    }
                }
            },
            data: [15,14],
            markPoint: {
                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0)',
                    formatter: function(params){
                        return '<img src="'
                            + params.data.symbol.replace('image://', '')
                            + '"/>';
                    }
                },
                data: [
                    {xAxis:0, y: 350, name:'昨日', symbolSize:20, symbol: 'image://../asset/ico/折线图.png'},
                    {xAxis:1, y: 350, name:'今日', symbolSize:20, symbol: 'image://../asset/ico/柱状图.png'},

                ]
            }
        }
    ]
};
myChart.setOption(option4);
//===========================================================5555折线图=========================================
var myChart = echarts.init(document.getElementById('main5'));
option5 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['今天','昨天'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['8:00','9:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"客流量",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'今天',
            type:'line',
            data:[9, 5, 10, 13, 12, 13, 10],
            // markPoint : {//显示最大最小点
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'昨天',
            type:'line',
            data:[11, 7, 15, 13, 12, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option5);

//====================================================================================================
var myChart = echarts.init(document.getElementById('main6'));
option6 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['本周','上周'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['周一','周二','周三','周四','周五','周六','周日']
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"客流量",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'本周',
            type:'line',
            data:[7, 11, 15, 13, 12, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'上周',
            type:'line',
            data:[10, 20, 6, 50, 10, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        }
    ]
};

myChart.setOption(option6);
var myChart = echarts.init(document.getElementById('main7'));
option7 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['本月','上月'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['1','2','3','4','5','6','7','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"客流量",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'本月',
            type:'line',
            data:[5, 11, 15, 8, 12, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'上月',
            type:'line',
            data:[10, 20, 7, 50, 6, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '月最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        }
    ]
};

myChart.setOption(option7);
//==========================777777777777777777777777777777777777777777777
//
var myChart = echarts.init(document.getElementById('main8'));
option8 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['今天','昨天'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['8:00','9:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"销售额",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'今天',
            type:'line',
            data:[3, 5, 10, 2, 12, 7, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'昨天',
            type:'line',
            data:[11, 11, 9, 13, 12, 5, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option8);
//
var myChart = echarts.init(document.getElementById('main9'));
option9 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['本周','上周'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['周一','周二','周三','周四','周五','周六','周日'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"销售额",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'本周',
            type:'line',
            data:[13, 5, 10, 8, 12, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'上周',
            type:'line',
            data:[11, 11, 6, 13, 12, 9, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option9);
//
var myChart = echarts.init(document.getElementById('main10'));
option10 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['本月','上月'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['1','2','3','4','5','6','7','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"销售额",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'本月',
            type:'line',
            data:[9, 5, 16, 13, 12, 13, 8],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'上月',
            type:'line',
            data:[11, 9, 15, 6, 12, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option10);
//=============================11,12,13

var myChart = echarts.init(document.getElementById('main11'));
option11 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['今天','昨天'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['8:00','9:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"客单量",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'今天',
            type:'line',
            data:[9, 5, 10, 13, 12, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'昨天',
            type:'line',
            data:[11, 11, 9, 13, 12, 2, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option11);
//
var myChart = echarts.init(document.getElementById('main12'));
option12 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['本周','上周'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['周一','周二','周三','周四','周五','周六','周日'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"客单量",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'本周',
            type:'line',
            data:[9, 5, 10, 13, 12, 7, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'上周',
            type:'line',
            data:[3, 11, 15, 13, 8, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option12);
//
var myChart = echarts.init(document.getElementById('main13'));
option13 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['本月','上月'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['1','2','3','4','5','6','7','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"客单量",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'本月',
            type:'line',
            data:[9, 5, 8, 13, 8, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'上月',
            type:'line',
            data:[11, 11, 6, 13, 12, 9, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option13);
//======================================================================14,15,16
var myChart = echarts.init(document.getElementById('main14'));
option14 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['今天','昨天'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['8:00','9:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"转化率",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'今天',
            type:'line',
            data:[9, 5, 10, 13, 12, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'昨天',
            type:'line',
            data:[6, 11, 15, 7, 12, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option14);
//
var myChart = echarts.init(document.getElementById('main15'));
option15 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['本周','上周'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['周一','周二','周三','周四','周五','周六','周日'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"转化率",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'本周',
            type:'line',
            data:[9, 5, 10, 13, 12, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'上周',
            type:'line',
            data:[11, 11, 15, 5, 12, 8, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option15);
//
var myChart = echarts.init(document.getElementById('main16'));
option16 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['本月','上月'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['1','2','3','4','5','6','7','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"转化率",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'15',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'本月',
            type:'line',
            data:[9, 5, 10, 7, 12, 3, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'上月',
            type:'line',
            data:[11, 11, 15, 6, 12, 8, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option16);