$('.weui_navbar>a').click(function(){
   var GLf=$(this).hasClass('weui_bar_item_on');
    var GL_num=$('.weui_bar_item_on').length*1;
    if(GLf){
        $(this).removeClass('weui_bar_item_on');
    }else if(GL_num<=1){
        console.log(GL_num+"====gl");
            $(this).addClass('weui_bar_item_on');
    }
    var GL_node=$('.weui_bar_item_on');
    var gl_type1=GL_node.eq(0).html();
    var gl_type2=GL_node.eq(1).html();
    console.log(gl_type1+gl_type2+"============高亮tab");
    if(gl_type1!=''&&gl_type2!=''){
        $('.type_name1').html(gl_type1);
        $('.type_name2').html(gl_type2);
    }
})
$('.sel_timek>div').click(function(){
    $(this).addClass('time_GL').siblings().removeClass('time_GL');
    var type_v=$(this).attr('type');
    console.log(type_v);
    // $('.tuk>div').hide();
    $('.tu_'+type_v).show().siblings().hide();
})
//=====================
//===========================================================5555折线图=========================================
var myChart = echarts.init(document.getElementById('main5'));
option5 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['今天','昨天'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:17
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['8:00','9:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'],
            //坐标显示完全
            axisLabel : {
                interval:0
            },
            grid:{
                width:300,
                height:200,
                left:0,
                right:0
            }
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"客流量",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'18',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'今天',
            type:'line',
            data:[9, 5, 10, 13, 12, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'昨天',
            type:'line',
            data:[11, 11, 15, 13, 12, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        }
    ]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option5);

//====================================================================================================
var myChart = echarts.init(document.getElementById('main6'));
option6 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['本周','上周'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:17
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['周一','周二','周三','周四','周五','周六','周日']
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"客流量",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'18',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'本周',
            type:'line',
            data:[11, 11, 15, 13, 12, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'上周',
            type:'line',
            data:[10, 20, 20, 50, 30, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        }
    ]
};

myChart.setOption(option6);
var myChart = echarts.init(document.getElementById('main7'));
option7 = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['本月','上月'],
        top:'-2%',
        itemWidth:20,
        itemHeight:17,
        textStyle:{
            // color:'red',
            fontSize:17
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['1','2','3','4','5','6','7','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"客流量",
            namelocation:'start',
            nameTextStyle:{
                fontSize:'18',
                fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'本月',
            type:'line',
            data:[11, 11, 15, 13, 12, 13, 10],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'上月',
            type:'line',
            data:[10, 20, 20, 50, 30, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '月最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        }
    ]
};

myChart.setOption(option7);