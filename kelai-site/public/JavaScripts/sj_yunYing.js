/**
 * Created by Administrator on 2016/11/10.
 */
var myChart = echarts.init(document.getElementById('main'));

// 指定图表的配置项和数据
//客流量
option = {
    // title: {
    //     text: '数据运营诊断表',
    //     // subtext: '备注',
    //     x:'center',
    //     top:'-1.2%',
    //     textStyle:{
    //         fontWeight:200,
    //         fontSize:16
    //     }
    // },
    // tooltip: {},
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     // data: ['预算', '实际'],
    //     data:['客流量','销售额','客单量','转化率'],
    //     top:'-1%',
    //     textStyle:{
    //         // color:'red',
    //         fontSize:13
    //     }
    // },
    radar: {
        // shape: 'circle',
        indicator: [
            { name: '店组0.65', max: 6500},
            { name: '店铺0.16', max: 16000},
            { name: '客流量0.3', max: 3000},
            { name: '销售额0.42', max: 4200},
            { name: '客单量0.52', max: 5200},
            { name: '转化率0.25', max: 2500},
            { name: '天气0.3', max: 3000},
            { name: '活动0.28', max: 3800},
            { name: '节假日0.52', max: 5200},
            { name: '商圈0.25', max: 2500}
        ]
    },
    series: [{
        // name: '预算 vs 开销（Budget vs spending）',
        type: 'radar',
        // areaStyle: {normal: {}},
        data : [
            {
                value : [500, 10000, 2800, 3500, 5000, 1900,500, 1000, 2800, 3500],
                name : '客流量'
            },
            {
                value :  [1500, 3300, 2000, 2500, 4200, 2100,500, 800, 2800,2500],
                name : '销售额'
            },
            {
                value : [5000, 1400, 2800, 3100, 4200, 2100,500, 1000, 2800, 1500],
                name : '客单量'
            },
            {
                value : [1100, 3300, 2000, 2100, 4200, 2100,500, 800, 2800,2500],
                name : '转化率'
            }
        ]
    }]
};
// 使用刚指定的配置项和数据显示图表。
myChart.setOption(option);
//折线图
var myChart = echarts.init(document.getElementById('main2'));
option2 = {
    // title: {
    //     text: '图形标题',
    //     subtext: '备注',
    //     x:'center'
    // },
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        // orient: 'vertical',
        // left: 'left',
        data:['客流量','销售额','客单量','转化率'],
        // top:'-1%',
        // itemWidth:20,
        // itemHeight:17,
        textStyle:{
            // color:'red',
            // fontSize:17
        }
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['周一','周二','周三','周四','周五','周六','周日']
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel : {
                formatter: '{value} '
            },
            name:"图形标题",//y轴标题
            namelocation:'start',
            nameTextStyle:{
                fontSize:'16'
                ,fontWeight:'bold'
            }
        }
    ],
    series : [
        {
            name:'客流量',
            type:'line',
            data:[11, 11, 15, 7, 12, 13, 6],
            // markPoint : {
            //     data : [
            //         {type : 'max', name: '最大值'},
            //         {type : 'min', name: '最小值'}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'销售额',
            type:'line',
            data:[10, 20, 15, 4, 30, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        },
        {
            name:'客单量',
            type:'line',
            data:[10, 9, 20, 50, 35, 20, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        },
        {
            name:'转化率',
            type:'line',
            data:[7, 20, 20, 5, 30, 15, 10],
            // markPoint : {
            //     data : [
            //         {name : '日最低', value : 2, xAxis: 1, yAxis: 1.5}
            //     ]
            // },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        }
    ]
};

myChart.setOption(option2);
//环形图
var myChart = echarts.init(document.getElementById('main3'));
option3 = {
    title : {
        text: '图形标题',
        subtext: '备注',
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data:['客流量','销售额','客单量','转化率'],
    },
    series : [
        {
            name: '访问来源',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:335, name:'客流量'},
                {value:310, name:'销售额'},
                {value:234, name:'客单量'},
                {value:135, name:'转化率'},
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};
myChart.setOption(option3);
//一周前三排名图
var myChart = echarts.init(document.getElementById('main4'));
option4 = {
    title: {
        x: 'center',
        text: '店铺一周盈利前三名',
        subtext: 'Shop profit top three a week'
    },
    tooltip: {
        trigger: 'item'
    },
    toolbox: {
        show: false,
        feature: {
            dataView: {show: true, readOnly: false},
            restore: {show: true},
            saveAsImage: {show: true}
        }
    },
    calculable: true,
    grid: {
        borderWidth: 0,
        y: 80,
        y2: 60
    },
    xAxis: [
        {
            type: 'category',
            show: false,
            data: ['昨日', '今日']
        }
    ],
    yAxis: [
        {
            type: 'value',
            show: false
        }
    ],
    series: [
        {
            name: '客流量',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: function(params) {
                        // build a color map as your need.
                        var colorList = [
                            '#B5C334','#C1232B'
                        ];
                        return colorList[params.dataIndex]
                    },
                    label: {
                        show: true,
                        position: 'top',
                        formatter: '{b}\n{c}'
                    }
                }
            },
            data: [10000,8000,7000],
            markPoint: {
                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0)',
                    formatter: function(params){
                        return '<img src="'
                            + params.data.symbol.replace('image://', '')
                            + '"/>';
                    }
                },
                data: [
                    {xAxis:0, y: 350, name:'店铺1', symbolSize:20, symbol: 'image://../asset/ico/折线图.png'},
                    {xAxis:1, y: 350, name:'店铺2', symbolSize:20, symbol: 'image://../asset/ico/柱状图.png'},
                    {xAxis:1, y: 350, name:'店铺3', symbolSize:20, symbol: 'image://../asset/ico/柱状图.png'},

                ]
            }
        }
    ]
};
myChart.setOption(option4);
var myChart = echarts.init(document.getElementById('main5'));
option5 = {
    title: {
        x: 'center',
        text: '店铺盈利NO1详情',
        subtext: 'Shop profit details first'
    },
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b}: {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        x: 'left',
        data:['客流量','销售额','客单量','转化率']
    },
    series: [
        {
            name:'访问来源',
            type:'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    show: true,
                    textStyle: {
                        fontSize: '30',
                        fontWeight: 'bold'
                    }
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data:[
                {value:335, name:'客流量'},
                {value:310, name:'客单量'},
                {value:234, name:'销售额'},
                {value:135, name:'转化率'}
            ]
        }
    ]
};
myChart.setOption(option5);
//第一个图start
/**
 * Created by Administrator on 2016/11/10.
 */
//====================================================================================================
var myChart = echarts.init(document.getElementById('main6'));
option6 = {
    // title: {
    //     text: '数据运营诊断表',
    //     subtext: '备注',
    //     x:'center'
    // },
    // tooltip: {},
    legend: {
        orient: 'vertical',
        left: 'left',
        // data: ['预算', '实际'],
        data:['客流量','销售额','客单量','转化率'],
        top:'-2%',
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    radar: {
        // shape: 'circle',
        indicator: [
            { name: '店组', max: 6500},
            { name: '店铺', max: 16000},
            { name: '客流量', max: 30000},
            { name: '销售额', max: 38000},
            { name: '客单量', max: 52000},
            { name: '转化率', max: 25000},
            { name: '天气', max: 30000},
            { name: '活动', max: 38000},
            { name: '节假日', max: 52000},
            { name: '商圈', max: 25000}
        ]
    },
    series: [{
        // name: '预算 vs 开销（Budget vs spending）',
        type: 'radar',
        // areaStyle: {normal: {}},
        data : [
            {
                value : [500, 10000, 28000, 35000, 50000, 19000,500, 10000, 28000, 35000],
                name : '客流量'
            },
            {
                value : [5000, 14000, 3000, 11000, 42000, 9000,500, 7000, 9000, 12000],
                name : '销售额'
            },
            {
                value : [5000, 14000, 28000, 31000, 42000, 21000,500, 10000, 28000, 15000],
                name : '客单量'
            },
            {
                value : [1100, 33000, 2000, 21000, 42000, 21000,500, 8000, 2800,25000],
                name : '转化率'
            }
        ]
    }]
};

myChart.setOption(option6);
//==========================777777777777777777777777777777777777777777777
//
var myChart = echarts.init(document.getElementById('main9'));
option9 = {
    // title: {
    //     text: '数据运营诊断表',
    //     subtext: '备注',
    //     x:'center'
    // },
    // tooltip: {},
    legend: {
        orient: 'vertical',
        left: 'left',
        // data: ['预算', '实际'],
        data:['客流量','销售额','客单量','转化率'],
        top:'-2%',
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    radar: {
        // shape: 'circle',
        indicator: [
            { name: '店组', max: 6500},
            { name: '店铺', max: 16000},
            { name: '客流量', max: 30000},
            { name: '销售额', max: 38000},
            { name: '客单量', max: 52000},
            { name: '转化率', max: 25000},
            { name: '天气', max: 30000},
            { name: '活动', max: 38000},
            { name: '节假日', max: 52000},
            { name: '商圈', max: 25000}
        ]
    },
    series: [{
        // name: '预算 vs 开销（Budget vs spending）',
        type: 'radar',
        // areaStyle: {normal: {}},
        data : [
            {
                value : [500, 7000, 28000, 35000, 50000, 1900,500, 10000, 28000, 3500],
                name : '客流量'
            },
            {
                value : [5000, 8000, 3000, 11000, 42000, 9000,500, 7000, 9000, 12000],
                name : '销售额'
            },
            {
                value : [5000, 11000, 28000, 31000, 42000, 21000,500, 10000, 28000, 15000],
                name : '客单量'
            },
            {
                value : [1100, 13000, 2000, 21000, 32000, 11000,500, 8000, 1800,2500],
                name : '转化率'
            }
        ]
    }]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option9);
//
//=============================11,12,13
var myChart = echarts.init(document.getElementById('main12'));
option12 = {
    // title: {
    //     text: '数据运营诊断表',
    //     subtext: '备注',
    //     x:'center'
    // },
    // tooltip: {},
    legend: {
        orient: 'vertical',
        left: 'left',
        // data: ['预算', '实际'],
        data:['客流量','销售额','客单量','转化率'],
        top:'-2%',
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    radar: {
        // shape: 'circle',
        indicator: [
            { name: '店组', max: 6500},
            { name: '店铺', max: 16000},
            { name: '客流量', max: 30000},
            { name: '销售额', max: 38000},
            { name: '客单量', max: 52000},
            { name: '转化率', max: 25000},
            { name: '天气', max: 30000},
            { name: '活动', max: 38000},
            { name: '节假日', max: 52000},
            { name: '商圈', max: 25000}
        ]
    },
    series: [{
        // name: '预算 vs 开销（Budget vs spending）',
        type: 'radar',
        // areaStyle: {normal: {}},
        data : [
            {
                value : [500, 8000, 28000, 3500, 50000, 19000,500, 10000, 28000, 35000],
                name : '客流量'
            },
            {
                value : [5000, 14000, 700, 11000, 42000, 4000,500, 7000, 9000, 12000],
                name : '销售额'
            },
            {
                value : [5000, 14000, 28000, 31000, 42000, 621000,500, 10000, 28000, 15000],
                name : '客单量'
            },
            {
                value : [1100, 33000, 2000, 21000, 42000, 9000,500, 8000, 2800,25000],
                name : '转化率'
            }
        ]
    }]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option12);
////======================================================================14,15,16
var myChart = echarts.init(document.getElementById('main15'));
option15 = {
    // title: {
    //     text: '数据运营诊断表',
    //     subtext: '备注',
    //     x:'center'
    // },
    // tooltip: {},
    legend: {
        orient: 'vertical',
        left: 'left',
        // data: ['预算', '实际'],
        data:['客流量','销售额','客单量','转化率'],
        top:'-2%',
        textStyle:{
            // color:'red',
            fontSize:15
        }
    },
    radar: {
        // shape: 'circle',
        indicator: [
            { name: '店组', max: 6500},
            { name: '店铺', max: 16000},
            { name: '客流量', max: 30000},
            { name: '销售额', max: 38000},
            { name: '客单量', max: 52000},
            { name: '转化率', max: 25000},
            { name: '天气', max: 30000},
            { name: '活动', max: 38000},
            { name: '节假日', max: 52000},
            { name: '商圈', max: 25000}
        ]
    },
    series: [{
        // name: '预算 vs 开销（Budget vs spending）',
        type: 'radar',
        // areaStyle: {normal: {}},
        data : [
            {
                value : [500, 10000, 28000, 35000, 50000, 19000,500, 10000, 28000, 35000],
                name : '客流量'
            },
            {
                value : [5000, 14000, 3000, 11000, 42000, 9000,500, 7000, 9000, 12000],
                name : '销售额'
            },
            {
                value : [5000, 14000, 28000, 31000, 42000, 21000,500, 10000, 28000, 15000],
                name : '客单量'
            },
            {
                value : [1100, 33000, 2000, 21000, 42000, 21000,500, 8000, 2800,25000],
                name : '转化率'
            }
        ]
    }]
};
// window.onresize = myChart.resize;
// console.log(window.onresize+"==============");
myChart.setOption(option15);
//第一个图over