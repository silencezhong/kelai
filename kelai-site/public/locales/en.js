Dolphin.i18n.addMessages({
	"homepage.homepage": "Homepage",
	"news.news": "News",
	"me.me": "Me",
	"overview.informationOverview": "information overview",
	"overview.PFNOTWeek": "ThisWeek's passenger flow",
	"overview.numberOfStores": "Stores",
	"overview.numberOfEquipments": "Equipments",
	"overview.numberOfAbnormalEquipments": "Abnormal equipments",
	"overview.thisWeek": "ThisWeek",
	"overview.lastWeek": "LastWeek",
	"overview.thisMonth": "ThisMonth",
	"overview.lastMonth": "LastMonth",
	"overview.storeSelection": "Selection",
	"overview.unbindedAccount": "Unbinded",
	"overview.RCRBAccount": "Requested contents requiring binding account",
	"overview.tobind": "To bind",
	"overview.bindAccount": "Bind account",
	"overview.cellphoneNumber": "Mobile",
	"overview.name": "Name",
	"overview.clientCode": "Client code",
	"overview.storeCode": "Store code",
	"overview.reistrationBinding": "Reistration binding",
	"overview.inputCellphone": "Please input your cellphone number",
	"overview.inputName": "Please input your name",
	"overview.inputClientCode": "Please input your client code",
	"overview.inputStoreCode": "Please input your store code",
	"views.storeName": "Name",
	"views.storeCode": "Code",
	"views.storeType": "Type",
	"views.inputName": "Please input name",
	"views.inputCodes": "Please input code",
	"views.addStore": "New",
	"views.address": "Address",
	"views.pleaseSelect": "Please select",
	"views.ok": "OK",
	"views.storeAddress": "Store address",
	"views.timezone": "Timezone",
	"views.openingStore": "Open date",
	"views.closingStore": "Clos dage",
	"views.telephone": "Telephone",
	"views.contactPerson": "Contact",
	"views.cellphoneNumber": "Mobile",
	"views.save": "Save",
	"views.pleaseInput": "Please input",
	"views.return": "Cancel",
	"views.addEquipment": "New",
	"views.subordinateStore": "Store",
	"views.equipmentName": "Name",
	"views.equipmentCode": "code",
	"views.equipmentType": "type",
	"views.equipmentPosition": "position",
	"views.recordCycle": "Record",
	"views.acquisitionCycle": "Acquisition",
	"views.dataDirection": "Direction",
	"views.positiveDirection": "Positive direction",
	"views.negtiveDirection": "Negtive direction",
	"views.myStore": "Store",
	"views.search": "Search",
	"views.loading": "Loadings",
	"views.code": "Code",
	"views.city": "City",
	"views.province": "Province",
	"views.businessTime": "Business time",
	"views.focusing": "Focusing",
	"views.looseFocus": "Loose focus",
	"views.normal": "Normal",
	"views.abnormal": "Abnormal",
	"views.powerVolumn": "Power volumn",
	"views.totalIntradayEntry": "Entry",
	"views.toalIntradayExit": "Exit",
	"views.myEquipment": "Equipment",
	"views.delete": "Delete",
	"views.store": "Store",
	"views.status": "Status",
	"views.readFullArticle": "All",
	"views.newsDetail": "News detail",
	"views.help": "Help",
	"views.toBind": "To bind",
	"views.timeRepairCall": "Repair time",
	"views.detailedAddress": "Detail address",
	"views.street": "street",
	"views.detailedDescription": "Detailed description",
	"views.currentStatus": "Current status",
	"views.noReportedData": "There is no reported data",
	"views.unknownState": "Unknown state",
	"views.equipmentCall": "Repair equipment",
	"views.passengerFlow": "Passenger flow",
	"views.weekPassengerFlow": "This week's passenger flow",
	"views.passengerLastWeek": "Passenger flow last week",
	"views.monthPassengerFlow": "This month's passenger flow",
	"views.lastPassengerFlow": "Last month's passenger flow",
	"views.myKlkl": "My klkl",
	"views.noData": "No data",
	"views.studyHistory": "History",
	"views.equipment repair": "Repair",
	"views.unbind": "Unbind",
	"views.systemDescription": "Description",
	"views.confirmRelieve": "Confirm relieve",
	"views.confirmAccount": "Do you confirm the WeChat binding to dissolve this account",
	"views.bindOnAccount": "Bind on account",
	"views.superior": "Superior",
	"views.phoneCannotEmpty": "Cell phone number can not be empty",
	"views.nameCannotEmpty": "The name cannot be empty",
	"views.codeCannotEmpty": "Customer code cannot be empty",
	"views.verification": "Verification",
	"views.callError": "Call error",
	"views.recentOnlineTime": "Recent online time",
	"views.unboundAccount": "Unbound account",
	"views.requestAccountBinding": "Request content requires account binding",
	"views.myHistoryRevision": "My history of revision",
	"views.unrecorded": "Unrecorded",
	"views.unspecified": "Unspecified",
	"views.noDescription": "No description",
	"views.equipmentRepair": "Repair",
	"views.personContact": "The name of the person you can contact",
	"views.confirmationRepairEquipment": "Confirmation of repair equipment",
	"views.reportThisEquipment": "Do you have to make sure that you need to report this equipment",
	"views.repairEquipment": "Equipment",
	"views.lookMyRevisionHistory": "Show my history",
	"views.describeProblem": "Please describe the problem of repairing equipment",
	"views.count":"Count"
});