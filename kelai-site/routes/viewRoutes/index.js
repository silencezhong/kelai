/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */
"use strict";
let api = require('../../utils/api');

const permission = {
    public: 0,
    protect: 1,
    private: 0
};

module.exports = {

    index:function (userData, req, res, callback, next) {
        callback({});
    },

    account: function (userData, req, res, callback, next) {
        callback({}, permission.protect);
    },

    news: function (userData, req, res, callback, next) {
        global.tool.send({
            method: 'post',
            uri: api.get('c97b00aa70fc4ed1b5922bced4468a0b'),
            json:{
                properties:{
                    model_code_obj_ae: req.query.modelCode || 'news'
                }
            }
        }, function (error, response, body) {
            body = body || {rows: []};
            callback(body.rows);
        });
    },

    newsDetail: function (userData, req, res, callback, next) {
        global.tool.send({
            method: 'get',
            uri: api.get('369cf93b979a4569887d3839b806b0fd').replace('{id}', req.query.id)
        }, function (error, response, body) {
            let data = body && body.value;
            callback(data || {});
        });
    },

    repairList: function (userData, req, res, callback, next) {
        global.tool.send({
            method: 'post',
            uri: api.get('fcca1aa0e7554b2bb871d3a1b8a7df36'),
            json: {
                properties: {
                    issueUser: req.session.userData.id
                }
            }
        }, function (error, response, body) {
            let data = (body && body.rows) || [];
            callback(data);
        });
    },

    repairCall:function (userData, req, res, callback, next) {
        let data = {};
        global.tool.send({
            method: 'post',
            uri: api.get('7c15daa3ea44c555c4bf2241c712f86d'),
            json:{
                properties:{
                    userId:userData.id
                }
            }
        }, function (error, response, body) {
            data.deviceList = body.rows || [];
            callback(data);
        });
    },

    myCustomer: function (userData, req, res, callback, next) {
        global.tool.send({
            method: 'get',
            uri: api.get('756af3ae31a64565a54ec0221f84d377') + 'shop/' + userData.userId
        }, function (error, response, body) {
            body = body || {rows: []};
            callback(body.rows);
        });
    },

    addDZ:function (userData, req, res, callback, next) {
        let data = {info:{}}, count = 0, sendLength = 2,
            queryData = req.query;
        if(queryData.id)
            sendLength = 3;
        function _callback(){
            count++;
            if(count === sendLength){
                callback(data);
            }
        }
        global.tool.send({
            method: 'get',
            uri: api.get('5b8e27d5bb9547f48235bd70aea199b9').replace('{code}', 'shopType')
        }, function (error, response, body) {
            data.shopType = body.rows || [];
            _callback();
        });
        global.tool.send({
            method: 'get',
            uri: api.get('c666378c5d3b417d8e150c456a489d24').replace('{accountId}', userData.id)
        }, function (error, response, body) {
            data.userParent = body.rows || [];
            _callback();
        });
        if(queryData.id){
            global.tool.send({
                method: 'get',
                uri: api.get('742445c653654caaa4a1fa69eb651a9a') + queryData.id
            }, function (error, response, body) {
                data.info = body.value || {};
                _callback();
            });
        }
    },

    addEquip:function (userData, req, res, callback, next) {
        let data = {};
        global.tool.send({
            method: 'get',
            uri: api.get('3c43c627b41e4fafbfdbfb526345b248').replace('{accountId}', userData.id)
        }, function (error, response, body) {
            data.shopList = body.rows || [];
            data.info = {};
            if(req.query.id){
                global.tool.send({
                    method: 'get',
                    uri: api.get('4e149d0fb488470fb3e493ade50564b6') + req.query.id
                }, function (error, response, body) {
                    data.info = body.value || {};
                    callback(data);
                });
            } else {
                callback(data);
            }
        });
    }

};