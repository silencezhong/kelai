"use strict";
const router = require('express').Router();
const request = require('request');
const log4js = require('log4js');
const api = require('../utils/api');
const logger = log4js.getLogger('device');

// router.get('/', function (req, res, next) {
//    res.end("Come here.")
// });

router.post('/', function (req, res, next) {
    logger.info("---receive data from agent- POST--");
    let body = req.body;
    logger.info(body);
    var start = new Date().getTime();
    global.tool.send({
        method: req.method,
        uri: api.get('579e72dde170433ea0b4b333e5113842'),
        json: body
    }, function (error, response, body) {
        logger.info(body);
        body = body || "Error";
        res.end(body+"");
    });
    var end = new Date().getTime();
    logger.info("--耗时 : "+(end-start)+"毫秒--");
    logger.info("--end--");
});

router.get('/', function (req, res, next) {
    logger.info("---receive data from agent- GET--");
    let body = req.query;
    logger.info(body);
    var start = new Date().getTime();
    global.tool.send({
        method: 'POST',
        uri: api.get('579e72dde170433ea0b4b333e5113842'),
        json: body
    }, function (error, response, body) {
        logger.info(body);
        body = body || "Error";
        res.end(body+"");
    });
    var end = new Date().getTime();
    logger.info("--耗时 : "+(end-start)+"毫秒--");
    logger.info("--end--");
});

module.exports = router;

// ---receive data from agent---
// [2016-11-29 10:14:14.005] [INFO] [default] - { cmd: 'getsetting',
//     flag: '0700',
//     data: 'B1098EF703 00 02 05 00 00 00 00 00 00 00 78D3000078D38DD64D3AC80023CD38A564C40022B0 F73F16B00F0101120702010000173A26C2' }
//     [2016-11-29 10:14:14.006] [INFO] [default] -
// --end--
