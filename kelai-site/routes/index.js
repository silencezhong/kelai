"use strict";
const router = require('express').Router();

router.get('/refresh/session', function(req, res, next) {
    delete req.session.userData;
    delete req.session.openId;
    res.send({success:true});
});

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

module.exports = router;
