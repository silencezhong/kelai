"use strict";

const router = require('express').Router();

/* mp.weixin.qq.com verify. */
router.use('/', function(req, res, next) {
    let data = req.query;

    let signature = data.signature,
        timestamp = data.timestamp,
        nonce = data.nonce,
        echostr = data.echostr || 'success',
        token = "estam2016";
    // let arr = [token, timestamp, nonce];
    // arr.sort();
    //TODO:按照微信官方的，应该验证arr的hash值与signature相等
    res.send(echostr);
});

module.exports = router;
