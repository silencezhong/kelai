/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package com.kelai.response;

import com.kelai.common.InfoPage;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * 返回给页面调用的json
 *
 * @author Zhong, An-Jing
 */
public final class JsonResponse<T extends Serializable> implements Response<T> {

    private long total = 1;

    private boolean success = true;

    private String msg = "Success";

    /**
     * 返回列表时，使用此
     */
    private Collection<T> rows;

    /**
     * 返回单个字段时候，使用此
     */
    private T value;

    /**
     * 开始时间
     */
    private long start;

    /**
     * 结束时间
     */
    private long end;

    private long slipTime;

    private Map<String,Object> extProps;

    private JsonResponse(long total, boolean success, Collection<T> rows) {
        this(total, success, rows, -1);
    }

    private JsonResponse(long total, boolean success, Collection<T> rows, long slipTime) {
        this.total = total;
        this.success = success;
        this.rows = rows;
        this.slipTime = slipTime;
    }

    private JsonResponse(boolean success, T value) {
        this(success, value, -1);
    }

    private JsonResponse(boolean success, T value, long slipTime) {
        this.success = success;
        this.value = value;
        this.slipTime = slipTime;
    }

    public static <T extends Serializable> JsonResponse<T> build(InfoPage<T> infoPage, long slipTime) {
        return new JsonResponse<T>(infoPage.getTotal(), true, infoPage.getContent(), slipTime);
    }

    public static <T extends Serializable> JsonResponse<T> build(InfoPage infoPage) {
        return new JsonResponse<T>(infoPage.getTotal(), true, infoPage.getContent(), -1);
    }

    public static <T extends Serializable> JsonResponse<T> build(Collection<T> values, long start) {
        return new JsonResponse<T>(values.size(), true, values, start);
    }

    public static <T extends Serializable> JsonResponse<T> build(Collection<T> values) {
        return new JsonResponse<T>(values.size(), true, values);
    }

    public static <T extends Serializable> JsonResponse<T> build(long total, Collection<T> values) {
        return new JsonResponse<T>(total, true, values);
    }

    public static <T extends Serializable> JsonResponse<T> build(long total, Collection<T> values, long slipTime) {
        return new JsonResponse<>(total, true, values, slipTime);
    }

    public static <T extends Serializable> JsonResponse<T> buildSingle(T obj) {
        return new JsonResponse<>(true, obj);
    }

    public static <T extends Serializable> JsonResponse<T> buildSingle(T obj, long slipTime) {
        return new JsonResponse<>(true, obj, slipTime);
    }

    public static <T extends Serializable> JsonResponse OK() {
        return new JsonResponse<T>(true, null);
    }

    public static <T extends Serializable> JsonResponse ERROR(String error) {
        return new JsonResponse<T>(false, null).buildMsg(error);
    }

    public JsonResponse buildMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public void clear() {
        this.success = true;
        msg = "操作成功.";
        this.total = 0;
        this.rows.clear();
        this.value = null;
    }

    public long getTotal() {
        return total;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMsg() {
        return msg;
    }

    public Collection<T> getRows() {
        return rows;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public long getStart() {
        return start;
    }

    public long getEnd() {
        return end;
    }

    public long getSlipTime() {
        return this.slipTime;
    }

    public Map<String, Object> getExtProps() {
        return extProps;
    }

    public void setExtProps(Map<String, Object> extProps) {
        this.extProps = extProps;
    }
}
