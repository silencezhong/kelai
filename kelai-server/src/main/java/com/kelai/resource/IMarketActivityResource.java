package com.kelai.resource;

import com.kelai.domain.MarketActivityEntity;

/**
 * 市场活动资源服务
 * Created by Silence on 2016/11/26.
 */
public interface IMarketActivityResource extends IResourceLayer<MarketActivityEntity> {
}
