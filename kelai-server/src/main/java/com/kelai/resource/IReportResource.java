package com.kelai.resource;

import com.kelai.domain.ReportEntity;

/**
 * 报表服务
 * Created by Luffy on 2017/07/30.
 */
public interface IReportResource extends IResourceLayer<ReportEntity> {

}
