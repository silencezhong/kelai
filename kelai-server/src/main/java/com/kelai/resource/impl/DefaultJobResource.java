package com.kelai.resource.impl;

import com.kelai.resource.IJobResource;
import com.kelai.resource.JsonCommonResource;
import com.kelai.response.JsonResponse;
import com.kelai.service.IJobService;
import com.kelai.task.quartz.QuartzConfigEntity;
import com.kelai.task.quartz.QuartzSchedulerService;
import com.kelai.utils.ContextUtil;
import javafx.util.Callback;
import org.quartz.SchedulerException;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
 * 默认的定时任务服务类
 * Created by Silence on 2016/04/14.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/job")
public class DefaultJobResource extends JsonCommonResource implements IJobResource {

    @Resource
    private IJobService jobService;

    private QuartzSchedulerService getQuzrtzSchedulerService() {
        return ContextUtil.getBean("quartzSchedulerService",QuartzSchedulerService.class);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public JsonResponse<QuartzConfigEntity> page(@RequestBody QuartzConfigEntity accountEntity) {
        return _pageAll(jobService, accountEntity);
    }


    @RequestMapping(value = "/exec" , method = RequestMethod.POST)
    @Override
    public JsonResponse exec(@RequestBody QuartzConfigEntity quartzConfigBean) throws SchedulerException {
        getQuzrtzSchedulerService().execJob(quartzConfigBean);
        return JsonResponse.OK();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<QuartzConfigEntity> findById(@PathVariable String id) {
        return _findOne(jobService, id, 0);
    }


    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public JsonResponse<QuartzConfigEntity> findByCode(@PathVariable String code) {
        return _findOne(jobService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<QuartzConfigEntity> save(@RequestBody QuartzConfigEntity account) throws Exception {
        getQuzrtzSchedulerService().removeJob(account);
        getQuzrtzSchedulerService().addJob(account);
        account.setCode(System.currentTimeMillis()+"");
        return _saveInfo(jobService, account, (Callback<QuartzConfigEntity, Object>) param -> {
//            if (StringUtils.isEmpty(param.getPassword()))
//                param.setPassword(BreezeeUtils.enCrypt(param.getCode() + "123"));
            return null;
        });
    }

    /**
     * 删除定时任务
     * @param id
     * @return
     * @throws SchedulerException
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<QuartzConfigEntity> delete(@PathVariable String id) throws SchedulerException {
        QuartzConfigEntity quartzConfigBean = jobService.findById(id);
        getQuzrtzSchedulerService().removeJob(quartzConfigBean);
        return _delete(jobService, id);
    }


    @Override
    public void initService() {
    }
}
