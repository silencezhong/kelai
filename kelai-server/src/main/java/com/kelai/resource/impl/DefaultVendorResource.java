package com.kelai.resource.impl;

import com.google.common.collect.Lists;
import com.kelai.domain.*;
import com.kelai.resource.IVendorResource;
import com.kelai.resource.JsonCommonResource;
import com.kelai.response.JsonResponse;
import com.kelai.response.Response;
import com.kelai.service.ICommonService;
import com.kelai.service.ICustomerService;
import com.kelai.service.impl.DefaultCommonService;
import com.kelai.utils.SystemTool;
import javafx.util.Callback;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 渠道商资源服务实现
 * Created by Silence on 2016/11/25.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/vendor")
public class DefaultVendorResource extends JsonCommonResource implements IVendorResource {

    private ICommonService vendorService;
    private ICommonService accountService;
    private ICommonService vendorCustomerService;



    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public Response<VendorEntity> page(@RequestBody VendorEntity vendorEntity) {
        return _pageAll(vendorService, vendorEntity);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @Override
    public Response<VendorEntity> list(@RequestBody VendorEntity vendorEntity) {
        return _pageAll(vendorService, vendorEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public Response<VendorEntity> findById(@PathVariable String id) {
        return _findOne(vendorService, id, 0);
    }

    @RequestMapping(value = "/findVendor/{id}", method = RequestMethod.GET)
    public Response<VendorEntity> findVendor(@PathVariable String id) {

        VendorEntity vendorEntity = new VendorEntity();
        vendorEntity.getProperties().put("id",id);
        Response<VendorEntity> result = _pageAll(vendorService, vendorEntity);

        return result;
    }

    @RequestMapping(value = "/findVendorsByAcntId/{accountId}", method = RequestMethod.GET)
    public Response<SelectEntity> findVendorsByAcntId(@PathVariable String accountId) {

        AccountEntity accountEntity = (AccountEntity)accountService.findById(accountId);
        VendorEntity vendorEntity = accountEntity.getVendor();

        List<SelectEntity> vendorList = Lists.newArrayList();
        if(vendorEntity == null){
            List<AbstractEntity> vendorEntities = vendorService.listAll(new VendorEntity());
            vendorList = vendorEntities.stream().map( vendor -> {
                SelectEntity se = new SelectEntity();
                se.setId(vendor.getId());
                se.setCode(vendor.getCode());
                se.setName(vendor.getName());
                se.setText(vendor.getName());
                return se;

            }).collect(Collectors.toList());
        }else{
            SelectEntity se = new SelectEntity();
            se.setId(vendorEntity.getId());
            se.setCode(vendorEntity.getCode());
            se.setName(vendorEntity.getName());
            se.setText(vendorEntity.getName());

            vendorList.add(se);
        }

        JsonResponse<SelectEntity> result = JsonResponse.build(vendorList, System.currentTimeMillis());

        return result;

    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public Response<VendorEntity> findByCode(@PathVariable String code) {
        return _findOne(vendorService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public Response<VendorEntity> save(@RequestBody VendorEntity vendorEntity) {
        return _saveInfo(vendorService, vendorEntity);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public Response<VendorEntity> update(@RequestBody VendorEntity vendorEntity) {

        VendorEntity dbVendorEntity = (VendorEntity)vendorService.findById(vendorEntity.getId());
        vendorEntity.setAccounts(dbVendorEntity.getAccounts());
        vendorEntity.setCustomers(dbVendorEntity.getCustomers());

        return _saveInfo(vendorService, vendorEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public Response<VendorEntity> delete(@PathVariable String id) {
        return _delete(vendorService, id);
    }

    @Override
    public void initService() {
        if (vendorService == null)
            vendorService = new DefaultCommonService("vendorRepository");
        if (vendorCustomerService == null)
            vendorCustomerService = new DefaultCommonService("vendorCustomerRepository");
        if (accountService == null)
            accountService = new DefaultCommonService("accountRepository");
    }

    @RequestMapping(value = "/customer/", method = RequestMethod.PUT)
    @Override
    public Response<VendorCustomerEntity> saveCustomer(@RequestBody VendorCustomerEntity vc) {
        return _saveInfo(vendorCustomerService, vc, new Callback<VendorCustomerEntity, Object>() {
            @Override
            public Object call(VendorCustomerEntity param) {
                param.setName("0");
                if (param.getCode() == null)
                    param.setCode(param.getVendor().getId() + param.getCustomer().getId());
                return null;
            }
        });
    }

    @RequestMapping(value = "/customer/{id}", method = RequestMethod.DELETE)
    @Override
    public Response<VendorCustomerEntity> removeCustomer(@PathVariable String id) {
        return _delete(vendorCustomerService, id);
    }

    @RequestMapping(value = "/customer/", method = RequestMethod.POST)
    @Override
    public Response<VendorCustomerEntity> findCustomer(@RequestBody VendorCustomerEntity vc) {
        return _pageAll(vendorCustomerService, vc);
    }

    @RequestMapping(value = "/tree/{parentId}", method = RequestMethod.GET)
    @Override
    public JsonResponse<CustomerEntity> treeVendor(@PathVariable String parentId) {
        VendorEntity vendorEntity = (VendorEntity)vendorService.findById(parentId);
        Set<VendorCustomerEntity> vcSet = vendorEntity.getCustomers();
        List<CustomerEntity> list = Lists.newArrayList();
        vcSet.forEach(vc -> {
            list.add(vc.getCustomer());
        });

        return JsonResponse.build(list);
    }
}
