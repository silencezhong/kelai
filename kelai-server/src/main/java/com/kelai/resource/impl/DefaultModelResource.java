package com.kelai.resource.impl;

import com.google.common.collect.Lists;
import javafx.util.Callback;
import org.springframework.web.bind.annotation.*;
import com.kelai.callback.IdCallback;
import com.kelai.domain.EmptyEntity;
import com.kelai.domain.ModelEntity;
import com.kelai.domain.ModelItemEntity;
import com.kelai.resource.IModelResource;
import com.kelai.resource.JsonCommonResource;
import com.kelai.response.JsonResponse;
import com.kelai.service.ICommonService;
import com.kelai.service.impl.DefaultCommonService;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 模型对外接口
 * Created by Silence on 2016/11/1.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/model")
public class DefaultModelResource extends JsonCommonResource implements IModelResource {

    private ICommonService modelService;

    private ICommonService modelItemService;

    @Resource
    private IdCallback idCallback;

    @Override
    public JsonResponse<ModelEntity> page(@RequestBody ModelEntity modelEntity) {
        return JsonResponse.ERROR("Not Support Page");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<ModelEntity> findById(@PathVariable String id) {
        return _findOne(modelService, id, 0);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public JsonResponse<ModelEntity> findByCode(@PathVariable String code) {
        return _findOne(modelService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<ModelEntity> save(@RequestBody ModelEntity model) {
        return _saveInfo(modelService, model);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<ModelEntity> delete(@PathVariable String id) {
        return _delete(modelService, id);
    }

    @RequestMapping("/tree")
    @Override
    public JsonResponse<ModelEntity> modelTree(@RequestParam(name = "parentId") String parentId) {
        return _pageAll(modelService, new EmptyEntity());
    }

    @RequestMapping(value = "/item", method = RequestMethod.PUT)
    @Override
    public JsonResponse<ModelItemEntity> saveItem(@RequestBody ModelItemEntity item) {
        return _saveInfo(modelItemService,item,(Callback<ModelItemEntity, Object>) param -> {
            idCallback.call(param);
            if (param.getCode() == null)
                param.setCode(param.getId());
            return null;
        });
    }

    @RequestMapping(value = "/item/", method = RequestMethod.POST)
    @Override
    public JsonResponse<ModelItemEntity> findItems(@RequestBody ModelItemEntity item) {
        JsonResponse<ModelItemEntity> jsonResponse = _pageAll(modelItemService, item);
        if (item.getProperties() != null && item.getProperties().size() > 0) {
            return jsonResponse;
        } else {
            List<ModelItemEntity> modelItemEntityArrayList = Lists.newArrayList();
            jsonResponse.getRows().forEach(a -> {
                ModelItemEntity ce = (ModelItemEntity) a;
                ModelItemEntity modelItemEntity = new ModelItemEntity();
                Date nowDate = new Date();
                if (ce.getExpireTime() == null || (ce.getExpireTime() != null && ce.getExpireTime().after(nowDate))) {
                    modelItemEntity.setActiveTime(ce.getActiveTime());
                    modelItemEntity.setCover(ce.getCover());
                    modelItemEntity.setLang(ce.getLang());
                    modelItemEntity.setModel(ce.getModel());
                    modelItemEntity.setSubTitle(ce.getSubTitle());
                    modelItemEntity.setUedit(ce.getUedit());
                    modelItemEntity.setExpireTime(ce.getExpireTime());
                    modelItemEntity.setId(ce.getId());
                    modelItemEntity.setCode(ce.getCode());
                    modelItemEntity.setName(ce.getName());
                    modelItemEntity.setStatus(ce.getStatus());
                    modelItemEntity.setRemark(ce.getRemark());
                    modelItemEntity.setCreatedDate(ce.getCreatedDate());
                    modelItemEntity.setCreateBy(ce.getCreateBy());
                    modelItemEntity.setModifiedDate(ce.getModifiedDate());
                    modelItemEntity.setModifiedBy(ce.getModifiedBy());
                    modelItemEntityArrayList.add(modelItemEntity);
                }
            });
            JsonResponse<ModelItemEntity> result = JsonResponse.build(modelItemEntityArrayList);
            return result;
        }
    }

    @RequestMapping(value = "/item/{itemId}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<ModelItemEntity> deleteItem(@PathVariable String itemId) {
        return _delete(modelItemService,itemId);
    }

    @RequestMapping(value = "/item/{itemId}", method = RequestMethod.GET)
    @Override
    public JsonResponse<ModelItemEntity> findItem(@PathVariable String itemId) {
        return _findOne(modelItemService,itemId,0);
    }

    @Override
    public void initService() {
        if(modelService==null)
            modelService = new DefaultCommonService("modelRepository");
        if(modelItemService ==null)
            modelItemService = new DefaultCommonService("modelItemRepository");
    }
}
