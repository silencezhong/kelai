package com.kelai.resource.impl;

import com.kelai.domain.SaleDataEntity;
import com.kelai.resource.ISaleDataResource;
import com.kelai.resource.JsonCommonResource;
import com.kelai.response.Response;
import com.kelai.service.ICommonService;
import com.kelai.service.impl.DefaultCommonService;
import javafx.util.Callback;
import org.springframework.web.bind.annotation.*;

/**
 * 销售数据资源服务
 * Created by Silence on 2016/11/26.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/sale")
public class DefaultSaleDataResource extends JsonCommonResource implements ISaleDataResource {

    private ICommonService saleDataService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public Response<SaleDataEntity> page(@RequestBody SaleDataEntity saleDataEntity) {
        return _pageAll(saleDataService, saleDataEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public Response<SaleDataEntity> findById(@PathVariable String id) {
        return _findOne(saleDataService, id, 0);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public Response<SaleDataEntity> findByCode(String code) {
        return _findOne(saleDataService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public Response<SaleDataEntity> save(@RequestBody SaleDataEntity saleDataEntity) {
        return _saveInfo(saleDataService, saleDataEntity, new Callback<SaleDataEntity,Object>() {
            @Override
            public Object call(SaleDataEntity param) {
                param.setCode(param.getCustomer().getId()+param.getHappenDate().getTime());
                param.setName(param.getHappenDate().toString());
                return null;
            }
        });
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public Response<SaleDataEntity> delete(@PathVariable String id) {
        return _delete(saleDataService, id);
    }

    @Override
    public void initService() {
        if (saleDataService == null)
            saleDataService = new DefaultCommonService("saleDataRepository");
    }
}
