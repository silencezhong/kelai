package com.kelai.resource.impl;

import com.kelai.domain.UserEntity;
import com.kelai.resource.IUserResource;
import com.kelai.resource.JsonCommonResource;
import com.kelai.response.JsonResponse;
import com.kelai.service.ICommonService;
import com.kelai.service.impl.DefaultCommonService;
import org.springframework.web.bind.annotation.*;

/**
 * 会员服务
 * Created by Silence on 2016/11/1.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/user")
public class DefaultUserResource extends JsonCommonResource implements IUserResource {

    private ICommonService userService;

    private ICommonService userReservedService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public JsonResponse<UserEntity> page(@RequestBody UserEntity userEntity) {
        return _pageAll(userService, userEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<UserEntity> findById(@PathVariable String id) {
        return _findOne(userService, id, 0);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public JsonResponse<UserEntity> findByCode(@PathVariable String code) {
        return _findOne(userService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<UserEntity> save(@RequestBody UserEntity userEntity) {
        return _saveInfo(userService, userEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<UserEntity> delete(@PathVariable String id) {
        return _delete(userService, id);
    }

    @Override
    public void initService() {
        if (userService == null)
            userService = new DefaultCommonService("userRepository");
    }
}
