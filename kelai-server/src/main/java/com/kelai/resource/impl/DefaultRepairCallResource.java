package com.kelai.resource.impl;

import com.kelai.common.constants.RequestStatusEnum;
import com.kelai.domain.DeviceEntity;
import com.kelai.domain.RepairCallEntity;
import com.kelai.resource.IRepairCallResource;
import com.kelai.resource.JsonCommonResource;
import com.kelai.response.Response;
import com.kelai.service.ICommonService;
import com.kelai.service.impl.DefaultCommonService;
import com.kelai.utils.BreezeeUtils;
import javafx.util.Callback;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;

/**
 * 报修资源
 * Created by Silence on 2016/11/1.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/repair")
public class DefaultRepairCallResource extends JsonCommonResource implements IRepairCallResource {

    private ICommonService repairCallService;
    private ICommonService deviceService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public Response<RepairCallEntity> page(@RequestBody RepairCallEntity repairCallEntity) {

        Object startDate = repairCallEntity.getProperties().get("_startDate");
        Object endDate = repairCallEntity.getProperties().get("_endDate");
        try {
            if (startDate != null)
                repairCallEntity.getProperties().put("issueTime_gt", BreezeeUtils.DATE_FORMAT_LONG.parse(startDate.toString() + " 00:00:00"));
            if (endDate != null)
                repairCallEntity.getProperties().put("issueTime_le", BreezeeUtils.DATE_FORMAT_LONG.parse(endDate.toString() + " 23:59:59"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return _pageAll(repairCallService, repairCallEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public Response<RepairCallEntity> findById(@PathVariable String id) {
        return _findOne(repairCallService, id, 0);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public Response<RepairCallEntity> findByCode(@PathVariable String code) {
        return _findOne(repairCallService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public Response<RepairCallEntity> save(@RequestBody RepairCallEntity repairCallEntity) {


        DeviceEntity deviceEntity = (DeviceEntity)deviceService.findById(repairCallEntity.getDevice().getId());

        repairCallEntity.setType(deviceEntity.getType());

        repairCallEntity.setIssueTime(new Date());

        return _saveInfo(repairCallService, repairCallEntity, new Callback<RepairCallEntity, Object>() {
            @Override
            public Object call(RepairCallEntity param) {
                if (param.getCode() == null)
                    param.setCode(System.currentTimeMillis() + "");
                return null;
            }
        });
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public Response<RepairCallEntity> delete(@PathVariable String id) {
        return _delete(repairCallService, id);
    }

    @Override
    public void initService() {
        if (repairCallService == null)
            repairCallService = new DefaultCommonService("repairCallRepository");
        if (deviceService == null)
            deviceService = new DefaultCommonService("deviceRepository");
    }

    @RequestMapping(value = "/handle", method = RequestMethod.POST)
    @Override
    public Response<RepairCallEntity> handleRepairCall(@RequestBody RepairCallEntity repairCall) {
        RepairCallEntity old = (RepairCallEntity) repairCallService.findById(repairCall.getId());
        old.setHandleTime(new Date());
        old.setHandleUser(repairCall.getCreateBy());
        old.setResult(repairCall.getResult());
        old.setStatus(RequestStatusEnum.PROCESSED.getValue());
        return _saveInfo(repairCallService, old);
    }
}
