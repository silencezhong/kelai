package com.kelai.resource.impl;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.kelai.common.cache.RedisBean;
import com.kelai.common.constants.AccountTypeEnum;
import com.kelai.common.constants.CustomerCategoryEnum;
import com.kelai.common.exception.BreezeeException;
import com.kelai.domain.*;
import com.kelai.dto.DeviceManagerExcelDto;
import com.kelai.dto.ShopManagerExcelDto;
import com.kelai.resource.ICustomerResource;
import com.kelai.resource.JsonCommonResource;
import com.kelai.response.JsonResponse;
import com.kelai.response.Response;
import com.kelai.service.IAccountService;
import com.kelai.service.ICommonService;
import com.kelai.service.ICustomerService;
import com.kelai.service.impl.DefaultCommonService;
import com.kelai.utils.ExcelUtils;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 客户资源服务实现
 * Created by Silence on 2016/11/19.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/customer")
public class DefaultCustomerResource extends JsonCommonResource implements ICustomerResource {

    private ICommonService commonCustomerService;
    private ICommonService vendorService;
    private ICommonService customerGroupService;
    private ICommonService vendorCustomerService;

    @Resource
    private ICustomerService customerService;
    @Resource
    private IAccountService accountService;
    @Resource
    private RedisBean redisBean;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public JsonResponse<CustomerEntity> page(@RequestBody CustomerEntity customerEntity) {
        if (customerEntity.getCode() != null)
            customerEntity.getProperties().put("code_like", customerEntity.getCode());
        if (customerEntity.getCategory() != null)
            customerEntity.getProperties().put("category", customerEntity.getCategory());
        if (customerEntity.getSource() != null)
            customerEntity.getProperties().put("source", customerEntity.getSource());
        if(customerEntity.getProperties().get("vendor_id_obj_ae") != null
                && !Objects.equals("",customerEntity.getProperties().get("vendor_id_obj_ae"))){
            List<Object[]> list =  customerService
                    .findCustomersByVendorId(customerEntity.getProperties().get("vendor_id_obj_ae").toString());
            List<String> customerList = list.stream().map( obj -> {
                return obj[0].toString();
            }).collect(Collectors.toList());
            customerEntity.getProperties().put("parent_id_obj_in_ae", Joiner.on(",").join(customerList));
            customerEntity.getProperties().remove("parent_id_obj_ae");
            customerEntity.getProperties().remove("vendor_id_obj_ae");
        }
        if(customerEntity.getProperties().get("_areaId") != null
                && !Objects.equals("",customerEntity.getProperties().get("_areaId"))){
            String shopIdCondition = customerEntity.getProperties().get("_areaId").toString().replaceAll("-",",");
            customerEntity.getProperties().put("id_in", shopIdCondition);
            customerEntity.getProperties().remove("parent_id_obj_ae");
        }
        if (customerEntity.getProperties().get("parent_id_obj_ae") != null
                && !Objects.equals("",customerEntity.getProperties().get("parent_id_obj_ae"))){
            List<Object[]> parentList = Lists.newArrayList();
            parentList = customerService
                    .findShopParentByCusId(customerEntity.getProperties().get("parent_id_obj_ae").toString());
            List<String> parentIdList = parentList.stream().map( obj -> {
                return obj[0].toString();
            }).collect(Collectors.toList());
            customerEntity.getProperties().put("parent_id_obj_in_ae", Joiner.on(",").join(parentIdList));
            customerEntity.getProperties().remove("parent_id_obj_ae");
        }
        if(customerEntity.getProperties().get("_busiHourFilter") != null
                && Objects.equals("1",customerEntity.getProperties().get("_busiHourFilter"))){
            customerEntity.getProperties().put("date_busihour", new Date());
        }

        return _pageAll(commonCustomerService, customerEntity);
    }

    /**
     * 导出
     * @param
     * @return
     */
    @RequestMapping(value = "/export_download", method = RequestMethod.POST , produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<InputStreamResource> export(String _areaId , String shopType ,String parent_id_obj_ae,
                                                      String parent_id_obj_in_ae ,String name_like,
                                                      String _busiHourFilter, String code_like , String category ,
                                                      String source, String vendor_id_obj_ae,String isQuery) throws IOException {

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.getProperties().put("_areaId",_areaId);
        customerEntity.getProperties().put("shopType",shopType);
        customerEntity.getProperties().put("parent_id_obj_ae",parent_id_obj_ae);
        customerEntity.getProperties().put("parent_id_obj_in_ae",parent_id_obj_in_ae);
        customerEntity.getProperties().put("_busiHourFilter",_busiHourFilter);
        customerEntity.getProperties().put("code_like",code_like);
        customerEntity.getProperties().put("category",category);
        customerEntity.getProperties().put("source",source);
        customerEntity.getProperties().put("vendor_id_obj_ae",vendor_id_obj_ae);
        customerEntity.getProperties().put("name_like",name_like);

        if(customerEntity.getProperties().get("vendor_id_obj_ae") != null
                && !Objects.equals("",customerEntity.getProperties().get("vendor_id_obj_ae"))){
            List<Object[]> list =  customerService
                    .findCustomersByVendorId(customerEntity.getProperties().get("vendor_id_obj_ae").toString());
            List<String> customerList = list.stream().map( obj -> {
                return obj[0].toString();
            }).collect(Collectors.toList());
            customerEntity.getProperties().put("parent_id_obj_in_ae", Joiner.on(",").join(customerList));
            customerEntity.getProperties().remove("parent_id_obj_ae");
            customerEntity.getProperties().remove("vendor_id_obj_ae");
        }
        if(customerEntity.getProperties().get("_areaId") != null
                && !Objects.equals("",customerEntity.getProperties().get("_areaId"))){
            String shopIdCondition = customerEntity.getProperties().get("_areaId").toString().replaceAll("-",",");
            customerEntity.getProperties().put("id_in", shopIdCondition);
            customerEntity.getProperties().remove("parent_id_obj_ae");
        }
        if (customerEntity.getProperties().get("parent_id_obj_ae") != null
                && !Objects.equals("",customerEntity.getProperties().get("parent_id_obj_ae"))){
            List<Object[]> parentList = Lists.newArrayList();
            parentList = customerService
                    .findShopParentByCusId(customerEntity.getProperties().get("parent_id_obj_ae").toString());
            List<String> parentIdList = parentList.stream().map( obj -> {
                return obj[0].toString();
            }).collect(Collectors.toList());
            customerEntity.getProperties().put("parent_id_obj_in_ae", Joiner.on(",").join(parentIdList));
            customerEntity.getProperties().remove("parent_id_obj_ae");
        }
        if(customerEntity.getProperties().get("_busiHourFilter") != null
                && Objects.equals("1",customerEntity.getProperties().get("_busiHourFilter"))){
            customerEntity.getProperties().put("date_busihour", new Date());
        }
        if (customerEntity.getProperties().get("name_like") != null
                && !Objects.equals("", customerEntity.getProperties().get("name_like"))) {
            customerEntity.getProperties().put("category", "3");
            customerEntity.getProperties().put("name_like", customerEntity.getProperties().get("name_like"));
        }
        List<ShopManagerExcelDto> datas = Lists.newArrayList();

        JsonResponse rep = _pageAll(commonCustomerService, customerEntity);

            rep.getRows().forEach(a -> {
                if (a != null) {
                    CustomerEntity ce = (CustomerEntity)a;

                    ShopManagerExcelDto excelDto = new ShopManagerExcelDto();
                    excelDto.setProvince(ce.getProvince());
                    excelDto.setCity(ce.getCity());
                    excelDto.setCode(ce.getCode());
                    excelDto.setName(ce.getName());
                    excelDto.setParentName(ce.getParentName());
                    excelDto.setOpenTime(ce.getOpenTime()+ "-"+ce.getCloseTime());
                    if(ce.getGuaranteeDeadLine()!=null){
                        excelDto.setGuaranteeDeadLine(ce.getGuaranteeDeadLine().toString().substring(0,10));
                    }
                    datas.add(excelDto);

                }
            });

        File templateFile = null;

        if("true".equals(isQuery)){//是否为店铺查询
            templateFile = ExcelUtils.export(ShopManagerExcelDto.class, "店铺查询统计", "店铺查询统计", datas);

        }else{
            templateFile = ExcelUtils.export(ShopManagerExcelDto.class, "店铺管理统计", "店铺管理统计", datas);
        }


        FileSystemResource file = new FileSystemResource(templateFile);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Content-Disposition", "attachment; filename="+new String((file.getFilename()).getBytes("gb2312"), "iso-8859-1"));
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentLength(file.contentLength())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(file.getInputStream()));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<CustomerEntity> findById(@PathVariable String id) {
        JsonResponse<CustomerEntity> result = _findOne(commonCustomerService, id, 0);
        return result;
    }

    @RequestMapping(value = "/findCustomer/{id}", method = RequestMethod.GET)
    public Response<CustomerEntity> findCustomer(@PathVariable String id) {
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.getProperties().put("id",id);
        Response<CustomerEntity> result = _pageAll(commonCustomerService, customerEntity);
        return result;

    }

    @RequestMapping(value = "/findVendorByCusId/{id}", method = RequestMethod.GET)
    public Response<EmptyEntity> findVendorByCusId(@PathVariable String id) {
        JsonResponse rep = JsonResponse.OK();
        String vendorId = customerService.findVendorByCusId(id);
        Map<String,Object> map = Maps.newConcurrentMap();
        map.put("vendorId",vendorId);
        rep.setExtProps(map);
        return rep;

    }

    /**
     * 根据帐号查找旗下所有店铺
     * @param accountId
     * @return
     */
    @RequestMapping(value = "/findShops/{accountId}", method = RequestMethod.GET)
    public Response<SelectEntity> findShops(@PathVariable String accountId) {

        Map<String, Object> resultMap = new HashMap<>();
        //根据帐号找店铺
        List<Object[]> shopList = Lists.newArrayList();
        AccountEntity accountEntity = accountService.findById(accountId);
        if(Objects.equals(accountEntity.getType(), AccountTypeEnum.CUSTOMER.getValue())){
            shopList = customerService.findShopsByCustomer(accountId);

        }else if(Objects.equals(accountEntity.getType(), AccountTypeEnum.VENDOR.getValue())){
            shopList = customerService.findShopsByVendor(accountId);

        }else{
            shopList = customerService.findShopsByAdmin();

        }

        List<SelectEntity> advShopList = shopList.stream().map( obj -> {
            SelectEntity se = new SelectEntity();
            se.setId(obj[0].toString());
            se.setCode(obj[1].toString());
            se.setName(obj[2].toString());
            se.setText(obj[2].toString());
            return se;
        }).collect(Collectors.toList());

        JsonResponse<SelectEntity> result = JsonResponse.build(advShopList, System.currentTimeMillis());

        return result;
    }

    /**
     * 根据客户ID或目录ID查找旗下所有店铺
     * @param emptyEntity
     * @return
     */
    @RequestMapping(value = "/findShopsByCusId", method = RequestMethod.POST)
    public Response<SelectEntity> findShopsByCusId(@RequestBody EmptyEntity emptyEntity) {

        //根据帐号找店铺
        List<Object[]> oriShopList = Lists.newArrayList();
        Object selectCustomerId = emptyEntity.getProperties().get("selectCustomerId");
        Object followed = emptyEntity.getProperties().get("followed");
        Object selectShopType = emptyEntity.getProperties().get("selectShopType");

        if(Objects.equals("Y", followed)) {
            oriShopList = customerService.findFollowedShopsByCusId(selectCustomerId+"");

        }else{
            oriShopList = customerService.findShopsByCusId(selectCustomerId+"");

        }
        Date now = new Date();
        List<Object[]> shopList = oriShopList.stream().filter(shop -> {
            Boolean flag = true;
            //开业时间
            if(shop[9] != null){
                if(now.compareTo((Date)shop[9]) < 0){
                    flag = false;
                }
            }
            //停业时间
            if(shop[10] != null){
                if(now.compareTo((Date)shop[10]) > 0){
                    flag = false;
                }
            }
            return flag;
        }).collect(Collectors.toList());

        List<SelectEntity> advShopList = Lists.newArrayList();

        if(selectShopType == null || Objects.equals("",selectShopType)){
                advShopList = shopList.stream().map( obj -> {
                    SelectEntity se = new SelectEntity();
                    se.setId(obj[0].toString());
                    se.setCode(obj[1].toString());
                    se.setName(obj[2].toString());
                    se.setText(obj[2].toString());
                    return se;
                }).collect(Collectors.toList());
        }else{
            for(Object[] obj : shopList){
                if(Objects.equals(selectShopType,obj[3] == null ? "" : obj[3].toString())){
                    SelectEntity se = new SelectEntity();
                    se.setId(obj[0].toString());
                    se.setCode(obj[1].toString());
                    se.setName(obj[2].toString());
                    se.setText(obj[2].toString());
                    advShopList.add(se);
                }
            }
        }




        JsonResponse<SelectEntity> result = JsonResponse.build(advShopList, System.currentTimeMillis());

        return result;
    }

    /**
     * 根据帐号查找旗下所有客户和目录
     * @param accountId
     * @return
     */
    @RequestMapping(value = "/findCutsAndGrps/{accountId}", method = RequestMethod.GET)
    public Response<SelectEntity> findCutsAndGrps(@PathVariable String accountId) {

        Map<String, Object> resultMap = new HashMap<>();
        List<Object[]> list = Lists.newArrayList();
        AccountEntity accountEntity = accountService.findById(accountId);
        if(Objects.equals(accountEntity.getType(), AccountTypeEnum.CUSTOMER.getValue())){
            list = customerService.findCsagsByCustomer(accountId);

        }else if(Objects.equals(accountEntity.getType(), AccountTypeEnum.VENDOR.getValue())){
            list = customerService.findCsagsByVendor(accountId);

        }else{
            list = customerService.findCsagsByAdmin();

        }

        List<SelectEntity> advShopList = list.stream().map( obj -> {
            SelectEntity se = new SelectEntity();
            se.setId(obj[0].toString());
            se.setCode(obj[1].toString());
            se.setName(obj[2].toString());
            se.setText(obj[2].toString());
            return se;
        }).collect(Collectors.toList());

        JsonResponse<SelectEntity> result = JsonResponse.build(advShopList, System.currentTimeMillis());

        return result;
    }

    /**
     * 不管是店铺账号客户账号还是供应商账号，查找所属对应的客户
     * @param accountId
     * @return
     */
    @RequestMapping(value = "/belongCustomer/{accountId}", method = RequestMethod.GET)
    public Response<SelectEntity> belongCustomer(@PathVariable String accountId) {

        Map<String, Object> resultMap = new HashMap<>();
        List<Object[]> list = Lists.newArrayList();
        AccountEntity accountEntity = accountService.findById(accountId);
        if(Objects.equals(accountEntity.getType(), AccountTypeEnum.CUSTOMER.getValue())){
            list = customerService.findCsagsByCustomer(accountId);

        }else if(Objects.equals(accountEntity.getType(), AccountTypeEnum.VENDOR.getValue())){
            list = customerService.findCsagsByVendor(accountId);

        }else if(Objects.equals(accountEntity.getType(), AccountTypeEnum.SHOP.getValue())){
            list = customerService.findCsagsByShop(accountId);

        }else{
            list = customerService.findCsagsByAdmin();

        }

        List<SelectEntity> advShopList = list.stream().filter(obj -> {
            if(Objects.equals(obj[3].toString(), CustomerCategoryEnum.CUSTOMER.getValue())){
                return true;
            }else{
                return false;
            }
        }).map( obj -> {
            SelectEntity se = new SelectEntity();
            se.setId(obj[0].toString());
            se.setCode(obj[1].toString());
            se.setName(obj[2].toString());
            se.setText(obj[2].toString());
            return se;
        }).collect(Collectors.toList());

        JsonResponse<SelectEntity> result = JsonResponse.build(advShopList, System.currentTimeMillis());

        return result;
    }



    /**
     * 根据帐号查找旗下所有客户
     * @param accountId
     * @return
     */
    @RequestMapping(value = "/findCustomers/{accountId}", method = RequestMethod.GET)
    public Response<SelectEntity> findCustomers(@PathVariable String accountId) {

        Map<String, Object> resultMap = new HashMap<>();
        List<Object[]> list = Lists.newArrayList();
        AccountEntity accountEntity = accountService.findById(accountId);
        if(Objects.equals(accountEntity.getType(), AccountTypeEnum.CUSTOMER.getValue())){
            list = customerService.findCustomersByCustomer(accountId);

        }else if(Objects.equals(accountEntity.getType(), AccountTypeEnum.VENDOR.getValue())){
            list = customerService.findCustomersByVendor(accountId);

        }else{
            list = customerService.findCustomersByAdmin();

        }

        List<SelectEntity> advShopList = list.stream().map( obj -> {
            SelectEntity se = new SelectEntity();
            se.setId(obj[0].toString());
            se.setCode(obj[1].toString());
            se.setName(obj[2].toString());
            se.setText(obj[2].toString());
            return se;
        }).collect(Collectors.toList());

        JsonResponse<SelectEntity> result = JsonResponse.build(advShopList, System.currentTimeMillis());

        return result;
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public JsonResponse<CustomerEntity> findByCode(@PathVariable String code) {
        return _findOne(commonCustomerService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<CustomerEntity> save(@RequestBody CustomerEntity customerEntity) {

        return _saveInfo(commonCustomerService, customerEntity, new Callback<CustomerEntity, Object>() {
            @Override
            public Object call(CustomerEntity param) {
                if (param.getCategory().equals("2")){//目录
                    param.setCode(param.getId());
                }else if("3".equals(param.getCategory())){//店铺
                    Map<String,String> shopMap = ImmutableMap.of("key",param.getId(),"value",param.getName());
                    redisBean.set(shopMap);
                    CustomerEntity parent = customerService.findById(param.getParentId());
                    if(parent != null){
                        Map<String,String> parentMap = ImmutableMap.of("key",param.getId()+"-parent","value",parent.getName());
                        redisBean.set(parentMap);
                    }else{
                        Map<String,String> parentMap = ImmutableMap.of("key",param.getId()+"-parent","value",param.getName());
                        redisBean.set(parentMap);
                    }
                }
                return null;
            }
        });
    }

    @RequestMapping(value = "/updateFollowStatus", method = RequestMethod.PUT)
    public JsonResponse<CustomerEntity> updateFollowStatus(@RequestBody CustomerEntity customerEntity) {

        customerService.updateFollowStatus(customerEntity.getFollowed(),customerEntity.getId());
        return JsonResponse.OK();

    }

    @RequestMapping(value = "/saveTree", method = RequestMethod.PUT)
    public JsonResponse<CustomerEntity> saveTree(@RequestBody CustomerEntity customerEntity) {

        String vendorId = (String)customerEntity.getProperties().get("vendorId");
        VendorEntity ve = (VendorEntity) vendorService.findById(vendorId);
        String customerId = customerEntity.getId();

        JsonResponse<CustomerEntity> jsonCus = _saveInfo(commonCustomerService, customerEntity, new Callback<CustomerEntity, Object>() {
            @Override
            public Object call(CustomerEntity param) {
                if (param.getCategory().equals("2"))
                    param.setCode(param.getId());
                return null;
            }
        });
        CustomerEntity ce = jsonCus.getValue();

        //新建客户
        if(StringUtils.isEmpty(customerId)){
            VendorCustomerEntity vce = new VendorCustomerEntity();
            vce.setCustomer(ce);
            vce.setVendor(ve);
            _saveInfo(vendorCustomerService, vce, new Callback<VendorCustomerEntity, Object>() {
                @Override
                public Object call(VendorCustomerEntity param) {
                    param.setName("0");
                    if (param.getCode() == null)
                        param.setCode(param.getVendor().getId() + param.getCustomer().getId());
                    return null;
                }
            });
        }
        //编辑客户
        else{

            String sql = "update r_ven_cus set ven_id = '"+vendorId+"' where cus_id = '"+customerId+"'";
            vendorCustomerService.executeSql(sql);

        }

        return jsonCus;

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<CustomerEntity> delete(@PathVariable String id) {
        return _delete(commonCustomerService, id);
    }

    @RequestMapping(value = "/delCustomer/{id}", method = RequestMethod.DELETE)
    public JsonResponse<CustomerEntity> delCustomer(@PathVariable String id) {

        String sql = "delete from R_VEN_CUS where CUS_ID = '"+id+"'";
        vendorCustomerService.executeSql(sql);

        return _delete(commonCustomerService, id);

    }

    @Override
    public void initService() {
        if (commonCustomerService == null)
            commonCustomerService = new DefaultCommonService("customerRepository");
        if (customerGroupService == null)
            customerGroupService = new DefaultCommonService("customerGroupRepository");
        if (vendorService == null)
            vendorService = new DefaultCommonService("vendorRepository");
        if (vendorCustomerService == null)
            vendorCustomerService = new DefaultCommonService("vendorCustomerRepository");
    }

    @RequestMapping(value = "/tree/{parentId}", method = RequestMethod.GET)
    @Override
    public JsonResponse<CustomerEntity> treeCustomer(@PathVariable String parentId) {
        CustomerEntity org = new CustomerEntity();
        org.getProperties().put("parent_obj", parentId);
        org.getProperties().put("category_not_equal","3");
        return _pageAll(commonCustomerService, org);
    }

    @RequestMapping(value = "/treeForVendor/{vendorId}", method = RequestMethod.GET)
    @Override
    public JsonResponse<CustomerEntity> treeForVendor(@PathVariable String vendorId) {

        VendorEntity vendor = (VendorEntity)vendorService.findById(vendorId);
        List<String> customerIdList = vendor.getCustomers().stream().map(customerVendorRel -> {
            return customerVendorRel.getCustomer().getId();
        }).collect(Collectors.toList());

        CustomerEntity org = new CustomerEntity();
        if(CollectionUtils.isEmpty(customerIdList)){
            org.getProperties().put("id_in", "-1");
        }else{
            org.getProperties().put("id_in", Joiner.on(",").join(customerIdList));
        }
        org.getProperties().put("category_not_equal","3");
        return _pageAll(commonCustomerService, org);
    }

    @RequestMapping(value = "/findChildren/{parentId}", method = RequestMethod.GET)
    @Override
    public JsonResponse<CustomerEntity> findChildren(@PathVariable String parentId) {
        CustomerEntity org = new CustomerEntity();
        org.getProperties().put("parent_obj", parentId);
        return _pageAll(commonCustomerService, org);
    }

    @RequestMapping(value = "/group/", method = RequestMethod.POST)
    @Override
    public JsonResponse<CustomerGroupEntity> listGroup(@RequestBody CustomerGroupEntity customerGroup) {
        return _pageAll(customerGroupService, customerGroup);
    }

    @RequestMapping(value = "/group/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<CustomerGroupEntity> saveGroup(@RequestBody CustomerGroupEntity customerGroup) {
        return _saveInfo(customerGroupService, customerGroup);
    }

    @RequestMapping(value = "/group/{groupId}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<CustomerGroupEntity> deleteGroup(@PathVariable String groupId) {
        return _delete(customerGroupService, groupId);
    }

    @RequestMapping(value = "/group/{groupId}", method = RequestMethod.GET)
    @Override
    public JsonResponse<CustomerGroupEntity> findGroupById(@PathVariable String groupId) {
        return _findOne(customerGroupService, groupId, 0);
    }

    @RequestMapping(value = "/group/customer/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<CustomerGroupEntity> saveGroupCustomer(@RequestBody Map<String, String> m) {
        String sql = "delete from R_GRP_CUS where CUS_ID = '" + m.get("cusId") + "' " + " and GRP_ID='" + m.get("grpId") + "'";
        String iSql = "insert into R_GRP_CUS (CUS_ID,GRP_ID) values ('" + m.get("cusId") + "','" + m.get("grpId") + "')";
        try {
            customerGroupService.executeSql(sql, iSql);
        } catch (BreezeeException e) {
            e.printStackTrace();
            return JsonResponse.ERROR(e.getMessage());
        }
        return JsonResponse.OK();
    }


}
