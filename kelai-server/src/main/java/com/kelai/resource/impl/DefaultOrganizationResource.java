package com.kelai.resource.impl;

import com.kelai.response.JsonResponse;
import org.springframework.web.bind.annotation.*;
import com.kelai.domain.OrganizationEntity;
import com.kelai.resource.IOrganizationResource;
import com.kelai.resource.JsonCommonResource;
import com.kelai.service.ICommonService;
import com.kelai.service.impl.DefaultCommonService;

/**
 * 组织资源
 * Created by Silence on 2016/11/9.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/organization")
public class DefaultOrganizationResource extends JsonCommonResource implements IOrganizationResource {

    private ICommonService organizationService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public JsonResponse<OrganizationEntity> page(@RequestBody OrganizationEntity organizationEntity) {
        return _pageAll(organizationService, organizationEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<OrganizationEntity> findById(@PathVariable String id) {
        return _findOne(organizationService, id, 0);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public JsonResponse<OrganizationEntity> findByCode(@PathVariable String code) {
        return _findOne(organizationService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<OrganizationEntity> save(@RequestBody OrganizationEntity organizationEntity) {
        return _saveInfo(organizationService, organizationEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<OrganizationEntity> delete(@PathVariable String id) {
        return _delete(organizationService, id);
    }

    @Override
    public void initService() {
        if (organizationService == null)
            organizationService = new DefaultCommonService("organizationRepository");
    }

    @RequestMapping(value = "/tree/{parentId}", method = RequestMethod.GET)
    @Override
    public JsonResponse<OrganizationEntity> treeOrganization(@PathVariable String parentId) {
        OrganizationEntity org = new OrganizationEntity();
        org.getProperties().put("parent_obj", parentId);
        return _pageAll(organizationService, org);
    }

}
