package com.kelai.resource;

import com.kelai.domain.DeviceDataEntity;
import com.kelai.domain.EmptyEntity;
import com.kelai.response.Response;

/**
 * 统计资源服务
 * Created by Silence on 2016/11/27.
 */
public interface IStaticResource {

    Response<EmptyEntity> obtainWeather(DeviceDataEntity entity);

    Response<EmptyEntity> staticInfo(String accountId);

    Response<EmptyEntity> chartInfo(EmptyEntity emptyEntity);

    Response<EmptyEntity> dataInfo(String shopId);

    Response<EmptyEntity> deviceWeekData(String shopId);
}
