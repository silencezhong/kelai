package com.kelai.resource;

import com.kelai.domain.SaleDataEntity;

/**
 * 销售数据资源服务
 * Created by Silence on 2016/11/26.
 */
public interface ISaleDataResource extends IResourceLayer<SaleDataEntity> {
}
