package com.kelai.resource;

import com.kelai.domain.DeviceEntity;
import com.kelai.domain.ErrorMsgEntity;
import com.kelai.response.JsonResponse;
import com.kelai.response.Response;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * 设备资源服务
 * Created by Silence on 2016/11/19.
 */
public interface IDeviceResource extends IResourceLayer<DeviceEntity> {

    String receiveData(Map<String,Object> map);

    Response<DeviceEntity> findDeviceByParentId(String parentId);

//    String stopCacheData();
//    String cacheSize();

    Response<ErrorMsgEntity> pageErrorMsg(ErrorMsgEntity errorMsgEntity);

    /**
     * 获得报修设备
     * @param deviceEntity
     * @return
     */
    JsonResponse<DeviceEntity> getRepairDevice(DeviceEntity deviceEntity);

    /**
     * 保存设备的备注信息
     * @param deviceEntity
     * @return
     */
    JsonResponse saveDeviceMemo(DeviceEntity deviceEntity);
    Response<DeviceEntity> updateCustomerId(DeviceEntity deviceEntity);

}
