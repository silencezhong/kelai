package com.kelai.resource;

import com.kelai.domain.RepairCallEntity;
import com.kelai.response.Response;

/**
 * 报修对外接口
 * Created by Silence on 2016/11/1.
 */
public interface IRepairCallResource extends IResourceLayer<RepairCallEntity> {
    Response<RepairCallEntity> handleRepairCall(RepairCallEntity repairCall);
}
