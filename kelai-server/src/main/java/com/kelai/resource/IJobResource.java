package com.kelai.resource;

import com.kelai.response.JsonResponse;
import com.kelai.task.quartz.QuartzConfigEntity;
import org.quartz.SchedulerException;


/**
 * 定时任务对外接口
 * Created by Luffy on 2017/04/15.
 */
public interface IJobResource extends IResourceLayer<QuartzConfigEntity> {

    JsonResponse exec(QuartzConfigEntity quartzConfigBean) throws SchedulerException;

}
