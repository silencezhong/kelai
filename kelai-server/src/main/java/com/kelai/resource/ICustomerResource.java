package com.kelai.resource;

import com.kelai.domain.CustomerEntity;
import com.kelai.domain.CustomerGroupEntity;
import com.kelai.response.JsonResponse;
import com.kelai.response.Response;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

/**
 * 客户资源服务
 * Created by Silence on 2016/11/19.
 */
public interface ICustomerResource extends IResourceLayer<CustomerEntity> {

    Response<CustomerEntity> findChildren(String parentId);
    /**
     * 客户树
     * @param parentId
     * @return
     */
    Response<CustomerEntity> treeCustomer(String parentId);
    Response<CustomerEntity> treeForVendor(String vendorId);

    Response<CustomerGroupEntity> listGroup(CustomerGroupEntity customerGroup);

    Response<CustomerGroupEntity> saveGroup(CustomerGroupEntity customerGroup);

    Response<CustomerGroupEntity> deleteGroup(String groupId);

    Response<CustomerGroupEntity> findGroupById(String groupId);

    Response<CustomerGroupEntity> saveGroupCustomer(Map<String,String> m);
}
