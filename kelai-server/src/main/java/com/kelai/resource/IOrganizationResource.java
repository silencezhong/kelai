package com.kelai.resource;

import com.kelai.domain.OrganizationEntity;
import com.kelai.response.Response;

/**
 * 组织服务接口
 * Created by Silence on 2016/11/9.
 */
public interface IOrganizationResource extends IResourceLayer<OrganizationEntity> {

    Response<OrganizationEntity> treeOrganization(String parentId);
}
