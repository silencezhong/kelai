package com.kelai.resource;

import com.kelai.domain.AccountEntity;
import com.kelai.domain.CustomerEntity;
import com.kelai.domain.RoleEntity;
import com.kelai.domain.VendorEntity;
import com.kelai.response.Response;

import java.util.Map;

/**
 * 账号对外接口
 * Created by Silence on 2016/10/31.
 */
public interface IAccountResource extends IResourceLayer<AccountEntity> {

    Response<AccountEntity> resetPassword(Map<String, Object> map);

    Response<AccountEntity> modifyPassword(Map<String, Object> map);

    Response<AccountEntity> checkLogin(AccountEntity account);

    Response<AccountEntity> saveRelation(VendorEntity ve);

    Response<AccountEntity> saveRelation(CustomerEntity ce);

    Response<AccountEntity> saveRelation(RoleEntity role);

    Response<CustomerEntity> findShopByAccountId(String accountId);

    Response<AccountEntity> bindOpenId(AccountEntity userEntity);

    Response<AccountEntity> saveForVendor(AccountEntity account);
}
