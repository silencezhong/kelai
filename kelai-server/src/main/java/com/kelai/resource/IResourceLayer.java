package com.kelai.resource;

import com.kelai.response.Response;
import org.quartz.SchedulerException;

/**
 * 资源抽象层
 * Created by Silence on 2016/10/31.
 */
public interface IResourceLayer<T> {

    Response<T> page(T t);

    Response<T> findById(String id);

    Response<T> findByCode(String code);

    Response<T> save(T t) throws Exception;

    Response<T> delete(String id) throws SchedulerException;

    default void initService(){}
}
