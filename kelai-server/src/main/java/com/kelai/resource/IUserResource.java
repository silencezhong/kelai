package com.kelai.resource;

import com.kelai.domain.UserEntity;

/**
 * 会员服务接口
 * Created by Silence on 2016/10/18.
 */
public interface IUserResource extends IResourceLayer<UserEntity> {
}
