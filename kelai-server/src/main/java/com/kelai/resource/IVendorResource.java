package com.kelai.resource;

import com.kelai.domain.CustomerEntity;
import com.kelai.domain.VendorCustomerEntity;
import com.kelai.domain.VendorEntity;
import com.kelai.response.Response;

/**
 * 渠道商资源服务
 * Created by Silence on 2016/11/25.
 */
public interface IVendorResource extends IResourceLayer<VendorEntity> {

    Response<VendorEntity> list(VendorEntity vendorEntity);

    Response<VendorCustomerEntity> saveCustomer(VendorCustomerEntity vc);

    Response<VendorCustomerEntity> removeCustomer(String id);

    Response<VendorCustomerEntity> findCustomer(VendorCustomerEntity vc);

    Response<CustomerEntity> treeVendor(String parentId);
}
