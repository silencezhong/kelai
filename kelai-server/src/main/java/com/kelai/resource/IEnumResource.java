package com.kelai.resource;

import com.kelai.domain.EnumEntity;
import com.kelai.domain.EnumItemEntity;
import com.kelai.response.Response;

/**
 * 枚举值接口服务
 * Created by Silence on 2016/11/1.
 */
public interface IEnumResource extends IResourceLayer<EnumEntity> {

    Response<EnumItemEntity> findItems(String code);
}
