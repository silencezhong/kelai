package com.kelai.resource;

import com.kelai.domain.AbstractEntity;
import com.kelai.domain.DeviceDataEntity;
import com.kelai.domain.EmptyEntity;
import com.kelai.domain.ShopDataDayEntity;
import com.kelai.response.Response;

import java.text.ParseException;
import java.util.Map;

/**
 * 设备数据资源
 * Created by Silence on 2016/11/26.
 */
public interface IDeviceDataResource extends IResourceLayer<DeviceDataEntity> {

    Response<AbstractEntity> dataQuery(DeviceDataEntity deviceDataEntity);
    Response<AbstractEntity> compare(DeviceDataEntity deviceDataEntity);
    Response<EmptyEntity> dateCompare(DeviceDataEntity deviceDataEntity) throws ParseException;
}
