package com.kelai.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 设备状态信息
 * Created by Silence on 2016/11/22.
 */
@Entity
@Table(name = "T_DEVICE_STATUS", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DeviceStatusEntity extends AbstractEntity {

    /**
     * 版本号，设备串号，电池电压值，剩余电量
     */
    String verb,sn,battery,voltage,crc;

    /**
     * 是否对焦，是否充电
     */
    Boolean focus,charge;
}
