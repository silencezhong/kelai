package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

/**
 * 渠道商与客户关联表
 * Created by Silence on 2016/11/25.
 */
@Entity
@Table(name = "R_VEN_CUS", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"vendor", "customer"})
public class VendorCustomerEntity extends AbstractEntity {

    Double profit;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "VEN_ID", referencedColumnName = "PK_ID")
    VendorEntity vendor;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "CUS_ID", referencedColumnName = "PK_ID")
    CustomerEntity customer;

    public String getCustomerName(){
        return customer==null?"":customer.getName();
    }
}
