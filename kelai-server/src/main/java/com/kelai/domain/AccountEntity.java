/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * 持久域：账号实体
 * Created by Silence on 2016/5/5.
 */
@Entity
@Table(name = "T_ACCOUNT", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"roles", "customer", "vendor", "password"})
public class AccountEntity extends AbstractEntity {

    String password, mobile, email, job;
    /**
     * type:customer,vendor,admin 1代表管理员 2代表客户 3代表渠道商 4代表目录 5代表店铺
     * 性别 1男 2女 ，来源 1PC 2手机
     */
    Integer type, sex, source;
    /**
     * 微信的OPENID
     */
    @Column(name = "WECHAT", nullable = false, unique = true)
    String wechat;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(name = "R_ORG_ACN", joinColumns = @JoinColumn(name = "ACN_ID", referencedColumnName = "PK_ID"),
            inverseJoinColumns = @JoinColumn(name = "ORG_ID", referencedColumnName = "PK_ID"))
    OrganizationEntity organization;

    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinTable(name = "R_ROLE_ACN", joinColumns = @JoinColumn(name = "ACN_ID", referencedColumnName = "PK_ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "PK_ID"))
    @OrderBy(value = "permits asc ")
    Set<RoleEntity> roles;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(name = "R_CUS_ACN", joinColumns = @JoinColumn(name = "ACN_ID", referencedColumnName = "PK_ID"),
            inverseJoinColumns = @JoinColumn(name = "CUS_ID", referencedColumnName = "PK_ID"))
    CustomerEntity customer;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(name = "R_VEN_ACN", joinColumns = @JoinColumn(name = "ACN_ID", referencedColumnName = "PK_ID"),
            inverseJoinColumns = @JoinColumn(name = "VEN_ID", referencedColumnName = "PK_ID"))
    VendorEntity vendor;


    public String[] getPermits() {
        List<String> s = new ArrayList<>();
        if (this.getRoles() != null) {
            this.getRoles().forEach(a -> {
                    String[] permitArray = a.getPermitStr();
                    if(permitArray != null && permitArray.length > 0){
                        for(String permit : permitArray){
                            if(!s.contains(permit)){
                                s.add(permit);
                            }
                        }
                    }
//                    Collections.addAll(s, a.getPermitStr());
            });
        }
        String[] arr = new String[s.size()];
        return s.toArray(arr);
    }

    public String getRoleStr() {

        if (roles == null || roles.size() == 0){
            return "";
        }else{
            StringBuilder sb = new StringBuilder();
            roles.forEach(a -> {
                sb.append(a.getCode()).append(",");
            });
            return sb.toString();
        }
    }

    public String getCustomerId() {
        if (customer != null)
            return customer.getId();
        return null;
    }

    public String getTopCustomer(){
        if (customer != null)
            return customer.getTopCustomerName();
        return null;
    }

    public String getVendorId() {
        return vendor!=null?vendor.getId():null;
    }

    public String getLogoUrl() {
        String result = null;
        if (vendor != null){
            result = vendor.getLogoUrl();
        }else if(customer != null){
            result = customer.getLogoUrl();
        }
        if(StringUtils.isEmpty(result)){
            result = "/uploadFiles/images/logo.png";
        }
        return result;
    }

//    public String getCustomerId() {
//        return this.getCustomer() != null ? this.getCustomer().getId() : null;
//    }
//
//    public String getVendorId(){
//        return this.getVendor()==null?null:this.getVendor().getId();
//    }
}
