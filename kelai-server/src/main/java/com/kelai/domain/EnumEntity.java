/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package com.kelai.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * 持久域：枚举项实体
 * Created by Silence on 2016/5/10.
 */
@Entity
@Table(name = "T_ENUM", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EnumEntity extends AbstractEntity {

    @OneToMany(mappedBy = "master", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, orphanRemoval = true)
    @OrderBy("rowNum")
    Set<EnumItemEntity> items;

    public void addItem(EnumItemEntity item) {
        item.setMaster(this);
        this.items.add(item);
    }
}
