package com.kelai.domain;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import com.kelai.common.PersistObject;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 所有实体的基类
 * Created by Silence on 2016/10/11.
 */
@MappedSuperclass
public abstract class AbstractEntity extends PersistObject {

    public final static String DB_SCHEMA = "";

    @Id
    @GeneratedValue(generator = "assigned-uid")
    @GenericGenerator(name = "assigned-uid", strategy = "assigned")
    @Column(name = "PK_ID", unique = true, nullable = false, updatable = false, length = 64)
    String id;

    @Column(name = "NAME", length = 1024)
    String name;

    @Column(name = "CODE", nullable = false, unique = true, length = 64)
    String code;

    Integer status = 1;

    @Column(name = "REMARK", length = 3000)
    String remark;

    @CreatedDate
    Date createdDate;

    @CreatedBy
    String createBy;

    @LastModifiedDate
    Date modifiedDate;

    @LastModifiedBy
    String modifiedBy;

    @Column(name = "ROW_NUM", nullable = false)
    Integer rowNum = 1;

    @Column(name = "NODE_HOST", nullable = false, length = 256)
    String node="unknown";

    @Column(name = "EQUIPMENT", nullable = false)
    String equipment = "unknown";

    @Column(name = "VERSION", nullable = false)
    Integer version = 0;

    String tenant;

    String remoteHost="unknown";

    @Transient
    Map<String, Object> properties;

    public Map<String, Object> getProperties() {
        if (properties == null)
            this.properties = new HashMap<>();
        return properties;
    }

    public String getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }
}
