package com.kelai.domain;

import lombok.*;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class WsSonyParamEntity implements Serializable {
    /**
     * 门店编号，客流日期(格式yyyy-MM-dd)
     */

    String shopCode,shopDate;

}
