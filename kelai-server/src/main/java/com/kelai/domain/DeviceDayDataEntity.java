package com.kelai.domain;

import lombok.*;

import javax.persistence.*;

/**
 * 每日设备数据求和数据
 * Created by Silence on 2016/11/26.
 */
@Entity
@Table(name = "T_DEVICE_DATA_DAY", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DeviceDayDataEntity extends DXInOut {


    Long happenTime;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "CUS_ID")
    CustomerEntity customer;

    String customerId,customerName;
}
