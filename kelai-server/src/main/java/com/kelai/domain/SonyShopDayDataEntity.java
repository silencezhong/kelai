package com.kelai.domain;

import lombok.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * 索尼店铺客流数
 * Created by LN on 2018/06/01.
 */
@Entity
@Table(name = "T_SONY_SHOP_DAY_DATA",schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SonyShopDayDataEntity extends AbstractEntity{

    /**
     * 店铺ID，店铺code,店铺名称，
     */
    String shopId,shopCode,shopName;
    /**
     * 客流发生时间
     */
    Date happenTime;
    /**
     * 客流数
     */
    Integer cntToday;

}
