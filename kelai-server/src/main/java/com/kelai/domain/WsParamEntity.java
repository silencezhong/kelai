package com.kelai.domain;

import lombok.*;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class WsParamEntity implements Serializable {

    String token,shopid,dim,start,end;

}
