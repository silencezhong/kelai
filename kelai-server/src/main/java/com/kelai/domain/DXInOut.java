package com.kelai.domain;

import javax.persistence.MappedSuperclass;

/**
 * Created by ZhouBing on 2017/8/4.
 * 进店人数、出店人数
 */
@MappedSuperclass
public class DXInOut extends AbstractEntity{
    Integer dxin, dxout;

    public Integer getDxin() {
        return dxin;
    }

    public Integer getDxout() {
        return dxout;
    }

    public void setDxin(Integer dxin) {
        this.dxin = dxin;
    }

    public void setDxout(Integer dxout) {
        this.dxout = dxout;
    }
}
