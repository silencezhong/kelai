package com.kelai.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * 模型配置实体
 * Created by Silence on 2016/11/1.
 */
@Entity
@Table(name = "T_MODEL", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ModelEntity extends AbstractEntity {

    @OneToOne
    @JoinColumn(name = "PARENT_ID", referencedColumnName = "PK_ID")
    ModelEntity parent;

    @OneToMany(mappedBy = "model", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @OrderBy("rowNum asc")
    Set<ModelItemEntity> items;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    Set<ModelEntity> children;

}
