package com.kelai.domain;

import lombok.*;

import javax.persistence.*;

/**
 * Created by ZhouBing on 2017/7/25.
 *
 * 设备上传的原始数据，用于统计报表
 */
@Entity
@Table(name = "T_DEVICE_STATE_DATA", schema = AbstractEntity.DB_SCHEMA)
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DeviceStateDataEntity extends AbstractEntity{

    //设备
    @ManyToOne
    @JoinColumn(name = "DEVICE_ID", referencedColumnName = "PK_ID")
    private DeviceEntity device;
    //设备数据发生时间，设备数据上传时间
    private String happenTime, receiveTime;
    //对焦,电量,进店数,出店数
    private Integer focus, battery, dxin, dxout;


}
