package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kelai.common.constants.ConstantEnum;
import com.kelai.common.constants.DeviceStatusEnum;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

/**
 * 设备数据包
 * Created by luffy on 2017/05/12.
 */
@Entity
@Table(name = "T_DEVICE_PACKAGE_DATA", schema = AbstractEntity.DB_SCHEMA,
        indexes = {
                @Index(name = "idx_device_package_data_sn", columnList = "sn"),
                @Index(name = "idx_device_package_data_receiveDateTime", columnList = "receiveDateTime")
        })
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"customer"})
public class DevicePackageDataEntity extends AbstractEntity {

    String cmd,flag,state,count,tend,sn;

    @Column(name = "DATA", length = 3000)
    String data;

    Date receiveDateTime;

    @OneToMany(mappedBy = "devicePackageData", cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @OrderBy(value = "rowNum asc")
    Set<DeviceDataEntity> deviceDatas;

    @Override
    public String toString() {
        return "{cmd:" + this.cmd + ",flag:" + this.flag + ",state:" + this.state +",count:" + this.count
                +",tend:" + this.tend+",sn:" + this.sn+",data:" + this.data+"}";
    }

}
