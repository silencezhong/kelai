package com.kelai.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;


/**
 * 提供给索尼的同步数据
 * Created by LN on 2018/06/14.
 */
@Entity
@Table(name = "T_SYNC_CUSTOMER_DC", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SyncCustomerDCEntity extends AbstractEntity {


    //1 建一张新的表字段包括，举个栗子，当天，本周，当月，历史六个月等等。。。

    /**
     * 状态码(200正常，其它有错误)
     */
    Integer statusCode;

    /**
     * 传输标识(0:未传输，1:已传输。默认:0)
     */
    Integer transFlag = 0;

    /**
     * 门店编码
     */
    String shopCode;
    /**
     * 门店名称
     */
    String shopName;
    /**
     * 日期(YYYY-MM-DD)
     */
    Date shopDate;
    /**
     * 当日客流数(以进客流为准)
     */
    Integer dayCnt;
    /**
     * 本周客流数
     */
    Integer weekCnt;

    /**
     * 本周每一天客流数
     */
    Date day1OfWeek;//时间
    Integer day1OfWeekCnt;//客流数
    Date day2OfWeek;
    Integer day2OfWeekCnt;
    Date day3OfWeek;
    Integer day3OfWeekCnt;
    Date day4OfWeek;
    Integer day4OfWeekCnt;
    Date day5OfWeek;
    Integer day5OfWeekCnt;
    Date day6OfWeek;
    Integer day6OfWeekCnt;
    Date day7OfWeek;
    Integer day7OfWeekCnt;
    /**
     * 本月的年份
     */
    Integer monthOfYear;
    /**
     * 本月的月份
     */
    Integer monthOfMonth;
    /**
     * 本月客流数
     */
    Integer monthCnt;
    /**
     * 本月每周客流数
     */
    Integer week1OfMonthCnt;//本月第一周客流数
    Integer week2OfMonthCnt;
    Integer week3OfMonthCnt;
    Integer week4OfMonthCnt;
    Integer week5OfMonthCnt;

    /**
     * 历史客流数据
     */

    Integer history1monthOfYear;//历史第一个月的年份
    Integer history1monthOfMonth;//历史第一个月的月份
    Integer history1MonthCnt;//历史第一个月的客流数

    Integer week1OfHistory1MonthCnt;//历史第一个月第一周的客流数
    Integer week2OfHistory1MonthCnt;//历史第一个月第二周的客流数
    Integer week3OfHistory1MonthCnt;//历史第一个月第三周的客流数
    Integer week4OfHistory1MonthCnt;//历史第一个月第四周的客流数
    Integer week5OfHistory1MonthCnt;//历史第一个月第五周的客流数


    Integer history2monthOfYear;//历史第二个月的年份
    Integer history2monthOfMonth;//历史第二个月的月份
    Integer history2MonthCnt;//历史第二个月的客流数

    Integer week1OfHistory2MonthCnt;//历史第二个月第一周的客流数
    Integer week2OfHistory2MonthCnt;//历史第二个月第二周的客流数
    Integer week3OfHistory2MonthCnt;//历史第二个月第三周的客流数
    Integer week4OfHistory2MonthCnt;//历史第二个月第四周的客流数
    Integer week5OfHistory2MonthCnt;//历史第二个月第五周的客流数


    Integer history3monthOfYear;//历史第三个月的年份
    Integer history3monthOfMonth;//历史第三个月的月份
    Integer history3MonthCnt;//历史第三个月的客流数

    Integer week1OfHistory3MonthCnt;//历史第三个月第一周的客流数
    Integer week2OfHistory3MonthCnt;//历史第三个月第二周的客流数
    Integer week3OfHistory3MonthCnt;//历史第三个月第三周的客流数
    Integer week4OfHistory3MonthCnt;//历史第三个月第四周的客流数
    Integer week5OfHistory3MonthCnt;//历史第三个月第五周的客流数


    Integer history4monthOfYear;//历史第四个月的年份
    Integer history4monthOfMonth;//历史第四个月的月份
    Integer history4MonthCnt;//历史第四个月的客流数

    Integer week1OfHistory4MonthCnt;//历史第四个月第一周的客流数
    Integer week2OfHistory4MonthCnt;//历史第四个月第二周的客流数
    Integer week3OfHistory4MonthCnt;//历史第四个月第三周的客流数
    Integer week4OfHistory4MonthCnt;//历史第四个月第四周的客流数
    Integer week5OfHistory4MonthCnt;//历史第四个月第五周的客流数


    Integer history5monthOfYear;//历史第五个月的年份
    Integer history5monthOfMonth;//历史第五个月的月份
    Integer history5MonthCnt;//历史第五个月的客流数

    Integer week1OfHistory5MonthCnt;//历史第五个月第一周的客流数
    Integer week2OfHistory5MonthCnt;//历史第五个月第二周的客流数
    Integer week3OfHistory5MonthCnt;//历史第五个月第三周的客流数
    Integer week4OfHistory5MonthCnt;//历史第五个月第四周的客流数
    Integer week5OfHistory5MonthCnt;//历史第五个月第五周的客流数


    Integer history6monthOfYear;//历史第六个月的年份
    Integer history6monthOfMonth;//历史第六个月的月份
    Integer history6MonthCnt;//历史第六个月的客流数

    Integer week1OfHistory6MonthCnt;//历史第六个月第一周的客流数
    Integer week2OfHistory6MonthCnt;//历史第六个月第二周的客流数
    Integer week3OfHistory6MonthCnt;//历史第六个月第三周的客流数
    Integer week4OfHistory6MonthCnt;//历史第六个月第四周的客流数
    Integer week5OfHistory6MonthCnt;//历史第六个月第五周的客流数

}
