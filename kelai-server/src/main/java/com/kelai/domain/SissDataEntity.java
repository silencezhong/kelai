package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kelai.common.constants.ConstantEnum;
import com.kelai.common.constants.DeviceStatusEnum;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * 提供给思讯的数据
 * Created by Luffy on 2017/06/28.
 */
@Entity
@Table(name = "T_SISS_DATA", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SissDataEntity extends AbstractEntity {

    /**
     *  客流发生时间，客流量，店铺ID，店铺CODE，是否被消费标识
     */
    String datetime,cnt,shopId,shopCode,consumeFlag;

}
