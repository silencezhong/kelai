package com.kelai.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * 按门店汇总的小时数据
 * Created by Silence on 2017/3/19.
 */
@Entity
@Table(name = "T_SHOP_WEEK", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ShopDataWeekEntity extends DXInOut {

    Integer  year;
    String happenTime;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "SHOP_ID")
    CustomerEntity customer;

    String customerId;

    public String getCustomerName() {
        if (customer != null)
            return customer.getTopCustomerName();
        return "";
    }

    public String getShopName(){
        return customer==null?"":customer.getName();
    }
}
