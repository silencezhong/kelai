package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kelai.common.constants.ConstantEnum;
import com.kelai.common.constants.DeviceStatusEnum;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * 设备实体
 * Created by Silence on 2016/11/19.
 */
@Entity
@Table(name = "T_DEVICE", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"customer"})
public class DeviceEntity extends AbstractEntity {
    /**
     * battery 由Integer 改为String类型
     */
    String type, ipAddress,battery, position , level = "5";

    Long lastReceiveTime;

    /**
     * 设备时间，营业时间
     */
    Date deviceTime, openTime;

    /**
     * 命令类型，探测速度,记录周期，上传周期，固定时间上传，上传间隔，运行模式，计数器显示
     */
    Integer commandType, speed, recordCycle, uploadCycle, fixTimeUpload, recordPeriod,uploadPeriod, RunModel, counterDisplay, focus, voltage, charge, direction = 1;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "CUS_ID")
    CustomerEntity customer;

    String customerId;

    //备注
    String memo;

    public String getStatusName() {
        for (ConstantEnum e : DeviceStatusEnum.values()) {
            if (Objects.equals(e.getValue(), status))
                return e.getText();
        }
        return status + "";
    }

    public String getProvince() {
        return customer != null ? customer.getProvince() : "";
    }

    public String getCity() {
        return customer == null ? "" : customer.getCity();
    }

    public String getCustomerCode() {
        return customer == null ? "" : customer.getCode();
    }

    public String getCustomerName() {
        return customer == null ? "" : customer.getName();
    }

    public String getCustomerId() {
        return customer == null ? "" : customer.getId();
    }

    public String findCustomerId(){
        return customerId;
    }

    public Integer getDirection(){
        return direction == null ? 1 : direction;
    }
}
