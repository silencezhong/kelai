package com.kelai.domain;

import lombok.*;
import java.io.Serializable;

/**
 * 用于下拉框展现通用实体
 * Created by luffy on 2017/05/19.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SelectEntity implements Serializable {

    String id,code,name,text;

}
