package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * 渠道商
 * Created by Silence on 2016/11/25.
 */
@Entity
@Table(name = "T_VENDOR", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"customers","accounts"})
public class VendorEntity extends AbstractEntity {

    @OneToMany(mappedBy = "vendor", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = true)
    @OrderBy("rowNum asc")
    Set<VendorCustomerEntity> customers;

    @OneToMany(mappedBy = "vendor", cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @OrderBy(value = "rowNum asc")
    Set<AccountEntity> accounts;

    Double profit;

    String contactPerson, mobile, province, city, district, address, email , level="1" , logoUrl;

    public String getType() {
            return "folder";
    }
}
