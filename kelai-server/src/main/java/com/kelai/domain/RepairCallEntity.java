package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kelai.common.constants.RequestStatusEnum;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.Date;
import java.util.Objects;

/**
 * 故障报修实体
 * Created by Silence on 2016/11/1.
 */
@Entity(name = "T_REPAIR")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"device"})
public class RepairCallEntity extends AbstractEntity {

    /**
     * 提出时间，处理事件，处理人
     */
    Date issueTime, handleTime;

    /**
     * 提出人，处理结果,报修类型
     */
    String issueUser, result, type, contactName, contactPhone, address, handleUser, userImg;

    /**
     * 存储最多5张图片
     */
    String aImage, bImage, cImage, dImage, eImage, mediaIds;

    /**
     * 故障设备
     */
    @OneToOne
    @JoinColumn(name = "DEVICE_ID", referencedColumnName = "PK_ID")
    DeviceEntity device;

    public String getDeviceId() {
        if (this.getDevice() != null)
            return this.getDevice().getId();
        return null;
    }

    public String getDeviceName() {
        if (this.getDevice() != null)
            return this.getDevice().getName();
        return null;
    }

    public String getStatusName() {
        for (RequestStatusEnum anEnum : RequestStatusEnum.values()) {
            if (Objects.equals(anEnum.getValue(), this.getStatus()))
                return anEnum.getText();
        }
        return this.getStatus() + "";
    }
}
