package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.Entity;
import java.util.Date;

/**
 * 会员信息实体
 * Created by Silence on 2016/10/16.
 */
@Entity(name = "T_USER")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"userReserveds", "contracts"})
public class UserEntity extends AbstractEntity {

    String password, sex, address, dn, idCard, weChat, phone, email;

    Date followDate;
}
