package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * 文章实体
 * Created by Silence on 2016/11/1.
 */
@Entity
@Table(name = "T_MODEL_ITEM", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"model"})
public class ModelItemEntity extends AbstractEntity {

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    @JoinColumn(name = "MODEL_ID", referencedColumnName = "PK_ID")
    ModelEntity model;

    String subTitle;

    String cover;

    Date activeTime;

    Date expireTime;

    @Lob
    @Column(length=20971520)
    String uedit;

    String lang;
}
