package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.*;

/**
 * 客户组合
 * Created by Silence on 2016/11/22.
 */
@Entity
@Table(name = "T_CUSTOMER_GROUP", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CustomerGroupEntity extends AbstractEntity {

    @OneToMany(mappedBy = "group", cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @OrderBy(value = "rowNum ASC")
    Set<CustomerEntity> customers;

}
