package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kelai.common.constants.DeviceStatusEnum;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

/**
 * 报表实体
 * Created by Luffy on 2017/07/30.
 */
@Entity
@Table(name = "T_REPORT", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({})
public class ReportEntity extends AbstractEntity {

    String shopType,shopCode,shopName,shopAddress,contactName,contactPhone,
    deviceCode,deviceStatus,customerFlow;


}
