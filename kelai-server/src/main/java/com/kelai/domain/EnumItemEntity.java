/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * 持久域：枚举行
 * Created by Silence on 2016/5/10.
 */
@Entity
@Table(name = "T_ENUM_ITEM", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"master"})
public class EnumItemEntity extends AbstractEntity {

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "ENUM_ID")
    EnumEntity master;
}
