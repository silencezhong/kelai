package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Joiner;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * 市场活动实体
 * Created by Silence on 2016/11/26.
 */
@Entity
@Table(name = "T_MARKET_ACTIVITY", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"customers"})
public class MarketActivityEntity extends AbstractEntity {


    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinTable(name = "R_CUS_MAR", joinColumns = @JoinColumn(name = "MAR_ID", referencedColumnName = "PK_ID"),
            inverseJoinColumns = @JoinColumn(name = "CUS_ID", referencedColumnName = "PK_ID"))
    List<CustomerEntity> customers;

    Date startDate, endDate;

    public String getCustomersName() {
        String result = "无";
        if (this.getCustomers() != null) {
            String result2 = "";
            for(CustomerEntity ce : this.getCustomers()){
                result2 += ce.getName() + ",";
            }
            if(result2.length() > 0){
                result = result2.substring(0,result2.length()-1);
            }
        }
        return result;
    }

    public String[] getCustomerArray() {
        String[] resultArray = null;
        if (this.getCustomers() != null) {
            int len = this.getCustomers().size();
            resultArray = new String[len];
            for(int i = 0 ; i < len ; i++){
                resultArray[i] = this.getCustomers().get(i).getId();
            }
        }
        return resultArray;
    }

}
