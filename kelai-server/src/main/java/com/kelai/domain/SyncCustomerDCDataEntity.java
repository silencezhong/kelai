package com.kelai.domain;


import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 提供给索尼的同步数据--接口
 * Created by LN on 2018/06/15.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SyncCustomerDCDataEntity implements Serializable {
    /**
     * 传输标识(0:未传输，1:已传输。默认:0)
     */
    String transFlag = String.valueOf(0);

    /**
     * 门店编码
     */
    String shopCode;
    /**
     * 门店名称
     */
    String shopName;
    /**
     * 日期(YYYY-MM-DD)
     */
    String shopDate;
    /**
     * 当日客流数(以进客流为准)
     */
    String dayCnt;
    /**
     * 本周客流数
     */
    String weekCnt;
    /**
     * 本周每一天客流数
     */
    List<ThisWeek2> dayOfWeek = new ArrayList<>();

    public static class ThisWeek2 {
        String dayOfWeek;//时间
        String dayOfWeekCnt;//客流数

        public ThisWeek2() {
        }

        public ThisWeek2(String dayOfWeek, String dayOfWeekCnt) {
            this.dayOfWeek = dayOfWeek;
            this.dayOfWeekCnt = dayOfWeekCnt;
        }

        public String getDayOfWeek() {
            return dayOfWeek;
        }

        public void setDayOfWeek(String dayOfWeek) {
            this.dayOfWeek = dayOfWeek;
        }

        public String getDayOfWeekCnt() {
            return dayOfWeekCnt;
        }

        public void setDayOfWeekCnt(String dayOfWeekCnt) {
            this.dayOfWeekCnt = dayOfWeekCnt;
        }

        @Override
        public String toString() {
            return "ThisWeek2{" +
                    "dayOfWeek='" + dayOfWeek + '\'' +
                    ", dayOfWeekCnt='" + dayOfWeekCnt + '\'' +
                    '}';
        }
    }

    /**
     * 本月的年份
     */

    String monthOfYear;
    /**
     * 本月的月份
     */
    String monthOfMonth;
    /**
     * 本月客流数
     */
    String monthCnt;
    /**
     * 本月每周客流数
     */
    //ThisMonth weekOfMonth;
    List<ThisMonth> weekOfMonth = new ArrayList<>();

    public static class ThisMonth {
        String weekOfMonthCnt;//本月第几周客流数

        public ThisMonth(String weekOfMonthCnt) {
            this.weekOfMonthCnt = weekOfMonthCnt;
        }

        public ThisMonth() {
        }

        public String getWeekOfMonthCnt() {
            return weekOfMonthCnt;
        }

        public void setWeekOfMonthCnt(String weekOfMonthCnt) {
            this.weekOfMonthCnt = weekOfMonthCnt;
        }

        @Override
        public String toString() {
            return "ThisMonth{" +
                    "weekOfMonthCnt='" + weekOfMonthCnt + '\'' +
                    '}';
        }
    }

    /**
     * 历史客流数据
     */
    List<historyMonth> historyData = new ArrayList<>();

    public static class historyMonth {
        String historyOfYear;//历史月份的年份
        String historyOfMonth;//历史月份的月份
        String historyMonthCnt;//历史月份客流数
        List<historyWeek> weekOfHistoryMonth = new ArrayList<>();//历史月份每周客流数

        @Override
        public String toString() {
            return "historyMonth{" +
                    "historyOfYear='" + historyOfYear + '\'' +
                    ", historyOfMonth='" + historyOfMonth + '\'' +
                    ", historyMonthCnt='" + historyMonthCnt + '\'' +
                    ", weekOfHistoryMonth=" + weekOfHistoryMonth +
                    '}';
        }

        public historyMonth(String historyOfYear, String historyOfMonth, String historyMonthCnt, List weekOfHistoryMonth) {
            this.historyOfYear = historyOfYear;
            this.historyOfMonth = historyOfMonth;
            this.historyMonthCnt = historyMonthCnt;
            this.weekOfHistoryMonth = weekOfHistoryMonth;
        }

        public String getHistoryOfYear() {
            return historyOfYear;
        }

        public void setHistoryOfYear(String historyOfYear) {
            this.historyOfYear = historyOfYear;
        }

        public String getHistoryOfMonth() {
            return historyOfMonth;
        }

        public void setHistoryOfMonth(String historyOfMonth) {
            this.historyOfMonth = historyOfMonth;
        }

        public String getHistoryMonthCnt() {
            return historyMonthCnt;
        }

        public void setHistoryMonthCnt(String historyMonthCnt) {
            this.historyMonthCnt = historyMonthCnt;
        }

        public List<historyWeek> getWeekOfHistoryMonth() {
            return weekOfHistoryMonth;
        }

        public void setWeekOfHistoryMonth(List<historyWeek> weekOfHistoryMonth) {
            this.weekOfHistoryMonth = weekOfHistoryMonth;
        }
    }

    /**
     * 历史月份每周客流数
     */
    public static class historyWeek {
        String weekOfHistoryMonthCnt;//历史月份周客流数

        public historyWeek(String weekOfHistoryMonthCnt) {
            this.weekOfHistoryMonthCnt = weekOfHistoryMonthCnt;
        }

        public historyWeek() {
        }

        public String getWeekOfHistoryMonthCnt() {
            return weekOfHistoryMonthCnt;
        }

        public void setWeekOfHistoryMonthCnt(String weekOfHistoryMonthCnt) {
            this.weekOfHistoryMonthCnt = weekOfHistoryMonthCnt;
        }

        @Override
        public String toString() {
            return "historyWeek{" +
                    "weekOfHistoryMonthCnt='" + weekOfHistoryMonthCnt + '\'' +
                    '}';
        }
    }

}




