package com.kelai.domain;

import com.kelai.common.PersistObject;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * 按门店汇总的小时数据
 * Created by Silence on 2017/3/19.
 */
@Entity
@Table(name = "T_SHOP_DAY", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ShopDataDayEntity extends DXInOut {

    Integer  year;
    Date happenTime;
    String shopId,shopName,customerName;


}
