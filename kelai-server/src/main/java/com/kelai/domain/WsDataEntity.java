package com.kelai.domain;

import lombok.*;
import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class WsDataEntity implements Serializable {

    String date,cnt;

}
