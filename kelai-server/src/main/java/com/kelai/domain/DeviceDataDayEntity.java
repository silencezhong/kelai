package com.kelai.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * 设备的小时数据汇总
 * Created by Silence on 2017/3/21.
 */
@Entity
@Table(name = "T_DEVICE_DATA_DAY_BI", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DeviceDataDayEntity extends DXInOut {
    Integer  year;

    Date happenTime;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "SHOP_ID")
    CustomerEntity customer;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "DEVICE_ID")
    DeviceEntity device;

    String customerId;

    public String getShopName() {
        if (this.getCustomer() != null)
            return this.getCustomer().getName();
        return null;
    }

    public String getCustomerName(){
        if(this.getCustomer()!=null)
            return this.getCustomer().getTopCustomerName();
        return null;
    }

    public String getName(){
        return device == null ? "" : device.getName();
    }

    public String getFullName(){
        return device == null ? "" : device.getCode() + "(" + device.getName() + ")";
    }

    public String getCode(){
        return device == null ? "" : device.getCode();
    }

    public String getPosition() {
        return device == null ? "" : device.getPosition();
    }
}
