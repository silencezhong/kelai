package com.kelai.domain;


import lombok.*;
import javax.persistence.*;


/**
 * 设备消息推送信息
 * Created by Luffy on 2017/08/15.
 */
@Entity
@Table(name = "T_ERROR_MSG", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ErrorMsgEntity extends AbstractEntity {

    /**
     * 错误消息 , 发送时间 , 发送对象姓名 , 发送对象ID , 发送对象联系方式
     */
    String errorMsg , sendDateTime , toUserName , toUserId , toUserPhone;





}
