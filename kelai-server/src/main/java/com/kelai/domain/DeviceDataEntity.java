package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * 设备原始数据，数据源
 * Created by Silence on 2016/11/22.
 */
@Entity
@Table(name = "T_DEVICE_DATA", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"device", "customer","devicePackageData"})
public class DeviceDataEntity extends DXInOut {

    //是否失焦,电量
    Integer focus,battery;
    //设备编码
    String sn;
    //数据发生时间
    Long happenTime;
    //电量


    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "DEVICE_ID")
    DeviceEntity device;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "CUS_ID")
    CustomerEntity customer;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "PACKAGE_ID")
    DevicePackageDataEntity devicePackageData;

    String customerId;

    public String getShopName() {
        if (this.getCustomer() != null)
            return this.getCustomer().getName();
        return null;
    }

    public String getCustomerName(){
        if(this.getCustomer()!=null)
            return this.getCustomer().getTopCustomerName();
        return null;
    }

    public String getPosition() {
        return device == null ? "" : device.getPosition();
    }
}
