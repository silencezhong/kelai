package com.kelai.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * 销售数据
 * Created by Silence on 2016/11/26.
 */
@Entity
@Table(name = "T_SALE_DATA", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"customer"})
public class SaleDataEntity extends AbstractEntity {

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "CUS_ID")
    CustomerEntity customer;

    Date happenDate;

    Double totalAmount;

    Integer totalQuantity, totalCount;

    public String getCustomerCode() {
        return customer == null ? "" : customer.getCode();
    }

    public String getCustomerName() {
        return customer == null ? "" : customer.getName();
    }

}
