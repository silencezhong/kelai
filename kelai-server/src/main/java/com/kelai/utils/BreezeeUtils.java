/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package com.kelai.utils;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * 项目中需要使用的一些工具类
 * Created by Silence on 2016/6/2.
 */
public final class BreezeeUtils {

    public final static String FORMAT_LONG = "yyyy-MM-dd HH:mm:ss";
    public final static String FORMAT_MID = "yyyy-MM-dd HH:mm";
    public final static String FORMAT_SHORT = "yyyy-MM-dd";
    public final static String FORMAT_MONTH = "yyyy-MM";

    //SimpleDateFormat是线程不安全的，并发可能会引起问题，旧代码暂时不动了，建议新代码采用JDK8新类DateTimeFormatter
    public final static SimpleDateFormat DATE_FORMAT_LONG = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public final static SimpleDateFormat DATE_FORMAT_MID = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public final static SimpleDateFormat DATE_FORMAT_SHORT = new SimpleDateFormat("yyyy-MM-dd");
    public final static SimpleDateFormat DATE_FORMAT_MONTH = new SimpleDateFormat("yyyy-MM");


    /**
     * 加密字符串
     *
     * @param s 需要加密字符串
     * @return 加密后的结果
     */
    public static String enCrypt(String s) {
        return s;
    }

    /**
     * 解密字符串
     *
     * @param s 需要解密的字符串
     * @return 解密后的结果
     */
    public static String deCrypt(String s) {
        return s;
    }

    public static <T> boolean isNotEmpty(T o) {
        return !isEmpty(o);
    }

    public static <T> boolean isEmpty(T o) {
        if (o == null) {
            return true;
        } else if (o instanceof String) {
            return StringUtils.isEmpty((String) o);
        } else if (o instanceof Collection) {
            return CollectionUtils.isEmpty((Collection<?>) o);
        } else if (o.getClass().isArray()) {
            return ArrayUtils.isEmpty((Object[]) o);
        } else if (o instanceof Map) {
            return MapUtils.isEmpty((Map<?, ?>) o);
        } else {
            return false;
        }
    }

    public static Date strToDateTime(String str , String format){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime ldt = LocalDateTime.parse(str, formatter);
        Date date = Date.from(ldt.atZone(zone).toInstant());
        return date;

    }

    public static Date strToDate(String str , String format){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        LocalDate ld = LocalDate.parse(str, formatter);
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = ld.atStartOfDay().atZone(zone).toInstant();
        Date date = Date.from(instant);
        return date;

    }

    public static String dateToStr(Date date){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        LocalDateTime newDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        String newDateStr = newDate.format(dateTimeFormatter);
        return newDateStr;
    }

    public static void main111(String[] args) {

        System.out.println(strToDate("2017-05-01",FORMAT_SHORT));
        System.out.println(strToDateTime("2017-05-01 11:23:10",FORMAT_LONG));

    }

    /**
     * jdk 1.8 并发处理
     * 字符串转日期
     * @param dateStr   yyyy-MM-dd
     * @return
     */
    public static Date str2Date(String dateStr){
        return Date.from(LocalDate.parse(dateStr, DateTimeFormatter.ISO_LOCAL_DATE)
                .atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date localDateTime2Date(LocalDateTime localDateTime) {
        Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }

}
