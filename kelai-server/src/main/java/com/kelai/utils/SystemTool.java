/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package com.kelai.utils;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * 与系统相关的一些常用工具方法
 * Created by Silence on 2016/6/1.
 */
public class SystemTool {

    private static InetAddress ia;

    static {
        try {
            ia = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取当前操作系统名称
     * return 操作系统名称
     */
    public static String getOSName() {
        return System.getProperty("os.name").toLowerCase();
    }


    /**
     * 获取本地主机名
     *
     * @return 本机主机名
     */
    public static String getHostName() {
        return ia == null ? "unknownHost" : ia.getHostName();
    }

    /**
     * 本机IP地址
     *
     * @return 本机IP地址
     */
    public static String getIPAddress() {
        return ia == null ? "unknownIp" : ia.getHostAddress();
    }

    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String timeLine(int offset) {
        return LocalDateTime.now().toString().replaceAll("-", "").replaceAll(":", "").replaceAll("T", "").replaceAll("\\.", "").substring(offset);
    }

    public static int stringValue(String s) {
        int sum = 0;
        for (char c : s.toCharArray()) {
            sum += (int) c;
        }
        return sum;
    }

    /**
     * 生成随机整数
     *
     * @param n   位数
     * @param ran 随机实例
     * @return 获取的结果
     * @throws IllegalArgumentException
     */
    public static String random(int n, Random ran) throws IllegalArgumentException {
        if (n < 1 || n > 10) {
            throw new IllegalArgumentException("cannot random " + n + " bit number");
        }
        if (ran == null)
            ran = new Random();
        if (n == 1) {
            return String.valueOf(ran.nextInt(10));
        }
        int bitField = 0;
        char[] chs = new char[n];
        for (int i = 0; i < n; i++) {
            while (true) {
                int k = ran.nextInt(10);
                if ((bitField & (1 << k)) == 0) {
                    bitField |= 1 << k;
                    chs[i] = (char) (k + '0');
                    break;
                }
            }
        }
        return new String(chs);
    }

    public static String binaryString2hexString(String bString) {
        if (bString == null || bString.equals("") || bString.length() % 8 != 0)
            return null;
        StringBuffer tmp = new StringBuffer();
        int iTmp = 0;
        for (int i = 0; i < bString.length(); i += 4) {
            iTmp = 0;
            for (int j = 0; j < 4; j++) {
                iTmp += Integer.parseInt(bString.substring(i + j, i + j + 1)) << (4 - j - 1);
            }
            tmp.append(Integer.toHexString(iTmp));
        }
        return tmp.toString();
    }


    public static String byte2HexString(byte[] b) {
        String a = "";
        for (int i = 0; i < b.length; i++) {
            String hex = Integer.toHexString(b[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            a = a + hex;
        }
        return a;
    }

    public static String crcCheck(String ss) {
        int n = ss.length() / 2;
        byte[] bs = new byte[n];
        for (int i = 0; i < n; i++) {
            bs[i] = Integer.valueOf(ss.substring(i * 2, i * 2 + 2), 16).byteValue();
        }
        return String.format("%04x", CRC16.calcCrc16(bs));
    }

    public static String hexString2binaryString(String hexString) {
        if (hexString == null || hexString.length() % 2 != 0)
            return null;
        String bString = "", tmp;
        for (int i = 0; i < hexString.length(); i++) {
            tmp = "0000"
                    + Integer.toBinaryString(Integer.parseInt(hexString
                    .substring(i, i + 1), 16));
            bString += tmp.substring(tmp.length() - 4);
        }
        return bString;
    }

    public static byte[] convert16HexToByte(String hex16Str) {
        char[] c = hex16Str.toCharArray();
        byte[] b = new byte[c.length / 2];
        for (int i = 0; i < b.length; i++) {
            int pos = i * 2;
            b[i] = (byte) ("0123456789ABCDEF".indexOf(c[pos]) << 4 | "0123456789ABCDEF".indexOf(c[pos + 1]));
        }
        return b;
    }


    public static void main111(String[] args) {
        System.out.println(LocalDate.now().toString().replaceAll("-", ""));
        System.out.println(uuid());
//        100B1E13160000 06000000 05000000 337D,
        Class cla = SystemTool.class;
        synchronized (cla) {
            System.out.println("*****************************************************************************************");
            System.out.println("*****************************************************************************************");
            while (true)
                try {
                    System.out.println("==========");
                    cla.wait();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

        }

    }

    private static String deHexFormat(String s) {
        Integer i = Integer.decode("0x" + s);
        return i < 10 ? "0" + i : i.toString();
    }


    public static int getDayOfWeek(Date d) {
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(d.toInstant(), zone);
        LocalDate localDate = localDateTime.toLocalDate();
        return localDate.getDayOfWeek().getValue();
    }
}
