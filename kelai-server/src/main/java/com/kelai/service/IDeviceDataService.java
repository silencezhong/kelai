package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.domain.DeviceDataEntity;

import java.util.Map;

/**
 * 设备数据服务
 * Created by Silence on 2016/11/26.
 */
public interface IDeviceDataService extends IServiceLayer<DeviceDataEntity> {

    void syncOldData();

    void syncOldStatus();

    void checkNewDeviceStatus();

    void statisticsSiss(String shopId,String shopCode);
    void initDataForSsis(String shopId,String shopCode);

    Map<String,Object> sumTodayData(String code);

    void checkShopStatus();

    void syncDeviceStatus();


}
