package com.kelai.service;


import com.kelai.domain.ShopDataHourEntity;


/**
 * 店铺小时统计服务
 * Created by Luffy on 2017/04/21.
 */
public interface IShopHourService extends IBiService<ShopDataHourEntity> {


}
