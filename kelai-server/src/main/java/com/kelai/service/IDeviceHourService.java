package com.kelai.service;


import com.kelai.domain.DeviceDataHourEntity;


/**
 * 设备小时统计服务
 * Created by Luffy on 2017/04/21.
 */
public interface IDeviceHourService extends IBiService<DeviceDataHourEntity> {


}
