package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.domain.ErrorMsgEntity;


/**
 * 异常设备微信消息
 * Created by luffy on 2017/08/15.
 */
public interface IErrorMsgService extends IServiceLayer<ErrorMsgEntity> {


}
