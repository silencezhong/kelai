package com.kelai.service;


import com.kelai.domain.DeviceDataWeekEntity;


/**
 * 设备周统计服务
 * Created by Luffy on 2017/04/21.
 */
public interface IDeviceWeekService extends IBiService<DeviceDataWeekEntity> {


}
