package com.kelai.service;


import com.kelai.domain.ShopDataDayEntity;


/**
 * 店铺日统计服务
 * Created by Luffy on 2017/04/21.
 */
public interface IShopDayService extends IBiService<ShopDataDayEntity> {


}
