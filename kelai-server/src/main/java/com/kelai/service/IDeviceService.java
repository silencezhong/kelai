package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.domain.CustomerEntity;
import com.kelai.domain.DeviceEntity;
import java.util.List;

/**
 * 设备服务
 * Created by luffy on 2017/05/18.
 */
public interface IDeviceService extends IServiceLayer<DeviceEntity> {

    List<DeviceEntity> findDevicesByShops(List<String> shopIds);

    void sendAbnormalMsg();

}
