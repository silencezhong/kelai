package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.domain.DevicePackageDataEntity;


/**
 * 设备数据包服务
 * Created by luffy on 2017/08/17.
 */
public interface IDevicePackageDataService extends IServiceLayer<DevicePackageDataEntity> {


}
