package com.kelai.service;

import com.kelai.common.InfoPage;
import com.kelai.common.PersistObject;

import java.util.List;
import java.util.Map;

/**
 * Bi服务
 * Created by Luffy on 2017/04/21.
 */
public interface IBiService<T extends PersistObject> {

    List<T> listAll(Map<String,Object> condition);

    InfoPage<T> pageAll(Map<String,Object> condition);

}
