package com.kelai.service;


import com.kelai.domain.ShopDataMonthEntity;

/**
 * 店铺月统计服务
 * Created by Luffy on 2017/04/21.
 */
public interface IShopMonthService extends IBiService<ShopDataMonthEntity> {


}
