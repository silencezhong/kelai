package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.common.exception.BreezeeException;
import com.kelai.domain.AbstractEntity;

/**
 * 通用的服务
 * Created by Silence on 2016/11/3.
 */
public interface ICommonService extends IServiceLayer<AbstractEntity> {

    void executeSql(String... sql) throws BreezeeException;
}
