package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.domain.SyncCustomerDCEntity;

import java.util.Map;

/**
 * 索尼客流统计同步
 * Created by Silence on 2018/06/14.
 */
public interface ISyncCustomerDCService extends IServiceLayer<SyncCustomerDCEntity>{

    void initDC();

    Map<String, Object> getDCtData(String paramDate, String shopCode);


}
