package com.kelai.service;


import java.io.IOException;

/**
 * 微信服务接口
 * Created by Luffy on 2017/8/15.
 */
public interface IWeChatService {

    void sendTemplate(String data) throws IOException;

}
