package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.domain.SissDataEntity;


/**
 * 提供给思讯服务
 * Created by luffy on 2017/06/28.
 */
public interface ISissService extends IServiceLayer<SissDataEntity> {


}
