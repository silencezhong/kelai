package com.kelai.service;


import com.kelai.domain.DeviceDataMonthEntity;

/**
 * 设备月统计服务
 * Created by Luffy on 2017/04/21.
 */
public interface IDeviceMonthService extends IBiService<DeviceDataMonthEntity> {


}
