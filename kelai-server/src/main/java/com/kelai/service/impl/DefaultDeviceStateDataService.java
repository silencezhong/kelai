package com.kelai.service.impl;

import com.kelai.callback.IdCallback;
import com.kelai.common.IRepository;
import com.kelai.domain.DeviceStateDataEntity;
import com.kelai.repository.IDeviceStateDataRepository;
import com.kelai.service.IDeviceStateDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 设备状态数据服务
 * Created by Luffy on 2017/08/17.
 */
@SuppressWarnings("unchecked")
@Service("deviceStateDataService")
public class DefaultDeviceStateDataService implements IDeviceStateDataService {

    private final static Logger LOGGER = LoggerFactory.getLogger(DefaultDeviceStateDataService.class);

    private final IDeviceStateDataRepository deviceStateDataRepository;

    private final JdbcTemplate jdbcTemplate;

    @Resource
    private IdCallback idCallback;

    @Autowired
    public DefaultDeviceStateDataService(IDeviceStateDataRepository deviceStateDataRepository,
                                         JdbcTemplate jdbcTemplate ) {
        this.deviceStateDataRepository = deviceStateDataRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public IRepository<DeviceStateDataEntity> getRepository() {
        return deviceStateDataRepository;
    }

}
