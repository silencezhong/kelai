package com.kelai.service.impl;


import com.kelai.common.IRepository;
import com.kelai.domain.ErrorMsgEntity;
import com.kelai.repository.IErrorMsgRepository;
import com.kelai.service.IErrorMsgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;


/**
 * 异常设备微信消息
 * Created by luffy on 2017/08/15.
 */
@Service("errorMsgService")
public class DefaultErrorMsgService implements IErrorMsgService {

    private final static Logger logger = LoggerFactory.getLogger(DefaultErrorMsgService.class);

    private final IErrorMsgRepository errorMsgRepository;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DefaultErrorMsgService(IErrorMsgRepository errorMsgRepository, JdbcTemplate jdbcTemplate) {
        this.errorMsgRepository = errorMsgRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public IRepository<ErrorMsgEntity> getRepository() {
        return errorMsgRepository;
    }


}
