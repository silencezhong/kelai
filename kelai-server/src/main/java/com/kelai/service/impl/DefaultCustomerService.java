package com.kelai.service.impl;

import com.kelai.common.IRepository;
import com.kelai.domain.CustomerEntity;
import com.kelai.repository.IAccountRepository;
import com.kelai.repository.ICustomerRepository;
import com.kelai.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.*;

/**
 * 客户目录店铺服务实现类
 * Created by luffy on 2017/05/17.
 */
@Service("customerService")
public class DefaultCustomerService implements ICustomerService {

    private final ICustomerRepository customerRepository;
    private final IAccountRepository accountRepository;

    private final JdbcTemplate jdbcTemplate;

    private final PlatformTransactionManager transactionManager;

    @Autowired
    public DefaultCustomerService(ICustomerRepository customerRepository,IAccountRepository accountRepository, JdbcTemplate jdbcTemplate, PlatformTransactionManager transactionManager) {
        this.customerRepository = customerRepository;
        this.accountRepository = accountRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.transactionManager = transactionManager;
    }

    @Override
    public IRepository<CustomerEntity> getRepository() {
        return customerRepository;
    }


    @Override
    public int updateFollowStatus(String followed, String id) {
        return customerRepository.updateFollowStatus(followed,id);
    }

    @Override
    public String findVendorByCusId(String cusId) {
        return customerRepository.findVendorByCusId(cusId);
    }

    @Override
    public List<Object[]> findShopsByVendor(String vendorId) {
        return customerRepository.findShopsByVendor(vendorId);
    }
    @Override
    public List<String> findShopIdsByVendor(String vendorId) {
        return customerRepository.findShopIdsByVendor(vendorId);
    }

    @Override
    public List<Object[]> findShopsByAdmin() {
        return customerRepository.findShopsByAdmin();
    }
    @Override
    public List<String> findShopIdsByAdmin() {
        return customerRepository.findShopIdsByAdmin();
    }

    @Override
    public List<Object[]> findShopsByShop(String customerId) {
        return customerRepository.findShopsByShop(customerId);
    }
    @Override
    public List<Object[]> findShopsByCustomer(String customerId) {
        return customerRepository.findShopsByCustomer(customerId);
    }
    @Override
    public List<String> findShopIdsByCustomer(String customerId) {
        return customerRepository.findShopIdsByCustomer(customerId);
    }

    @Override
    public List<Object[]> findCsagsByCustomer(String customerAcctId) {
        return customerRepository.findCsagsByCustomer(customerAcctId);
    }

    @Override
    public List<Object[]> findCsagsByVendor(String vendorAcctId) {
        return customerRepository.findCsagsByVendor(vendorAcctId);
    }

    @Override
    public List<Object[]> findCsagsByShop(String shopAcctId) {
        return customerRepository.findCsagsByShop(shopAcctId);
    }

    @Override
    public List<Object[]> findCsagsByAdmin() {
        return customerRepository.findCsagsByAdmin();
    }

    @Override
    public List<Object[]> findCustomersByCustomer(String customerAcctId) {
        return customerRepository.findCustomersByCustomer(customerAcctId);
    }

    @Override
    public List<Object[]> findCustomersByVendor(String vendorAcctId) {
        return customerRepository.findCustomersByVendor(vendorAcctId);
    }

    @Override
    public List<Object[]> findCustomersByAdmin() {
        return customerRepository.findCustomersByAdmin();
    }

    @Override
    public List<Object[]> findShopsByCusId(String customerId) {
        return customerRepository.findShopsByCusId(customerId);
    }
    @Override
    public List<Object[]> findFollowedShopsByCusId(String customerId) {
        return customerRepository.findFollowedShopsByCusId(customerId);
    }
    @Override
    public List<Object[]> findShopParentByCusId(String customerId) {
        return customerRepository.findShopParentByCusId(customerId);
    }
    @Override
    public List<Object[]> findCustomersByVendorId(String vendorId) {
        return customerRepository.findCustomersByVendorId(vendorId);
    }



}
