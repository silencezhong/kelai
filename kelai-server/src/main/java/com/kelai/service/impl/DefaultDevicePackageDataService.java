package com.kelai.service.impl;

import com.kelai.callback.IdCallback;
import com.kelai.common.IRepository;
import com.kelai.domain.DevicePackageDataEntity;
import com.kelai.domain.DeviceStateDataEntity;
import com.kelai.repository.IDevicePackageDataRepository;
import com.kelai.repository.IDeviceStateDataRepository;
import com.kelai.service.IDevicePackageDataService;
import com.kelai.service.IDeviceStateDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 设备数据包服务
 * Created by Luffy on 2017/08/17.
 */
@SuppressWarnings("unchecked")
@Service("devicePackageDataService")
public class DefaultDevicePackageDataService implements IDevicePackageDataService {

    private final static Logger LOGGER = LoggerFactory.getLogger(DefaultDevicePackageDataService.class);

    private final IDevicePackageDataRepository devicePackageDataRepository;

    private final JdbcTemplate jdbcTemplate;

    @Resource
    private IdCallback idCallback;

    @Autowired
    public DefaultDevicePackageDataService(IDevicePackageDataRepository devicePackageDataRepository,
                                           JdbcTemplate jdbcTemplate ) {
        this.devicePackageDataRepository = devicePackageDataRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public IRepository<DevicePackageDataEntity> getRepository() {
        return devicePackageDataRepository;
    }

}
