package com.kelai.service.impl;

import com.kelai.common.IRepository;
import com.kelai.common.exception.BreezeeException;
import com.kelai.domain.AbstractEntity;
import com.kelai.service.ICommonService;
import com.kelai.utils.ContextUtil;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * 默认的方法
 * Created by Silence on 2016/11/3.
 */
public class DefaultCommonService implements ICommonService {

    private IRepository repository;

    public DefaultCommonService(String repositoryBean) {
        setRepository(repositoryBean);
    }

    @Override
    public IRepository<AbstractEntity> getRepository() {
        return repository;
    }

    @Override
    public void setRepository(String repositoryBean) {
        repository = ContextUtil.getBean(repositoryBean,IRepository.class);
    }

    @Override
    public void executeSql(String... sql) throws BreezeeException {
        JdbcTemplate jdbcTemplate = ContextUtil.getBean("jdbcTemplate",JdbcTemplate.class);
        try {
            for(String s : sql){
                jdbcTemplate.execute(s);
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
            throw new BreezeeException(e.getMessage());
        }
    }
}
