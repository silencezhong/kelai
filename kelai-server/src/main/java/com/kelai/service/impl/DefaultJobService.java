package com.kelai.service.impl;

import com.kelai.common.IRepository;
import com.kelai.common.InfoPage;
import com.kelai.common.PageInfo;
import com.kelai.common.exception.BreezeeException;
import com.kelai.repository.IJobRepository;
import com.kelai.service.IJobService;
import com.kelai.task.quartz.QuartzConfigEntity;
import com.kelai.utils.DynamicSpecifications;
import javafx.util.Callback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;


/**
 * 定时任务服务实现类
 * Created by Silence on 2016/11/26.
 */
@Service("jobService")
public class DefaultJobService implements IJobService {

    private final IJobRepository jobRepository;

    private final JdbcTemplate jdbcTemplate;

    private final PlatformTransactionManager transactionManager;

    @Autowired
    public DefaultJobService(IJobRepository jobRepository, JdbcTemplate jdbcTemplate, PlatformTransactionManager transactionManager) {
        this.jobRepository = jobRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.transactionManager = transactionManager;
    }

    @Override
    public IRepository<QuartzConfigEntity> getRepository() {
        return jobRepository;
    }

    @Override
    public QuartzConfigEntity saveInfo(QuartzConfigEntity r, Callback<QuartzConfigEntity, Object>... callback) throws BreezeeException {
        QuartzConfigEntity a = getRepository().saveEntity(r, callback);

        return a;
    }

    @Override
    public InfoPage<QuartzConfigEntity> pageAll(QuartzConfigEntity t) throws BreezeeException {
        PageInfo pageInfo = new PageInfo(t.getProperties());

        Page<QuartzConfigEntity> page = getRepository().findAll(DynamicSpecifications.createSpecification(t.getProperties()), pageInfo);
        return new InfoPage<QuartzConfigEntity>(page.getContent(), page.getTotalElements());
    }

}
