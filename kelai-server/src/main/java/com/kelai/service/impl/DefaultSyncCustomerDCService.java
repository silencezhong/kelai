package com.kelai.service.impl;

import com.kelai.common.IRepository;
import com.kelai.domain.SyncCustomerDCEntity;
import com.kelai.repository.ISyncCustomerDCRepository;
import com.kelai.service.ICustomerService;
import com.kelai.service.ISyncCustomerDCService;
import com.kelai.utils.DateUtils;
import com.kelai.utils.SystemTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class DefaultSyncCustomerDCService{

    /*@Autowired
    ICustomerService customerService;
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    ISyncCustomerDCRepository syncCustomerDCRepository;
    @Resource
    ISyncCustomerDataService syncCustomerDataService;

    @Autowired
    public DefaultSyncCustomerDCService(ISyncCustomerDCRepository syncCustomerDCRepository, JdbcTemplate jdbcTemplate) {
        this.syncCustomerDCRepository = syncCustomerDCRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    private SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

    *//**
     * 提供给索尼的同步数据
     *//*
    @Override
    public void initDC() {
        *//**
         for(String shopCode : shopCods){
         for(Date date : rangDate){  // rangDate从2018-01-01 到现在(2018-6-14)
         Map<String, Object> currentData = syncCustomerDataService.getCurrentData(date, shopCode);
         Map<String, Object> historyData = syncCustomerDataService.getHistoeryData(date, shopCode);
         data2Table(); //需要把当前和历史两部分数据转成同步数据新建的entity*//*
        String sonyCustomerId = "9236b103947f4f71b261bcf958286b2c";
        List<Object[]> customerList = (List<Object[]>) customerService.findShopsByCusId(sonyCustomerId);
        System.out.println(("索尼旗下总共 ：" + customerList.size() + "家店"));
        List<String> shopCodes = customerList.stream().map(objArray -> {
            return objArray[1].toString();
        }).collect(Collectors.toList());
        String startDate = LocalDate.of(2018, 1, 1).format(DateTimeFormatter.ISO_LOCAL_DATE);
        String endDate = LocalDate.of(2018, 6, 14).format(DateTimeFormatter.ISO_LOCAL_DATE);
        try {
            Date StartTime = sf.parse(startDate);
            Date EndTime = sf.parse(endDate);
            Long CountDay = (EndTime.getTime() - StartTime.getTime()) / (24 * 60 * 60 * 1000);
            List<Date> rangDate = new ArrayList<Date>();
            Calendar cal = Calendar.getInstance();
            for (int i = 0; i <= CountDay; i++) {
                cal.setTime(StartTime);
                cal.add(Calendar.DAY_OF_YEAR, i);
                rangDate.add(cal.getTime());
            }
            System.out.println(rangDate.size());
            //2 然后遍历门店和日期
            for (String shopCode : shopCodes) {
                for (Date date : rangDate) {  // rangDate从2018-01-01 到现在(2018-6-14)
                    String shopDate = sf.format(date);
                    Map<String, Object> currentData = syncCustomerDataService.getCurrentData(shopDate, shopCode);
                    Map<String, Object> historyData = syncCustomerDataService.getHistoryData(shopDate, shopCode);
                    Map<String, LocalDate> weekMap = DateUtils.getWeekByDate(shopDate);
                    Date day1OfWeek = DateUtils.LocalDateToDate(weekMap.get(DateUtils.MONDAY));
                    Date day2OfWeek = DateUtils.LocalDateToDate(weekMap.get(DateUtils.TUESDAY));
                    Date day3OfWeek = DateUtils.LocalDateToDate(weekMap.get(DateUtils.WEDNESDAY));
                    Date day4OfWeek = DateUtils.LocalDateToDate(weekMap.get(DateUtils.THURSDAY));
                    Date day5OfWeek = DateUtils.LocalDateToDate(weekMap.get(DateUtils.FRIDAY));
                    Date day6OfWeek = DateUtils.LocalDateToDate(weekMap.get(DateUtils.SATURDAY));
                    Date day7OfWeek = DateUtils.LocalDateToDate(weekMap.get(DateUtils.SUNDAY));
                    LocalDate oriDate = LocalDate.parse(shopDate);
                    String history1 = sf.format(DateUtils.LocalDateToDate(oriDate.minusMonths(1)));
                    String history2 = sf.format(DateUtils.LocalDateToDate(oriDate.minusMonths(2)));
                    String history3 = sf.format(DateUtils.LocalDateToDate(oriDate.minusMonths(3)));
                    String history4 = sf.format(DateUtils.LocalDateToDate(oriDate.minusMonths(4)));
                    String history5 = sf.format(DateUtils.LocalDateToDate(oriDate.minusMonths(5)));
                    String history6 = sf.format(DateUtils.LocalDateToDate(oriDate.minusMonths(6)));
                    //需要把当前和历史两部分数据转成同步数据新建的entity  data2Table();
                    SyncCustomerDCEntity entity = new SyncCustomerDCEntity();
                    String id = SystemTool.uuid();
                    entity.setId(id);
                    entity.setCode(id);
                    entity.setShopCode("P110140939");
                    entity.setShopDate(date);
                    entity.setShopName(String.valueOf(currentData.get("shopName")));
                    entity.setDayCnt((Integer) currentData.get("dayCnt"));
                    entity.setWeekCnt((Integer) currentData.get("weekCnt"));
                    entity.setDay1OfWeek(day1OfWeek);
                    entity.setDay1OfWeekCnt((Integer) currentData.get("day1OfWeekCnt"));
                    entity.setDay2OfWeek(day2OfWeek);
                    entity.setDay2OfWeekCnt((Integer) currentData.get("day2OfWeekCnt"));
                    entity.setDay3OfWeek(day3OfWeek);
                    entity.setDay3OfWeekCnt((Integer) currentData.get("day3OfWeekCnt"));
                    entity.setDay4OfWeek(day4OfWeek);
                    entity.setDay4OfWeekCnt((Integer) currentData.get("day4OfWeekCnt"));
                    entity.setDay5OfWeek(day5OfWeek);
                    entity.setDay5OfWeekCnt((Integer) currentData.get("day5OfWeekCnt"));
                    entity.setDay6OfWeek(day6OfWeek);
                    entity.setDay6OfWeekCnt((Integer) currentData.get("day6OfWeekCnt"));
                    entity.setDay7OfWeek(day7OfWeek);
                    entity.setDay7OfWeekCnt((Integer) currentData.get("day7OfWeekCnt"));
                    entity.setMonthOfYear(Integer.valueOf(shopDate.substring(0, 4)));
                    entity.setMonthOfMonth(Integer.valueOf(shopDate.substring(5, 7)));
                    entity.setMonthCnt((Integer) currentData.get("monthCnt"));
                    entity.setWeek1OfMonthCnt((Integer) currentData.get("Week1OfMonthCnt"));
                    entity.setWeek2OfMonthCnt((Integer) currentData.get("Week2OfMonthCnt"));
                    entity.setWeek3OfMonthCnt((Integer) currentData.get("Week3OfMonthCnt"));
                    entity.setWeek4OfMonthCnt((Integer) currentData.get("Week4OfMonthCnt"));
                    entity.setWeek5OfMonthCnt(currentData.get("Week5OfMonthCnt") != null ?
                            (Integer) currentData.get("Week5OfMonthCnt") : 0);
                    entity.setHistory1monthOfYear(Integer.valueOf(history1.substring(0, 4)));
                    entity.setHistory1monthOfMonth(Integer.valueOf(history1.substring(5, 7)));
                    entity.setHistory1MonthCnt((Integer) historyData.get("history1MonthCnt"));
                    entity.setWeek1OfHistory1MonthCnt((Integer) historyData.get("week1OfHistory1MonthCnt"));
                    entity.setWeek2OfHistory1MonthCnt((Integer) historyData.get("week2OfHistory1MonthCnt"));
                    entity.setWeek3OfHistory1MonthCnt((Integer) historyData.get("week3OfHistory1MonthCnt"));
                    entity.setWeek4OfHistory1MonthCnt((Integer) historyData.get("week4OfHistory1MonthCnt"));
                    entity.setWeek5OfHistory1MonthCnt(historyData.get("week5OfHistory1MonthCnt") != null ?
                            (Integer) historyData.get("week5OfHistory1MonthCnt") : 0);
                    entity.setHistory2monthOfYear(Integer.valueOf(history2.substring(0, 4)));
                    entity.setHistory2monthOfMonth(Integer.valueOf(history2.substring(5, 7)));
                    entity.setHistory2MonthCnt((Integer) historyData.get("history2MonthCnt"));
                    entity.setWeek1OfHistory2MonthCnt((Integer) historyData.get("week1OfHistory2MonthCnt"));
                    entity.setWeek2OfHistory2MonthCnt((Integer) historyData.get("week2OfHistory2MonthCnt"));
                    entity.setWeek3OfHistory2MonthCnt((Integer) historyData.get("week3OfHistory2MonthCnt"));
                    entity.setWeek4OfHistory2MonthCnt((Integer) historyData.get("week4OfHistory2MonthCnt"));
                    entity.setWeek5OfHistory2MonthCnt(historyData.get("week5OfHistory2MonthCnt") != null ?
                            (Integer) historyData.get("week5OfHistory2MonthCnt") : 0);
                    entity.setHistory3monthOfYear(Integer.valueOf(history3.substring(0, 4)));
                    entity.setHistory3monthOfMonth(Integer.valueOf(history3.substring(5, 7)));
                    entity.setHistory3MonthCnt((Integer) historyData.get("history3MonthCnt"));
                    entity.setWeek1OfHistory3MonthCnt((Integer) historyData.get("week1OfHistory3MonthCnt"));
                    entity.setWeek2OfHistory3MonthCnt((Integer) historyData.get("week2OfHistory3MonthCnt"));
                    entity.setWeek3OfHistory3MonthCnt((Integer) historyData.get("week3OfHistory3MonthCnt"));
                    entity.setWeek4OfHistory3MonthCnt((Integer) historyData.get("week4OfHistory3MonthCnt"));
                    entity.setWeek5OfHistory3MonthCnt(historyData.get("week5OfHistory3MonthCnt") != null ?
                            (Integer) historyData.get("week5OfHistory3MonthCnt") : 0);
                    entity.setHistory4monthOfYear(Integer.valueOf(history4.substring(0, 4)));
                    entity.setHistory4monthOfMonth(Integer.valueOf(history4.substring(5, 7)));
                    entity.setHistory4MonthCnt((Integer) historyData.get("history4MonthCnt"));
                    entity.setWeek1OfHistory4MonthCnt((Integer) historyData.get("week1OfHistory4MonthCnt"));
                    entity.setWeek2OfHistory4MonthCnt((Integer) historyData.get("week2OfHistory4MonthCnt"));
                    entity.setWeek3OfHistory4MonthCnt((Integer) historyData.get("week3OfHistory4MonthCnt"));
                    entity.setWeek4OfHistory4MonthCnt((Integer) historyData.get("week4OfHistory4MonthCnt"));
                    entity.setWeek5OfHistory4MonthCnt(historyData.get("week5OfHistory4MonthCnt") != null ?
                            (Integer) historyData.get("week5OfHistory4MonthCnt") : 0);
                    entity.setHistory5monthOfYear(Integer.valueOf(history5.substring(0, 4)));
                    entity.setHistory5monthOfMonth(Integer.valueOf(history5.substring(5, 7)));
                    entity.setHistory5MonthCnt((Integer) historyData.get("history5MonthCnt"));
                    entity.setWeek1OfHistory5MonthCnt((Integer) historyData.get("week1OfHistory5MonthCnt"));
                    entity.setWeek2OfHistory5MonthCnt((Integer) historyData.get("week2OfHistory5MonthCnt"));
                    entity.setWeek3OfHistory5MonthCnt((Integer) historyData.get("week3OfHistory5MonthCnt"));
                    entity.setWeek4OfHistory5MonthCnt((Integer) historyData.get("week4OfHistory5MonthCnt"));
                    entity.setWeek5OfHistory5MonthCnt(historyData.get("week5OfHistory5MonthCnt") != null ?
                            (Integer) historyData.get("week5OfHistory5MonthCnt") : 0);
                    entity.setHistory6monthOfYear(Integer.valueOf(history6.substring(0, 4)));
                    entity.setHistory6monthOfMonth(Integer.valueOf(history6.substring(5, 7)));
                    entity.setHistory6MonthCnt((Integer) historyData.get("history1MonthCnt"));
                    entity.setWeek1OfHistory6MonthCnt((Integer) historyData.get("week1OfHistory6MonthCnt"));
                    entity.setWeek2OfHistory6MonthCnt((Integer) historyData.get("week2OfHistory6MonthCnt"));
                    entity.setWeek3OfHistory6MonthCnt((Integer) historyData.get("week3OfHistory6MonthCnt"));
                    entity.setWeek4OfHistory6MonthCnt((Integer) historyData.get("week4OfHistory6MonthCnt"));
                    entity.setWeek5OfHistory6MonthCnt(historyData.get("week5OfHistory6MonthCnt") != null ?
                            (Integer) historyData.get("week5OfHistory6MonthCnt") : 0);
                    //转换完后插入新表
                    syncCustomerDCRepository.save(entity);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Map<String, Object> getDCtData(String paramDate, String shopCode) {



        return null;
    }


    @Override
    public IRepository<SyncCustomerDCEntity> getRepository() {
        return syncCustomerDCRepository;
    }*/
}
