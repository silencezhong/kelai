package com.kelai.service.impl;

import com.google.common.collect.Sets;
import com.kelai.common.InfoPage;
import com.kelai.common.PageInfo;
import com.kelai.common.constants.ShopStatusEnum;
import com.kelai.common.constants.WebAppConstants;
import com.kelai.domain.CustomerEntity;
import com.kelai.domain.ShopDataHourEntity;
import com.kelai.repository.ICustomerRepository;
import com.kelai.service.IShopHourService;
import com.kelai.utils.BreezeeUtils;
import com.kelai.utils.ContextUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.olap4j.*;
import org.olap4j.metadata.Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

/**
 * 统计服务
 * Created by Luffy on 2017/04/21.
 */
@Service("shopHourService")
public class DefaultShopHourService implements IShopHourService {
    private final static Logger logger = LoggerFactory.getLogger(DefaultShopHourService.class);
    private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final ICustomerRepository customerRepository;

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DefaultShopHourService(ICustomerRepository customerRepository, JdbcTemplate jdbcTemplate) {
        this.customerRepository = customerRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<ShopDataHourEntity> listAll(Map condition) {

        PageInfo pageInfo = new PageInfo(condition);
        if (condition.get("_sort") != null) {
            pageInfo.setSort((Sort) condition.get("_sort"));
        } else {
            pageInfo.setSort(new Sort(Sort.Direction.ASC, "rowNum","id"));
        }

        List<ShopDataHourEntity> l = new ArrayList<>();
        try {
            ICustomerRepository customerRepository = ContextUtil.getBean("customerRepository", ICustomerRepository.class);
            logger.info("--ShopHourService listAll--");
            Class.forName("org.olap4j.driver.xmla.XmlaOlap4jDriver");
            OlapConnection con = (OlapConnection) DriverManager.getConnection("jdbc:xmla:Server=http://"+WebAppConstants.BI_LINK_IP+"/MDKL/msmdpump.dll;Catalog="+ WebAppConstants.BI_LINK_NAME);
            OlapWrapper wrapper = (OlapWrapper) con;
            OlapConnection olapConnection = wrapper.unwrap(OlapConnection.class);
            OlapStatement stmt = olapConnection.createStatement();

            String startDate = String.valueOf(condition.get("_startDate"));
            String endDate = String.valueOf(condition.get("_endDate"));

            StringBuffer strBuffer = new StringBuffer();
            strBuffer.append(" ");
            strBuffer.append(" SELECT NON EMPTY { [Measures].[进], [Measures].[出] } ON 0, ");
            strBuffer.append("  {  [组织].[组织ID].[组织ID].ALLMEMBERS *  ");
            strBuffer.append(" [时间].[年].[年].ALLMEMBERS * ");
            strBuffer.append(" [时间].[月].[月].ALLMEMBERS *  ");
            strBuffer.append(" [时间].[日].[日].ALLMEMBERS * ");
            strBuffer.append(" [时间].[小时].[小时].ALLMEMBERS } on 1 ");
            strBuffer.append(" FROM ( SELECT ( { [时间].[年月日小时].[小时].&["+startDate+"T00:00:00]:[时间].[年月日小时].[小时].&["+endDate+"T23:00:00]},{ ");

            if(StringUtils.isNotEmpty(condition.get("_customerId")+"") &&
                    (StringUtils.isEmpty(condition.get("customer_id_obj_ae")+"")||condition.get("customer_id_obj_ae")==null)&&(condition.get("_shopIds") == null || CollectionUtils.isEmpty((List<String>)condition.get("_shopIds")))){
                CustomerEntity parentCustomer = customerRepository.findOne(condition.get("_customerId")+"");
                Set<CustomerEntity> shops = Sets.newConcurrentHashSet();
                parentCustomer.findDeepChildren(shops);
                Object shopType = condition.get("shopType");
                shops.stream().forEach( shop -> {
                    if(Objects.equals(shop.getStatus(), ShopStatusEnum.ENABLE.getValue())) {
                        if (shopType != null && !Objects.equals("", shopType)) {
                            if (Objects.equals(shop.getShopType(), shopType)) {
                                strBuffer.append("  [组织].[组织层次].&[" + shop.getId() + "] , ");
                            }
                        } else {
                            strBuffer.append("  [组织].[组织层次].&[" + shop.getId() + "] , ");
                        }
                    }
                });
                strBuffer.append("  [组织].[组织层次].&[-1] ");
            }else if(condition.get("_shopIds") != null && CollectionUtils.isNotEmpty((List<String>)condition.get("_shopIds"))){
                ((List<String>)condition.get("_shopIds")).stream().forEach( shopId -> {
                    strBuffer.append("  [组织].[组织层次].&["+shopId+"] , ");
                });
                strBuffer.append("  [组织].[组织层次].&[-1] ");
            }else{
                strBuffer.append("  [组织].[组织层次].&["+condition.get("customer_id_obj_ae")+"]  ");
            }
            strBuffer.append(" } ) ON COLUMNS FROM [客流]) ");

            CellSet cellSet = stmt.executeOlapQuery(strBuffer.toString());

            for (Position rowPos : cellSet.getAxes().get(1)) {
                String s = "";
                for (Member member : rowPos.getMembers()) {
                    s += member.getCaption() + "*";
                }
                for (Position colPos : cellSet.getAxes().get(0)) {
                    Cell cell = cellSet.getCell(colPos, rowPos);
                    s += cell.getFormattedValue() + "*";
                }
                String[] ss = s.split("\\*");
                String id = ss[0] + "-" + ss[1] + ss[2] + ss[3] + ss[4];
                ShopDataHourEntity sdde = new ShopDataHourEntity();
                sdde.setId(id);
                sdde.setCode(id);
                CustomerEntity ce = customerRepository.findOne(ss[0]);
                if (ce == null) {
                    System.out.println("no customer"+ss[0]);
                    continue;
                }
                sdde.setCustomer(ce);
                sdde.setCustomerId(ce.getParentId());
                try{
                    sdde.setHappenTime(BreezeeUtils.strToDateTime(ss[1] + "-" + ss[2] + "-" + ss[3] + " " + ss[4] + ":00:00", BreezeeUtils.FORMAT_LONG));
                }catch(Exception e){
                    logger.info(e.getMessage()+" "+ss[1] + "-" + ss[2] + "-" + ss[3] + " " + ss[4]);
                    sdde.setHappenTime(BreezeeUtils.strToDateTime("1970-01-01 00:00:00", BreezeeUtils.FORMAT_LONG));
                }

                sdde.setYear(Integer.parseInt(ss[1]));
                if(ss.length < 6){
                    sdde.setDxin(0);
                    sdde.setDxout(0);
                }else{
                    sdde.setDxin(Integer.parseInt(ss[5]));
                    sdde.setDxout(Integer.parseInt(ss[6]));
                }
                l.add(sdde);
            }

        } catch (Exception e1) {
            e1.printStackTrace();
        }

        return l;
    }

    @Override
    public InfoPage<ShopDataHourEntity> pageAll(Map condition) {
        PageInfo pageInfo = new PageInfo(condition);
        if (condition.get("_sort") != null) {
            pageInfo.setSort((Sort) condition.get("_sort"));
        } else {
            pageInfo.setSort(new Sort(Sort.Direction.ASC, "rowNum","id"));
        }

        List<ShopDataHourEntity> l = new ArrayList<>();
        String cnt = "0";
        try {
            ICustomerRepository customerRepository = ContextUtil.getBean("customerRepository", ICustomerRepository.class);
            logger.info("--ShopHourService pageAll--");
            Class.forName("org.olap4j.driver.xmla.XmlaOlap4jDriver");
            OlapConnection con = (OlapConnection) DriverManager.getConnection("jdbc:xmla:Server=http://"+WebAppConstants.BI_LINK_IP+"/MDKL/msmdpump.dll;Catalog="+WebAppConstants.BI_LINK_NAME);
            OlapWrapper wrapper = (OlapWrapper) con;
            OlapConnection olapConnection = wrapper.unwrap(OlapConnection.class);
            OlapStatement stmt = olapConnection.createStatement();

            Integer pageStart = Integer.parseInt(condition.get("pageNumber").toString())*
                    Integer.parseInt(condition.get("pageSize").toString());

            String startDate = String.valueOf(condition.get("_startDate"));
            String endDate = String.valueOf(condition.get("_endDate"));

            Object shopType = condition.get("shopType");

            StringBuffer strPageBuffer = new StringBuffer();
            strPageBuffer.append(" ");
            strPageBuffer.append(" SELECT NON EMPTY { [Measures].[进], [Measures].[出] } ON 0, ");
            strPageBuffer.append("  {  subset ");
            strPageBuffer.append(" (( ");
            strPageBuffer.append(" [组织].[组织ID].[组织ID].ALLMEMBERS *  ");
            strPageBuffer.append(" [时间].[年].[年].ALLMEMBERS * ");
            strPageBuffer.append(" [时间].[月].[月].ALLMEMBERS *  ");
            strPageBuffer.append(" [时间].[日].[日].ALLMEMBERS * ");
            strPageBuffer.append(" [时间].[小时].[小时].ALLMEMBERS ) ");
            strPageBuffer.append(" ,"+pageStart+" ,"+condition.get("pageSize")+")} on 1 ");
            strPageBuffer.append(" FROM ( SELECT ( { [时间].[年月日小时].[小时].&["+startDate+"T00:00:00]:[时间].[年月日小时].[小时].&["+endDate+"T23:00:00]}, {");

            if(StringUtils.isNotEmpty(condition.get("_customerId")+"") &&
                    (StringUtils.isEmpty(condition.get("customer_id_obj_ae")+"")||condition.get("customer_id_obj_ae")==null)&&(condition.get("_shopIds") == null || CollectionUtils.isEmpty((List<String>)condition.get("_shopIds")))){
                CustomerEntity parentCustomer = customerRepository.findOne(condition.get("_customerId")+"");
                Set<CustomerEntity> shops = Sets.newConcurrentHashSet();
                parentCustomer.findDeepChildren(shops);

                shops.stream().forEach( shop -> {
                    if(Objects.equals(shop.getStatus(), ShopStatusEnum.ENABLE.getValue())) {
                        if (shopType != null && !Objects.equals("", shopType)) {
                            if (Objects.equals(shop.getShopType(), shopType)) {
                                strPageBuffer.append("  [组织].[组织层次].&[" + shop.getId() + "] , ");
                            }
                        } else {
                            strPageBuffer.append("  [组织].[组织层次].&[" + shop.getId() + "] , ");
                        }
                    }
                });
                strPageBuffer.append("  [组织].[组织层次].&[-1] ");
            }else if(condition.get("_shopIds") != null && CollectionUtils.isNotEmpty((List<String>)condition.get("_shopIds"))){
                ((List<String>)condition.get("_shopIds")).stream().forEach( shopId -> {
                    strPageBuffer.append("  [组织].[组织层次].&["+shopId+"] , ");
                });
                strPageBuffer.append("  [组织].[组织层次].&[-1] ");
            }else{
                strPageBuffer.append("  [组织].[组织层次].&["+condition.get("customer_id_obj_ae")+"]  ");
            }

            strPageBuffer.append(" } ) ON COLUMNS ");
            strPageBuffer.append(" FROM [客流]) ");


            CellSet cellSet = stmt.executeOlapQuery(strPageBuffer.toString());

            for (Position rowPos : cellSet.getAxes().get(1)) {
                String s = "";
                for (Member member : rowPos.getMembers()) {
                    s += member.getCaption() + "*";
                }
                for (Position colPos : cellSet.getAxes().get(0)) {
                    Cell cell = cellSet.getCell(colPos, rowPos);
                    s += cell.getFormattedValue() + "*";
                }
                String[] ss = s.split("\\*");
                String id = ss[0] + "-" + ss[1] + ss[2] + ss[3] + ss[4];
                ShopDataHourEntity sdde = new ShopDataHourEntity();
                sdde.setId(id);
                sdde.setCode(id);
                CustomerEntity ce = customerRepository.findOne(ss[0]);
                if (ce == null) {
                    System.out.println("no customer"+ss[0]);
                    continue;
                }
                sdde.setCustomer(ce);
                sdde.setCustomerId(ce.getParentId());
                try{
                    sdde.setHappenTime(BreezeeUtils.strToDateTime(ss[1] + "-" + ss[2] + "-" + ss[3] + " " + ss[4] + ":00:00", BreezeeUtils.FORMAT_LONG));
                }catch(Exception e){
                    logger.info(e.getMessage()+" "+ss[1] + "-" + ss[2] + "-" + ss[3] + " " + ss[4]);
                    sdde.setHappenTime(BreezeeUtils.strToDateTime("1970-01-01 00:00:00", BreezeeUtils.FORMAT_LONG));
                }

                sdde.setYear(Integer.parseInt(ss[1]));
                if(ss.length < 6){
                    sdde.setDxin(0);
                    sdde.setDxout(0);
                }else{
                    sdde.setDxin(Integer.parseInt(ss[5]));
                    sdde.setDxout(Integer.parseInt(ss[6]));
                }
                l.add(sdde);
            }


//        System.out.println(" 客户       门店       设备名称 设备位置 进店数 出店数 时间");
//        l.forEach( shopDayData -> {
//            System.out.println( shopDayData.getCustomerName() + " "+shopDayData.getShopName() + "     --    --      " + shopDayData.getDxin() + " "+shopDayData.getDxout() + " " +shopDayData.getHappenTime());
//        });

        StringBuffer strCntBuffer = new StringBuffer();
        strCntBuffer.append(" ");
        strCntBuffer.append(" WITH ");
        strCntBuffer.append(" SET [xx] as  ({[组织].[组织ID].[组织ID].ALLMEMBERS * [时间].[年].[年].ALLMEMBERS * [时间].[月].[月].ALLMEMBERS *[时间].[日].[日].ALLMEMBERS* [时间].[小时].[小时].ALLMEMBERS * [Measures].[进]}) ");
        strCntBuffer.append(" MEMBER [Measures].[X] AS count([xx]) ");
        strCntBuffer.append(" SELECT Measures.X ON 0 ");
        strCntBuffer.append(" FROM (  SELECT ( { [时间].[年月日小时].[小时].&["+startDate+"T00:00:00]:[时间].[年月日小时].[小时].&["+endDate+"T23:00:00]},{ ");

            if(StringUtils.isNotEmpty(condition.get("_customerId")+"") &&
                    (StringUtils.isEmpty(condition.get("customer_id_obj_ae")+"")||condition.get("customer_id_obj_ae")==null)&&(condition.get("_shopIds") == null || CollectionUtils.isEmpty((List<String>)condition.get("_shopIds")))){
                CustomerEntity parentCustomer = customerRepository.findOne(condition.get("_customerId")+"");
                Set<CustomerEntity> shops = Sets.newConcurrentHashSet();
                parentCustomer.findDeepChildren(shops);
                shops.stream().forEach( shop -> {
                    if(Objects.equals(shop.getStatus(), ShopStatusEnum.ENABLE.getValue())) {
                        if (shopType != null && !Objects.equals("", shopType)) {
                            if (Objects.equals(shop.getShopType(), shopType)) {
                                strCntBuffer.append("  [组织].[组织层次].&[" + shop.getId() + "] , ");
                            }
                        } else {
                            strCntBuffer.append("  [组织].[组织层次].&[" + shop.getId() + "] , ");
                        }
                    }
                });
                strCntBuffer.append("  [组织].[组织层次].&[-1] ");
            }else if(condition.get("_shopIds") != null && CollectionUtils.isNotEmpty((List<String>)condition.get("_shopIds"))){
                ((List<String>)condition.get("_shopIds")).stream().forEach( shopId -> {
                    strCntBuffer.append("  [组织].[组织层次].&["+shopId+"] , ");
                });
                strCntBuffer.append("  [组织].[组织层次].&[-1] ");
            }else{
                strCntBuffer.append("  [组织].[组织层次].&["+condition.get("customer_id_obj_ae")+"]  ");
            }

        strCntBuffer.append("  } ) ON COLUMNS ");
        strCntBuffer.append(" FROM [客流]) ");

        CellSet cellSetCnt = stmt.executeOlapQuery(strCntBuffer.toString());

        for (Position rowPos : cellSetCnt.getAxes().get(0)) {
            Cell cell = cellSetCnt.getCell(rowPos);
            cnt = cell.getFormattedValue();
        }
    } catch (Exception e1) {
        e1.printStackTrace();
    }

        return new InfoPage<ShopDataHourEntity>(l,new Long(cnt));
    }
}
