package com.kelai.service.impl;


import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.kelai.callback.IdCallback;
import com.kelai.common.IRepository;
import com.kelai.common.cache.RedisBean;
import com.kelai.domain.*;
import com.kelai.repository.*;
import com.kelai.service.*;
import com.kelai.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Liangnan on 2018/06/05.
 */
@Slf4j
public class DefaultSyncCustomerDataService {

    /*private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    ICustomerService customerService;
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    RedisBean redisBean;
    @Resource
    private IdCallback idCallback;
    @Resource
    private IShopDayService shopDayService;
    @Resource
    private IShopWeekService shopWeekService;
    @Resource
    private IShopMonthService shopMonthService;

    @Autowired
    ISyncCustomerDataRepository syncCustomerDataRepository;

    @Autowired
    public DefaultSyncCustomerDataService(ISyncCustomerDataRepository syncCustomerDataRepository, IDeviceDayDataRepository deviceDayDataRepository, IDeviceRepository deviceRepository, ISaleDataRepository saleDataRepository, JdbcTemplate jdbcTemplate) {
        this.syncCustomerDataRepository = syncCustomerDataRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void init(){
        //2 在customer表里加一个字段，来代表是否初始化过，默认是未初始化(0.1)

        //3 初始化数据的定时任务，3分钟执行一次。然后取未初始化过数据的门店，取第一条。
        String sonyCustomerId = "9236b103947f4f71b261bcf958286b2c";
        List<Object[]> customerList = (List<Object[]>) customerService.findShopsByCusId(sonyCustomerId);
        log.info(("索尼旗下总共 ：" + customerList.size() + "家店"));
        List<String> shopCodes = customerList.stream().map(objArray -> {
            return objArray[1].toString();
        }).collect(Collectors.toList());
        Map<String, String> shopIdCodeMap = customerList.stream().collect(Collectors.toMap(objArray1 -> {
                    return objArray1[0].toString();
                },
                objArray2 -> {
                    return objArray2[1].toString();
                }));
        CustomerEntity conditon = new CustomerEntity();
        conditon.getProperties().put("category","3");
        conditon.getProperties().put("initFlag",0);
        conditon.getProperties().put("code_in",shopCodes);
        List<CustomerEntity> cList = customerService.listAll(conditon);
        System.out.println(cList.size());
        if(cList!=null  &&  cList.size()>0){
            String shopId = cList.get(0).getId();
            log.info(cList.get(0).getId());
            //4 初始化数据时间范围从2018-01-01号开始，一直当2018-06-13。( 缓存 可以通过门店ID取门店名称)
            List<String> shopIdList = Lists.newArrayList(shopId);
            System.out.println(shopIdList);
            String startDate = LocalDate.of(2018,1,1).format(DateTimeFormatter.ISO_LOCAL_DATE);
            String endDate = LocalDate.of(2018,6,13).format(DateTimeFormatter.ISO_LOCAL_DATE);
            Map<String, Object> conditionDay = Maps.newConcurrentMap();
            conditionDay.put("_startDate", startDate);
            conditionDay.put("_endDate", endDate);
            conditionDay.put("_shopIds",shopIdList);
            List<ShopDataDayEntity> shopDataDayEntityList = shopDayService.listAll(conditionDay);
            System.out.println(shopDataDayEntityList.size());
            for (ShopDataDayEntity shopDataDayEntity: shopDataDayEntityList) {
                SyncCustomerDataEntity entity = new SyncCustomerDataEntity();
                String id = SystemTool.uuid();
                entity.setId(id);
                entity.setCode(id);
                entity.setShopCode(shopIdCodeMap.get(shopDataDayEntity.getShopId()));
                entity.setHappenTime(shopDataDayEntity.getHappenTime());
                entity.setShopId(shopDataDayEntity.getShopId());
                entity.setShopName(redisBean.get(shopDataDayEntity.getShopId()));
                entity.setCntToday(String.valueOf(shopDataDayEntity.getDxin()));
                entity.setSource("2");
                entity.setTransFlag("0");
                syncCustomerDataRepository.save(entity);
            }
            //5 初始化完毕后，将该门店的初始化标识修改为已初始化
            String updateInitFlagsql="update t_customer set init_flag=1 where pk_id='"+shopId+"'";
            jdbcTemplate.update(updateInitFlagsql);
        }
    }

    @Override
    public void initData(){
        CustomerEntity condition = new CustomerEntity();
        List<CustomerEntity> customerList = customerService.listAll(condition);
        Map<String, String> shopIdCodeMap = customerList.stream().collect(Collectors.toMap(customer -> {
                    return customer.getId();
                },
                customer -> {
                    return customer.getCode();
                }));

        List<String> shopIdList = Lists.newArrayList("031c57eb1e1f4396809d93ed19a121f6","66f8287c84b9495c9cbd5eb9e7c1e1cb");

        String startDate = LocalDate.of(2018,1,1).format(DateTimeFormatter.ISO_LOCAL_DATE);
        String endDate = LocalDate.of(2018,6,12).format(DateTimeFormatter.ISO_LOCAL_DATE);
        Map<String, Object> conditionDay = Maps.newConcurrentMap();
        conditionDay.put("_startDate", startDate);
        conditionDay.put("_endDate", endDate);
        conditionDay.put("_shopIds", shopIdList);
        List<ShopDataDayEntity> shopDataDayEntityList = shopDayService.listAll(conditionDay);

        log.info("ready to insert data...");
        for (ShopDataDayEntity shopDataDayEntity: shopDataDayEntityList) {

            SyncCustomerDataEntity entity = new SyncCustomerDataEntity();
            String id = SystemTool.uuid();
            entity.setId(id);
            entity.setCode(id);
            entity.setShopCode(shopIdCodeMap.get(shopDataDayEntity.getShopId()));
            entity.setHappenTime(shopDataDayEntity.getHappenTime());
            entity.setShopId(shopDataDayEntity.getShopId());
            entity.setShopName(shopDataDayEntity.getShopName());
            entity.setCntToday(String.valueOf(shopDataDayEntity.getDxin()));
            entity.setSource("2");
            entity.setTransFlag("0");
            syncCustomerDataRepository.save(entity);
        }

    }

    @Override
    public void syncSonyCustomerData() {
        //1 把索尼旗下的所有门店找到
        String sonyCustomerId = "9236b103947f4f71b261bcf958286b2c";
        List<Object[]> customerList = (List<Object[]>) customerService.findShopsByCusId(sonyCustomerId);
        log.info("索尼旗下总共 ：" + customerList.size() + "家店");
        List<String> shopIds = customerList.stream().map(objArray -> {
            //log.info(objArray[1].toString() + " " + objArray[2].toString());
            return objArray[0].toString();
        }).collect(Collectors.toList());
        Map<String, String> shopIdCodeMap = customerList.stream().collect(Collectors.toMap(objArray1 -> {
                    return objArray1[0].toString();
                },
                objArray2 -> {
                    return objArray2[1].toString();
                }));

        //2 通过BI查找当天，昨天，本周，上周，本月，上月数据
        *//**当天**//*
        String nowStr = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
        Map<String, Object> conditionDay = Maps.newConcurrentMap();
        conditionDay.put("_startDate", nowStr);
        conditionDay.put("_endDate", nowStr);
        conditionDay.put("_shopIds", shopIds);
        List<ShopDataDayEntity> shopDataDayEntityList = shopDayService.listAll(conditionDay);
        //TODO 昨天
        String yestStr = LocalDate.now().minusDays(1).format(DateTimeFormatter.ISO_LOCAL_DATE);
        Map<String, Object> conditionYestDay = Maps.newConcurrentMap();
        conditionYestDay.put("_startDate", yestStr);
        conditionYestDay.put("_endDate", yestStr);
        conditionYestDay.put("_shopIds", shopIds);
        List<ShopDataDayEntity> shopDataYestDayEntityList = shopDayService.listAll(conditionYestDay);
        //TODO 本周
        TemporalField fieldISO = WeekFields.of(Locale.CHINESE).dayOfWeek();
        String thisWeek_startDate = LocalDate.now().minusWeeks(0).with(fieldISO, 1).toString();
        String thisWeek_endDate = LocalDate.now().minusWeeks(0).with(fieldISO, 7).toString();
        Map<String, Object> conditionWeek = Maps.newConcurrentMap();
        conditionWeek.put("_startDate", thisWeek_startDate);
        conditionWeek.put("_endDate", thisWeek_endDate);
        conditionWeek.put("_shopIds", shopIds);
        List<ShopDataWeekEntity> shopDataWeekEntityList = shopWeekService.listAll(conditionWeek);
        //TODO 上周
        String lastWeek_startDate = LocalDate.now().minusWeeks(1).with(fieldISO, 1).toString();
        String lastWeek_endDate = LocalDate.now().minusWeeks(1).with(fieldISO, 7).toString();
        log.info(lastWeek_startDate);
        Map<String, Object> conditionLastWeek = Maps.newConcurrentMap();
        conditionLastWeek.put("_startDate", lastWeek_startDate);
        conditionLastWeek.put("_endDate", lastWeek_endDate);
        conditionLastWeek.put("_shopIds", shopIds);
        List<ShopDataWeekEntity> shopDataLastWeekEntityList = shopWeekService.listAll(conditionLastWeek);
        //TODO 本月
        String thisMonth_startDate = LocalDate.now().minusMonths(0).with(TemporalAdjusters.firstDayOfMonth()).toString();
        String thisMonth_endDate = LocalDate.now().minusMonths(0).with(TemporalAdjusters.lastDayOfMonth()).toString();
        Map<String, Object> conditionThisMonth = Maps.newConcurrentMap();
        conditionThisMonth.put("_startDate", thisMonth_startDate);
        conditionThisMonth.put("_endDate", thisMonth_endDate);
        conditionThisMonth.put("_shopIds", shopIds);
        List<ShopDataMonthEntity> shopDataThisMonthEntityList = shopMonthService.listAll(conditionThisMonth);
        //TODO 上月
        String LastMonth_startDate = LocalDate.now().minusMonths(1).with(TemporalAdjusters.firstDayOfMonth()).toString();
        String LastMonth_endDate = LocalDate.now().minusMonths(1).with(TemporalAdjusters.lastDayOfMonth()).toString();
        Map<String, Object> conditionLastMonth = Maps.newConcurrentMap();
        conditionLastMonth.put("_startDate", LastMonth_startDate);
        conditionLastMonth.put("_endDate", LastMonth_endDate);
        conditionLastMonth.put("_shopIds", shopIds);
        List<ShopDataMonthEntity> shopDataLastMonthEntityList = shopMonthService.listAll(conditionLastMonth);
        //code


//3 插入及更新规则，一个门店一天只有一条记录，若没有则插入，若已存在则更新

//        String shopIdCondition = Joiner.on("','").join(shopIds);
//        String delSyncCustomerByDate = "delete from T_SYNC_CUSTOMER_DATA where datetime = '" + nowStr
//                + "' and shop_id in ('" + shopIdCondition + "')";
//        jdbcTemplate.update(delSyncCustomerByDate);
        for (int i = 0; i < shopIds.size(); i++) {
            SyncCustomerDataEntity condition = new SyncCustomerDataEntity();
            condition.getProperties().put("datetime", nowStr);
            condition.getProperties().put("shopId", shopIds.get(i));
            long cnt = syncCustomerDataRepository.count(DynamicSpecifications.createSpecification(condition.getProperties()));
            if (cnt > 0) {
                String updateSyncCustomerByDate = " update T_SYNC_CUSTOMER_DATA set " +
                        "cnt_today ='" + shopDataDayEntityList.get(i).getDxin() + "' , " +
                        "cnt_week ='" + shopDataWeekEntityList.get(i).getDxin() + "' ," +
                        "cnt_month = '" + shopDataThisMonthEntityList.get(i).getDxin() + "' " +
                        "where datetime = '" + nowStr + "' and shop_id = '" + shopDataDayEntityList.get(i).getShopId() + "' ";
                jdbcTemplate.update(updateSyncCustomerByDate);
            } else {
                SyncCustomerDataEntity entity = new SyncCustomerDataEntity();
                String id = SystemTool.uuid();
                entity.setId(id);
                entity.setCode(id);
                entity.setShopCode(shopIdCodeMap.get(shopDataDayEntityList.get(i).getShopId()));
                entity.setCntLastmonth(String.valueOf(shopDataLastMonthEntityList.get(i).getDxin()));
                entity.setCntLastweek(String.valueOf(shopDataLastWeekEntityList.get(i).getDxin()));
                entity.setCntMonth(String.valueOf(shopDataThisMonthEntityList.get(i).getDxin()));
                entity.setCntToday((String.valueOf(shopDataDayEntityList.get(i).getDxin())));
                entity.setCntWeek(String.valueOf(shopDataWeekEntityList.get(i).getDxin()));
                entity.setCntYestaday(String.valueOf(shopDataYestDayEntityList.get(i).getDxin()));
                entity.setDatetime(nowStr);
                entity.setShopId(shopDataDayEntityList.get(i).getShopId());
                entity.setShopName(shopDataDayEntityList.get(i).getShopName());
                entity.setSource("2");
                entity.setTransFlag("0");
                syncCustomerDataRepository.save(entity);
            }
        }
    }

    @Override
    public void syncRuicCustomerData() {

        String RuicCustomerId = "b17ea4e0958d418186eb0dffaad735c4";
        List<Object[]> customerList = (List<Object[]>) customerService.findShopsByCusId(RuicCustomerId);
        log.info("芮程旗下总共 ：" + customerList.size() + "家店");
        List<String> shopIds = customerList.stream().map(objArray -> {
            return objArray[0].toString();
        }).collect(Collectors.toList());
        Map<String, String> shopIdCodeMap = customerList.stream().collect(Collectors.toMap(objArray1 -> {
                    return objArray1[0].toString();
                },
                objArray2 -> {
                    return objArray2[1].toString();
                }));
        //2 通过BI查找当天，昨天，本周，上周，本月，上月数据
        //TODO 当天
        String nowStr = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
        Map<String, Object> conditionDay = Maps.newConcurrentMap();
        conditionDay.put("_startDate", nowStr);
        conditionDay.put("_endDate", nowStr);
        conditionDay.put("_shopIds", shopIds);
        List<ShopDataDayEntity> shopDataDayEntityList = shopDayService.listAll(conditionDay);
        //TODO 昨天
        String yestStr = LocalDate.now().minusDays(1).format(DateTimeFormatter.ISO_LOCAL_DATE);
        Map<String, Object> conditionYestDay = Maps.newConcurrentMap();
        conditionYestDay.put("_startDate", yestStr);
        conditionYestDay.put("_endDate", yestStr);
        conditionYestDay.put("_shopIds", shopIds);
        List<ShopDataDayEntity> shopDataYestDayEntityList = shopDayService.listAll(conditionYestDay);
        //TODO 本周
        TemporalField fieldISO = WeekFields.of(Locale.CHINESE).dayOfWeek();
        String thisWeek_startDate = LocalDate.now().minusWeeks(0).with(fieldISO, 1).toString();
        String thisWeek_endDate = LocalDate.now().minusWeeks(0).with(fieldISO, 7).toString();
        log.info(thisWeek_endDate);
        Map<String, Object> conditionWeek = Maps.newConcurrentMap();
        conditionWeek.put("_startDate", thisWeek_startDate);
        conditionWeek.put("_endDate", thisWeek_endDate);
        conditionWeek.put("_shopIds", shopIds);
        List<ShopDataWeekEntity> shopDataWeekEntityList = shopWeekService.listAll(conditionWeek);
        //TODO 上周
        String lastWeek_startDate = LocalDate.now().minusWeeks(1).with(fieldISO, 1).toString();
        String lastWeek_endDate = LocalDate.now().minusWeeks(1).with(fieldISO, 7).toString();
        log.info(lastWeek_startDate);
        Map<String, Object> conditionLastWeek = Maps.newConcurrentMap();
        conditionLastWeek.put("_startDate", lastWeek_startDate);
        conditionLastWeek.put("_endDate", lastWeek_endDate);
        conditionLastWeek.put("_shopIds", shopIds);
        List<ShopDataWeekEntity> shopDataLastWeekEntityList = shopWeekService.listAll(conditionLastWeek);
        //TODO 本月
        String thisMonth_startDate = LocalDate.now().minusMonths(0).with(TemporalAdjusters.firstDayOfMonth()).toString();
        String thisMonth_endDate = LocalDate.now().minusMonths(0).with(TemporalAdjusters.lastDayOfMonth()).toString();
        Map<String, Object> conditionThisMonth = Maps.newConcurrentMap();
        conditionThisMonth.put("_startDate", thisMonth_startDate);
        conditionThisMonth.put("_endDate", thisMonth_endDate);
        conditionThisMonth.put("_shopIds", shopIds);
        List<ShopDataMonthEntity> shopDataThisMonthEntityList = shopMonthService.listAll(conditionThisMonth);
        //TODO 上月
        String LastMonth_startDate = LocalDate.now().minusMonths(1).with(TemporalAdjusters.firstDayOfMonth()).toString();
        String LastMonth_endDate = LocalDate.now().minusMonths(1).with(TemporalAdjusters.lastDayOfMonth()).toString();
        Map<String, Object> conditionLastMonth = Maps.newConcurrentMap();
        conditionLastMonth.put("_startDate", LastMonth_startDate);
        conditionLastMonth.put("_endDate", LastMonth_endDate);
        conditionLastMonth.put("_shopIds", shopIds);
        List<ShopDataMonthEntity> shopDataLastMonthEntityList = shopMonthService.listAll(conditionLastMonth);

        //3 插入及更新规则，一个门店一天只有一条记录，若没有则插入，若已存在则更新

//        String shopIdCondition = Joiner.on("','").join(shopIds);
//        String delSyncCustomerByDate = "delete from T_SYNC_CUSTOMER_DATA where datetime = '" + nowStr
//                + "' and shop_id in ('" + shopIdCondition + "')";
//        jdbcTemplate.update(delSyncCustomerByDate);

        for (int i = 0; i < shopIds.size(); i++) {
            SyncCustomerDataEntity condition = new SyncCustomerDataEntity();
            condition.getProperties().put("datetime", nowStr);
            condition.getProperties().put("shopId", shopIds.get(i));
            long cnt = syncCustomerDataRepository.count(DynamicSpecifications.createSpecification(condition.getProperties()));
            if (cnt > 0) {
                String updateSyncCustomerByDate = " update T_SYNC_CUSTOMER_DATA set " +
                        "cnt_today ='" + shopDataDayEntityList.get(i).getDxin() + "' , " +
                        "cnt_week ='" + shopDataWeekEntityList.get(i).getDxin() + "' ," +
                        "cnt_month = '" + shopDataThisMonthEntityList.get(i).getDxin() + "' " +
                        "where datetime = '" + nowStr + "' and shop_id = '" + shopIds.get(i) + "' ";
                jdbcTemplate.update(updateSyncCustomerByDate);
            } else {
                SyncCustomerDataEntity entity = new SyncCustomerDataEntity();
                String id = SystemTool.uuid();
                entity.setId(id);
                entity.setCode(id);
                entity.setShopCode(shopIdCodeMap.get(shopDataDayEntityList.get(i).getShopId()));
                entity.setCntLastmonth(String.valueOf(shopDataLastMonthEntityList.get(i).getDxin()));
                entity.setCntLastweek(String.valueOf(shopDataLastWeekEntityList.get(i).getDxin()));
                entity.setCntMonth(String.valueOf(shopDataThisMonthEntityList.get(i).getDxin()));
                entity.setCntToday((String.valueOf(shopDataDayEntityList.get(i).getDxin())));
                entity.setCntWeek(String.valueOf(shopDataWeekEntityList.get(i).getDxin()));
                entity.setCntYestaday(String.valueOf(shopDataYestDayEntityList.get(i).getDxin()));
                entity.setDatetime(nowStr);
                entity.setShopId(shopDataDayEntityList.get(i).getShopId());
                entity.setShopName(shopDataDayEntityList.get(i).getShopName());
                entity.setSource("2");
                entity.setTransFlag("0");
                syncCustomerDataRepository.save(entity);
            }
        }


    }

    public void syncCustomerDataTest() {
        //1 把索尼旗下的所有门店找到
        String sonyCustomerId = "9236b103947f4f71b261bcf958286b2c";
        List<Object[]> customerList = (List<Object[]>) customerService.findShopsByCusId(sonyCustomerId);
        log.info("索尼旗下总共 ：" + customerList.size() + "家店");
        List<String> shopIds = customerList.stream().map(objArray -> {
            return objArray[0].toString();
        }).collect(Collectors.toList());
        Map<String, String> shopIdCodeMap = customerList.stream().collect(Collectors.toMap(objArray1 -> {
                    return objArray1[0].toString();
                },
                objArray2 -> {
                    return objArray2[1].toString();
                }));

        List<String> shopIdList = Lists.newArrayList("031c57eb1e1f4396809d93ed19a121f6");

        *//**当天**//*
        String startDate = LocalDate.of(2018,5,1).format(DateTimeFormatter.ISO_LOCAL_DATE);
        String endDate = LocalDate.of(2018,6,10).format(DateTimeFormatter.ISO_LOCAL_DATE);
        Map<String, Object> conditionDay = Maps.newConcurrentMap();
        conditionDay.put("_startDate", startDate);
        conditionDay.put("_endDate", endDate);
        conditionDay.put("_shopIds", shopIdList);
        List<ShopDataDayEntity> shopDataDayEntityList = shopDayService.listAll(conditionDay);

        for (ShopDataDayEntity shopDataDayEntity: shopDataDayEntityList) {

            SyncCustomerDataEntity entity = new SyncCustomerDataEntity();
            String id = SystemTool.uuid();
            entity.setId(id);
            entity.setCode(id);
            entity.setShopCode(shopIdCodeMap.get(shopDataDayEntity.getShopId()));
            entity.setHappenTime(shopDataDayEntity.getHappenTime());
            entity.setShopId(shopDataDayEntity.getShopId());
            entity.setShopName(shopDataDayEntity.getShopName());
            entity.setCntToday(String.valueOf(shopDataDayEntity.getDxin()));
            entity.setSource("2");
            entity.setTransFlag("0");
            syncCustomerDataRepository.save(entity);
        }
    }


    @Override
    public Map<String, Object> getCurrentData(String paramDate, String shopCode) {

        String shopDate = paramDate;
        String currentDayStart = paramDate + " 00:00:00";
        String currentDayEnd = paramDate + " 23:59:59";
        Map<String, LocalDate> weekMap = DateUtils.getWeekByDate(shopDate);
        LocalDate day1OfWeek = weekMap.get(DateUtils.MONDAY);
        LocalDate day2OfWeek = weekMap.get(DateUtils.TUESDAY);
        LocalDate day3OfWeek = weekMap.get(DateUtils.WEDNESDAY);
        LocalDate day4OfWeek = weekMap.get(DateUtils.THURSDAY);
        LocalDate day5OfWeek = weekMap.get(DateUtils.FRIDAY);
        LocalDate day6OfWeek = weekMap.get(DateUtils.SATURDAY);
        LocalDate day7OfWeek = weekMap.get(DateUtils.SUNDAY);
        String day1OfWeekStart = day1OfWeek.toString() + " 00:00:00";
        String day1OfWeekEnd = day1OfWeek.toString() + " 23:59:59";
        String day2OfWeekStart = day2OfWeek.toString() + " 00:00:00";
        String day2OfWeekEnd = day2OfWeek.toString() + " 23:59:59";
        String day3OfWeekStart = day3OfWeek.toString() + " 00:00:00";
        String day3OfWeekEnd = day3OfWeek.toString() + " 23:59:59";
        String day4OfWeekStart = day4OfWeek.toString() + " 00:00:00";
        String day4OfWeekEnd = day4OfWeek.toString() + " 23:59:59";
        String day5OfWeekStart = day5OfWeek.toString() + " 00:00:00";
        String day5OfWeekEnd = day5OfWeek.toString() + " 23:59:59";
        String day6OfWeekStart = day6OfWeek.toString() + " 00:00:00";
        String day6OfWeekEnd = day6OfWeek.toString() + " 23:59:59";
        String day7OfWeekStart = day7OfWeek.toString() + " 00:00:00";
        String day7OfWeekEnd = day7OfWeek.toString() + " 23:59:59";

        Map<String, LocalDate> monthMap = DateUtils.getMonthByDate(paramDate);
        LocalDate firstDayOfMonth = monthMap.get(DateUtils.FIRST_DAY_OF_MONTH);
        LocalDate lastDayOfMonth = monthMap.get(DateUtils.LAST_DAY_OF_MONTH);
        String firstDayOfMonthStart = firstDayOfMonth.toString() + " 00:00:00";
        String lastDayOfMonthEnd = lastDayOfMonth.toString() + " 23:59:59";

        Map<String, LocalDate> weekOfMonthMap = DateUtils.getWeekByMonth(paramDate);
        LocalDate firstWeekStart = weekOfMonthMap.get(DateUtils.FIRST_WEEK_START);
        LocalDate firstWeekEnd = weekOfMonthMap.get(DateUtils.FIRST_WEEK_END);
        LocalDate secondWeekStart = weekOfMonthMap.get(DateUtils.SECOND_WEEK_START);
        LocalDate secondWeekEnd = weekOfMonthMap.get(DateUtils.SECOND_WEEK_END);
        LocalDate thirdWeekStart = weekOfMonthMap.get(DateUtils.THIRD_WEEK_START);
        LocalDate thirdWeekEnd = weekOfMonthMap.get(DateUtils.THIRD_WEEK_END);
        LocalDate fourthWeekStart = weekOfMonthMap.get(DateUtils.FOURTH_WEEK_START);
        LocalDate fourthWeekEnd = weekOfMonthMap.get(DateUtils.FOURTH_WEEK_END);
        LocalDate fifthWeekStart = weekOfMonthMap.get(DateUtils.FIFTH_WEEK_START);
        LocalDate fifthWeekEnd = weekOfMonthMap.get(DateUtils.FIFTH_WEEK_END);

        String firstWeekStartStr = firstWeekStart.toString() + " 00:00:00";
        String firstWeekEndStr = firstWeekEnd.toString() + " 23:59:59";
        String secondWeekStartStr = secondWeekStart + " 00:00:00";
        String secondWeekEndStr = secondWeekEnd.toString() + " 23:59:59";
        String thirdWeekStartStr = thirdWeekStart.toString() + " 00:00:00";
        String thirdWeekEndStr = thirdWeekEnd.toString() + " 23:59:59";
        String fourthWeekStartStr = fourthWeekStart.toString() + " 00:00:00";
        String fourthWeekEndStr = fourthWeekEnd.toString() + " 23:59:59";



        StringBuilder strBuilder = new StringBuilder("");
        strBuilder.append(" select max(shop_name) shopName,");
        strBuilder.append(" sum(case when happen_time >= '"+currentDayStart+"' and happen_time <= '"+currentDayEnd+"' then cnt_today else 0 end) as dayCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+day1OfWeekStart+"' and happen_time <= '"+day7OfWeekEnd+"' then cnt_today else 0 end) as weekCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+firstDayOfMonthStart+"' and happen_time <= '"+lastDayOfMonthEnd+"' then cnt_today else 0 end) as monthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+day1OfWeekStart+"' and happen_time <= '"+day1OfWeekEnd+"' then cnt_today else 0 end) as day1OfWeekCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+day2OfWeekStart+"' and happen_time <= '"+day2OfWeekEnd+"' then cnt_today else 0 end) as day2OfWeekCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+day3OfWeekStart+"' and happen_time <= '"+day3OfWeekEnd+"' then cnt_today else 0 end) as day3OfWeekCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+day4OfWeekStart+"' and happen_time <= '"+day4OfWeekEnd+"' then cnt_today else 0 end) as day4OfWeekCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+day5OfWeekStart+"' and happen_time <= '"+day5OfWeekEnd+"' then cnt_today else 0 end) as day5OfWeekCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+day6OfWeekStart+"' and happen_time <= '"+day6OfWeekEnd+"' then cnt_today else 0 end) as day6OfWeekCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+day7OfWeekStart+"' and happen_time <= '"+day7OfWeekEnd+"' then cnt_today else 0 end) as day7OfWeekCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+firstWeekStartStr+"' and happen_time <= '"+firstWeekEndStr+"' then cnt_today else 0 end) as week1OfMonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+secondWeekStartStr+"' and happen_time <= '"+secondWeekEndStr+"' then cnt_today else 0 end) as week2OfMonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+thirdWeekStartStr+"' and happen_time <= '"+thirdWeekEndStr+"' then cnt_today else 0 end) as week3OfMonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+fourthWeekStartStr+"' and happen_time <= '"+fourthWeekEndStr+"' then cnt_today else 0 end) as week4OfMonthCnt ");
        if(fifthWeekStart != null){
            String fifthWeekStartStr = fifthWeekStart.toString() + " 00:00:00";
            String fifthWeekEndStr = fifthWeekEnd.toString() + " 23:59:59";
            strBuilder.append(", sum(case when happen_time >= '"+fifthWeekStartStr+"' and happen_time <= '"+fifthWeekEndStr+"' then cnt_today else 0 end) as week5OfMonthCnt ");
        }

        strBuilder.append(" from t_sync_customer_data where shop_code = '"+shopCode+"'");

        List<Map<String,Object>> resultList = jdbcTemplate.queryForList(strBuilder.toString());
        return resultList.get(0);



    }


    @Override
    public Map<String, Object> getHistoryData(String paramDate, String shopCode) {

        String shopDate = paramDate;
        LocalDate oriDate = LocalDate.parse(shopDate);
        Map<String, LocalDate> historyDate = DateUtils.getHistoryByDate(shopDate);

        LocalDate history1Start = historyDate.get("history1Start");
        String history1StartStr = history1Start.toString() + " 00:00:00";
        LocalDate history1End = historyDate.get("history1End");
        String history1EndStr = history1End.toString() + " 23:59:59";
        LocalDate history1= oriDate.minusMonths(1);
        Map<String, LocalDate> weeksOfHistory1 = DateUtils.getWeekByMonth(history1.toString());
        LocalDate firstWeekStart1 = weeksOfHistory1.get(DateUtils.FIRST_WEEK_START);
        LocalDate firstWeekEnd1 = weeksOfHistory1.get(DateUtils.FIRST_WEEK_END);
        LocalDate secondWeekStart1 = weeksOfHistory1.get(DateUtils.SECOND_WEEK_START);
        LocalDate secondWeekEnd1 = weeksOfHistory1.get(DateUtils.SECOND_WEEK_END);
        LocalDate thirdWeekStart1 = weeksOfHistory1.get(DateUtils.THIRD_WEEK_START);
        LocalDate thirdWeekEnd1 = weeksOfHistory1.get(DateUtils.THIRD_WEEK_END);
        LocalDate fourthWeekStart1 = weeksOfHistory1.get(DateUtils.FOURTH_WEEK_START);
        LocalDate fourthWeekEnd1 = weeksOfHistory1.get(DateUtils.FOURTH_WEEK_END);
        LocalDate fifthWeekStart1 = weeksOfHistory1.get(DateUtils.FIFTH_WEEK_START);
        LocalDate fifthWeekEnd1 = weeksOfHistory1.get(DateUtils.FIFTH_WEEK_END);
        String firstWeekStart1Str = firstWeekStart1.toString() + " 00:00:00";
        String firstWeekEnd1Str = firstWeekEnd1.toString() + " 23:59:59";
        String secondWeekStart1Str = secondWeekStart1.toString() + " 00:00:00";
        String secondWeekEnd1Str = secondWeekEnd1.toString() + " 23:59:59";
        String thirdWeekStart1Str = thirdWeekStart1.toString() + " 00:00:00";
        String thirdWeekEnd1Str = thirdWeekEnd1.toString() + " 23:59:59";
        String fourthWeekStart1Str = fourthWeekStart1.toString() + " 00:00:00";
        String fourthWeekEnd1Str = fourthWeekEnd1.toString() + " 23:59:59";

        LocalDate history2Start = historyDate.get("history2Start");
        String history2StartStr = history2Start.toString() + " 00:00:00";
        LocalDate history2End = historyDate.get("history2End");
        String history2EndStr = history2End.toString() + " 23:59:59";
        LocalDate history2= oriDate.minusMonths(2);
        Map<String, LocalDate> weeksOfHistory2 = DateUtils.getWeekByMonth(history2.toString());
        LocalDate firstWeekStart2 = weeksOfHistory2.get(DateUtils.FIRST_WEEK_START);
        LocalDate firstWeekEnd2 = weeksOfHistory2.get(DateUtils.FIRST_WEEK_END);
        LocalDate secondWeekStart2 = weeksOfHistory2.get(DateUtils.SECOND_WEEK_START);
        LocalDate secondWeekEnd2 = weeksOfHistory2.get(DateUtils.SECOND_WEEK_END);
        LocalDate thirdWeekStart2 = weeksOfHistory2.get(DateUtils.THIRD_WEEK_START);
        LocalDate thirdWeekEnd2 = weeksOfHistory2.get(DateUtils.THIRD_WEEK_END);
        LocalDate fourthWeekStart2 = weeksOfHistory2.get(DateUtils.FOURTH_WEEK_START);
        LocalDate fourthWeekEnd2 = weeksOfHistory2.get(DateUtils.FOURTH_WEEK_END);
        LocalDate fifthWeekStart2 = weeksOfHistory2.get(DateUtils.FIFTH_WEEK_START);
        LocalDate fifthWeekEnd2 = weeksOfHistory2.get(DateUtils.FIFTH_WEEK_END);
        String firstWeekStart2Str = firstWeekStart2.toString() + " 00:00:00";
        String firstWeekEnd2Str = firstWeekEnd2.toString() + " 23:59:59";
        String secondWeekStart2Str = secondWeekStart2.toString() + " 00:00:00";
        String secondWeekEnd2Str = secondWeekEnd2.toString() + " 23:59:59";
        String thirdWeekStart2Str = thirdWeekStart2.toString() + " 00:00:00";
        String thirdWeekEnd2Str = thirdWeekEnd2.toString() + " 23:59:59";
        String fourthWeekStart2Str = fourthWeekStart2.toString() + " 00:00:00";
        String fourthWeekEnd2Str = fourthWeekEnd2.toString() + " 23:59:59";

        LocalDate history3Start = historyDate.get("history3Start");
        String history3StartStr = history3Start.toString() + " 00:00:00";
        LocalDate history3End = historyDate.get("history3End");
        String history3EndStr = history3End.toString() + " 23:59:59";
        LocalDate history3= oriDate.minusMonths(3);
        Map<String, LocalDate> weeksOfHistory3 = DateUtils.getWeekByMonth(history3.toString());
        LocalDate firstWeekStart3 = weeksOfHistory3.get(DateUtils.FIRST_WEEK_START);
        LocalDate firstWeekEnd3 = weeksOfHistory3.get(DateUtils.FIRST_WEEK_END);
        LocalDate secondWeekStart3 = weeksOfHistory3.get(DateUtils.SECOND_WEEK_START);
        LocalDate secondWeekEnd3 = weeksOfHistory3.get(DateUtils.SECOND_WEEK_END);
        LocalDate thirdWeekStart3 = weeksOfHistory3.get(DateUtils.THIRD_WEEK_START);
        LocalDate thirdWeekEnd3 = weeksOfHistory3.get(DateUtils.THIRD_WEEK_END);
        LocalDate fourthWeekStart3 = weeksOfHistory3.get(DateUtils.FOURTH_WEEK_START);
        LocalDate fourthWeekEnd3 = weeksOfHistory3.get(DateUtils.FOURTH_WEEK_END);
        LocalDate fifthWeekStart3 = weeksOfHistory3.get(DateUtils.FIFTH_WEEK_START);
        LocalDate fifthWeekEnd3 = weeksOfHistory3.get(DateUtils.FIFTH_WEEK_END);
        String firstWeekStart3Str = firstWeekStart3.toString() + " 00:00:00";
        String firstWeekEnd3Str = firstWeekEnd3.toString() + " 23:59:59";
        String secondWeekStart3Str = secondWeekStart3.toString() + " 00:00:00";
        String secondWeekEnd3Str = secondWeekEnd3.toString() + " 23:59:59";
        String thirdWeekStart3Str = thirdWeekStart3.toString() + " 00:00:00";
        String thirdWeekEnd3Str = thirdWeekEnd3.toString() + " 23:59:59";
        String fourthWeekStart3Str = fourthWeekStart3.toString() + " 00:00:00";
        String fourthWeekEnd3Str = fourthWeekEnd3.toString() + " 23:59:59";

        LocalDate history4Start = historyDate.get("history4Start");
        String history4StartStr = history4Start.toString() + " 00:00:00";
        LocalDate history4End = historyDate.get("history4End");
        String history4EndStr = history4End.toString() + " 23:59:59";
        LocalDate history4= oriDate.minusMonths(4);
        Map<String, LocalDate> weeksOfHistory4 = DateUtils.getWeekByMonth(history4.toString());
        LocalDate firstWeekStart4 = weeksOfHistory4.get(DateUtils.FIRST_WEEK_START);
        LocalDate firstWeekEnd4 = weeksOfHistory4.get(DateUtils.FIRST_WEEK_END);
        LocalDate secondWeekStart4 = weeksOfHistory4.get(DateUtils.SECOND_WEEK_START);
        LocalDate secondWeekEnd4 = weeksOfHistory4.get(DateUtils.SECOND_WEEK_END);
        LocalDate thirdWeekStart4 = weeksOfHistory4.get(DateUtils.THIRD_WEEK_START);
        LocalDate thirdWeekEnd4 = weeksOfHistory4.get(DateUtils.THIRD_WEEK_END);
        LocalDate fourthWeekStart4 = weeksOfHistory4.get(DateUtils.FOURTH_WEEK_START);
        LocalDate fourthWeekEnd4 = weeksOfHistory4.get(DateUtils.FOURTH_WEEK_END);
        LocalDate fifthWeekStart4 = weeksOfHistory4.get(DateUtils.FIFTH_WEEK_START);
        LocalDate fifthWeekEnd4 = weeksOfHistory4.get(DateUtils.FIFTH_WEEK_END);
        String firstWeekStart4Str = firstWeekStart4.toString() + " 00:00:00";
        String firstWeekEnd4Str = firstWeekEnd4.toString() + " 23:59:59";
        String secondWeekStart4Str = secondWeekStart4.toString() + " 00:00:00";
        String secondWeekEnd4Str = secondWeekEnd4.toString() + " 23:59:59";
        String thirdWeekStart4Str = thirdWeekStart4.toString() + " 00:00:00";
        String thirdWeekEnd4Str = thirdWeekEnd4.toString() + " 23:59:59";
        String fourthWeekStart4Str = fourthWeekStart4.toString() + " 00:00:00";
        String fourthWeekEnd4Str = fourthWeekEnd4.toString() + " 23:59:59";

        LocalDate history5Start = historyDate.get("history5Start");
        String history5StartStr = history5Start.toString() + " 00:00:00";
        LocalDate history5End = historyDate.get("history5End");
        String history5EndStr = history5End.toString() + " 23:59:59";
        LocalDate history5= oriDate.minusMonths(5);
        Map<String, LocalDate> weeksOfHistory5 = DateUtils.getWeekByMonth(history5.toString());
        LocalDate firstWeekStart5 = weeksOfHistory5.get(DateUtils.FIRST_WEEK_START);
        LocalDate firstWeekEnd5 = weeksOfHistory5.get(DateUtils.FIRST_WEEK_END);
        LocalDate secondWeekStart5 = weeksOfHistory5.get(DateUtils.SECOND_WEEK_START);
        LocalDate secondWeekEnd5 = weeksOfHistory5.get(DateUtils.SECOND_WEEK_END);
        LocalDate thirdWeekStart5 = weeksOfHistory5.get(DateUtils.THIRD_WEEK_START);
        LocalDate thirdWeekEnd5 = weeksOfHistory5.get(DateUtils.THIRD_WEEK_END);
        LocalDate fourthWeekStart5 = weeksOfHistory5.get(DateUtils.FOURTH_WEEK_START);
        LocalDate fourthWeekEnd5 = weeksOfHistory5.get(DateUtils.FOURTH_WEEK_END);
        LocalDate fifthWeekStart5 = weeksOfHistory5.get(DateUtils.FIFTH_WEEK_START);
        LocalDate fifthWeekEnd5 = weeksOfHistory5.get(DateUtils.FIFTH_WEEK_END);
        String firstWeekStart5Str = firstWeekStart5.toString() + " 00:00:00";
        String firstWeekEnd5Str = firstWeekEnd5.toString() + " 23:59:59";
        String secondWeekStart5Str = secondWeekStart5.toString() + " 00:00:00";
        String secondWeekEnd5Str = secondWeekEnd5.toString() + " 23:59:59";
        String thirdWeekStart5Str = thirdWeekStart5.toString() + " 00:00:00";
        String thirdWeekEnd5Str = thirdWeekEnd5.toString() + " 23:59:59";
        String fourthWeekStart5Str = fourthWeekStart5.toString() + " 00:00:00";
        String fourthWeekEnd5Str = fourthWeekEnd5.toString() + " 23:59:59";

        LocalDate history6Start = historyDate.get("history6Start");
        String history6StartStr = history6Start.toString() + " 00:00:00";
        LocalDate history6End = historyDate.get("history6End");
        String history6EndStr = history6End.toString() +  " 23:59:59";
        LocalDate history6= oriDate.minusMonths(6);
        Map<String, LocalDate> weeksOfHistory6 = DateUtils.getWeekByMonth(history6.toString());
        LocalDate firstWeekStart6 = weeksOfHistory6.get(DateUtils.FIRST_WEEK_START);
        LocalDate firstWeekEnd6 = weeksOfHistory6.get(DateUtils.FIRST_WEEK_END);
        LocalDate secondWeekStart6 = weeksOfHistory6.get(DateUtils.SECOND_WEEK_START);
        LocalDate secondWeekEnd6 = weeksOfHistory6.get(DateUtils.SECOND_WEEK_END);
        LocalDate thirdWeekStart6 = weeksOfHistory6.get(DateUtils.THIRD_WEEK_START);
        LocalDate thirdWeekEnd6 = weeksOfHistory6.get(DateUtils.THIRD_WEEK_END);
        LocalDate fourthWeekStart6 = weeksOfHistory6.get(DateUtils.FOURTH_WEEK_START);
        LocalDate fourthWeekEnd6 = weeksOfHistory6.get(DateUtils.FOURTH_WEEK_END);
        LocalDate fifthWeekStart6 = weeksOfHistory6.get(DateUtils.FIFTH_WEEK_START);
        LocalDate fifthWeekEnd6 = weeksOfHistory6.get(DateUtils.FIFTH_WEEK_END);
        String firstWeekStart6Str = firstWeekStart6.toString() + " 00:00:00";
        String firstWeekEnd6Str = firstWeekEnd6.toString() + " 23:59:59";
        String secondWeekStart6Str = secondWeekStart6.toString() + " 00:00:00";
        String secondWeekEnd6Str = secondWeekEnd6.toString() + " 23:59:59";
        String thirdWeekStart6Str = thirdWeekStart6.toString() + " 00:00:00";
        String thirdWeekEnd6Str = thirdWeekEnd6.toString() + " 23:59:59";
        String fourthWeekStart6Str = fourthWeekStart6.toString() + " 00:00:00";
        String fourthWeekEnd6Str = fourthWeekEnd6.toString() + " 23:59:59";

        StringBuilder strBuilder = new StringBuilder("");
        strBuilder.append(" select max(shop_name) shopName,");
        strBuilder.append(" sum(case when happen_time >= '"+history1StartStr+"' and happen_time <= '"+history1EndStr+"' then cnt_today else 0 end) as history1MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+firstWeekStart1Str+"' and happen_time <= '"+firstWeekEnd1Str+"' then cnt_today else 0 end) as week1OfHistory1MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+secondWeekStart1Str+"' and happen_time <= '"+secondWeekEnd1Str+"' then cnt_today else 0 end) as week2OfHistory1MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+thirdWeekStart1Str+"' and happen_time <= '"+thirdWeekEnd1Str+"' then cnt_today else 0 end) as week3OfHistory1MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+fourthWeekStart1Str+"' and happen_time <= '"+fourthWeekEnd1Str+"' then cnt_today else 0 end) as week4OfHistory1MonthCnt, ");
        if(fifthWeekStart1 != null){
            String fifthWeekStart1Str = fifthWeekStart1.toString() + " 00:00:00";
            String fifthWeekEnd1Str = fifthWeekEnd1.toString() + " 23:59:59";
            strBuilder.append(" sum(case when happen_time >= '"+fifthWeekStart1Str+"' and happen_time <= '"+fifthWeekEnd1Str+"' then cnt_today else 0 end) as week5OfHistory1MonthCnt, ");
        }
        strBuilder.append(" sum(case when happen_time >= '"+history2StartStr+"' and happen_time <= '"+history2EndStr+"' then cnt_today else 0 end) as history2MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+firstWeekStart2Str+"' and happen_time <= '"+firstWeekEnd2Str+"' then cnt_today else 0 end) as week1OfHistory2MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+secondWeekStart2Str+"' and happen_time <= '"+secondWeekEnd2Str+"' then cnt_today else 0 end) as week2OfHistory2MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+thirdWeekStart2Str+"' and happen_time <= '"+thirdWeekEnd2Str+"' then cnt_today else 0 end) as week3OfHistory2MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+fourthWeekStart2Str+"' and happen_time <= '"+fourthWeekEnd2Str+"' then cnt_today else 0 end) as week4OfHistory2MonthCnt, ");
        if(fifthWeekStart2 != null){
            String fifthWeekStart2Str = fifthWeekStart2.toString() + " 00:00:00";
            String fifthWeekEnd2Str = fifthWeekEnd2.toString() + " 23:59:59";
            strBuilder.append(" sum(case when happen_time >= '"+fifthWeekStart2Str+"' and happen_time <= '"+fifthWeekEnd2Str+"' then cnt_today else 0 end) as week5OfHistory2MonthCnt, ");
        }
        strBuilder.append(" sum(case when happen_time >= '"+history3StartStr+"' and happen_time <= '"+history3EndStr+"' then cnt_today else 0 end) as history3MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+firstWeekStart3Str+"' and happen_time <= '"+firstWeekEnd3Str+"' then cnt_today else 0 end) as week1OfHistory3MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+secondWeekStart3Str+"' and happen_time <= '"+secondWeekEnd3Str+"' then cnt_today else 0 end) as week2OfHistory3MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+thirdWeekStart3Str+"' and happen_time <= '"+thirdWeekEnd3Str+"' then cnt_today else 0 end) as week3OfHistory3MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+fourthWeekStart3Str+"' and happen_time <= '"+fourthWeekEnd3Str+"' then cnt_today else 0 end) as week4OfHistory3MonthCnt, ");
        if(fifthWeekStart3 != null){
            String fifthWeekStart3Str = fifthWeekStart3.toString() + " 00:00:00";
            String fifthWeekEnd3Str = fifthWeekEnd3.toString() + " 23:59:59";
            strBuilder.append(" sum(case when happen_time >= '"+fifthWeekStart3Str+"' and happen_time <= '"+fifthWeekEnd3Str+"' then cnt_today else 0 end) as week5OfHistory3MonthCnt, ");
        }
        strBuilder.append(" sum(case when happen_time >= '"+history4StartStr+"' and happen_time <= '"+history4EndStr+"' then cnt_today else 0 end) as history4MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+firstWeekStart4Str+"' and happen_time <= '"+firstWeekEnd4Str+"' then cnt_today else 0 end) as week1OfHistory4MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+secondWeekStart4Str+"' and happen_time <= '"+secondWeekEnd4Str+"' then cnt_today else 0 end) as week2OfHistory4MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+thirdWeekStart4Str+"' and happen_time <= '"+thirdWeekEnd4Str+"' then cnt_today else 0 end) as week3OfHistory4MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+fourthWeekStart4Str+"' and happen_time <= '"+fourthWeekEnd4Str+"' then cnt_today else 0 end) as week4OfHistory4MonthCnt, ");
        if(fifthWeekStart4 != null){
            String fifthWeekStart4Str = fifthWeekStart4.toString() + " 00:00:00";
            String fifthWeekEnd4Str = fifthWeekEnd4.toString() + " 23:59:59";
            strBuilder.append(" sum(case when happen_time >= '"+fifthWeekStart4Str+"' and happen_time <= '"+fifthWeekEnd4Str+"' then cnt_today else 0 end) as week5OfHistory4MonthCnt, ");
        }
        strBuilder.append(" sum(case when happen_time >= '"+history5StartStr+"' and happen_time <= '"+history5EndStr+"' then cnt_today else 0 end) as history5MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+firstWeekStart5Str+"' and happen_time <= '"+firstWeekEnd5Str+"' then cnt_today else 0 end) as week1OfHistory5MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+secondWeekStart5Str+"' and happen_time <= '"+secondWeekEnd5Str+"' then cnt_today else 0 end) as week2OfHistory5MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+thirdWeekStart5Str+"' and happen_time <= '"+thirdWeekEnd5Str+"' then cnt_today else 0 end) as week3OfHistory5MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+fourthWeekStart5Str+"' and happen_time <= '"+fourthWeekEnd5Str+"' then cnt_today else 0 end) as week4OfHistory5MonthCnt, ");
        if(fifthWeekStart5 != null){
            String fifthWeekStart5Str = fifthWeekStart5.toString() + " 00:00:00";
            String fifthWeekEnd5Str = fifthWeekEnd5.toString() + " 23:59:59";
            strBuilder.append(" sum(case when happen_time >= '"+fifthWeekStart5Str+"' and happen_time <= '"+fifthWeekEnd5Str+"' then cnt_today else 0 end) as week5OfHistory5MonthCnt, ");
        }
        strBuilder.append(" sum(case when happen_time >= '"+history6StartStr+"' and happen_time <= '"+history6EndStr+"' then cnt_today else 0 end) as history6MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+firstWeekStart6Str+"' and happen_time <= '"+firstWeekEnd6Str+"' then cnt_today else 0 end) as week1OfHistory6MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+secondWeekStart6Str+"' and happen_time <= '"+secondWeekEnd6Str+"' then cnt_today else 0 end) as week2OfHistory6MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+thirdWeekStart6Str+"' and happen_time <= '"+thirdWeekEnd6Str+"' then cnt_today else 0 end) as week3OfHistory6MonthCnt, ");
        strBuilder.append(" sum(case when happen_time >= '"+fourthWeekStart6Str+"' and happen_time <= '"+fourthWeekEnd6Str+"' then cnt_today else 0 end) as week4OfHistory6MonthCnt ");
        if(fifthWeekStart6 != null){
            String fifthWeekStart6Str = fifthWeekStart6.toString() + " 00:00:00";
            String fifthWeekEnd6Str = fifthWeekEnd6.toString() + " 23:59:59";
            strBuilder.append(", sum(case when happen_time >= '"+fifthWeekStart6Str+"' and happen_time <= '"+fifthWeekEnd6Str+"' then cnt_today else 0 end) as week5OfHistory6MonthCnt ");
        }

        strBuilder.append(" from t_sync_customer_data where shop_code = '"+shopCode+"'");

        List<Map<String,Object>> resultList = jdbcTemplate.queryForList(strBuilder.toString());
        return resultList.get(0);






    }


    *//**
     * 更新当天数据
     *//*
    @Override
    public void updateDayData(){
        String sonyCustomerId = "9236b103947f4f71b261bcf958286b2c";
        List<Object[]> customerList = (List<Object[]>) customerService.findShopsByCusId(sonyCustomerId);
        log.info(("索尼旗下总共 ：" + customerList.size() + "家店"));
        List<String> shopIdList = customerList.stream().map(objArray -> {
            return objArray[0].toString();
        }).collect(Collectors.toList());
        Map<String, String> shopIdCodeMap = customerList.stream().collect(Collectors.toMap(objArray1 -> {
                    return objArray1[0].toString();
                },
                objArray2 -> {
                    return objArray2[1].toString();
                }));

        // 1 查询所有门店当天的客流数据
        LocalDate now = LocalDate.now();
        Map<String, Object> conditionDay = Maps.newConcurrentMap();
        conditionDay.put("_startDate", now.toString());
        conditionDay.put("_endDate", now.toString());
        conditionDay.put("_shopIds",shopIdList);
        List<ShopDataDayEntity> shopDataDayEntityList = shopDayService.listAll(conditionDay);

        // 2 遍历数据，没有插入，有就更新
        for (ShopDataDayEntity shopDataDayEntity : shopDataDayEntityList) {

            SyncCustomerDataEntity condition = new SyncCustomerDataEntity();
            condition.getProperties().put("shopCode", shopIdCodeMap.get(shopDataDayEntity.getShopId()));
            condition.getProperties().put("happenTime_gt", now + " 00:00:00");
            condition.getProperties().put("happenTime_le", now + " 23:59:59");
            List<SyncCustomerDataEntity> list = listAll(condition);

            SyncCustomerDataEntity entity = null;
            if (CollectionUtils.isEmpty(list)) {//插入
                entity = new SyncCustomerDataEntity();
                String id = SystemTool.uuid();
                entity.setId(id);
                entity.setCode(id);
                entity.setShopCode(shopIdCodeMap.get(shopDataDayEntity.getShopId()));
                entity.setHappenTime(shopDataDayEntity.getHappenTime());
                entity.setShopId(shopDataDayEntity.getShopId());
                entity.setShopName(redisBean.get(shopDataDayEntity.getShopId()));
                entity.setCntToday(String.valueOf(shopDataDayEntity.getDxin()));
                entity.setSource("2");
                entity.setTransFlag("0");
            } else {//更新
                entity = list.get(0);
                entity.setCntToday(String.valueOf(shopDataDayEntity.getDxin()));
            }
            syncCustomerDataRepository.save(entity);


        }

    }

    @Override
    public IRepository<SyncCustomerDataEntity> getRepository() {
        return syncCustomerDataRepository;
    }*/

}
