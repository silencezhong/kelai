package com.kelai.service.impl;

import com.kelai.common.IRepository;
import com.kelai.domain.CustomerEntity;
import com.kelai.domain.ReportEntity;
import com.kelai.repository.IAccountRepository;
import com.kelai.repository.ICustomerRepository;
import com.kelai.repository.IReportRepository;
import com.kelai.service.ICustomerService;
import com.kelai.service.IReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.List;

/**
 * 报表服务实现类
 * Created by luffy on 2017/07/30.
 */
@Service("reportService")
public class DefaultReportService implements IReportService {

    private final IReportRepository reportRepository;

    private final JdbcTemplate jdbcTemplate;

    private final PlatformTransactionManager transactionManager;

    @Autowired
    public DefaultReportService(IReportRepository reportRepository, IAccountRepository accountRepository, JdbcTemplate jdbcTemplate, PlatformTransactionManager transactionManager) {
        this.reportRepository = reportRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.transactionManager = transactionManager;
    }

    @Override
    public IRepository<ReportEntity> getRepository() {
        return reportRepository;
    }






}
