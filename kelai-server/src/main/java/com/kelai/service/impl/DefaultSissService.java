package com.kelai.service.impl;

import com.kelai.callback.IdCallback;
import com.kelai.common.IRepository;
import com.kelai.domain.*;
import com.kelai.repository.*;
import com.kelai.service.ISissService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * 思讯数据服务
 * Created by Luffy on 2017/06/28.
 */
@SuppressWarnings("unchecked")
@Service("sissService")
public class DefaultSissService implements ISissService {

    private final static Logger logger = LoggerFactory.getLogger(DefaultSissService.class);

    private final ISissRepository sissRepository;

    private final JdbcTemplate jdbcTemplate;

    @Resource
    private IdCallback idCallback;

    @Autowired
    public DefaultSissService(ISissRepository sissRepository,
                              JdbcTemplate jdbcTemplate ) {
        this.sissRepository = sissRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public IRepository<SissDataEntity> getRepository() {
        return sissRepository;
    }

}
