package com.kelai.service.impl;

import com.kelai.common.InfoPage;
import com.kelai.common.PageInfo;
import com.kelai.common.constants.WebAppConstants;
import com.kelai.domain.CustomerEntity;
import com.kelai.domain.DeviceDataWeekEntity;
import com.kelai.domain.DeviceEntity;
import com.kelai.repository.ICustomerRepository;
import com.kelai.repository.IDeviceRepository;
import com.kelai.service.IDeviceWeekService;
import com.kelai.utils.BreezeeUtils;
import com.kelai.utils.ContextUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.olap4j.*;
import org.olap4j.metadata.Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 设备周统计服务
 * Created by Luffy on 2017/04/21.
 */
@Service("deviceWeekService")
public class DefaultDeviceWeekService implements IDeviceWeekService {
    private final static Logger logger = LoggerFactory.getLogger(DefaultDeviceWeekService.class);
    private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final ICustomerRepository customerRepository;

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DefaultDeviceWeekService(ICustomerRepository customerRepository, JdbcTemplate jdbcTemplate) {
        this.customerRepository = customerRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<DeviceDataWeekEntity> listAll(Map condition) {

        PageInfo pageInfo = new PageInfo(condition);
        if (condition.get("_sort") != null) {
            pageInfo.setSort((Sort) condition.get("_sort"));
        } else {
            pageInfo.setSort(new Sort(Sort.Direction.ASC, "rowNum","id"));
        }

        List<DeviceDataWeekEntity> l = new ArrayList<>();
        try {
            ICustomerRepository customerRepository = ContextUtil.getBean("customerRepository", ICustomerRepository.class);
            IDeviceRepository deviceRepository = ContextUtil.getBean("deviceRepository", IDeviceRepository.class);
            System.out.println("----");
            Class.forName("org.olap4j.driver.xmla.XmlaOlap4jDriver");
            OlapConnection con = (OlapConnection) DriverManager.getConnection("jdbc:xmla:Server=http://"+WebAppConstants.BI_LINK_IP+"/MDKL/msmdpump.dll;Catalog="+ WebAppConstants.BI_LINK_NAME);
            OlapWrapper wrapper = (OlapWrapper) con;
            OlapConnection olapConnection = wrapper.unwrap(OlapConnection.class);
            OlapStatement stmt = olapConnection.createStatement();

            StringBuffer strPageBuffer = new StringBuffer();
            strPageBuffer.append(" ");
            strPageBuffer.append(" SELECT NON EMPTY { [Measures].[进], [Measures].[出] } ON 0, ");
            strPageBuffer.append(" {  [设备].[设备ID].[设备ID].ALLMEMBERS *  ");
            strPageBuffer.append(" [时间].[年].[年].ALLMEMBERS *  ");
            strPageBuffer.append(" [时间].[周].[周].ALLMEMBERS } on 1 ");
            strPageBuffer.append(" FROM ( SELECT ( { [时间].[年月日小时].[日].&["+condition.get("_startDate")+"T00:00:00]:[时间].[年月日小时].[日].&["+condition.get("_endDate")+"T00:00:00]}, {");

            if(condition.get("_deviceIds") != null && CollectionUtils.isNotEmpty((List<String>)condition.get("_deviceIds"))){
                ((List<String>)condition.get("_deviceIds")).stream().forEach( deviceId -> {
                    strPageBuffer.append("  [设备].[设备ID].[设备ID].&["+deviceId+"] , ");
                });
                strPageBuffer.append("  [设备].[设备ID].[设备ID].&[-1] ");
            }else{
                strPageBuffer.append(" [设备].[设备ID].[设备ID].&["+condition.get("device_id_obj_ae")+"]  ");
            }

            strPageBuffer.append(" } ) ON COLUMNS ");
            strPageBuffer.append(" FROM [客流]) ");

            CellSet cellSet = stmt.executeOlapQuery(strPageBuffer.toString());

            for (Position rowPos : cellSet.getAxes().get(1)) {
                String s = "";
                for (Member member : rowPos.getMembers()) {
                    s += member.getCaption() + "*";
                }
                for (Position colPos : cellSet.getAxes().get(0)) {
                    Cell cell = cellSet.getCell(colPos, rowPos);
                    s += cell.getFormattedValue() + "*";
                }
                String[] ss = s.split("\\*");
                String id = ss[0] + "-" + ss[1] + ss[2];
                DeviceDataWeekEntity sdde = new DeviceDataWeekEntity();
                sdde.setId(id);
                sdde.setCode(id);
                DeviceEntity de = deviceRepository.findOne(ss[0]);
                if (de == null)
                    continue;
                CustomerEntity ce = de.getCustomer();
                if (ce == null) {
                    System.out.println("device : "+de.getCode() +" no customer");
                    continue;
                }
                sdde.setCustomer(ce);
                sdde.setCustomerId(ce.getParentId());
                sdde.setDevice(de);

                //将某周的周一转化为时间
//                Calendar cal = Calendar.getInstance();
//                cal.set(Calendar.YEAR, Integer.valueOf(ss[1]));
//                cal.set(Calendar.WEEK_OF_YEAR, Integer.valueOf(ss[2]));
//                cal.set(Calendar.DAY_OF_WEEK, 2); // 1表示周日，2表示周一，7表示周六
//                Date date = cal.getTime();

                try{
                    sdde.setHappenTime(ss[2]);
                }catch(Exception e){
                    logger.info(e.getMessage()+" "+ ss[2]);
                    sdde.setHappenTime("0");
                }

                sdde.setYear(Integer.parseInt(ss[1]));
                if(ss.length < 4){
                    sdde.setDxin(0);
                    sdde.setDxout(0);
                }else{
                    sdde.setDxin(Integer.parseInt(ss[3]));
                    sdde.setDxout(Integer.parseInt(ss[4]));
                }
                l.add(sdde);
            }

        } catch (Exception e1) {
            e1.printStackTrace();
        }

        return l;
    }

    @Override
    public InfoPage<DeviceDataWeekEntity> pageAll(Map condition) {
        PageInfo pageInfo = new PageInfo(condition);
        if (condition.get("_sort") != null) {
            pageInfo.setSort((Sort) condition.get("_sort"));
        } else {
            pageInfo.setSort(new Sort(Sort.Direction.ASC, "rowNum","id"));
        }

        List<DeviceDataWeekEntity> l = new ArrayList<>();
        String cnt = "0";
        try {
            ICustomerRepository customerRepository = ContextUtil.getBean("customerRepository", ICustomerRepository.class);
            IDeviceRepository deviceRepository = ContextUtil.getBean("deviceRepository", IDeviceRepository.class);
            System.out.println("----");
            Class.forName("org.olap4j.driver.xmla.XmlaOlap4jDriver");
            OlapConnection con = (OlapConnection) DriverManager.getConnection("jdbc:xmla:Server=http://"+WebAppConstants.BI_LINK_IP+"/MDKL/msmdpump.dll;Catalog="+WebAppConstants.BI_LINK_NAME);
            OlapWrapper wrapper = (OlapWrapper) con;
            OlapConnection olapConnection = wrapper.unwrap(OlapConnection.class);
            OlapStatement stmt = olapConnection.createStatement();

            Integer pageStart = Integer.parseInt(condition.get("pageNumber").toString())*
                    Integer.parseInt(condition.get("pageSize").toString());

            StringBuffer strPageBuffer = new StringBuffer();
            strPageBuffer.append(" ");
            strPageBuffer.append(" SELECT NON EMPTY { [Measures].[进], [Measures].[出] } ON 0, ");
            strPageBuffer.append(" { subset ");
            strPageBuffer.append(" (( ");
            strPageBuffer.append(" [设备].[设备ID].[设备ID].ALLMEMBERS *  ");
            strPageBuffer.append(" [时间].[年].[年].ALLMEMBERS *  ");
            strPageBuffer.append(" [时间].[周].[周].ALLMEMBERS)  ");
            strPageBuffer.append(" ,"+pageStart+","+condition.get("pageSize")+")} on 1 ");
            strPageBuffer.append(" FROM ( SELECT ( { [时间].[年月日小时].[日].&["+condition.get("_startDate")+"T00:00:00]:[时间].[年月日小时].[日].&["+condition.get("_endDate")+"T00:00:00]}, {");

            if(condition.get("_deviceIds") != null && CollectionUtils.isNotEmpty((List<String>)condition.get("_deviceIds"))){
                ((List<String>)condition.get("_deviceIds")).stream().forEach( deviceId -> {
                    strPageBuffer.append("  [设备].[设备ID].[设备ID].&["+deviceId+"] , ");
                });
                strPageBuffer.append("  [设备].[设备ID].[设备ID].&[-1] ");
            }else{
                strPageBuffer.append(" [设备].[设备ID].[设备ID].&["+condition.get("device_id_obj_ae")+"]  ");
            }

            strPageBuffer.append(" } ) ON COLUMNS  ");
            strPageBuffer.append(" FROM [客流]) ");

            CellSet cellSet = stmt.executeOlapQuery(strPageBuffer.toString());

            for (Position rowPos : cellSet.getAxes().get(1)) {
                String s = "";
                for (Member member : rowPos.getMembers()) {
                    s += member.getCaption() + "*";
                }
                for (Position colPos : cellSet.getAxes().get(0)) {
                    Cell cell = cellSet.getCell(colPos, rowPos);
                    s += cell.getFormattedValue() + "*";
                }
                String[] ss = s.split("\\*");
                String id = ss[0] + "-" + ss[1] + ss[2];
                DeviceDataWeekEntity sdde = new DeviceDataWeekEntity();
                sdde.setId(id);
                sdde.setCode(id);
                DeviceEntity de = deviceRepository.findOne(ss[0]);
                if (de == null)
                    continue;
                CustomerEntity ce = de.getCustomer();
                if (ce == null) {
                    System.out.println("device : "+de.getCode() +" no customer");
                    continue;
                }
                sdde.setCustomer(ce);
                sdde.setCustomerId(ce.getParentId());
                sdde.setDevice(de);

                //将某周的周一转化为时间
//                Calendar cal = Calendar.getInstance();
//                cal.set(Calendar.YEAR, Integer.valueOf(ss[1]));
//                cal.set(Calendar.WEEK_OF_YEAR, Integer.valueOf(ss[2]));
//                cal.set(Calendar.DAY_OF_WEEK, 2); // 1表示周日，2表示周一，7表示周六
//                Date date = cal.getTime();
                try{
                    sdde.setHappenTime(ss[2]);
                }catch(Exception e){
                    logger.info(e.getMessage()+" "+ ss[2]);
                    sdde.setHappenTime("0");
                }

                sdde.setYear(Integer.parseInt(ss[1]));
                if(ss.length < 4){
                    sdde.setDxin(0);
                    sdde.setDxout(0);
                }else{
                    sdde.setDxin(Integer.parseInt(ss[3]));
                    sdde.setDxout(Integer.parseInt(ss[4]));
                }
                l.add(sdde);
            }


//        System.out.println(" 客户       门店       设备名称 设备位置 进店数 出店数 时间");
//        l.forEach( shopDayData -> {
//            System.out.println( shopDayData.getCustomerName() + " "+shopDayData.getShopName() + "     --    --      " + shopDayData.getDxin() + " "+shopDayData.getDxout() + " " +shopDayData.getHappenTime());
//        });

        StringBuffer strCntBuffer = new StringBuffer();
        strCntBuffer.append(" ");
        strCntBuffer.append(" WITH ");
        strCntBuffer.append(" SET [xx] as   ({[设备].[设备ID].[设备ID].ALLMEMBERS * [时间].[年].[年].ALLMEMBERS * [时间].[周].[周].ALLMEMBERS * [Measures].[进]}) ");
        strCntBuffer.append(" MEMBER [Measures].[X] AS count([xx]) ");
        strCntBuffer.append(" SELECT Measures.X ON 0 ");
        strCntBuffer.append(" FROM (  SELECT ( { [时间].[年月日小时].[日].&["+condition.get("_startDate")+"T00:00:00]:[时间].[年月日小时].[日].&["+condition.get("_endDate")+"T00:00:00]}, {");

            if(condition.get("_deviceIds") != null && CollectionUtils.isNotEmpty((List<String>)condition.get("_deviceIds"))){
                ((List<String>)condition.get("_deviceIds")).stream().forEach( deviceId -> {
                    strCntBuffer.append("  [设备].[设备ID].[设备ID].&["+deviceId+"] , ");
                });
                strCntBuffer.append("  [设备].[设备ID].[设备ID].&[-1] ");
            }else{
                strCntBuffer.append(" [设备].[设备ID].[设备ID].&["+condition.get("device_id_obj_ae")+"]  ");
            }

        strCntBuffer.append(" } ) ON COLUMNS ");
        strCntBuffer.append(" FROM [客流]) ");

        CellSet cellSetCnt = stmt.executeOlapQuery(strCntBuffer.toString());

        for (Position rowPos : cellSetCnt.getAxes().get(0)) {
            Cell cell = cellSetCnt.getCell(rowPos);
            cnt = cell.getFormattedValue();
        }
    } catch (Exception e1) {
        e1.printStackTrace();
    }

        return new InfoPage<DeviceDataWeekEntity>(l,new Long(cnt));
    }
}
