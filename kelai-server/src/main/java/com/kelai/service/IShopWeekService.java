package com.kelai.service;


import com.kelai.domain.ShopDataWeekEntity;


/**
 * 店铺周统计服务
 * Created by Luffy on 2017/04/21.
 */
public interface IShopWeekService extends IBiService<ShopDataWeekEntity> {


}
