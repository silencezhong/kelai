package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.domain.EmptyEntity;

import java.util.Map;

/**
 * 统计服务
 * Created by Silence on 2016/11/27.
 */
public interface IStaticService extends IServiceLayer<EmptyEntity> {

    /**
     * 首页的计数
     * @param accountId
     * @return
     */
    EmptyEntity getCountStaticInfo(String accountId);

    /**
     * 获得某账户下的所有设备
     * @param accountId
     * @return
     */
    EmptyEntity getAllDevice(String accountId);


    EmptyEntity queryChartsData(Map<String,Object> map);

    /**
     * 首页的下方环比实现
     * @param shopId
     * @return
     */
    EmptyEntity getDataStaticInfo(String shopId);


    EmptyEntity getDeviceWeekData(String shopId);

    //不同粒度汇总进出人数
    //实时
    EmptyEntity sumForRealTime(Map<String,Object> map);
    //日 设备
    EmptyEntity sumForDeviceDay(Map<String,Object> map);
    //日 店铺
    EmptyEntity sumForShopDay(Map<String,Object> map);
    //小时 设备
    EmptyEntity sumForDeviceHour(Map<String,Object> map);
    //小时 店铺
    EmptyEntity sumForShopHour(Map<String,Object> map);
    //周 设备
    EmptyEntity sumForDeviceWeek(Map<String,Object> map);
    //周 店铺
    EmptyEntity sumForShopWeek(Map<String,Object> map);
    //月 设备
    EmptyEntity sumForDeviceMonth(Map<String,Object> map);
    //月 店铺
    EmptyEntity sumForShopMonth(Map<String,Object> map);

}
