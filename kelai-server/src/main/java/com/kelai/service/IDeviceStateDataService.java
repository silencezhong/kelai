package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.domain.DeviceStateDataEntity;


/**
 * 设备状态数据服务
 * Created by luffy on 2017/08/17.
 */
public interface IDeviceStateDataService extends IServiceLayer<DeviceStateDataEntity> {


}
