package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.domain.CustomerEntity;
import com.kelai.domain.EmptyEntity;
import com.kelai.domain.MarketActivityEntity;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;

/**
 * 客户目录店铺服务
 * Created by luffy on 2017/05/17.
 */
public interface ICustomerService extends IServiceLayer<CustomerEntity> {

    int updateFollowStatus(String followed, String id);

    String findVendorByCusId(String cusId);

    List<Object[]> findShopsByShop(String customerId);
    List<Object[]> findShopsByCustomer(String customerId);
    List<Object[]> findShopsByVendor(String vendorId);
    List<Object[]> findShopsByAdmin();

    List<String> findShopIdsByCustomer(String customerId);
    List<String> findShopIdsByVendor(String vendorId);
    List<String> findShopIdsByAdmin();

    List<Object[]> findCsagsByCustomer(String customerAcctId);
    List<Object[]> findCsagsByVendor(String vendorAcctId);
    List<Object[]> findCsagsByShop(String vendorAcctId);
    List<Object[]> findCsagsByAdmin();
    List<Object[]> findCustomersByCustomer(String customerAcctId);
    List<Object[]> findCustomersByVendor(String vendorAcctId);
    List<Object[]> findCustomersByAdmin();

    List<Object[]> findShopsByCusId(String customerId);
    List<Object[]> findFollowedShopsByCusId(String customerId);
    List<Object[]> findShopParentByCusId(String customerId);
    List<Object[]> findCustomersByVendorId(String vendorId);


}
