package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.domain.AccountEntity;
import com.kelai.domain.CustomerEntity;
import com.kelai.domain.RoleEntity;
import com.kelai.domain.VendorEntity;

import java.util.Set;

/**
 * 账号服务
 * Created by Silence on 2016/11/26.
 */
public interface IAccountService extends IServiceLayer<AccountEntity> {

    void saveAccounts(VendorEntity ve);

    void saveAccounts(CustomerEntity ce);

    void saveAccounts(RoleEntity role);

    Set<CustomerEntity> findMyShop(String accountId);

    void saveAccountDirect(AccountEntity ae);

    void saveRefRoleAcn(String roleId, String acnId);
}
