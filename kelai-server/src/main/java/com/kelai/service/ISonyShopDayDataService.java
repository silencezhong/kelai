package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.domain.SonyShopDayDataEntity;

import java.util.Map;

/**
 * sony数据接口服务
 * Created by LN on 2018/06/10.
 */
public interface ISonyShopDayDataService extends IServiceLayer<SonyShopDayDataEntity>{

    /**
     * 初始化索尼客流数据
     */
    void initSonyShopDayData();

    /**
     * 查找当前客流数据(包含当天，本周，本月)
     * @param paramDate
     * @param shopCode
     * @return
     */
    Map<String, Object> queryCurrentData(String paramDate, String shopCode);

    /**
     * 查找历史客流数据（目前支持6个月）
     * @param paramDate
     * @param shopCode
     * @return
     */
    Map<String, Object> queryHistoryData(String paramDate, String shopCode);

    /**
     * 每小时更新最新数据
     */
    void updateDayData();

}
