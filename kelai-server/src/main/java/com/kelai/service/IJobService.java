package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.task.quartz.QuartzConfigEntity;


/**
 * 定时任务服务
 * Created by Luffy on 2017/04/15.
 */
public interface IJobService extends IServiceLayer<QuartzConfigEntity> {


}
