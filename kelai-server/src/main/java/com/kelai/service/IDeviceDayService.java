package com.kelai.service;


import com.kelai.domain.DeviceDataDayEntity;


/**
 * 设备日统计服务
 * Created by Luffy on 2017/04/21.
 */
public interface IDeviceDayService extends IBiService<DeviceDataDayEntity> {


}
