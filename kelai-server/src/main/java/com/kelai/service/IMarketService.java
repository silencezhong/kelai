package com.kelai.service;

import com.kelai.common.IServiceLayer;
import com.kelai.domain.CustomerEntity;
import com.kelai.domain.MarketActivityEntity;
import java.util.List;

/**
 * 市场活动服务
 * Created by luffy on 2017/05/16.
 */
public interface IMarketService extends IServiceLayer<MarketActivityEntity> {

    void saveMarkets(String pkId , List<CustomerEntity> ces);

}
