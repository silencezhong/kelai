package com.kelai.repository;

import org.springframework.stereotype.Repository;
import com.kelai.common.IRepository;
import com.kelai.domain.UserEntity;

/**
 * 会员持久化服务
 * Created by Silence on 2016/10/16.
 */
@Repository("userRepository")
public interface IUserRepository extends IRepository<UserEntity> {
}
