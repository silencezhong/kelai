package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.SaleDataEntity;
import org.springframework.stereotype.Repository;

/**
 * 销售数据
 * Created by Silence on 2016/11/26.
 */
@Repository("saleDataRepository")
public interface ISaleDataRepository extends IRepository<SaleDataEntity> {
}
