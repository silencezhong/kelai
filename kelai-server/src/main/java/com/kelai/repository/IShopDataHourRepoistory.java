package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.ShopDataHourEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Silence on 2017/3/20.
 */
@Repository("shopDataHourRepoistory")
public interface IShopDataHourRepoistory extends IRepository<ShopDataHourEntity> {
}
