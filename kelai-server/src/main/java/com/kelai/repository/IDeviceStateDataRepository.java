package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.DeviceStateDataEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by ZhouBing on 2017/7/28.
 *
 */
@Repository("deviceStateDataRepository")
public interface IDeviceStateDataRepository extends IRepository<DeviceStateDataEntity>{

}
