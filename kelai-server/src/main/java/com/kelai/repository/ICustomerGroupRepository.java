package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.CustomerGroupEntity;
import org.springframework.stereotype.Repository;

/**
 * 店租持久化服务
 * Created by Silence on 2016/11/22.
 */
@Repository("customerGroupRepository")
public interface ICustomerGroupRepository extends IRepository<CustomerGroupEntity> {
}
