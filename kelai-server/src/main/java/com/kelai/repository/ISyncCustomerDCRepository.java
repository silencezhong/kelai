package com.kelai.repository;


import com.kelai.common.IRepository;
import com.kelai.domain.SyncCustomerDCEntity;
import org.springframework.stereotype.Repository;

/**
 * 同步客流数据的持久化服务
 * Created by liangnan on 2018/06/14.
 */
@Repository("syncCustomerDCRepository")
public interface ISyncCustomerDCRepository extends IRepository<SyncCustomerDCEntity> {
}
