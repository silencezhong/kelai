package com.kelai.repository;

import org.springframework.stereotype.Repository;
import com.kelai.common.IRepository;
import com.kelai.domain.RepairCallEntity;

/**
 * 报修实体持久化
 * Created by Silence on 2016/11/1.
 */
@Repository("repairCallRepository")
public interface IRepairCallRepository extends IRepository<RepairCallEntity> {
}
