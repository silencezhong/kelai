package com.kelai.repository;

import org.springframework.stereotype.Repository;
import com.kelai.common.IRepository;
import com.kelai.domain.AccountEntity;

/**
 * 账号的持久化服务
 * Created by Silence on 2016/10/24.
 */
@Repository("accountRepository")
public interface IAccountRepository extends IRepository<AccountEntity> {
}
