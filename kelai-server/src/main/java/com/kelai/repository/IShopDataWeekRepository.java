package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.ShopDataWeekEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Luffy on 2017/4/12.
 */
@Repository("shopDataWeekRepository")
public interface IShopDataWeekRepository extends IRepository<ShopDataWeekEntity> {
}
