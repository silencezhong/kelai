package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.OrganizationEntity;
import org.springframework.stereotype.Repository;

/**
 * 组织持久化服务
 * Created by Silence on 2016/10/25.
 */
@Repository("organizationRepository")
public interface IOrganizationRepository extends IRepository<OrganizationEntity> {
}
