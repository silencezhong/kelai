package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.CustomerEntity;
import com.kelai.domain.DeviceEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 设备持久化服务
 * Created by Silence on 2016/11/19.
 */
@Repository("deviceRepository")
public interface IDeviceRepository extends IRepository<DeviceEntity> {

    @Query("select d from DeviceEntity d where d.customer.id in (:shopIds)")
    List<DeviceEntity> findDevicesByShops(@Param(value = "shopIds") List<String> shopIds);




}
