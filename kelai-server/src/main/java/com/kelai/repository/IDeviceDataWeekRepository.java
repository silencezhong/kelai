package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.DeviceDataWeekEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Luffy on 2017/4/12.
 */
@Repository("deviceDataWeekRepository")
public interface IDeviceDataWeekRepository extends IRepository<DeviceDataWeekEntity> {
}
