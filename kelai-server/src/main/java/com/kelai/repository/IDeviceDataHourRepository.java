package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.DeviceDataHourEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Silence on 2017/3/21.
 */
@Repository("deviceDataHourRepository")
public interface IDeviceDataHourRepository extends IRepository<DeviceDataHourEntity> {
}
