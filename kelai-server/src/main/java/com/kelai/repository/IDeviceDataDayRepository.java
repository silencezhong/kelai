package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.DeviceDataDayEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Silence on 2017/3/21.
 */
@Repository("deviceDataDayRepository")
public interface IDeviceDataDayRepository extends IRepository<DeviceDataDayEntity> {
}
