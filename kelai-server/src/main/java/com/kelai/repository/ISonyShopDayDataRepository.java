package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.SonyShopDayDataEntity;
import org.springframework.stereotype.Repository;

/**
 * sony客流数据的持久化服务
 * Created by liangnan on 2018/06/06.
 */
@Repository("sonyShopDayDataRepository")
public interface ISonyShopDayDataRepository extends IRepository<SonyShopDayDataEntity> {
}
