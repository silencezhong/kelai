package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.ShopDataDayEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Silence on 2017/3/19.
 */
@Repository("shopDataDayRepository")
public interface IShopDataDayRepository extends IRepository<ShopDataDayEntity> {
}
