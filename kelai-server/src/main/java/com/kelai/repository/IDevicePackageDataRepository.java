package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.DevicePackageDataEntity;
import org.springframework.stereotype.Repository;



@Repository("devicePackageDataRepository")
public interface IDevicePackageDataRepository extends IRepository<DevicePackageDataEntity> {

}
