package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.ReportEntity;
import org.springframework.stereotype.Repository;

/**
 * 报表持久化服务
 * Created by Luffy on 2017/07/30.
 */
@Repository("reportRepository")
public interface IReportRepository extends IRepository<ReportEntity> {

}
