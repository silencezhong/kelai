package com.kelai.repository;

import org.springframework.stereotype.Repository;
import com.kelai.common.IRepository;
import com.kelai.domain.ModelItemEntity;

/**
 * 模型条目持久化
 * Created by Silence on 2016/11/1.
 */
@Repository("modelItemRepository")
public interface IModelItemRepository extends IRepository<ModelItemEntity> {
}
