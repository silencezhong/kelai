package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.DeviceDataMonthEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Luffy on 2017/4/12.
 */
@Repository("deviceDataMonthRepository")
public interface IDeviceDataMonthRepository extends IRepository<DeviceDataMonthEntity> {
}
