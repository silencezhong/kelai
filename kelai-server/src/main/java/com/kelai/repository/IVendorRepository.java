package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.VendorEntity;
import org.springframework.stereotype.Repository;

/**
 * 渠道商持久化服务
 * Created by Silence on 2016/11/25.
 */
@Repository("vendorRepository")
public interface IVendorRepository extends IRepository<VendorEntity> {
}
