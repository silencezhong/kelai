package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.DeviceDayDataEntity;
import org.springframework.stereotype.Repository;

/**
 * 设备每日数据
 * Created by Silence on 2016/11/26.
 */
@Repository("deviceDayDataRepository")
public interface IDeviceDayDataRepository extends IRepository<DeviceDayDataEntity> {
}
