package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.VendorCustomerEntity;
import org.springframework.stereotype.Repository;

/**
 * 渠道商与客户的关系
 * Created by Silence on 2016/11/25.
 */
@Repository("vendorCustomerRepository")
public interface IVendorCustomerRepository extends IRepository<VendorCustomerEntity> {

}
