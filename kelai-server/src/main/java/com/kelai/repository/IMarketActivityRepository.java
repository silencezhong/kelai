package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.MarketActivityEntity;
import org.springframework.stereotype.Repository;

/**
 * 市场活动持久化服务
 * Created by Silence on 2016/11/26.
 */
@Repository("marketActivityRepository")
public interface IMarketActivityRepository extends IRepository<MarketActivityEntity> {
}
