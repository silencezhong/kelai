package com.kelai.repository;

import org.springframework.stereotype.Repository;
import com.kelai.common.IRepository;
import com.kelai.domain.RoleEntity;

/**
 * 角色持久化服务
 * Created by Silence on 2016/10/25.
 */
@Repository("roleRepository")
public interface IRoleRepository extends IRepository<RoleEntity> {
}
