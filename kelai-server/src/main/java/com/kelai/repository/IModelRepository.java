package com.kelai.repository;

import org.springframework.stereotype.Repository;
import com.kelai.common.IRepository;
import com.kelai.domain.ModelEntity;

/**
 * 模型持久化服务
 * Created by Silence on 2016/11/1.
 */
@Repository("modelRepository")
public interface IModelRepository extends IRepository<ModelEntity> {
}
