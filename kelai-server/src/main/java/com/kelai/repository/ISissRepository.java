package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.DeviceDataEntity;
import com.kelai.domain.SissDataEntity;
import org.springframework.stereotype.Repository;

/**
 * 提供给思讯的数据源
 * Created by Luffy on 2017/06/28.
 */
@Repository("sissRepository")
public interface ISissRepository extends IRepository<SissDataEntity> {
}
