package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.DeviceEntity;
import com.kelai.domain.ErrorMsgEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 异常消息持久化服务
 * Created by Luffy on 2017/07/21.
 */
@Repository("errorMsgRepository")
public interface IErrorMsgRepository extends IRepository<ErrorMsgEntity> {

}
