package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.DeviceDataEntity;
import org.springframework.stereotype.Repository;

/**
 * 设备上传数据
 * Created by Silence on 2016/11/26.
 */
@Repository("deviceDataRepository")
public interface IDeviceDataRepository extends IRepository<DeviceDataEntity> {
}
