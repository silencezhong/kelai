package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.domain.ShopDataMonthEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Silence on 2017/3/20.
 */
@Repository("shopDataMonthRepository")
public interface IShopDataMonthRepository extends IRepository<ShopDataMonthEntity> {
}
