package com.kelai.repository;

import com.kelai.common.IRepository;
import com.kelai.task.quartz.QuartzConfigEntity;
import org.springframework.stereotype.Repository;

/**
 * 定时任务的持久化服务
 * Created by Silence on 2016/10/24.
 */
@Repository("jobRepository")
public interface IJobRepository extends IRepository<QuartzConfigEntity> {
}
