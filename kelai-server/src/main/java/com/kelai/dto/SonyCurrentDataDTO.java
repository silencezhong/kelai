package com.kelai.dto;

import com.google.common.collect.Lists;
import lombok.*;
import java.io.Serializable;
import java.util.List;

/**
 * 提供给索尼的客流数据--当前部分
 * Created by LN on 2018/06/11.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SonyCurrentDataDTO implements Serializable {

    /**
     * 门店编码
     */
    String shopCode;
    /**
     * 门店名称
     */
    String shopName;
    /**
     * 客流发生日期(YYYY-MM-DD)
     */
    String shopDate;
    /**
     * 当日客流数(以进客流为准)
     */
    String dayCnt;
    /**
     * 本周客流数
     */
    String weekCnt;
    /**
     * 本周每一天客流数（两种结构，一种动态List ，另一种周一到周日写死，目前暂时用的第一种）
     */
//    ThisWeekStatic dayOfWeek = new ThisWeek();
    List<ThisWeekDynamic> dayOfWeek = Lists.newArrayList();
    /**
     * 本月的年份
     */

    String monthOfYear;
    /**
     * 本月的月份
     */
    String monthOfMonth;
    /**
     * 本月客流数
     */
    String monthCnt;
    /**
     * 本月每周客流数
     */
    ThisMonth weekOfMonth;

    /**
     * 一周内每天数据结构
     */
    public static class ThisWeekDynamic{
        String   dayOfWeek;//时间
        String   dayOfWeekCnt;//客流数

        public ThisWeekDynamic() {
        }

        public ThisWeekDynamic(String dayOfWeek, String dayOfWeekCnt) {
            this.dayOfWeek = dayOfWeek;
            this.dayOfWeekCnt = dayOfWeekCnt;
        }

        public String getDayOfWeek() {
            return dayOfWeek;
        }

        public void setDayOfWeek(String dayOfWeek) {
            this.dayOfWeek = dayOfWeek;
        }

        public String getDayOfWeekCnt() {
            return dayOfWeekCnt;
        }

        public void setDayOfWeekCnt(String dayOfWeekCnt) {
            this.dayOfWeekCnt = dayOfWeekCnt;
        }

        @Override
        public String toString() {
            return "ThisWeek2{" +
                    "dayOfWeek='" + dayOfWeek + '\'' +
                    ", dayOfWeekCnt='" + dayOfWeekCnt + '\'' +
                    '}';
        }
    }

    /**
     * 一周内每天数据结构
     */
    public class ThisWeekStatic {
        String   day1OfWeek;//时间
        String   day1OfWeekCnt;//客流数
        String   day2OfWeek;
        String   day2OfWeekCnt;
        String   day3OfWeek;
        String   day3OfWeekCnt;
        String   day4OfWeek;
        String   day4OfWeekCnt;
        String   day5OfWeek;
        String   day5OfWeekCnt;
        String   day6OfWeek;
        String   day6OfWeekCnt;
        String   day7OfWeek;
        String   day7OfWeekCnt;

        public String getDay1OfWeek() {
            return day1OfWeek;
        }

        public void setDay1OfWeek(String day1OfWeek) {
            this.day1OfWeek = day1OfWeek;
        }

        public String getDay1OfWeekCnt() {
            return day1OfWeekCnt;
        }

        public void setDay1OfWeekCnt(String day1OfWeekCnt) {
            this.day1OfWeekCnt = day1OfWeekCnt;
        }

        public String getDay2OfWeek() {
            return day2OfWeek;
        }

        public void setDay2OfWeek(String day2OfWeek) {
            this.day2OfWeek = day2OfWeek;
        }

        public String getDay2OfWeekCnt() {
            return day2OfWeekCnt;
        }

        public void setDay2OfWeekCnt(String day2OfWeekCnt) {
            this.day2OfWeekCnt = day2OfWeekCnt;
        }

        public String getDay3OfWeek() {
            return day3OfWeek;
        }

        public void setDay3OfWeek(String day3OfWeek) {
            this.day3OfWeek = day3OfWeek;
        }

        public String getDay3OfWeekCnt() {
            return day3OfWeekCnt;
        }

        public void setDay3OfWeekCnt(String day3OfWeekCnt) {
            this.day3OfWeekCnt = day3OfWeekCnt;
        }

        public String getDay4OfWeek() {
            return day4OfWeek;
        }

        public void setDay4OfWeek(String day4OfWeek) {
            this.day4OfWeek = day4OfWeek;
        }

        public String getDay4OfWeekCnt() {
            return day4OfWeekCnt;
        }

        public void setDay4OfWeekCnt(String day4OfWeekCnt) {
            this.day4OfWeekCnt = day4OfWeekCnt;
        }

        public String getDay5OfWeek() {
            return day5OfWeek;
        }

        public void setDay5OfWeek(String day5OfWeek) {
            this.day5OfWeek = day5OfWeek;
        }

        public String getDay5OfWeekCnt() {
            return day5OfWeekCnt;
        }

        public void setDay5OfWeekCnt(String day5OfWeekCnt) {
            this.day5OfWeekCnt = day5OfWeekCnt;
        }

        public String getDay6OfWeek() {
            return day6OfWeek;
        }

        public void setDay6OfWeek(String day6OfWeek) {
            this.day6OfWeek = day6OfWeek;
        }

        public String getDay6OfWeekCnt() {
            return day6OfWeekCnt;
        }

        public void setDay6OfWeekCnt(String day6OfWeekCnt) {
            this.day6OfWeekCnt = day6OfWeekCnt;
        }

        public String getDay7OfWeek() {
            return day7OfWeek;
        }

        public void setDay7OfWeek(String day7OfWeek) {
            this.day7OfWeek = day7OfWeek;
        }

        public String getDay7OfWeekCnt() {
            return day7OfWeekCnt;
        }

        public void setDay7OfWeekCnt(String day7OfWeekCnt) {
            this.day7OfWeekCnt = day7OfWeekCnt;
        }
    }

    /**
     * 本月每周客流数
     */
   public class ThisMonth{

        //本月第一周客流数
        String week1OfMonthCnt;
        //本月第二周客流数
        String week2OfMonthCnt;
        //本月第三周客流数
        String week3OfMonthCnt;
        //本月第四周客流数
        String week4OfMonthCnt;
        //本月第五周客流数
        String week5OfMonthCnt;

        public String getWeek1OfMonthCnt() {
            return week1OfMonthCnt;
        }

        public void setWeek1OfMonthCnt(String week1OfMonthCnt) {
            this.week1OfMonthCnt = week1OfMonthCnt;
        }

        public String getWeek2OfMonthCnt() {
            return week2OfMonthCnt;
        }

        public void setWeek2OfMonthCnt(String week2OfMonthCnt) {
            this.week2OfMonthCnt = week2OfMonthCnt;
        }

        public String getWeek3OfMonthCnt() {
            return week3OfMonthCnt;
        }

        public void setWeek3OfMonthCnt(String week3OfMonthCnt) {
            this.week3OfMonthCnt = week3OfMonthCnt;
        }

        public String getWeek4OfMonthCnt() {
            return week4OfMonthCnt;
        }

        public void setWeek4OfMonthCnt(String week4OfMonthCnt) {
            this.week4OfMonthCnt = week4OfMonthCnt;
        }

        public String getWeek5OfMonthCnt() {
            return week5OfMonthCnt;
        }

        public void setWeek5OfMonthCnt(String week5OfMonthCnt) {
            this.week5OfMonthCnt = week5OfMonthCnt;
        }

    }


}




