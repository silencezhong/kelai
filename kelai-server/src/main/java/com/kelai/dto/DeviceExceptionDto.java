package com.kelai.dto;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.io.Serializable;

/**
 * Created by ZhouBing on 2017/7/30.
 * 按设备统计异常信息
 */
@SuppressWarnings("serial")
@ExcelTarget("deviceExceptionExcel")
public class DeviceExceptionDto implements Serializable{

    @Excel(name="门店类别" , width=15)
    String shopType;
    @Excel(name="门店代码" , width=20)
    String shopCode;
    @Excel(name="门店名称")
    String shopName;
    @Excel(name="地址")
    String shopAddress;
    @Excel(name="联系人")
    String contactName;
    @Excel(name="联系电话")
    String contactPhone;
    @Excel(name="设备编码")
    String deviceCode;
    @Excel(name="异常率")
    String exceptionRate;
    @Excel(name="当前设备状态")
    String state;
    @Excel(name="备注")
    String memo;

    public DeviceExceptionDto() {
    }

    public String getMemo() {
        return memo;
    }

    public String getShopType() {
        return shopType;
    }

    public String getShopCode() {
        return shopCode;
    }

    public String getShopName() {
        return shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public String getExceptionRate() {
        return exceptionRate;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }


    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public void setExceptionRate(String exceptionRate) {
        this.exceptionRate = exceptionRate;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}
