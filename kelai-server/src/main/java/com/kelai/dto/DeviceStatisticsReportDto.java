package com.kelai.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * Created by luffyluo on 2017/8/15.
 */
public class DeviceStatisticsReportDto {

    /**
     * 总数
     */
    Long total;
    /**
     * 明细
     */
    List<Map<String,LocalDateTime>> itemList;

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<Map<String, LocalDateTime>> getItemList() {
        return itemList;
    }

    public void setItemList(List<Map<String, LocalDateTime>> itemList) {
        this.itemList = itemList;
    }
}
