package com.kelai.dto;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.io.Serializable;

/**
 * 门店管理excel格式
 * Created by Luffy on 2017/04/25.
 */
@SuppressWarnings("serial")
@ExcelTarget("shopManagerExcel")
public class ShopManagerExcelDto implements Serializable {

    @Excel(name="门店名称")
    String name;
    @Excel(name="门店编码")
    String code;
    @Excel(name="上级")
    String parentName;
    @Excel(name="省份")
    String province;
    @Excel(name="城市")
    String city;
    @Excel(name="营业时间")
    String openTime;
    @Excel(name="保修截止时间")
    String guaranteeDeadLine;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getGuaranteeDeadLine() {
        return guaranteeDeadLine;
    }

    public void setGuaranteeDeadLine(String guaranteeDeadLine) {
        this.guaranteeDeadLine = guaranteeDeadLine;
    }
}
