package com.kelai.dto;


import com.google.common.collect.Lists;
import lombok.*;
import java.io.Serializable;
import java.util.List;

/**
 * 提供给索尼的客流数据--历史部分
 * Created by LN on 2018/06/11.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SonyHistoryDataDTO implements Serializable {

    /**
     * 历史客流数据
      */
    List<historyMonth> historyData = Lists.newArrayList();

    /**
     * 历史某月数据
     */
    public static class historyMonth {
        String shopCode;//门店编码
        String shopName;// 门店名称
        String historyOfYear;//历史年份
        String historyOfMonth;//历史月份
        String historyMonthCnt;//历史月份客流数
        List<historyWeek> weekOfHistoryMonth = Lists.newArrayList();//历史某月中的每周数据

        public historyMonth() {
        }

        public historyMonth(String shopCode, String shopName, String historyOfYear, String historyOfMonth, String historyMonthCnt, List<historyWeek> weekOfHistoryMonth) {
            this.shopCode = shopCode;
            this.shopName = shopName;
            this.historyOfYear = historyOfYear;
            this.historyOfMonth = historyOfMonth;
            this.historyMonthCnt = historyMonthCnt;
            this.weekOfHistoryMonth = weekOfHistoryMonth;
        }

        @Override
        public String toString() {
            return "historyMonth{" +
                    "shopCode='" + shopCode + '\'' +
                    ", shopName='" + shopName + '\'' +
                    ", historyOfYear='" + historyOfYear + '\'' +
                    ", historyOfMonth='" + historyOfMonth + '\'' +
                    ", historyMonthCnt='" + historyMonthCnt + '\'' +
                    ", weekOfHistoryMonth=" + weekOfHistoryMonth +
                    '}';
        }

        public String getShopCode() {
            return shopCode;
        }

        public void setShopCode(String shopCode) {
            this.shopCode = shopCode;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public String getHistoryOfYear() {
            return historyOfYear;
        }

        public void setHistoryOfYear(String historyOfYear) {
            this.historyOfYear = historyOfYear;
        }

        public String getHistoryOfMonth() {
            return historyOfMonth;
        }

        public void setHistoryOfMonth(String historyOfMonth) {
            this.historyOfMonth = historyOfMonth;
        }

        public String getHistoryMonthCnt() {
            return historyMonthCnt;
        }

        public void setHistoryMonthCnt(String historyMonthCnt) {
            this.historyMonthCnt = historyMonthCnt;
        }

        public List<historyWeek> getWeekOfHistoryMonth() {
            return weekOfHistoryMonth;
        }

        public void setWeekOfHistoryMonth(List<historyWeek> weekOfHistoryMonth) {
            this.weekOfHistoryMonth = weekOfHistoryMonth;
        }
    }

    /**
     * 历史某月中每周客流数
     */
    public static class historyWeek {

        String weekOfHistoryMonth;//周数
        String weekOfHistoryMonthCnt;//历史月份周客流数
        Long weekNO;//第几周

        public historyWeek(String weekOfHistoryMonth, String weekOfHistoryMonthCnt, Long weekNO) {
            this.weekOfHistoryMonth = weekOfHistoryMonth;
            this.weekOfHistoryMonthCnt = weekOfHistoryMonthCnt;
            this.weekNO = weekNO;
        }

        @Override
        public String toString() {
            return "historyWeek{" +
                    "weekOfHistoryMonth='" + weekOfHistoryMonth + '\'' +
                    ", weekOfHistoryMonthCnt='" + weekOfHistoryMonthCnt + '\'' +
                    ", weekNO=" + weekNO +
                    '}';
        }

        public Long getWeekNO() {
            return weekNO;
        }

        public void setWeekNO(Long weekNO) {
            this.weekNO = weekNO;
        }

        public String getWeekOfHistoryMonth() {
            return weekOfHistoryMonth;
        }

        public void setWeekOfHistoryMonth(String weekOfHistoryMonth) {
            this.weekOfHistoryMonth = weekOfHistoryMonth;
        }

        public String getWeekOfHistoryMonthCnt() {
            return weekOfHistoryMonthCnt;
        }

        public void setWeekOfHistoryMonthCnt(String weekOfHistoryMonthCnt) {
            this.weekOfHistoryMonthCnt = weekOfHistoryMonthCnt;
        }

    }

}
