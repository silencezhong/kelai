package com.kelai.dto;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.io.Serializable;

/**
 * 按设备统计excel格式
 * Created by Luffy on 2017/04/26.
 */
@SuppressWarnings("serial")
@ExcelTarget("deviceExcel")
public class DeviceExcelDto implements Serializable {

    @Excel(name="客户")
    String customerName;
    @Excel(name="门店")
    String shopName;
    @Excel(name="设备名称")
    String deviceName;
    @Excel(name="设备位置")
    String devicePosition;
    @Excel(name="时间")
    String happenTime;
    @Excel(name="进店数",type=10)
    String dxin;
    @Excel(name="出店数",type=10)
    String dxout;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDevicePosition() {
        return devicePosition;
    }

    public void setDevicePosition(String devicePosition) {
        this.devicePosition = devicePosition;
    }

    public String getHappenTime() {
        return happenTime;
    }

    public void setHappenTime(String happenTime) {
        this.happenTime = happenTime;
    }

    public String getDxin() {
        return dxin;
    }

    public void setDxin(String dxin) {
        this.dxin = dxin;
    }

    public String getDxout() {
        return dxout;
    }

    public void setDxout(String dxout) {
        this.dxout = dxout;
    }
}
