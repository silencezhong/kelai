package com.kelai.dto;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.io.Serializable;

/**
 * 周报excel格式
 * Created by Luffy on 2017/07/30.
 */
@SuppressWarnings("serial")
@ExcelTarget("shopDataExcel")
public class ShopDataExcelDto implements Serializable {

    @Excel(name="门店类别" , width=15)
    String shopType;
    @Excel(name="门店代码" , width=20)
    String shopCode;
    @Excel(name="门店名称")
    String shopName;
    @Excel(name="地址")
    String shopAddress;
    @Excel(name="保修截止日期")
    String guaranteeDeadLine;
    @Excel(name="联系人")
    String contactName;
    @Excel(name="联系电话")
    String contactPhone;
    @Excel(name="设备编码")
    String deviceCode;
    @Excel(name="设备状态")
    String deviceStatus;
    @Excel(name="异常率")
    String exceptionRate;
    @Excel(name="客流量",type=10)
    String customerFlow;
    @Excel(name="备注")
    String memo;



    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getShopName() {
        return shopName;
    }

    public String getExceptionRate() {
        return exceptionRate;
    }

    public String getGuaranteeDeadLine() {
        return guaranteeDeadLine;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getMemo() {
        return memo;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public String getCustomerFlow() {
        return customerFlow;
    }

    public void setCustomerFlow(String customerFlow) {
        this.customerFlow = customerFlow;
    }

    public void setExceptionRate(String exceptionRate) {
        this.exceptionRate = exceptionRate;
    }

    public void setGuaranteeDeadLine(String guaranteeDeadLine) {
        this.guaranteeDeadLine = guaranteeDeadLine;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}
