package com.kelai.dto;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;
import java.io.Serializable;

/**
 * 设备管理excel格式
 * Created by Luffy on 2017/07/11.
 */
@SuppressWarnings("serial")
@ExcelTarget("deviceManagerExcel")
public class DeviceManagerExcelDto implements Serializable {

    String id;
    @Excel(name="省份")
    String province;
    @Excel(name="城市")
    String city;
    @Excel(name="设备编码")
    String code;
    @Excel(name="门店名称")
    String customerName;
    @Excel(name="网络")
    String statusName;
    @Excel(name="对焦")
    String focus;
    @Excel(name="电池电量")
    String voltage;
    @Excel(name="最后数据上传时间")
    String lastReceiveTime;
    @Excel(name="当天总进",type=10)
    String dxin;
    @Excel(name="当天总出",type=10)
    String dxout;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getFocus() {
        return focus;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getLastReceiveTime() {
        return lastReceiveTime;
    }

    public void setLastReceiveTime(String lastReceiveTime) {
        this.lastReceiveTime = lastReceiveTime;
    }

    public String getDxin() {
        return dxin;
    }

    public void setDxin(String dxin) {
        this.dxin = dxin;
    }

    public String getDxout() {
        return dxout;
    }

    public void setDxout(String dxout) {
        this.dxout = dxout;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
