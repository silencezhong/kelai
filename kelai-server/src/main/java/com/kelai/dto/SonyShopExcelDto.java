package com.kelai.dto;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.io.Serializable;

/**
 * 按门店统计excel格式
 * Created by Luffy on 2017/04/25.
 */
@SuppressWarnings("serial")
@ExcelTarget("shopExcel")
public class SonyShopExcelDto implements Serializable {

    @Excel(name="客户" , width=15)
    String customerName;
    @Excel(name="门店" , width=20)
    String shopName;
    @Excel(name="时间")
    String happenTime;
    @Excel(name="进店数",type=10)
    String dxin;



    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getHappenTime() {
        return happenTime;
    }

    public void setHappenTime(String happenTime) {
        this.happenTime = happenTime;
    }

    public String getDxin() {
        return dxin;
    }

    public void setDxin(String dxin) {
        this.dxin = dxin;
    }
}
