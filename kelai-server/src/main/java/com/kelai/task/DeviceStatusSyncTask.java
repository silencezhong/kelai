package com.kelai.task;

import com.kelai.domain.DevicePackageDataEntity;
import com.kelai.jms.listenner.adapter.RedisQueueMessageHandleAdapter;
import com.kelai.jms.message.queue.DeviceDataPacketQueueMessage;
import com.kelai.service.ICommonService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 索尼中国老设备的状态同步 TODO 现在已经抛弃，不使用了
 * Created by Silence on 2017/3/29.
 */
//@Component
public class DeviceStatusSyncTask {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(DeviceStatusSyncTask.class);

    private final static String SQL = "select" +
            "    tci.id ," +
            "    tgr.low," +
            "    tgr.Last_ScanTime," +
            "    tgr.focus," +
            "    tgr.ischarge," +
            "    tgr.lastcharge," +
            "    tgr.lastbat," +
            "    tgr.islink" +
            "    from [shopcusrec].[dbo].t_condition_info tci,[shopcusrec].[dbo].[t_gprs_register] tgr" +
            "    where tci.module_no=tgr.module_no";

    private ICommonService deviceService;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier(value = "DeviceDataPacketQueueMessageHandleAdapter")
    private RedisQueueMessageHandleAdapter<DeviceDataPacketQueueMessage, Object> deviceDataPacketQueueMessageHandleAdapter;

    /*@Scheduled(cron = "0 *//*10 * * * ?")
    public void execute() {
        if (deviceService == null)
            deviceService = new DefaultCommonService("deviceRepository");
        jdbcTemplate.queryForList(SQL).forEach(a -> {
            DeviceEntity de = (DeviceEntity) deviceService.findByCode(a.get("id").toString());
            if (de != null) {
                if (a.get("islink") == null || !a.get("islink").toString().equals("1")) {
                    de.setStatus(0);
                } else {
                    de.setStatus(2);
                }
                if (a.get("lastbat") != null)
                    de.setVoltage(Integer.parseInt(a.get("lastbat").toString()));
                de.setFocus(Integer.parseInt(a.get("focus").toString()));
                if (a.get("Last_ScanTime") != null) {
                    try {
                        de.setLastReceiveTime(BreezeeUtils.DATE_FORMAT_LONG.parse(a.get("Last_ScanTime").toString()).getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                deviceService.saveInfo(de);
            }
        });
    }*/

    @Scheduled(cron = "0 37 23 * * ?")
    public void execute() {

        LOGGER.info("~~~~~~~~~测试redis异步消息程序开始~~~~~~~~~~~~~~~~");
        int num = 999;
        for(int i = 0 ; i < num ; i++){
            LOGGER.info("~~~~~~~~~发送第"+i+"条消息~~~~~~~~~~~~~~~~");
            DevicePackageDataEntity packData = new DevicePackageDataEntity();
            packData.setRemark("test_redis");
            DeviceDataPacketQueueMessage msg = new DeviceDataPacketQueueMessage(packData);
            deviceDataPacketQueueMessageHandleAdapter.sendMessage(msg);
            try{
                Thread.currentThread().sleep(100);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        LOGGER.info("~~~~~~~~~测试redis异步消息程序结束~~~~~~~~~~~~~~~~");

    }

}
