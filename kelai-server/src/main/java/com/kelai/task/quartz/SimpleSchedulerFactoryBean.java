package com.kelai.task.quartz;

import org.quartz.spi.JobFactory;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

public class SimpleSchedulerFactoryBean extends SchedulerFactoryBean {
    public SimpleSchedulerFactoryBean(JobFactory jobFactory) {
        super();
        this.setJobFactory(jobFactory);
    }
}
