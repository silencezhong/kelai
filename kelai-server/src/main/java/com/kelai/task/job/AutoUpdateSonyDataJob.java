package com.kelai.task.job;


import com.kelai.service.ISonyShopDayDataService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.scheduling.quartz.QuartzJobBean;

import javax.annotation.Resource;


@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class AutoUpdateSonyDataJob extends QuartzJobBean {

    @Resource
    private ISonyShopDayDataService sonyShopDayDataService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        Long start = System.currentTimeMillis();
        sonyShopDayDataService.updateDayData();
        Long end = System.currentTimeMillis();
        System.out.println("刷新当天最新客流数共耗时："+ (end-start)/1000 + "秒");


    }
}