package com.kelai.task.job;


import com.kelai.service.IDeviceService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.scheduling.quartz.QuartzJobBean;
import javax.annotation.Resource;


@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class AbnormalDeviceSendMsgJob extends QuartzJobBean {

    @Resource
    private IDeviceService deviceService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        deviceService.sendAbnormalMsg();

    }
}