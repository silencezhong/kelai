package com.kelai.task.job;

import com.kelai.service.IDeviceDataService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.scheduling.quartz.QuartzJobBean;

import javax.annotation.Resource;

@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class CheckDeviceStatusJob extends QuartzJobBean {

    @Resource
    private IDeviceDataService deviceDataService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

//        deviceDataService.checkNewDeviceStatus();

    }
}