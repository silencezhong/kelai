package com.kelai.task.job;

import com.kelai.service.ISonyShopDayDataService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 初始化索尼客流数据，3分钟一家门店，初始数据完毕后不再执行该job
 */
@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class InitSonyCustomerDataJob extends QuartzJobBean {

    @Autowired
    ISonyShopDayDataService sonyShopDayDataService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Long start = System.currentTimeMillis();
        sonyShopDayDataService.initSonyShopDayData();
        Long end = System.currentTimeMillis();
        System.out.println("初始化某门店客流量共耗时："+ (end-start)/1000 + "秒");
    }
}