package com.kelai.task.job;


import com.kelai.domain.CustomerEntity;
import com.kelai.service.ICustomerService;
import com.kelai.service.IDeviceDataService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.scheduling.quartz.QuartzJobBean;
import javax.annotation.Resource;
import java.util.List;


@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class StatisticsSissJob extends QuartzJobBean {

    @Resource
    private IDeviceDataService deviceDataService;
    @Resource
    private ICustomerService customerService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        CustomerEntity customerCondition = new CustomerEntity();
        customerCondition.getProperties().put("category","3");
//        customerCondition.getProperties().put("code_not_equal","R0101");
        List<CustomerEntity> customerList = customerService.listAll(customerCondition);

        for(CustomerEntity customer : customerList){
            deviceDataService.statisticsSiss(customer.getId(),customer.getCode());
//            deviceDataService.initDataForSsis(customer.getId(),customer.getCode());
        }

    }
}