package com.kelai.jms;

import com.kelai.jms.message.queue.DeviceDataPacketQueueMessage;

/**
 * redis消息通道
 * Created by Luffy on 2017/08/17.
 */
public enum RedisMessageChannelEnum {

    /**
     * 设备数据包消息队列
     */
    DEVICE_DATA_PACKET_QUEUE_WEBSERVICE("DEVICE_DATA_PACKET_QUEUE_WEBSERVICE_ONLINE",
                                         DeviceDataPacketQueueMessage.class);

    private String channel;

    private Class<? extends RedisMessage> type;

    private RedisMessageChannelEnum(String channel, Class<? extends RedisMessage> type) {
        this.channel = channel;
        this.type = type;
    }

    public String channel() {
        return this.channel;
    }

    public Class<? extends RedisMessage> type() {
        return this.type;
    }
}
