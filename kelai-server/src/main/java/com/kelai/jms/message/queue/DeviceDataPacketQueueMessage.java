package com.kelai.jms.message.queue;

import com.kelai.jms.message.RedisQueueMessage;
import com.kelai.domain.DevicePackageDataEntity;

/**
 * 设备数据包消息
 * Created by Luffy on 2017/08/17.
 */
public class DeviceDataPacketQueueMessage extends RedisQueueMessage {

    private static final long serialVersionUID = -5388541954542505709L;

    private DevicePackageDataEntity deviceDataPacket;


    public DeviceDataPacketQueueMessage() {
    }

    public DeviceDataPacketQueueMessage(DevicePackageDataEntity deviceDataPacket) {
        this.deviceDataPacket = deviceDataPacket;
    }

    public DevicePackageDataEntity getDeviceDataPacket() {
        return deviceDataPacket;
    }

    public void setDeviceDataPacket(DevicePackageDataEntity deviceDataPacket) {
        this.deviceDataPacket = deviceDataPacket;
    }
}
