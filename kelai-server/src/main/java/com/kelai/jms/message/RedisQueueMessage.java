package com.kelai.jms.message;

import com.kelai.jms.RedisMessage;

/**
 * redis队列消息
 * Created by Luffy on 2017/08/17.
 */
public abstract class RedisQueueMessage extends RedisMessage {

    private static final long serialVersionUID = 1471425126957043149L;

    protected int execCount = 0;

    public int getExecCount() {
        return execCount;
    }

    public void setExecCount(int execCount) {
        this.execCount = execCount;
    }

}
