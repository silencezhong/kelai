package com.kelai.jms.handel;

import com.kelai.jms.RedisMessageChannelEnum;
import com.kelai.jms.message.RedisQueueMessage;

/**
 * 队列消息异步执行基类
 * Created by Luffy on 2017/08/17.
 */
public abstract class RedisQueueMessageDelegateHandle<REQ extends RedisQueueMessage, RES>
        extends RedisMessageDelegateHandle<REQ, RES> {
    /**
     * 重写
     */
    @Override
    public void handleMessage(REQ req) {
        req.setExecCount(req.getExecCount() + 1);
        super.handleMessage(req);
    }

    protected RedisMessageChannelEnum channelEnum;

    public RedisQueueMessageDelegateHandle(RedisMessageChannelEnum channelEnum) {
        this.channelEnum = channelEnum;
    }

    public RedisMessageChannelEnum getChannelEnum() {
        return channelEnum;
    }

    public void setChannelEnum(RedisMessageChannelEnum channelEnum) {
        this.channelEnum = channelEnum;
    }

}
