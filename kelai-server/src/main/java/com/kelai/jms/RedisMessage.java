package com.kelai.jms;

import java.io.Serializable;
import java.util.Date;

/**
 * redis消息模板基类
 * Created by Luffy on 2017/08/17.
 */
public abstract class RedisMessage implements Serializable {

    private static final long serialVersionUID = -8184477759019044728L;

    public RedisMessage() {
        this.id = "sony" + "-" + System.nanoTime();
        this.createDate = new Date();
    }

    // key
    protected String id;
    // 创建时间
    protected Date createDate;
    // 处理时间
    protected Date processTime;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Date processTime) {
        this.processTime = processTime;
    }

}
