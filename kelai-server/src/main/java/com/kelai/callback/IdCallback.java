package com.kelai.callback;

import javafx.util.Callback;
import org.springframework.util.StringUtils;
import com.kelai.domain.AbstractEntity;
import com.kelai.utils.SystemTool;

import java.util.Date;

/**
 * 实体主键生成方法
 * Created by Silence on 2016/11/1.
 */
public class IdCallback implements Callback<AbstractEntity, Object> {

    @Override
    public Object call(AbstractEntity object) {
        if (StringUtils.isEmpty(object.getId())) {
            object.setId(SystemTool.uuid());
            object.setCreatedDate(new Date());
        }
        object.setModifiedDate(new Date());
        return null;
    }
}
