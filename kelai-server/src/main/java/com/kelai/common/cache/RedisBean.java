package com.kelai.common.cache;

import com.google.common.collect.Lists;
import com.kelai.domain.CustomerEntity;
import com.kelai.service.ICustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@Component("redisBean")
public class RedisBean {
    private final static Logger logger = LoggerFactory.getLogger(RedisBean.class);

    @Resource
    private ICustomerService customerService;
    @Resource
    private RedisTemplate<String,String> redisTemplate;
    @Resource
    private StringRedisTemplate stringRedisTemplate ;


    public Boolean initCustomers() {
        logger.info("~~系统初始化，将店铺信息载入缓存 开始~~");
        boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
            public Boolean doInRedis(RedisConnection connection)
                    throws DataAccessException {
                RedisSerializer<String> serializer = redisTemplate.getStringSerializer();
                CustomerEntity conditon = new CustomerEntity();
                conditon.getProperties().put("category","3");
                List<CustomerEntity> customerList = customerService.listAll(conditon);
                customerList.stream().forEach(customer -> {
                    byte[] shopkey  = serializer.serialize(customer.getId());
                    byte[] shopname = serializer.serialize(customer.getName());
                    connection.setNX(shopkey, shopname);
                    byte[] customerkey = serializer.serialize(customer.getId()+"-parent");
                    byte[] customername = serializer.serialize(customer.getTopCustomerName());
                    connection.setNX(customerkey, customername);
                });
                return true;
            }
        }, false, true);
        logger.info("~~系统初始化，将店铺信息载入缓存 结束~~");
        return result;

    }


    /**
     * 通过key获取
     * <br>------------------------------<br>
     * @param keyId
     * @return
     */
    public String get(final String keyId) {
        String result = redisTemplate.execute(new RedisCallback<String>() {
            public String doInRedis(RedisConnection connection)
                    throws DataAccessException {
                RedisSerializer<String> serializer = redisTemplate.getStringSerializer();
                byte[] key = serializer.serialize(keyId);
                byte[] value = connection.get(key);
                if (value == null) {
                    return null;
                }
                String name = serializer.deserialize(value);
                return name;
            }
        });
        return result;
    }

    /**
     * 删除
     * <br>------------------------------<br>
     * @param key
     */
    public void delete(String key) {
        List<String> list = Lists.newArrayList(key);
        delete(list);
    }

    /**
     * 删除多个
     * <br>------------------------------<br>
     * @param keys
     */
    public void delete(List<String> keys) {
        redisTemplate.delete(keys);
    }

    /**
     * 新增
     *<br>------------------------------<br>
     * @param map
     * @return
     */
    public boolean add(final Map<String,String> map) {
        boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
            public Boolean doInRedis(RedisConnection connection)
                    throws DataAccessException {
                RedisSerializer<String> serializer = redisTemplate.getStringSerializer();
                byte[] key  = serializer.serialize(map.get("key"));
                byte[] name = serializer.serialize(map.get("value"));
                return connection.setNX(key, name);
            }
        });
        return result;
    }

    public void set(final Map<String,String> map){
        String key = map.get("key");
        String value = map.get("value");
        stringRedisTemplate.opsForValue().set(key,value);
    }

}
