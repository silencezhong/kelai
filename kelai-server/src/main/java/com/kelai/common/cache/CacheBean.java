package com.kelai.common.cache;

import com.kelai.common.IServiceLayer;
import com.kelai.common.PersistObject;
import com.kelai.common.exception.BreezeeException;
import com.kelai.domain.DeviceDataEntity;
import com.kelai.domain.DeviceEntity;
import com.kelai.domain.DevicePackageDataEntity;
import com.kelai.domain.DeviceStateDataEntity;
import com.kelai.response.JsonResponse;
import com.kelai.service.ICommonService;
import com.kelai.service.IDeviceDataService;
import com.kelai.service.impl.DefaultCommonService;
import com.kelai.utils.ContextUtil;
import com.kelai.utils.SystemTool;
import javafx.util.Callback;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 使用队列来改善入库性能
 * 基于设备必须要在3s内得到结果
 * 我们采用异步的方式来实现入库
 * Created by luffyluo on 2017/5/15.
 */
//@Component("cacheBean")
public class CacheBean {
    private final static Logger logger = LoggerFactory.getLogger(CacheBean.class);

    /**
     * 队列池
     */
    private LinkedList<DevicePackageDataEntity> packageDataQueue =
            new LinkedList<>();

    private ICommonService devicePackageDataService;
    private ICommonService deviceExceptionDataService;

    @Resource
    private IDeviceDataService deviceDataService;


    private static Boolean flag = true;

    public void add(DevicePackageDataEntity obj) {
        packageDataQueue.add(obj);
    }

    /**
     * 返回当前的大小
     * @return
     */
    public int getQueueSize() {
        return packageDataQueue.size();
    }

    public void stop(){
        CacheBean.flag = false;
    }

    public Boolean status(){
        return CacheBean.flag;
    }

    /**
     * while true太耗cpu资源了。后续要成Thread.join()来实现 --Anjing
     */
    public void start() {
        logger.info("~~处理设备上传数据线程 running...~~");
        while (CacheBean.flag) {
            try {
                DevicePackageDataEntity devicePackageDataEntity = null;
                while (CollectionUtils.isNotEmpty(packageDataQueue) && (devicePackageDataEntity = packageDataQueue.removeFirst()) != null) {
                    cacheData(devicePackageDataEntity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.currentThread().sleep(800);//毫秒
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void cacheData(DevicePackageDataEntity devicePackageDataEntity) {
        if (devicePackageDataService == null){
            devicePackageDataService = new DefaultCommonService("devicePackageDataRepository");
        }
        if(deviceExceptionDataService == null){
            deviceExceptionDataService = new DefaultCommonService("deviceStateDataRepository");
        }
        String data = devicePackageDataEntity.getData();
        String sn = getSn(devicePackageDataEntity.getState().substring(4, 12)).toUpperCase();
        Integer focus = Integer.decode("0x" + devicePackageDataEntity.getState().substring(12, 14));
        Integer battery = Integer.decode("0x" + devicePackageDataEntity.getState().substring(18, 20));
        DeviceEntity device = (DeviceEntity) devicePackageDataEntity.getProperties().get("device");
        Boolean isSonyDevice = false;
        if(device != null){
            if(Objects.equals("9236b103947f4f71b261bcf958286b2c",device.findCustomerId())){
                isSonyDevice = true;
            }
            logger.info("~~得到设备ID~~" + device.getId());
        }else{
            logger.info("~~设备:" +sn+"不存在");
        }

        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        DevicePackageDataEntity dpde = new DevicePackageDataEntity();

        dpde.getProperties().put("data", data);
        dpde.getProperties().put("state_like",devicePackageDataEntity.getState().substring(4, 12));
        List list = devicePackageDataService.listAll(dpde);
        devicePackageDataEntity.setRemark("async");
        if (CollectionUtils.isEmpty(list)) {
            _saveInfo(devicePackageDataService, devicePackageDataEntity, new Callback<DevicePackageDataEntity, Object>() {
                @Override
                public Object call(DevicePackageDataEntity param) {
                    param.setCode(param.getId());
                    return null;
                }
            });
            String[] ss = data.split(",");
//            String[] mss = new String[ss.length];
//            logger.info("~~mss~~"+mss);
            if (Objects.equals(devicePackageDataEntity.getProperties().get("flag"), true)) {
                DeviceDataEntity de;
                String happenTimeStr = "";
                Integer dxIn = 0;
                Integer dxOut = 0;
                for (String s : ss) {
                    dxIn = Integer.decode("0x" + s.substring(20, 22) + s.substring(18, 20) + s.substring(16, 18) + s.substring(14, 16));
                    dxOut = Integer.decode("0x" + s.substring(28, 30) + s.substring(26, 28) + s.substring(24, 26) + s.substring(22, 24));
                    if (dxIn + dxOut > 0) {
                        //100B1E131600000000000000000000D5FD
                        happenTimeStr = "20" + deHexFormat(s.substring(0, 2)) + deHexFormat(s.substring(2, 4))
                                + deHexFormat(s.substring(4, 6)) + deHexFormat(s.substring(6, 8))
                                + deHexFormat(s.substring(8, 10)) + deHexFormat(s.substring(10, 12));
                        de = deviceDataService.findByCode(sn + happenTimeStr);
                        if (de == null) {
                            de = _dataDevice(sn, happenTimeStr , device);
                        } else {
                            dxIn += (de.getDxin() == null ? 0 : de.getDxin());
                            dxOut += (de.getDxout() == null ? 0 : de.getDxout());
                        }
                        de.setFocus(focus);
                        de.setBattery(battery);
                        if (device != null && device.getDirection() == 0) {
                            de.setDxin(dxOut);
                            de.setDxout(dxIn);
                        // FIXME 原来的判断条件貌似有问题} else if (device == null || device.getDirection() == 1) {
                        } else if (device != null && device.getDirection() == 1) {
                            de.setDxin(dxIn);
                            de.setDxout(dxOut);
                        }
                        if (devicePackageDataEntity.getId() != null)
                            de.setDevicePackageData(devicePackageDataEntity);
                        de.setRemark("async");
                        _saveInfo(deviceDataService, de);
                    }

                    /*保存索尼中国的设备状态流水信息*/
                    if(isSonyDevice){
                        DeviceStateDataEntity dede = new DeviceStateDataEntity();
                        dede.setDevice(device);
                        if(StringUtils.isNotEmpty(happenTimeStr)){
                            dede.setHappenTime(happenTimeStr);
                        }
                        dede.setReceiveTime(now);
                        dede.setFocus(device.getFocus());
                        dede.setBattery(device.getVoltage());
                        dede.setDxin(dxIn);
                        dede.setDxout(dxOut);
                        _saveInfo(deviceExceptionDataService, dede, new Callback<DeviceStateDataEntity, Object>() {
                            @Override
                            public Object call(DeviceStateDataEntity param) {
                                param.setCode(param.getId());
                                return null;
                            }
                        });

                    }
                }
            }
        }
    }

    private static String getSn(String hexString) {
        String hv = null;
        StringBuilder sb = new StringBuilder();
        byte[] bb = SystemTool.convert16HexToByte(hexString);
        for (byte b : bb) {
            int v = b & 0xFF;
            hv = Integer.toHexString(v);
            hv = (hv.length() == 1 ? "0" + hv : hv);
            sb.insert(0, hv);
        }
        return sb.toString();
    }

    private static String hexFormat(Integer v) {
        String s = Integer.toHexString(v);
        return s.length() == 1 ? "0" + s : s;
    }

    private static String deHexFormat(String s) {
        Integer i = Integer.decode("0x" + s);
        return i < 10 ? "0" + i : i.toString();
    }


    private DeviceDataEntity _dataDevice(String sn, String t, DeviceEntity device) {
        DeviceDataEntity de = new DeviceDataEntity();
        de.setCode(sn + t);
        de.setName(sn);
        de.setSn(sn);
        de.setHappenTime(Long.parseLong(t));
        if (device != null) {
            de.setDevice(device);
            if (device.getCustomer() != null) {
                de.setCustomer(device.getCustomer());
                de.setCustomerId(device.getCustomer().getTopCustomerId());
            }
        }
        return de;
    }

    protected <T extends PersistObject> JsonResponse _saveInfo(IServiceLayer service, T info, Callback... callback) {
        try {
            Callback cb = ContextUtil.getBean("idCallback", Callback.class);
            if (cb != null) {
                List<Callback> l = new ArrayList<>(Arrays.asList(callback));
                Callback[] cbs = new Callback[l.size()];
                l.add(0, ContextUtil.getBean("idCallback", Callback.class));
                service.saveInfo(info, l.toArray(cbs));
            } else {
                service.saveInfo(info, callback);
            }
        } catch (BreezeeException e) {
            e.printStackTrace();
            return JsonResponse.ERROR(e.getMessage());
        }
        return JsonResponse.buildSingle(info);
    }



}
