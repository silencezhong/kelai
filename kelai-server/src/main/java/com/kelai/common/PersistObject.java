package com.kelai.common;

import java.io.Serializable;
import java.util.Map;

/**
 * 持久化对象
 * Created by Silence on 2016/10/16.
 */
public abstract class PersistObject implements Serializable {
    public abstract String getId();

    public abstract String getCode();

    public abstract Map<String, Object> getProperties();
}
