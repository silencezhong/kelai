package com.kelai.common;

import com.kelai.service.*;
import com.kelai.utils.ContextUtil;

/**
 * 从BI取数工厂类
 * Created by luffyluo on 2017/4/21.
 */
public class BiFactory {

    public IBiService createService(String model) {

        IBiService biService = null;

        if (model.equals("SHOP_HOUR")) { //店铺维度 按小时统计
            biService = ContextUtil.getBean("shopHourService", IShopHourService.class);
        } else if (model.equals("SHOP_DAY")) {//店铺维度 按天统计
            biService = ContextUtil.getBean("shopDayService", IShopDayService.class);
        } else if (model.equals("SHOP_WEEK")) {//店铺维度 按周统计
            biService = ContextUtil.getBean("shopWeekService", IShopWeekService.class);
        } else if (model.equals("SHOP_MONTH")) {//店铺维度 按月统计
            biService = ContextUtil.getBean("shopMonthService", IShopMonthService.class);
        } else if (model.equals("DEVICE_HOUR")) {//设备维度 按小时统计
            biService = ContextUtil.getBean("deviceHourService", IDeviceHourService.class);
        } else if (model.equals("DEVICE_DAY")) {//设备维度 按天统计
            biService = ContextUtil.getBean("deviceDayService", IDeviceDayService.class);
        } else if (model.equals("DEVICE_WEEK")) {//设备维度 按周统计
            biService = ContextUtil.getBean("deviceWeekService", IDeviceWeekService.class);
        } else if (model.equals("DEVICE_MONTH")) {//设备维度 按月统计
            biService = ContextUtil.getBean("deviceMonthService", IDeviceMonthService.class);
        }

        return biService;
    }
}
