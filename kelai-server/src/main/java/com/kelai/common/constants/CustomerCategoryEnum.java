package com.kelai.common.constants;

import java.io.Serializable;

/**
 * 1客户2目录3店铺
 * Created by luffy on 2017/05/18.
 */
public enum CustomerCategoryEnum implements Serializable {

    CUSTOMER("客户", "1"),
    GROUP("目录", "2"),
    SHOP("店铺", "3");

    private final String text;

    private final String value;

    CustomerCategoryEnum(String text, String value) {
        this.text = text;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }
}
