package com.kelai.common.constants;

/**
 * 帐号类型 1代表管理员 2代表客户 3代表渠道商 4代表目录 5代表店铺
 * Created by luffy on 2017/05/18.
 */
public enum AccountTypeEnum implements ConstantEnum{

    ADMIN("管理员", 1),
    CUSTOMER("客户", 2),
    VENDOR("渠道商", 3),
    GROUP("目录", 4),
    SHOP("店铺", 5);

    private final String text;

    private final Integer value;

    AccountTypeEnum(String text, Integer value) {
        this.text = text;
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getText() {
        return text;
    }
}
