package com.kelai.common.constants;

/**
 * 设备状态
 * Created by Silence on 2016/11/22.
 */
public enum DeviceStatusEnum implements ConstantEnum{
    DISABLE("异常", 0),
    ENABLE("正常", 2),
    UNKNOWN("离线", 1);

    private final String text;

    private final Integer value;

    DeviceStatusEnum(String text, Integer value) {
        this.text = text;
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getText() {
        return text;
    }

    public static String getTextByVal(Integer value) {
        for (DeviceStatusEnum e : DeviceStatusEnum.values()) {
            if (value.equals(e.getValue())) {
                return e.getText();
            }
        }
        return "";
    }
}
