package com.kelai.common.constants;

/**
 * 门店状态
 * Created by Luffy on 2017/07/23.
 */
public enum ShopStatusEnum implements ConstantEnum{
    UNINSTALLED("未开店", 0),
    ENABLE("正常", 1),
    SHUTDOWN("撤店", 2),
    DECORATE("装修中", 3),
    REPAIR("设备维修", 4);

    private final String text;

    private final Integer value;

    ShopStatusEnum(String text, Integer value) {
        this.text = text;
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getText() {
        return text;
    }
}
