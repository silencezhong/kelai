package com.kelai.extend;

import com.kelai.common.IDistribution;
import com.kelai.domain.DeviceDataEntity;
import javafx.util.Callback;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * 百丽的数据分发服务
 * Created by Silence on 2016/12/19.
 */
@Component("belleDistribution")
public class BelleDistribution implements IDistribution,
        Callback<DeviceDataEntity, Object>, InitializingBean {

    private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

    private final static String sql = "insert into t_equipment_five " +
            "(equipment_no,Module_no,info,up,down,No1_status,Operate_date) values (?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    private String shopCode;

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Object call(DeviceDataEntity param) {
//        if (param.getCustomer() != null)
//            System.out.println(jdbcTemplate + "--" + this.shopCode + "---" + param.getCustomer().getCode());
//        else
//            System.out.println("the customer is null....Belle Distribution");
        if (jdbcTemplate == null)
            return null;
        try {
            if (param.getCustomer() != null && param.getDevice() != null && param.getCustomer().getCode().equals(this.shopCode)) {
                System.out.println("Belle Distribution:" + param.getCustomer().getCode() + "," + param.getDevice().getCode());
                jdbcTemplate.update(sql, "1",
                        param.getCustomer().getCode(),
                        "1",
                        param.getDxin(),
                        param.getDxout(), null,
                        sdf.parse(param.getHappenTime().toString()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void init() {

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("-------init the belle datasource ...............");
        //TODO: set jdbcTemplate
        this.shopCode = "1006";
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        dataSource.setUrl("jdbc:sqlserver://116.228.193.76:1433;DatabaseName=shopcusrec");
        dataSource.setUsername("sa");
        dataSource.setPassword("winnerinf");
        dataSource.setValidationQuery("select 1");
        jdbcTemplate = new JdbcTemplate(dataSource);
//        create table t_equipment_five(id int(4) primary key not null auto_increment,equipment_no varchar(100) not null,
//                Module_no varchar(100) not null,
//                info varchar(100) null,
//                up int null,
//                down int null,No1_status char(1),
//                Operate_date datetime)
    }
}
