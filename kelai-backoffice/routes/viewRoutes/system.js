"use strict";
module.exports = {

    shopManager : function (req, res, callback) {
        var param = {isFilterShow : ''};
        if(req.query.isFilterShow){
            param.isFilterShow = 1;
        }
        callback(param);
    },
    shopQuery : function (req, res, callback) {
        var param = {isFilterShow : ''};
        if(req.query.isFilterShow){
            param.isFilterShow = 1;
        }
        callback(param);
    },

    deviceList : function (req, res, callback) {
        var param = {isFilterShow : ''};
        if(req.query.isFilterShow != ''){
            param.isFilterShow = req.query.isFilterShow;
        }
        callback(param);
    },

    deviceQuery : function (req, res, callback) {
        var param = {isFilterShow : ''};
        if(req.query.isFilterShow != ''){
            param.isFilterShow = req.query.isFilterShow;
        }
        callback(param);
    }

};
