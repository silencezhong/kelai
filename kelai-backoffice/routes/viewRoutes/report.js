"use strict";
module.exports = {
    dataQuery : function (req, res, callback) {

        let today = new Date(),f = new Date(today.getFullYear(), today.getMonth(), 1);
        today.setHours(0);
        today.setMinutes(0);
        today.setSeconds(0);
        today.setMilliseconds(0);
        let oneday = 1000 * 60 * 60 * 24;
        callback({
            today:global.tool.dateFormatter(today, 'yyyy-MM-dd'),
            yesterday:global.tool.dateFormatter(new Date(today.getTime()-oneday), 'yyyy-MM-dd'),
            tomorrow: global.tool.dateFormatter(new Date(today.getTime()+oneday), 'yyyy-MM-dd'),
            tweekFirst:showWeekFirstDay(today,0),
            tweekLast:showWeekLastDay(today,0),
            lweekFirst:showWeekFirstDay(today,7),
            lweekLast:showWeekLastDay(today,7),
            tmonthFirst: global.tool.dateFormatter(f, 'yyyy-MM-dd'),
            tmonthLast:global.tool.dateFormatter(new Date(today.getTime()+1000*60*60*24), 'yyyy-MM-dd'),
            lmonthFirst:showMonthFirstDay(today,1),
            lmonthLast:showMonthLastDay(today,1),
            tyearFirst: showYearFirstDay(today),
            tyearLast:global.tool.dateFormatter(new Date(today.getTime()+1000*60*60*24), 'yyyy-MM-dd'),
            lyearFirst:showLastYearFirstDay(today),
            lyearLast:showLastYearLastDay(today),
            isFilterShow: req.query.isFilterShow?req.query.isFilterShow:''
        });
    },
    compare : function (req, res, callback) {
        let today = new Date(), f = new Date(today.getFullYear(), today.getMonth(), 1);
        today.setHours(0);
        today.setMinutes(0);
        today.setSeconds(0);
        today.setMilliseconds(0);
        let oneday = 1000 * 60 * 60 * 24;
        callback({
            today:global.tool.dateFormatter(today, 'yyyy-MM-dd'),
            yesterday:global.tool.dateFormatter(new Date(today.getTime()-oneday), 'yyyy-MM-dd'),
            tomorrow: global.tool.dateFormatter(new Date(today.getTime()+oneday), 'yyyy-MM-dd'),
            tweekFirst:showWeekFirstDay(today,0),
            tweekLast:showWeekLastDay(today,0),
            lweekFirst:showWeekFirstDay(today,7),
            lweekLast:showWeekLastDay(today,7),
            tmonthFirst: global.tool.dateFormatter(f, 'yyyy-MM-dd'),
            tmonthLast:global.tool.dateFormatter(new Date(today.getTime()+1000*60*60*24), 'yyyy-MM-dd'),
            lmonthFirst:showMonthFirstDay(today,1),
            lmonthLast:showMonthLastDay(today,1),
            tyearFirst: showYearFirstDay(today),
            tyearLast:global.tool.dateFormatter(new Date(today.getTime()+1000*60*60*24), 'yyyy-MM-dd'),
            lyearFirst:showLastYearFirstDay(today),
            lyearLast:showLastYearLastDay(today)
        });
    }
};

/**
 * 本周第一天
 */
function showWeekFirstDay(Nowdate, offset) {
    let WeekFirstDay = new Date(Nowdate - (Nowdate.getDay() - 1) * 86400000 - offset * 86400000);
    return global.tool.dateFormatter(WeekFirstDay, 'yyyy-MM-dd');
}
/**
 * 本周最后一天
 */
function showWeekLastDay(Nowdate, offset) {
    let WeekFirstDay = new Date(Nowdate - (Nowdate.getDay() - 1) * 86400000 - offset * 86400000);
    let WeekLastDay = new Date((WeekFirstDay / 1000 + 6 * 86400) * 1000+1000*60*60*24);
    return global.tool.dateFormatter(WeekLastDay, 'yyyy-MM-dd');
}
/**
 * 本月第一天
 */
function showMonthFirstDay(Nowdate, offset) {
    let MonthFirstDay = new Date(Nowdate.getFullYear(), Nowdate.getMonth() - offset, 1);
    return global.tool.dateFormatter(MonthFirstDay, 'yyyy-MM-dd');
}
/**
 * 本月最后一天
 */
function showMonthLastDay(Nowdate, offset) {
    let MonthNextFirstDay = new Date(Nowdate.getFullYear(), Nowdate.getMonth() - offset + 1, 1);
    let MonthLastDay = new Date(MonthNextFirstDay - 86400000+1000*60*60*24);
    return global.tool.dateFormatter(MonthLastDay, 'yyyy-MM-dd');
}

/**
 * 今年第一天
 */
function showYearFirstDay(Nowdate){
    let yearFirstDay = new Date(Nowdate.getFullYear(), 0 , 1);
    return global.tool.dateFormatter(yearFirstDay, 'yyyy-MM-dd');

}

/**
 * 去年第一天
 */
function showLastYearFirstDay(Nowdate){
    let yearFirstDay = new Date(Nowdate.getFullYear()-1, 0 , 1);
    return global.tool.dateFormatter(yearFirstDay, 'yyyy-MM-dd');

}

/**
 * 去年最后一天
 */
function showLastYearLastDay(Nowdate){
    let yearLastDay = new Date(Nowdate.getFullYear()-1, 11 , 31);
    return global.tool.dateFormatter(yearLastDay, 'yyyy-MM-dd');

}