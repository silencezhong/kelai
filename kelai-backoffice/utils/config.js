/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */
let path = require('path');
//prod
module.exports = {
    title: '客流平台',
    contextPath: '/kelai-bo',
    viewPrefix: '/view',
    publicPath: "/static-kelai-bo",
    imagePath: "",
    mode: "",
    mock: false,
    cdn: '//cdn.bootcss.com',
    // cdn: '',
    production: false,
    uploadPath: __dirname+'/../public/uploadFiles/images/',
    apiServerUrl: 'http://127.0.0.1:9111',
    apiFilePath: '/../apiConfig.json',
    i18n : {
        locales: ['en', 'zh', 'tw'],
        directory: path.join('utils','local'),
        defaultLocale : 'en',
        extension: '.json'
    }
};

//dev
/*module.exports = {
    title: '客流平台',
    contextPath: '/',// '/kelai-bo',
    viewPrefix: 'view',
    publicPath: "",// "/static-kelai-bo",
    imagePath: "",
    mode: "",
    mock: false,
    cdn: '//cdn.bootcss.com',
    // cdn: '',
    production: false,
    uploadPath: __dirname+'/../public/uploadFiles/images/',
    apiServerUrl: 'http://127.0.0.1:9111',
    apiFilePath: '/../apiConfig.json',
    i18n : {
        locales: ['en', 'zh', 'tw'],
        directory: path.join('utils','local'), //'utils\\local',
        defaultLocale : 'en',
        extension: '.json'
    }
};*/
