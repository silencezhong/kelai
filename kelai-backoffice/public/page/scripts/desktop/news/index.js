/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */

$(function () {
    org.breezee.menu.topSelect('news');

    org.breezee.page = {

        init: function () {
            this.categoryTree();
            this.itemLine();
            this.initEvent();
        },

        initEvent: function () {
            var me = this;
            $('.add_new').click(function () {
                Dolphin.form.empty('#categoryForm');
                $('#myModal').modal('show');
            });

            $('.modelSave').click(function () {
                var data = Dolphin.form.getValue('categoryForm', '"');
                if(!data.parent.id)
                    delete data.parent;
                Dolphin.ajax({
                    url: '/api/6afc0d04df00417f9251f2068fd3fb3e',
                    type: Dolphin.requestMethod.PUT,
                    data: Dolphin.json2string(data),
                    onSuccess: function (reData) {
                        Dolphin.alert(Dolphin.i18n.get("news.saved"), {
                            callback: function () {
                                me._categoryTree.reload();
                                $('#myModal').modal('hide');
                            }
                        })
                    }
                });
            });

            $("#articleAdd").bind('click', function () {
                if(!me.selectNode) {
                    alert(Dolphin.i18n.get("news.pleaseSelectType"));
                    return;
                }
                location.href = "articleManagement-edit?modelId=" + me.selectNode['id'];
            });

            $("#articleUpdate").click(function () {
                if (me.curSelect) {
                    location.href = "articleManagement-edit?modelId=" + me.selectNode['id'] + "&id=" + me.curSelect.id;
                } else {
                    Dolphin.alert(Dolphin.i18n.get("news.pleaseSelectRecord"));
                }
            });

            $('#articleDel').bind('click', function () {
                Dolphin.ajax({
                    url: "/api/369cf93b979a4569887d3839b806b0fd@id=" + me.curSelect.id,
                    type: Dolphin.requestMethod.DELETE,
                    onSuccess: function (reData) {
                        Dolphin.alert(Dolphin.i18n.get("news.deleted"), {
                            callback: function () {
                                me._itemList.reload();
                            }
                        })
                    }
                });
            });
        },

        categoryTree: function () {
            var me = this;
            this._categoryTree = new Dolphin.TREE({
                panel: '#categoryTree',
                url: '/api/e91244a3044e49c6af69c423f908b097?parentId=-1',
                mockPathData: ['id'],
                multiple: false,
                onChecked: function (data) {
                    me.selectNode = data;
                    me._itemList.query({
                        model_id_obj_ae: data.id
                    });
                }
            });
        },

        itemLine: function () {
            var me = this;
            this._itemList = new Dolphin.LIST({
                panel: '#itemList',
                url: '/api/c97b00aa70fc4ed1b5922bced4468a0b',
                ajaxType: 'post',
                mockPathData: ['id'],
                data: {rows: [], total: 0},
                pagination: true,
                multiple: false,
                columns: [{
                    code: 'name',
                    title: Dolphin.i18n.get("news.headline")
                }, {
                    code: 'cover',
                    title: Dolphin.i18n.get("news.coverPictures")
                }, {
                    code: 'lang',
                    title: Dolphin.i18n.get("news.language"),
                    formatter: function (value, record) {
                        return Dolphin.i18n.get("news.chinese");
                    }
                }, {
                    code: 'modifiedBy',
                    title: Dolphin.i18n.get("news.updatingPerson")
                }, {
                    code: 'modifiedDate',
                    title: Dolphin.i18n.get("news.updatingTime")
                }, {
                    code: 'content',
                    title: Dolphin.i18n.get("news.details"),
                    formatter: function (value, record) {
                        return '<a href="articleManagement-detail?id=' + record.id + '" target="_blank">'+ Dolphin.i18n.get("news.details")+'</a>';
                    }
                }],
                onLoadSuccess: function (data) {
                },
                onCheck: function (data, row, thisInput) {
                },
                onChecked: function (data, row, thisCheckbox) {
                    me.curSelect = data;
                },
                onUnchecked: function (data) {
                }
            });
        }
    };

    org.breezee.page.init();
});