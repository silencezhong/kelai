/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

$(function () {
    org.breezee.page = {
        /**
         * 初始化页面
         */
        init: function () {
            this.destroy();
            this._enumList = this.enumList('#enumList');
            this._enumItemList = this.enumItemList('#enumItemList', false);
            this.initSearch();
            this.initEvent();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;
            $(".newEnum").click(function () {
                $('#enum_win').modal('show');
            });

            $(".relateShop").click(function () {
                Dolphin.form.empty('.shop-form');
                var checkedData = me._enumList.getChecked();
                if (checkedData.length == 0) {
                    Dolphin.alert('请选择一个店组');
                    return;
                }
                Dolphin.form.setValue({
                    grpId: checkedData[0].id
                }, '.shop-form');
                $("#shop_win").modal('show');
            });

            $(".enum_submit").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/45c8ab38a1694ce79a98a1563992dc36',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._enumList.reload();
                            $('#enum_win').modal('hide');
                        }
                    });

                }
            });

            $(".shop_submit").click(function () {
                var ef = $(".shop-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/6bab0aae39ae47c0b1e6d699a628c3b0',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            $('#shop_win').modal('hide');
                        }
                    });

                }
            });
        },
        /**
         * 枚举信息列表，不分页
         * @param panelId
         * @returns {*|LIST}
         */
        enumList: function (panelId) {
            var me = this;
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'id',
                queryParams:{},
                columns: [{
                    code: 'name',
                    title: '店组名称',
                    textAlign: 'left'
                }, {
                    code: 'code',
                    title: '枚举编码'
                }, {
                    code: '',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        return '<a class="btn btn-outline btn-circle btn-sm purple editBtn" data-id="'+row.id+'" href="javascript:;">' +
                            '<i class="fa fa-edit"></i>&nbsp;'+Dolphin.i18n.get("account.edit")+'</a>'
                            +
                            '<a class="btn btn-outline btn-circle btn-sm dark delBtn" data-id="'+row.id+'" href="javascript:;">' +
                            '<i class="fa fa-trash-o"></i>&nbsp;'+Dolphin.i18n.get("role.delete")+'</a>';
                    }
                }],
                multiple: false,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/45c8ab38a1694ce79a98a1563992dc36',
                pagination: false,
                onClick: function (data, thisRow, event) {
                    me._enumItemList.loadData({rows: data.customers});
                },
                onLoadSuccess: function () {
                    org.breezee.buttons.editCallback('12d44efe76ce468cb466cf3e33fc07a1', 'id', function () {
                        $('#enum_win').modal('show');
                    });
                    org.breezee.buttons.delCallback('12d44efe76ce468cb466cf3e33fc07a1', function () {
                        me._enumList.reload();
                    });
                }
            });
        },
        /**
         * 枚举项列表
         * @param panelId
         * @param edit
         * @returns {*|LIST}
         */
        enumItemList: function (panelId, edit) {
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'pkId',
                columns: [{
                    code: 'id',
                    hidden: true,
                    title: '数据主键'
                }, {
                    code: 'name',
                    title: '门店名称'
                }, {
                    code: 'code',
                    title: '门店编码'
                }, {
                    code: 'deviceSize',
                    title: '设备数'
                }],
                multiple: false,
                rowIndex: false,
                checkbox: false,
                editFlag: edit,
                data: {rows: []},
                pagination: false,
                editListName: 'items',
                onClick: function (data, thisRow, event) {

                },
                onRemoveRow: function (data, event, thisRow) {
                    if (thisRow.find('[listName="id"]').val()) {
                        thisRow.remove();
                    }
                }
            });
        },

        initSearch: function () {
            $("#holdercode").bsSuggest({
                delayUntilKeyup: true,
                allowNoKeyword: false,   //是否允许无关键字时请求数据。为 false 则无输入时不执行过滤请求
                multiWord: false,         //以分隔符号分割的多关键字支持
                separator: ",",          //多关键字支持时的分隔符，默认为空格
                getDataMethod: "url",    //获取数据的方式，总是从 URL 获取
                effectiveFields: ["code", "name", "type", "store"],
                effectiveFieldsAlias: {code: "编码", name: "名称", type: "类型", store: '门店'},
                idField: 'id',
                keyField: 'code',
                url: (org.breezee.context.contextPath == '/' ? '' : org.breezee.context.contextPath) + '/api/742445c653654caaa4a1fa69eb651a9a', //优先从url ajax 请求 json 帮助数据，注意最后一个参数为关键字请求参数
                listStyle: {
                    'padding-top': 0,
                    'max-height': '425px',
                    'max-width': '800px',
                    'overflow': 'auto',
                    'border': '1px solid',
                    'width': 'auto',
                    'transition': '0.3s',
                    '-webkit-transition': '0.3s',
                    '-moz-transition': '0.3s',
                    '-o-transition': '0.3s'
                },                              //列表的样式控制
                listAlign: 'left',              //提示列表对齐位置，left/right/auto
                listHoverStyle: 'background: #07d; color:#000', //提示框列表鼠标悬浮的样式
                fnAdjustAjaxParam: function (keyword, opt) {
                    return {
                        type: 'POST',
                        data: {
                            category: '3',
                            code: keyword
                        },
                        dataType: 'json',
                        beforeSend: function (XMLHttpRequest) {
                            var requestHeaderParam = Dolphin.defaults.ajax.requestHeader;
                            for (var key in requestHeaderParam) {
                                XMLHttpRequest.setRequestHeader(key, requestHeaderParam[key]);
                            }
                        }
                    };
                },
                fnProcessData: function (json) {    // url 获取数据时，对数据的处理，作为 fnGetData 的回调函数
                    var index, len, data = {value: []};
                    data.value = json.rows;
                    //字符串转化为 js 对象
                    return data;
                }
            }).on('onDataRequestSuccess', function (e, result) {
                if (result.total == 0) {
                    $("#shopName").val('');
                }
            }).on('onSetSelectValue', function (e, keyword, data) {
                $("#shopName").val(data.name);
                $("#holderid").val(data.id);
            }).on('onUnsetSelectValue', function () {
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
    org.breezee.page.init();
});
