/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */
"use strict";
$(function () {
    org.breezee.page = {
        init: function () {
            Dolphin.form.parse();
            this._architTree = this.architTree('#architTree');
            this._shopList = this.shopList("#shopList");
            this._deviceList = this.deviceList("#deviceList");

            this.initEvent();
            $('.dropdown-toggle').dropdown();
            var loginAccountId = $("#_loginAccountId").val();
            if(loginAccountId){

                var psel = $("#parent_select");
                Dolphin.ajax({
                    url: '/api/c666378c5d3b417d8e150c456a489d23@accountId='+loginAccountId,
                    type: Dolphin.requestMethod.GET,
                    async: false,
                    onSuccess: function (reData) {
                        psel.append(
                            '<option value="">--'+Dolphin.i18n.get("account.pleaseSelect")+'--</option>');
                        $.each(reData.rows, function (index, obj) {
                            psel.append(
                                '<option value="' + obj.id + '">'
                                + obj.name + '</option>');
                        });
                        psel.select2();
                    }
                });

                var vsel = $("#vendorSelect");
                Dolphin.ajax({
                    url: '/api/2d8841311e89471a92709511b1ac05ed@accountId='+loginAccountId,
                    type: Dolphin.requestMethod.GET,
                    async: false,
                    onSuccess: function (reData) {
                        vsel.append(
                            '<option value="">--'+Dolphin.i18n.get("account.pleaseSelect")+'--</option>');
                        $.each(reData.rows, function (index, obj) {
                            vsel.append(
                                '<option value="' + obj.id + '">'
                                + obj.name + '</option>');

                        });
                    }
                });
                $("#vendorSelect").select2();

                var csel = $("#customerSelect");
                Dolphin.ajax({
                    url: '/api/a5f6cef1dae94e42ad22f1dead43c2c8@accountId='+loginAccountId,
                    type: Dolphin.requestMethod.GET,
                    async: false,
                    onSuccess: function (reData) {
                        csel.append(
                            '<option value="">--'+Dolphin.i18n.get("account.pleaseSelect")+'--</option>');
                        $.each(reData.rows, function (index, obj) {
                            csel.append(
                                '<option value="' + obj.id + '">'
                                + obj.name + '</option>');

                        });
                    }
                });
                $("#customerSelect").select2();

                var gsel = $("#groupSelect");
                Dolphin.ajax({
                    url: '/api/c666378c5d3b417d8e150c456a489d23@accountId='+loginAccountId,
                    type: Dolphin.requestMethod.GET,
                    async: false,
                    onSuccess: function (reData) {
                        gsel.append(
                            '<option value="">--'+Dolphin.i18n.get("account.pleaseSelect")+'--</option>');
                        $.each(reData.rows, function (index, obj) {
                            gsel.append(
                                '<option value="' + obj.id + '">'
                                + obj.name + '</option>');
                        });

                    }
                });
                $("#groupSelect").select2();

                var ssel = $("#myShopSel");
                if(loginAccountId){
                    Dolphin.ajax({
                        url: '/api/3c43c627b41e4fafbfdbfb526345b248@accountId='+loginAccountId,
                        type: Dolphin.requestMethod.GET,
                        async: false,
                        onSuccess: function (reData) {
                            ssel.append(
                                '<option value="">--'+Dolphin.i18n.get("account.pleaseSelect")+'--</option>');
                            $.each(reData.rows, function (index, obj) {
                                ssel.append(
                                    '<option value="' + obj.id + '">'
                                    + obj.name + '</option>');
                            });
                        }
                    });
                }
                $("#myShopSel").select2();
            }

        },
        /**
         * 初始化事件
         */
        initEvent: function(){
            let me = this;
            //上传图片
            var publicContextPath = $("#publicContextPath").val() == '/' ? '' : $("#publicContextPath").val();
            //新建设备
            $(".newDevice").click(function(){
                Dolphin.form.empty('.device-edit-form');
                Dolphin.form.setValue({
                    type:'dtk3',
                    recordPeriod:'10',
                    uploadPeriod:'30',
                    level:'5'
                }, '.device-edit-form');
                $('#device_win').modal('show');
                $("#myShopSel").val($("#editId").val());
            });
            //新建架构实体
            $(".newArchit").click(function(){
                Dolphin.form.empty('.edit-form');
                $("#architTypeVal").val('');
                $('#archit_win').modal('show');
                $('#architTypeTab').tab('show');
            });
            //编辑实体
            $(".editArchit").click(function(){
                var editId = $("#editId").val();
                var editType = $("#editType").val();
                var editParentId = $("#editParentId").val();
                console.log("editId: "+editId + " editType: "+editType + " editParentId: "+editParentId);
                var userType = $("#_userType").val();
                var userVendorId = $("#_loginVendorId").val();
                switch(editType){
                    case '1':
                        $('#vendor_win').modal('show');
                        Dolphin.ajax({
                            url: '/api/2315af794bbd432fbee70f533e8cdbad@id=' + editId,
                            type: Dolphin.requestMethod.GET,
                            onSuccess: function (reData) {
                                Dolphin.form.setValue(reData.value, '.vendor-edit-form');

                                var link = '/uploadFiles/images/logo.png';
                                if(reData.value.logoUrl != '' && reData.value.logoUrl != null){
                                    link = reData.value.logoUrl;
                                }
                                var p = $('.vendor-edit-form').find('.add-image');
                                p.empty();
                                $('.vendor-edit-form').find(".js-remove-sku-atom").show();
                                $('<img style="width : 180px ; height : 52px ;" src="'+publicContextPath+link+'" />')
                                    .click(function(){
                                        $("#showBigImg").attr("src", publicContextPath+link);
                                        $('#myModal').modal('show');
                                    })
                                    .appendTo(p);

                            }
                        });
                        break;
                    case '2':
                        $('#customer_win').modal('show');
                        Dolphin.ajax({
                            url: '/api/1423c7225d134248944babc34f3a36f4@id=' + editId,
                            type: Dolphin.requestMethod.GET,
                            onSuccess: function (reData) {
                                if(editParentId == null || editParentId == ''){
                                    Dolphin.ajax({
                                        url: '/api/975be87b3c7b445da6dd88543b7ba0b8@id=' + editId,
                                        type: Dolphin.requestMethod.GET,
                                        async: false,
                                        onSuccess: function (data) {
                                            reData.value.properties.vendorId = data.extProps.vendorId;
                                        }
                                    });

                                }else{
                                    reData.value.properties.vendorId = editParentId;
                                }

                                //if(userType == 2 && !userVendorId){
                                    $(".customer-win-vendor-select").attr("disabled",true);
                               // }

                                Dolphin.form.setValue(reData.value, '.customer-edit-form');

                                var link = '/uploadFiles/images/logo.png';
                                if(reData.value.logoUrl != '' && reData.value.logoUrl != null){
                                    link = reData.value.logoUrl;
                                }
                                var p = $('.customer-edit-form').find('.add-image');
                                p.empty();
                                $('.customer-edit-form').find(".js-remove-sku-atom").show();
                                $('<img style="width : 180px ; height : 52px ;" src="'+publicContextPath+link+'" />')
                                    .click(function(){
                                        $("#showBigImg").attr("src", publicContextPath+link);
                                        $('#myModal').modal('show');
                                    })
                                    .appendTo(p);

                            }
                        });
                        break;
                    case '3':
                        $('#group_win').modal('show');
                        Dolphin.ajax({
                            url: '/api/1423c7225d134248944babc34f3a36f4@id=' + editId,
                            type: Dolphin.requestMethod.GET,
                            onSuccess: function (reData) {
                                reData.value.parent = {};
                                reData.value.parent.id = editParentId;
                                Dolphin.form.setValue(reData.value, '.group-edit-form');
                            }
                        });
                        break;
                    case '4':
                        $('#shop_win').modal('show');
                        Dolphin.ajax({
                            url: '/api/1423c7225d134248944babc34f3a36f4@id=' + editId,
                            type: Dolphin.requestMethod.GET,
                            onSuccess: function (reData) {
                                reData.value.parent = {};
                                reData.value.parent.id = editParentId;
                                Dolphin.form.setValue(reData.value, '.shop-edit-form');
                                $("#parent_select").val(editParentId).trigger("change");
                                updateShopType("edit_shopType");
                                if(reData.value.openDate){
                                    $("#openDateWin").val(reData.value.openDate.substr(0,10));
                                }else{
                                    $("#openDateWin").val("");
                                }
                                if(reData.value.closeDate){
                                    $("#closeDateWin").val(reData.value.closeDate.substr(0,10));
                                }else{
                                    $("#closeDateWin").val("");
                                }
                                if(reData.value.guaranteeDeadLine){
                                    $("#guaranteeDeadLineWin").val(reData.value.guaranteeDeadLine.substr(0,10));
                                }else{
                                    $("#guaranteeDeadLineWin").val("");
                                }
                            }
                        });
                        break;
                    default:
                        ;
                }
            });
            //删除实体
            $(".delArchit").click(function(){
                var editId = $("#editId").val();
                var editType = $("#editType").val();
                var editParentId = $("#editParentId").val();
                switch(editType){
                    case '1':
                        Dolphin.confirm(Dolphin.i18n.get("account.confirmDelete"), {
                            callback: function (flag) {
                                if (flag) {
                                    Dolphin.ajax({
                                        url: '/api/2315af794bbd432fbee70f533e8cdbad@id='+editId,
                                        type: Dolphin.requestMethod.DELETE,
                                        onSuccess: function (reData) {
                                            me._architTree.reload();
                                        }
                                    });
                                }
                            }
                        });
                        break;
                    case '2':
                        Dolphin.confirm(Dolphin.i18n.get("account.confirmDelete"), {
                            callback: function (flag) {
                                if (flag) {
                                    Dolphin.ajax({
                                        url: '/api/bddacd0f54d44d6c9b919dfd91d94569@id='+editId,
                                        type: Dolphin.requestMethod.DELETE,
                                        onSuccess: function (reData) {
                                            me._architTree.reload();
                                        }
                                    });
                                }
                            }
                        });
                        break;
                    case '3':
                        Dolphin.confirm(Dolphin.i18n.get("account.confirmDelete"), {
                            callback: function (flag) {
                                if (flag) {
                                    Dolphin.ajax({
                                        url: '/api/1423c7225d134248944babc34f3a36f4@id='+editId,
                                        type: Dolphin.requestMethod.DELETE,
                                        onSuccess: function (reData) {
                                            me._architTree.reload();
                                        }
                                    });
                                }
                            }
                        });
                        break;
                    case '4':
                        Dolphin.confirm(Dolphin.i18n.get("account.confirmDelete"), {
                            callback: function (flag) {
                                if (flag) {
                                    Dolphin.ajax({
                                        url: '/api/1423c7225d134248944babc34f3a36f4@id='+editId,
                                        type: Dolphin.requestMethod.DELETE,
                                        onSuccess: function (reData) {
                                            me._architTree.reload();
                                        }
                                    });
                                }
                            }
                        });
                        break;
                    default:
                        ;
                }
            });
            //下一步
            $('#step_next').click(function(){
                $('#architInfoTab').tab('show');
                var architTypeVal = $("#architTypeVal").val();
                switch(architTypeVal){
                    case '1':
                        $(".non-vendor-form").hide();
                        $(".vendor-form").show();
                        break;
                    case '2':
                        // $(".customer-form").find('#vendorSelect').empty();
                        // Dolphin.form.parseSelect($(".customer-form").find('#vendorSelect'));
                        $("#vendorSelect").val([]).trigger('change');
                        $(".non-customer-form").hide();
                        $(".customer-form").show();
                        break;
                    case '3':
                        // $(".group-form").find('#customerSelect').empty();
                        // Dolphin.form.parseSelect($(".group-form").find('#customerSelect'));
                        $(".non-group-form").hide();
                        $(".group-form").show();
                        break;
                    case '4':
                        updateShopType("new_shopType");
                        // $(".shop-form").find('#groupSelect').empty();
                        // Dolphin.form.parseSelect($(".shop-form").find('#groupSelect'));
                        $('#timeZoneNew').val(8);
                        $(".non-shop-form").hide();
                        $(".shop-form").show();
                        console.log($("#editId").val() +' '+ $("#editType").val());
                        $(".shop_parent").val($("#editId").val());
                        break;
                    default:
                        ;
                }
            });
            //上一步
            $('#step_prev').click(function(){
                $('#architTypeTab').tab('show');
            });
            //保存
            $("#step_save").click(function() {
                var architType = $("#architTypeVal").val();
                var logoUrl = $("#logoUrl").val();
                switch(architType){
                    case '1':
                        var ef = $(".vendor-form");
                        if (Dolphin.form.validate(ef)) {
                            var data = Dolphin.form.getValue(ef, '"');
                            data.type = 0;
                            data.level = '1';
                            data.logoUrl = logoUrl;
                            Dolphin.ajax({
                                url: '/api/c477f55806ed4b3f934c3a21f8788dde',
                                type: Dolphin.requestMethod.PUT,
                                data: Dolphin.json2string(data),
                                onSuccess: function (reData) {
                                    $('#archit_win').modal('hide');
                                    $('#architTypeTab').tab('show');
                                    me._architTree.reload();
                                }
                            });
                        }
                        break;
                    case '2':
                        var ef = $(".customer-form");
                        if (Dolphin.form.validate(ef)) {
                            let data = Dolphin.form.getValue(ef, '"');
                            if (data.parent && !data.parent.id)
                                delete data.parent;
                            data.category = data.category || '1';
                            data.level = '2';
                            data.logoUrl = logoUrl;

                            Dolphin.ajax({
                                url: '/api/8967480e5ee04fd08232da22f650f916',
                                type: Dolphin.requestMethod.PUT,
                                data: Dolphin.json2string(data),
                                onSuccess: function (reData) {
                                    $('#archit_win').modal('hide');
                                    $('#architTypeTab').tab('show');
                                    me._architTree.reload();
                                }
                            });
                        }
                        break;
                    case '3':
                        var ef = $(".group-form");
                        if (Dolphin.form.validate(ef)) {
                            let data = Dolphin.form.getValue(ef, '"');
                            if (data.parent && !data.parent.id)
                                delete data.parent;
                            data.category = data.category || '2';
                            data.level = '3';
                            Dolphin.ajax({
                                url: '/api/742445c653654caaa4a1fa69eb651a9a',
                                type: Dolphin.requestMethod.PUT,
                                data: Dolphin.json2string(data),
                                onSuccess: function (reData) {
                                    $('#archit_win').modal('hide');
                                    $('#architTypeTab').tab('show');
                                    me._architTree.reload();
                                }
                            });
                        }
                        break;
                    case '4':
                        var ef = $(".shop-form");
                        if (Dolphin.form.validate(ef)) {
                            let data = Dolphin.form.getValue(ef, '"');
                            if (data.parent && !data.parent.id)
                                delete data.parent;
                            data.category = data.category || '3';
                            data.level = '4';
                            if(data.openDate)
                                data.openDate = data.openDate + " 00:00:00";
                            if(data.closeDate)
                                data.closeDate = data.closeDate + " 23:59:59";
                            if(data.guaranteeDeadLine)
                                data.guaranteeDeadLine = data.guaranteeDeadLine + " 23:59:59";

                            Dolphin.ajax({
                                url: '/api/742445c653654caaa4a1fa69eb651a9a',
                                type: Dolphin.requestMethod.PUT,
                                data: Dolphin.json2string(data),
                                onSuccess: function (reData) {

                                    $('#archit_win').modal('hide');
                                    $('#architTypeTab').tab('show');
                                    me._architTree.reload();

                                }
                            });

                        }
                        break;
                    default:
                }
            });
            $('.content-refresh').click(function () {
                me._deviceList.reload();
            });
            $('.archit-content-refresh').click(function () {
                me._architTree.reload();
            });
            /**
             * 设备保存按钮
             */
            $(".btn_submit").click(function () {
                let ef = $(".device-edit-form");
                if (Dolphin.form.validate(ef)) {
                    let data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/b6a1808e4fcd4844836eb802249ac30c',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._deviceList.load();
                            $('#device_win').modal('hide');
                        }
                    });

                }
            });

            /**
             * 编辑界面保存按钮
             */
            $(".btn_save_edit_form").click(function(){
                var editType = $("#editType").val();
                var logoUrl = $("#logoUrl").val();
                switch(editType){
                    case '1':
                        var ef = $(".vendor-edit-form");
                        if (Dolphin.form.validate(ef)) {
                            var data = Dolphin.form.getValue(ef, '"');
                            data.type = 0;
                            data.level = '1';
                            data.logoUrl = logoUrl;
                            Dolphin.ajax({
                                url: '/api/5ddc3611392544c894a5d03e90a493cf',
                                type: Dolphin.requestMethod.PUT,
                                data: Dolphin.json2string(data),
                                onSuccess: function (reData) {
                                    $('#vendor_win').modal('hide');
                                    me._architTree.reload();
                                }
                            });
                        }
                        break;
                    case '2':
                        var ef = $(".customer-edit-form");
                        if (Dolphin.form.validate(ef)) {
                            let data = Dolphin.form.getValue(ef, '"');
                            if (data.parent && !data.parent.id)
                                delete data.parent;
                            data.category = data.category || '1';
                            data.level = '2';
                            data.logoUrl = logoUrl;
                            Dolphin.ajax({
                                url: '/api/8967480e5ee04fd08232da22f650f916',
                                type: Dolphin.requestMethod.PUT,
                                data: Dolphin.json2string(data),
                                onSuccess: function (reData) {
                                    $('#customer_win').modal('hide');
                                    me._architTree.reload();
                                }
                            });
                        }
                        break;
                    case '3':
                        var ef = $(".group-edit-form");
                        if (Dolphin.form.validate(ef)) {
                            let data = Dolphin.form.getValue(ef, '"');
                            if (data.parent && !data.parent.id)
                                delete data.parent;
                            data.category = data.category || '2';
                            data.level = '3';
                            Dolphin.ajax({
                                url: '/api/742445c653654caaa4a1fa69eb651a9a',
                                type: Dolphin.requestMethod.PUT,
                                data: Dolphin.json2string(data),
                                onSuccess: function (reData) {
                                    $('#group_win').modal('hide');
                                    me._architTree.reload();
                                }
                            });
                        }
                        break;
                    case '4':
                        var ef = $(".shop-edit-form");
                        if (Dolphin.form.validate(ef)) {
                            let data = Dolphin.form.getValue(ef, '"');
                            if (data.parent && !data.parent.id)
                                delete data.parent;
                            data.category = data.category || '3';
                            data.level = '4';
                            if(data.openDate)
                                data.openDate = data.openDate + " 12:00:00";
                            if(data.closeDate)
                                data.closeDate = data.closeDate + " 12:00:00";
                            if(data.guaranteeDeadLine)
                                data.guaranteeDeadLine = data.guaranteeDeadLine + " 12:00:00";
                            Dolphin.ajax({
                                url: '/api/742445c653654caaa4a1fa69eb651a9a',
                                type: Dolphin.requestMethod.PUT,
                                data: Dolphin.json2string(data),
                                onSuccess: function (resData) {
                                    Dolphin.ajax({
                                        url: '/api/1423c7225d134248944babc34f3a36f4@id=' + resData.value.id,
                                        type: Dolphin.requestMethod.GET,
                                        onSuccess: function (reData) {
                                            var condition = { properties : {
                                                _customerId : reData.value.topCustomerId,
                                                customer_id_obj_ae : reData.value.id
                                            }}
                                            Dolphin.ajax({
                                                url: '/api/f3e359d7dc5b4584b83b044bce0e5ae4',
                                                type: Dolphin.requestMethod.PUT,
                                                data: Dolphin.json2string(condition),
                                                onSuccess: function (reData) {
                                                    $('#shop_win').modal('hide');
                                                    me._architTree.reload();
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                        break;
                    default:
                        ;
                }

            });

            $('.imgupload').fileupload({
                url: publicContextPath + '/file/',
                dataType: 'json',
                done: function (e, data) {
                    var p = $(e.target).closest('.add-image');
                    var imgUrl = "/uploadFiles"+data.result.url;
                    p.empty();
                    $(".js-remove-sku-atom").show();
                    $('<img style="width : 180px ; height : 52px ;" src="'+publicContextPath+imgUrl+'" />')
                        .click(function(){
                            $("#showBigImg").attr("src", publicContextPath+imgUrl);
                            $('#myModal').modal('show');
                        })
                        .appendTo(p);
                    $("#logoUrl").val(imgUrl);

                    // thisPage.layoutData.img = data.result.originalName;
                    // thisPage.layoutData.link = imgUrl;
                },
                progressall: function (e, data) {
                }
            });
            //删除图片
            $(".js-remove-sku-atom").click(function(){
                var aa = $('.add-image').empty().html('+ ');
                $("#logoUrl").val("");
                $('<input style="position:absolute ; top:0 ; right:0 ; margin:0 ; opacity:0 ; cursor:pointer;"class="imgupload"  type="file" name="upfile" multiple>')
                    .appendTo(aa).fileupload({
                    url: publicContextPath + '/file/',
                    dataType: 'json',
                    done: function (e, data) {
                        var p = $(e.target).closest('.add-image');

                        var imgUrl = "/uploadFiles"+data.result.url;
                        p.empty();
                        $(".js-remove-sku-atom").show();
                        $('<img style="width : 180px ; height : 52px ;" src="'+publicContextPath+imgUrl+'" />')
                            .click(function(){
                                $("#showBigImg").attr("src", publicContextPath+imgUrl);
                                $('#myModal').modal('show');
                            })
                            .appendTo(p);
                        $("#logoUrl").val(imgUrl);
                        // thisPage.layoutData.img = data.result.originalName;
                        // thisPage.layoutData.link = imgUrl;
                    },
                    progressall: function (e, data) {
                    }
                });
                $(".js-remove-sku-atom").hide();
            });

        },
        /**
         * 组织树
         * @returns {*|TREE}
         */
        architTree: function () {
            let me = this;
            var accountType = org.breezee.context.userData.accountType;
            var url;
            switch(accountType){
                case 2: //客户
                    url = '/api/7848b7d9172340f29025c7c1a6ff0388@id='+org.breezee.context.userData.customerId;
                    break;
                case 3: //渠道商
                    url = '/api/b38af2da36324c61820a18fcda265d53@id='+org.breezee.context.userData.vendorId;
                    break;
                case 4: //目录
                    url = '/api/1423c7225d134248944babc34f3a36f4@id='+org.breezee.context.userData.customerId;
                    break;
                case 5: //店铺
                    url = '/api/1423c7225d134248944babc34f3a36f4@id='+org.breezee.context.userData.customerId;
                    break;
                default: //管理员
                    url = '/api/c477f55806ed4b3f934c3a21f8788dde';

            }

            return new Dolphin.TREE({
                panel: "#architTree",
                url:  url,
                idField : '_id',
                title : Dolphin.i18n.get("framework.tree"),
                multiple: false,
                onChecked: function (data) {
                    var publicContextPath = $("#publicContextPath").val() == '/' ? '' : $("#publicContextPath").val();
                    me.selectData = data;
                    $("#editId").val(data.id);
                    $("#editType").val(data.level);
                    if(data._parent != undefined){
                        $("#editParentId").val(data._parent.id);
                    }
                    switch(data.level){
                        case '1'://1代表渠道商
                            $("#device-panel").hide();
                            $("#shop-panel").hide();
                            $(".vendorBaseInfo").show();
                            $(".nonVendorBaseInfo").hide();
                            Dolphin.form.setValue(data, '#vendorBaseInfo');
                            $('#vendorShowImg').empty();
                            var link = '/uploadFiles/images/logo.png';
                            if(data.logoUrl != '' && data.logoUrl != null){
                                link = data.logoUrl;
                            }
                            $('<img style="width : 180px ; height : 52px ; cursor: pointer" src="'+publicContextPath+link +'" />')
                                .click(function(){
                                    $("#showBigImg").attr("src", publicContextPath+link);
                                    $('#myModal').modal('show');
                                })
                                .appendTo('#vendorShowImg');

                            me._shopList.load(('/api/742445c653654caaa4a1fa69eb651a9a'),{
                                "vendor_id_obj_ae": data.id,
                                "category":"3"
                            })

                            break;
                        case '2'://2代表客户
                            $("#device-panel").hide();
                            $("#shop-panel").show();
                            $(".customerBaseInfo").show();
                            $(".nonCustomerBaseInfo").hide();
                            if (!data.sourceCaption)
                                data.sourceCaption = Dolphin.enum.getEnumText("source", data.source) || data.source;
                            if (!data.industryCaption)
                                data.industryCaption = Dolphin.enum.getEnumText("industry", data.industry);
                            Dolphin.form.setValue(data, '#customerBaseInfo');
                            $('#customerShowImg').empty();
                            var link = '/uploadFiles/images/logo.png';
                            if(data.logoUrl != '' && data.logoUrl != null){
                                link = data.logoUrl;
                            }
                            $('<img style="width : 180px ; height : 52px ; cursor: pointer" src="'+publicContextPath+link +'" />')
                                .click(function(){
                                    $("#showBigImg").attr("src", publicContextPath+link);
                                    $('#myModal').modal('show');
                                })
                                .appendTo('#customerShowImg');

                            me._shopList.load(('/api/742445c653654caaa4a1fa69eb651a9a'),{
                                "parent_id_obj_ae": data.id,
                                "category":"3"
                            })

                            break;
                        case '3'://3代表目录
                            $("#device-panel").hide();
                            $("#shop-panel").hide();
                            $(".groupBaseInfo").show();
                            $(".nonGroupBaseInfo").hide();
                            Dolphin.form.setValue(data, '#groupBaseInfo');
                            break;
                        case '4'://4代表店铺
                            $("#device-panel").show();
                            $("#shop-panel").hide();
                            $(".shopBaseInfo").show();
                            $(".nonShopBaseInfo").hide();

                            if(data.guaranteeDeadLine){
                                data.guaranteeDeadLine = data.guaranteeDeadLine.substring(0,10);
                            }

                            Dolphin.form.setValue(data, '#shopBaseInfo');

                            $("#shopTypeTemp").html($("#shopType").find("option:selected").text());


                            if($("#followFlag")){
                                $("#followFlag").remove();
                            }
                            if(data.followed != '' && data.followed != null && data.followed == 'Y'){
                                var dataFollowed = 'N';
                                $('<a id="followFlag" title="'+Dolphin.i18n.get("framework.unfollow")
                                    +'" style="margin-left:10px;padding-top:6px;color:red;" href="#"><i class="glyphicon glyphicon-heart"></i>'
                                    +Dolphin.i18n.get("framework.following")+'</a>')
                                    .click(function(){
                                        var follow_this = this;
                                        Dolphin.confirm(Dolphin.i18n.get("framework.confirm")+$(follow_this).attr("title")+Dolphin.i18n.get("framework.thisStore")+"?", {
                                            callback: function (flag) {
                                                if (flag) {
                                                    data.followed = dataFollowed;
                                                    Dolphin.ajax({
                                                        url: '/api/94825f3cfd9d4dbaa3ff391687d50134',
                                                        type: Dolphin.requestMethod.PUT,
                                                        data: Dolphin.json2string({id : data.id , followed : dataFollowed }),
                                                        onSuccess: function (reData) {
                                                            if('N' == dataFollowed){
                                                                $(follow_this).attr("title", Dolphin.i18n.get("framework.toFollow"));
                                                                $(follow_this).empty().html('<i class="glyphicon glyphicon-heart-empty"></i>'+Dolphin.i18n.get("framework.notYetFollow"));
                                                                dataFollowed = 'Y'
                                                            }else{
                                                                $(follow_this).attr("title",Dolphin.i18n.get("framework.unfollow"));
                                                                $(follow_this).empty().html('<i class="glyphicon glyphicon-heart"></i>'+Dolphin.i18n.get("framework.following"));
                                                                dataFollowed = 'N'
                                                            }

                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    })
                                    .appendTo($("#shopForPos"));

                            }else{
                                var dataFollowed = 'Y';
                                $('<a id="followFlag" title='+Dolphin.i18n.get("framework.toFollow")+'" style="margin-left:10px;padding-top:6px;color:red;" href="#">' +
                                    '<i class="glyphicon glyphicon-heart-empty"></i>'+Dolphin.i18n.get("framework.notYetFollow")+'</a>')
                                    .click(function(){
                                        var follow_this = this;
                                        Dolphin.confirm(Dolphin.i18n.get("framework.confirm")+$(follow_this).attr("title")+Dolphin.i18n.get("framework.thisStore")+'？', {
                                            callback: function (flag) {
                                                if (flag) {
                                                    data.followed = dataFollowed;
                                                    Dolphin.ajax({
                                                        url: '/api/94825f3cfd9d4dbaa3ff391687d50134',
                                                        type: Dolphin.requestMethod.PUT,
                                                        data: Dolphin.json2string({id : data.id , followed : dataFollowed }),
                                                        onSuccess: function (reData) {
                                                            if('Y' == dataFollowed){
                                                                $(follow_this).attr("title",Dolphin.i18n.get("framework.unfollow"));
                                                                $(follow_this).empty().html('<i class="glyphicon glyphicon-heart"></i>'+Dolphin.i18n.get("framework.following"));
                                                                // $(follow_this).children("i").removeClass().addClass("glyphicon glyphicon-heart");
                                                                dataFollowed = 'N'

                                                            }else{
                                                                $(follow_this).attr("title",Dolphin.i18n.get("framework.toFollow"));
                                                                $(follow_this).empty().html('<i class="glyphicon glyphicon-heart-empty"></i>'+Dolphin.i18n.get("framework.notYetFollow"));
                                                                // $(follow_this).children("i").removeClass().addClass("glyphicon glyphicon-heart-empty");
                                                                dataFollowed = 'Y'
                                                            }

                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    })
                                    .appendTo($("#shopForPos"));
                            }
                            me._deviceList.load(('/api/b6a1808e4fcd4844836eb802249ac30d'),{
                                "customer_id_obj_ae" : data.id
                            });

                            break;
                        case '5'://5代表设备
                            $(".deviceBaseInfo").show();
                            $(".nonDeviceBaseInfo").hide();
                            Dolphin.form.setValue(data, '#deviceBaseInfo');
                            break;
                        default:
                            $("#device-panel").hide();
                            $("#shop-panel").hide();

                    }
                },
                onLoad: function () {
                    $(".vendorBaseInfo").hide();
                    $(".nonVendorBaseInfo").hide();
                    $("#device-panel").hide();
                    $("#shop-panel").hide();

                },
                formatterExpend: function (node, renderTree) {
                    //this   thisTree
                    //node   展开点击的所有信息
                    //renderTree   返回数据以后的回调函数，这个必须要调用，
                    //例如：
                    let childrenData;
                    switch(node.level){//1代表渠道商 2代表客户 3代表目录 4代表店铺 5代表设备
                        case '1':
                            Dolphin.ajax({
                                url: '/api/2d8841311e89471a92709511b1ac05ec@parentId='+node.id,
                                onSuccess: function (data) {
                                    renderTree(data);
                                }
                            });
                            break;
                        case '2':
                            Dolphin.ajax({
                                url: '/api/25712cc627f749889af30df1828fdc11@parentId='+node.id,
                                onSuccess: function (data) {
                                    renderTree(data);
                                }
                            });
                            break;
                        case '3':
                            Dolphin.ajax({
                                url: '/api/25712cc627f749889af30df1828fdc11@parentId='+node.id,
                                onSuccess: function (data) {
                                    renderTree(data);
                                }
                            });
                            break;
                        case '4':
                            Dolphin.ajax({
                                url: '/api/8ef98356469e46f3b53ae876000fc613@parentId='+node.id,
                                onSuccess: function (data) {
                                    renderTree(data);
                                }
                            });
                            break;
                        default:
                            childrenData = {rows:[{code:0, name:Dolphin.i18n.get("framework.negtive")}]};
                            renderTree(childrenData);

                    }
                }
            });
        },

        /**
         *  店铺列表
         * @param panelId
         * @returns {*|LIST}
         */
        shopList: function (panelId) {
            let me = this;
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: Dolphin.i18n.get("framework.storeName")
                }, {
                    code: 'code',
                    title: Dolphin.i18n.get("framework.storeCode"),
                }, {
                    code: 'parentName',
                    title: Dolphin.i18n.get("framework.superior"),
                },{
                    code: 'province',
                    title: Dolphin.i18n.get("framework.province"),
                }, {
                    code: 'city',
                    title: Dolphin.i18n.get("framework.city"),
                },{
                    code: 'openTime',
                    title: Dolphin.i18n.get("framework.businessTime"),
                    formatter:function (val,row) {
                        return row.openTime+"-"+row.closeTime;
                    }
                }],
                multiple: false,
                rowIndex: true,
                checkbox: false,
                ajaxType: Dolphin.requestMethod.POST,
                data: {rows: []},
                pagination: true,
                onLoadSuccess: function (data) {
                }
            });
        },

        /**
         *  设备列表
         * @param panelId
         * @returns {*|LIST}
         */
        deviceList: function (panelId) {
            let me = this;
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                queryParams:{
                    customerId:org.breezee.context.userData.customerId,
                    _day:'1'
                },
                idField: 'id',
                columns: [{
                    code: 'city',
                    title: Dolphin.i18n.get("framework.city"),
                    width: '70px',
                    formatter:function (val,row) {
                        return row.province+'<br/>'+row.city;
                    }
                }, {
                    code: 'customerName',
                    title: Dolphin.i18n.get("framework.store"),
                },{
                    code: 'code',
                    title: Dolphin.i18n.get("framework.equipment"),
                    width: '125px',
                    formatter:function (val,row) {
                        let colors = ['yellow', 'red', 'green'];
                        var statusVal = (row.status == 1 ? 1 : 2);
                        var statusText = (row.status == 1 ?Dolphin.i18n.get("equipment.offLine") :Dolphin.i18n.get("equipment.normal"));

                        var focus = '-';
                        if(row.status != 1) {
                            let v = row.focus ? Dolphin.i18n.get("equipment.looseFocus") : Dolphin.i18n.get("equipment.normal");
                            focus = '<i class="fa fa-circle" aria-hidden="true" style="margin-right: 10px;' + (row.focus == 1 ? 'color: red;' : 'color: green;') + '"></i>' + v;
                        }
                        var voltage = '-';
                        if(row.status != 1){
                            voltage = '<i class="fa fa-circle" aria-hidden="true" style="margin-right: 10px;' + (row.voltage < 10 ? 'color: red;' : 'color: green;') + '"></i>' + row.voltage+'%';
                        }

                        return Dolphin.i18n.get("framework.code")+row.code+'<br/>'
                            +Dolphin.i18n.get("framework.net")+'<i class="fa fa-circle" aria-hidden="true" style="margin-right: 10px;color:'+colors[statusVal]+';)"></i>'+statusText+'<br/>'
                            +Dolphin.i18n.get("framework.focusing")+focus+'<br/>'
                            +Dolphin.i18n.get("framework.powerVolumn")+voltage;
                    }
                }, {
                    code: 'lastReceiveTime',
                    title: Dolphin.i18n.get("framework.latestTime"),
                    width: '180px',
                    formatter:function (val) {
                        return val?Dolphin.longDate2string(val,'yyyy-MM-dd hh:mm:ss'):'-';
                    }
                }, {
                    code: 'properties.dxIn',
                    title: Dolphin.i18n.get("framework.totalNumberIntradayAndexit"),
                    formatter:function (val,row) {
                        return Dolphin.i18n.get("equipment.totalIntradayEntry")+":" + (val?val:'-')
                        +'<br/>'+Dolphin.i18n.get("equipment.toalIntradayExit")+":"+ (row.properties.dxOut ? row.properties.dxOut : '-');
                    }
                }, {
                    code: 'properties.dxOut',
                    title: Dolphin.i18n.get("equipment.toalIntradayExit"),
                    hidden: true
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '10px',
                    formatter: function (val, row, index) {
                        return org.breezee.buttons.edit({
                                id: row.id
                            })+'<p>'
                            + org.breezee.buttons.del({
                                id: row.id
                            });
                    }
                }],
                multiple: false,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,

                pagination: true,
                onLoadSuccess: function (ddd) {
                    org.breezee.buttons.editCallback('a7b6f1f193a2446d9a5579885a62e60f', 'id', function (data) {
                        $('#device_win').modal('show');
                        $("#myShopSel").val(data.value.customerId).trigger("change");
                    },'.device-edit-form');
                    org.breezee.buttons.delCallback('a7b6f1f193a2446d9a5579885a62e60f', function () {
                        me._deviceList.reload()
                    });
                    if(ddd.rows.length==0){
                        $("#emptyMsg").show();
                    } else {
                        $("#emptyMsg").hide();
                    }
                }
            });
        }
    };
    org.breezee.page.init();
});
