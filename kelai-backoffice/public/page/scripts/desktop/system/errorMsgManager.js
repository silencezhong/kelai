/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */
"use strict";
$(function () {
    org.breezee.page = {
        init: function () {
            Dolphin.form.parse();
            this.initEvent();
            this._msgList = this.msgList('#msgList');

        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            let me = this;
            /**
             * 绑定回车事件，触发搜索
             */
            $(document).keypress(function(e) {
                var eCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
                if (eCode == 13){
                    $(".btn-query").click();
                }
            });
            $('.content-refresh').click(function () {
                me._msgList.reload();
            });


            //查询
            $(".btn-query").click(function () {

                var queryCondition = Dolphin.form.getValue('.query-form');
                me._msgList.load('', queryCondition);
            });
        },
        msgList: function (panelId, queryParams) {
            let me = this, colors = ['yellow', 'red', 'green'];
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                queryParams: queryParams,
                idField: 'id',
                columns: [{
                    code: 'toUserName',
                    title: Dolphin.i18n.get("abnormal.receiver"),
                    width: '85px'
                }, {
                    code: 'toUserPhone',
                    title: Dolphin.i18n.get("framework.telephone"),
                    width: '75px'
                }, {
                    code: 'sendDateTime',
                    title: Dolphin.i18n.get("abnormal.sendingTime"),
                    width: '200px'
                }, {
                    code: 'errorMsg',
                    title: Dolphin.i18n.get("abnormal.sendingNews")
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '20px',
                    formatter: function (val, row, index) {

                    }
                }],
                multiple: false,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/265c624265ac4bb7ab43bfbbe3490c68',
                pagination: true,
                onLoadSuccess: function () {


                }
            });
        }
    };
    org.breezee.page.init();
});