/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */

$(function () {
    org.breezee.page = {
        /**
         * 初始化页面
         */
        init: function () {
            this.destroy();
            this._enumList = this.enumList('#enumList');
            this._enumItemList = this.enumItemList('#enumItemList', false);
            this._enumWinList = this.enumItemList('#enum_option_list', true);
            this.initEvent();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;
            $(".newEnum").click(function () {
                $('#enum_win').modal('show');
            });

            $(".enum_submit").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/7de6bd4a4b1941698babee70447ada68',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._enumList.reload();
                            $('#enum_win').modal('hide');
                        }
                    });

                }
            });
        },
        /**
         * 枚举信息列表，不分页
         * @param panelId
         * @returns {*|LIST}
         */
        enumList: function (panelId) {
            var me = this;
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: Dolphin.i18n.get("digital.enumerationName"),
                    textAlign: 'left'
                }, {
                    code: 'code',
                    title: Dolphin.i18n.get("digital.enumerationCode"),
                }, {
                    code: 'status',
                    title: Dolphin.i18n.get("digital.startUsingOrNot"),
                }, {
                    code: '',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        return '<a class="btn btn-outline btn-circle btn-sm purple editBtn" data-id="'+row.id+'" href="javascript:;">' +
                            '<i class="fa fa-edit"></i>&nbsp;'+Dolphin.i18n.get("account.edit")+'</a>'
                            +
                            '<a class="btn btn-outline btn-circle btn-sm dark delBtn" data-id="'+row.id+'" href="javascript:;">' +
                            '<i class="fa fa-trash-o"></i>&nbsp;'+Dolphin.i18n.get("role.delete")+'</a>';
                    }
                }],
                multiple: false,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/7de6bd4a4b1941698babee70447ada68',
                pagination: false,
                onClick: function (data, thisRow, event) {
                    me._enumItemList.loadData({rows: data.items});
                },
                onLoadSuccess: function () {
                    org.breezee.buttons.editCallback('fb941394b425441b858d31a2a75db60e', 'id', function (data) {
                        console.log(data);
                        me._enumWinList.loadData({rows: data.value.items});
                        $('#enum_win').modal('show');
                    });
                    org.breezee.buttons.delCallback('fb941394b425441b858d31a2a75db60e', function () {
                        me._enumList.reload();
                    });
                }
            });
        },
        /**
         * 枚举项列表
         * @param panelId
         * @param edit
         * @returns {*|LIST}
         */
        enumItemList: function (panelId, edit) {
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'pkId',
                columns: [{
                    code: 'id',
                    hidden: true,
                    title: Dolphin.i18n.get("dictionary.primaryKey"),
                }, {
                    code: 'name',
                    title: Dolphin.i18n.get("digital.termName"),
                }, {
                    code: 'code',
                    title: Dolphin.i18n.get("digital.termEncoding"),
                }, {
                    code: 'rowNum',
                    title: Dolphin.i18n.get("digital.sorting"),
                }],
                multiple: false,
                rowIndex: false,
                checkbox: false,
                editFlag: edit,
                data: {rows: []},
                pagination: false,
                editListName: 'items',
                onClick: function (data, thisRow, event) {

                },
                onRemoveRow: function (data, event, thisRow) {
                    if (thisRow.find('[listName="id"]').val()) {
                        thisRow.remove();
                    }
                }
            });
        },
        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
    org.breezee.page.init();
});
