/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

$(function () {
    org.breezee.page = {

        /**
         * 初始化页面
         */
        init: function () {
            this._planTypeList = this.planTypeList("#planTypeList");
            this.initEvent();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;
            $('.btn_submit').click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    Dolphin.ajax({
                        url: '/api/40e8c36d3ff6428899772e8bfd6e4233',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(Dolphin.form.getValue(ef)),
                        onSuccess: function (reData) {
                            me._planTypeList.reload();
                            $('#setting_win').modal('hide');
                        }
                    });
                }
            });
        },

        planTypeList: function (panelId) {
            var me = this;
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'id',
                ajaxType: Dolphin.requestMethod.GET,
                url: '/api/0816c303e7a949ca8962387969098257',
                columns: [{
                    code: 'name',
                    title: '计划类型描述'
                }, {
                    code: 'startDay',
                    title: '开始日期'
                },
                    {
                        code: 'endDay',
                        title: '结束日期'
                    }, {
                        code: 'code',
                        title: '值'
                    }, {
                        code: 'gift',
                        title: '礼盒标识',
                        formatter: function (val, row, index) {
                            if (val)
                                return "Y";
                            else
                                return "N";
                        }
                    }, {
                        code: 'offsetValue',
                        title: '月份偏移'
                    }, {
                        code: 'updator',
                        title: '更新人'
                    }, {
                        code: 'updateTime',
                        title: '更新时间'
                    }, {
                        code: '',
                        title: '&nbsp;',
                        width: '200px',
                        formatter: function (val, row, index) {
                            return org.breezee.buttons.del({
                                    id: row.id
                                }) + org.breezee.buttons.edit({
                                    id: row.id
                                });
                        }
                    }],
                multiple: false,
                pagination: false,
                onClick: function (data, thisRow, event) {
                },
                onLoadSuccess: function () {
                    org.breezee.buttons.editCallback('3502f8e58cb648869116d0d38c3c709d', 'id', function (data) {
                        $('#setting_win').modal('show');
                    });
                }
            });
        },

    };

    org.breezee.page.init();
});