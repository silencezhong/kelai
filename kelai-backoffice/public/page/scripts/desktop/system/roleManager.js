/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */

$(function () {
    'use strict';
    org.breezee.page = {
        init: function () {
            this._roleList = this.roleList('#roleList');
            this._accountList = this.accountList('#accountList');
            this._selectList = this.selectUserList('#selectedList');
            this._unSelectList = this.unSelectUserList('#unselectedList');
            this.initEvent();
            this.permitSelect = $("#permits_select").multipleSelect();
            $(".ms-parent ").css('width','100%');
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            let me = this;
            $(".newRole").click(function () {
                $('#role_win').modal('show');
            });
            /**
             * 保存按钮
             */
            $(".btn_submit").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    if (data._permits)
                        data.permits = data._permits.join(',');
                    Dolphin.ajax({
                        url: '/api/94c352955d4047f39c3b8bc6c4a12341',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._roleList.reload();
                            $('#role_win').modal('hide');
                        }
                    });

                }
            });
            $('.content-refresh').click(function () {
                me._roleList.reload();
            });

            $('.roleAccountBtn').click(function () {
                if (!me.selectData) {
                    alert(Dolphin.i18n.get("role.chooseRole"));
                    return;
                }
                $('#role_relation_win').modal('show');
            });
            /**
             * 保存角色人员关系
             */
            $('.relation_submit').click(function () {
                let data = {};
                data.id = me.selectData.id;
                data.accounts = [];
                let selData = me._selectList.data.rows;
                for (let i = 0; i < selData.length; i++) {
                    data.accounts.push({id:selData[i].id});
                }
                Dolphin.ajax({
                    url: '/api/41daac00b76c4769970e8cc7e12ddb8e',
                    type: Dolphin.requestMethod.POST,
                    data: Dolphin.json2string(data),
                    onSuccess: function (reData) {
                        me._accountList.load('/api/756af3ae31a64565a54ec0221f84d377', {"_roleId": data.id});
                        $('#role_relation_win').modal('hide');
                    }
                });
            });
            /**
             * 选择面板中的按钮
             */
            $('[change]').click(function () {
                let thisButton = $(this), allFlag = !!thisButton.attr('all'),
                    sourceList, targetList,
                    checkedData,
                    i;
                if (thisButton.attr("change") == "select") {
                    sourceList = me._unSelectList;
                    targetList = me._selectList;
                } else {
                    sourceList = me._selectList;
                    targetList = me._unSelectList;
                }
                if (allFlag) {
                    checkedData = [].concat(sourceList.data.rows);
                } else {
                    checkedData = sourceList.getChecked();
                }
                for (i = 0; i < checkedData.length; i++) {
                    if (!checkedData[i].inheritFlag) {
                        sourceList.removeRow(checkedData[i].__id__);
                        targetList.addRowWithData(checkedData[i]);
                    }
                }
            });

            $('#role_relation_win').on('show.bs.modal', function (e) {
                me._selectList.load('/api/4f78a136e6a745238434fc5f0a9c931c', {"_roleId": me.selectData.id});

                me._unSelectList.load('/api/756af3ae31a64565a54ec0221f84d377', {"_not_roleId": me.selectData.id});
            });

            $('.unselect-btn').click(function () {
                me._unSelectList.reload('',Dolphin.form.getValue('.unselect-account-form'));
            });

        },
        roleList: function (panelId) {
            let me = this;
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                title: Dolphin.i18n.get("role.list"),
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: Dolphin.i18n.get("role.name"),
                    width: '120px'
                }, {
                    code: 'code',
                    title: Dolphin.i18n.get("role.roleCode"),
                    width: '100px'
                }, {
                    code: 'permits',
                    title: Dolphin.i18n.get("role.authority"),
                    className: 'text-overflow',
                    formatter: function (val) {
                        return '<div style="max-width: 260px;overflow: hidden;text-overflow:ellipsis;white-space:nowrap;">' + val + '</div>'
                    }
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        return '<a class="btn btn-outline btn-circle btn-sm purple editBtn" data-id="'+row.id+'" href="javascript:;">' +
                            '<i class="fa fa-edit"></i>&nbsp;'+Dolphin.i18n.get("account.edit")+'</a>'
                            +
                            '<a class="btn btn-outline btn-circle btn-sm dark delBtn" data-id="'+row.id+'" href="javascript:;">' +
                            '<i class="fa fa-trash-o"></i>&nbsp;'+Dolphin.i18n.get("role.delete")+'</a>';
                    }
                }],
                multiple: false,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/94c352955d4047f39c3b8bc6c4a12341',
                pagination: false,
                onLoadSuccess: function () {
                    org.breezee.buttons.editCallback('b2e406a3d1d241aca116107a65e366a1', 'id', function (data) {
                        if(data.value && data.value.permits){
                            var pp = data.value.permits.split(",");
                            console.log(pp);
                            $("#permits_select").multipleSelect('setSelects',pp);
                        }
                        $('#role_win').modal('show');
                    });

                    org.breezee.buttons.delCallback('b2e406a3d1d241aca116107a65e366a1',function (data) {
                        me._roleList.reload();
                    });
                },
                onChecked:function (data) {
                    me.selectData = data;
                    me._accountList.load('/api/756af3ae31a64565a54ec0221f84d377', {"_roleId": data.id});
                }
            });
        },
        accountList: function (panelId) {
            let me = this;
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'id',
                panelType: 'panel-info',
                title: Dolphin.i18n.get("role.personnel.roles"),
                columns: [{
                    code: 'name',
                    title: Dolphin.i18n.get("role.userName"),
                    width: '120px'
                }, {
                    code: 'code',
                    title: Dolphin.i18n.get("role.userCode"),
                    width: '150px'
                }],
                multiple: false,
                rowIndex: true,
                checkbox: false,
                ajaxType: Dolphin.requestMethod.POST,
                data: {rows: []},
                pagination: true,
                onLoadSuccess: function (data) {
                }
            });
        },
        selectUserList: function (panelId) {
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                panelType: 'panel-warning',
                title: Dolphin.i18n.get("role.selectedList"),
                idField: 'id',
                maxHeight: '400px',
                columns: [{
                    code: 'name',
                    title: Dolphin.i18n.get("role.userName"),
                    width: '120px'
                }, {
                    code: 'code',
                    title: Dolphin.i18n.get("role.userCode"),
                    width: '100px'
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                data: {rows: []},
                pagination: false
            });
        },
        unSelectUserList: function (panelId) {
            return new Dolphin.LIST({
                panel: panelId,
                title: Dolphin.i18n.get("role.unselectedList"),
                panelType: 'panel-info',
                queryParams:{},
                maxHeight: '650px',
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: Dolphin.i18n.get("role.userName")
                }, {
                    code: 'code',
                    title: Dolphin.i18n.get("role.userCode"),
                    width: '120px'
                }],
                multiple: true,
                rowIndex: false,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                data: {rows: []},
                pagination: true
            });
        }
    };
    org.breezee.page.init();
});