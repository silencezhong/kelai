/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */

$(function () {
    org.breezee.page = {
        init: function () {
            Dolphin.form.parse();
            this.initEvent();
            this._dataList = this.dataList('#dataList');
            this.initSearch();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {

            var me = this;
            $('.btn-query').click(function () {
                var d = Dolphin.form.getValue('.query-form');
                me._dataList.reload(null,d);
            });

            $(".newData").click(function () {
                Dolphin.form.empty('.edit-form');
                $('#data_win').modal('show');
            });
            /**
             * 保存按钮
             */
            $(".btn_submit").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/6fee3ef986814f3483d783d6234cfadd',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.reload();
                            $('#data_win').modal('hide');
                        }
                    });
                }
            });


        },
        dataList: function (panelId) {
            var me = this;
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                queryParams: {

                },
                idField: 'id',
                columns: [{
                    code: 'code',
                    title: '员工编号',
                    width: '120px'
                }, {
                    code: 'name',
                    title: '股东姓名',
                    width: '120px'
                }, {
                    code: 'type',
                    title: '股东类型',
                    width: '120px'
                }, {
                    code: 'job',
                    title: '岗位',
                    width: '90px'
                }, {
                    code: 'store',
                    title: '归属门店',
                    width: '120px'
                }, {
                    code: 'realQuantity',
                    title: '实股',
                    width: '90px'
                }, {
                    code: 'virtualQuantity',
                    title: '虚股',
                    width: '90px'
                }, {
                    code: 'futureQuantity',
                    title: '期股',
                    width: '90px'
                }, {
                    code: 'totalQuantity',
                    title: '总股数',
                    width: '120px'
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        return org.breezee.buttons.edit({
                                id: row.id
                            })
                            + org.breezee.buttons.del({
                                id: row.id
                            });
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/6fee3ef986814f3483d783d6234cfadd',
                pagination: true,
                onLoadSuccess: function () {
                    org.breezee.buttons.editCallback('89d9476f7fed407c9b3832e676753922', 'id', function (data) {
                        $('#data_win').modal('show');
                    });
                    org.breezee.buttons.delCallback('89d9476f7fed407c9b3832e676753922', function () {
                        me._dataList.reload();
                    });
                }
            });
        },

        initSearch: function () {
            $("#customerId").bsSuggest({
                delayUntilKeyup: true,
                allowNoKeyword: false,   //是否允许无关键字时请求数据。为 false 则无输入时不执行过滤请求
                multiWord: false,         //以分隔符号分割的多关键字支持
                separator: ",",          //多关键字支持时的分隔符，默认为空格
                getDataMethod: "url",    //获取数据的方式，总是从 URL 获取
                effectiveFields: ["code", "name", "typeName"],
                effectiveFieldsAlias: {code: "编码", name: "名称", typeName: "类型"},
                idField: 'id',
                keyField: 'code',
                url:  (org.breezee.context.contextPath=='/'?'':org.breezee.context.contextPath)+'/api/5d7373dc094948ad977910b099d9abe2', //优先从url ajax 请求 json 帮助数据，注意最后一个参数为关键字请求参数
                listStyle: {
                    'padding-top': 0,
                    'max-height': '425px',
                    'max-width': '800px',
                    'overflow': 'auto',
                    'border': '1px solid',
                    'width': 'auto',
                    'transition': '0.3s',
                    '-webkit-transition': '0.3s',
                    '-moz-transition': '0.3s',
                    '-o-transition': '0.3s'
                },                              //列表的样式控制
                listAlign: 'left',              //提示列表对齐位置，left/right/auto
                listHoverStyle: 'background: #07d; color:#000', //提示框列表鼠标悬浮的样式
                fnAdjustAjaxParam: function (keyword, opt) {
                    return {
                        type: 'POST',
                        data: {
                            company: org.breezee.context.company,
                            province: org.breezee.context.userData.province,
                            channel: org.breezee.context.userData.channel,
                            customerId: keyword
                        },
                        dataType: 'json',
                        beforeSend: function (XMLHttpRequest) {
                            var requestHeaderParam = Dolphin.defaults.ajax.requestHeader;
                            for (var key in requestHeaderParam) {
                                XMLHttpRequest.setRequestHeader(key, requestHeaderParam[key]);
                            }
                        }
                    };
                },
                fnProcessData: function (json) {    // url 获取数据时，对数据的处理，作为 fnGetData 的回调函数
                    var index, len, data = {value: []};
                    data.value = json.rows;
                    //字符串转化为 js 对象
                    return data;
                }
            }).on('onDataRequestSuccess', function (e, result) {
                if (result.total == 0) {
                    $("#customername").val('');
                    $('#_hidden_customerName').val('');
                }
            }).on('onSetSelectValue', function (e, keyword, data) {
                $("#customername").val(data.name);
                $("#_hidden_customerName").val(data.code);
            }).on('onUnsetSelectValue', function () {
            });
        }
    };
    org.breezee.page.init();
});