/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */

$(function () {
    'use strict';
    org.breezee.page = {
        init: function () {
            Dolphin.form.parse();
            this.initEvent();
            this._userList = this.userList('#userList');
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;
            $(".newAccount").click(function () {
                Dolphin.form.empty('.edit-form');
                $('#account_win').modal('show');
            });

            /**
             * 保存按钮
             */
            $(".btn_submit").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    console.log(data);
                    Dolphin.ajax({
                        url: '/api/366fabe58fc34ce4a273df9f941ff1f1',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._userList.load();
                            $('#account_win').modal('hide');
                        }
                    });

                }
            });
            $('.content-refresh').click(function () {
                me._userList.reload();
            });

            $(".btn-query").click(function () {
                me._userList.load('', Dolphin.form.getValue('.query-form'));
            });

            $('.all-btn-lock').click(function () {
                var data = Dolphin.form.getValue('.query-form');
                me.userLock({properties: data, status: 0});
            });

            $('.all-btn-unlock').click(function () {
                var data = Dolphin.form.getValue('.query-form');
                me.userLock({properties: data, status: 1});
            });
            //155d0ad633dc46f98246926c4acceca1
            $('.restPwd').click(function () {
                var items = me._userList.getChecked();
                if (items.length > 0) {
                    Dolphin.confirm(Dolphin.i18n.get("timed.confirmResetPassword"), {
                        callback: function (flg) {
                            if (flg) {
                                var pl = [];
                                for (var i = 0; i < items.length; i++) {
                                    pl.push(items[i].id);
                                }
                                Dolphin.ajax({
                                    url: '/api/155d0ad633dc46f98246926c4acceca1',
                                    data: Dolphin.json2string({id: pl.join(',')}),
                                    type: Dolphin.requestMethod.POST,
                                    onSuccess: function (reData) {
                                        $(".btn-query").click();
                                    }
                                });
                            }
                        }
                    });
                }
            });

            $("#acctType").change(function () {
                var v = $(this).val();
                $(".cusCate").addClass('hidden');
                if (v == 1) {

                } else if (v == 2) {
                    $(".cusId").removeClass('hidden');
                } else if (v == 3) {
                    $(".chanId").removeClass('hidden');
                }
            });
        },
        userList: function (panelId) {
            var me = this;
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'id',
                queryParams:{

                },
                columns: [{
                    code: 'groupName',
                    title: Dolphin.i18n.get("timed.groupName")
                }, {
                    code: 'jobName',
                    title: Dolphin.i18n.get("timed.taskName")
                }, {
                    code: 'classPath',
                    title: Dolphin.i18n.get("timed.JOBAndPATH")
                }, {
                    code: 'description',
                    title: Dolphin.i18n.get("timed.taskDescription")
                }, {
                    code: 'cronExpression',
                    title: Dolphin.i18n.get("timed.kulunExpression")
                },{
                    code: 'status',
                    title: Dolphin.i18n.get("timed.operationStatus")
                },{
                    code: 'id',
                    title: '&nbsp;',
                    width: '230px',
                    formatter: function (val, row, index) {
                        return '<a class="btn btn-outline btn-circle btn-sm purple editBtn" data-id="'+row.id+'" href="javascript:;">' +
                            '<i class="fa fa-edit"></i>&nbsp;'+Dolphin.i18n.get("account.edit")+'</a>'
                            +
                            '<a class="btn btn-outline btn-circle btn-sm dark delBtn" data-id="'+row.id+'" href="javascript:;">' +
                            '<i class="fa fa-trash-o"></i>&nbsp;'+Dolphin.i18n.get("role.delete")+'</a>'
                            + '<a href="javascript:;" data-groupname="' + row.groupName
                            + '" data-jobname="' + row.jobName + '" ' +
                            'class="btn btn-outline btn-circle btn-sm dark lock_btn">' +
                            '<i class="fa fa-eye"></i>&nbsp;'+Dolphin.i18n.get("timed.execution")+'</a>';
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/366fabe58fc34ce4a273df9f941ff1f1',
                pagination: true,
                onLoadSuccess: function () {
                    org.breezee.buttons.editCallback('3d353971af7b4364a9f335df3e49b734', 'id', function (data) {
                        $('#account_win').modal('show');
                        $("#acctType").change();
                    });
                    org.breezee.buttons.delCallback('3d353971af7b4364a9f335df3e49b734',function (data) {
                        me._userList.reload();
                    });
                    $('.lock_btn').click(function () {
                        var _this = $(this);
                        me.userLock(_this.data());
                    });
                }
            });
        },
        userLock: function (data) {
            var me = this;
            Dolphin.ajax({
                url: '/api/b2d9bf7c278d4847896b909cbef9da13',
                data: Dolphin.json2string({'jobName' : data.jobname , 'groupName' : data.groupname}),
                type: Dolphin.requestMethod.POST,
                onSuccess: function (reData) {
                    console.log(reData);
                    // $(".btn-query").click();
                }
            });
        }
    };
    org.breezee.page.init();
});