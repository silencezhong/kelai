/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */

$(function () {
    org.breezee.page = {
        init: function () {
            this.sex_constant = ["", "男", "女"];
            this.initEvent();
            this._userList = this.userList('#userList');
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;
            $(".newAccount").click(function () {
                Dolphin.form.empty('.edit-form');
                $('#account_win').modal('show');
            });
            /**
             * 保存按钮
             */
            $(".btn_submit").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    if (data.company)
                        data.company = data.company.join(',');
                    Dolphin.ajax({
                        url: '/api/4c7b346d00854494aa6b97dd47f965eb',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._userList.load();
                            $('#account_win').modal('hide');
                        }
                    });

                }
            });
            $('.content-refresh').click(function () {
                me._userList.reload();
            });

            $(".btn-query").click(function () {
                me._userList.load('', Dolphin.form.getValue('.query-form'));
            });

            $('.all-btn-lock').click(function () {
                var data = Dolphin.form.getValue('.query-form');
                me.userLock({properties: data, status: 0});
            });

            $('.all-btn-unlock').click(function () {
                var data = Dolphin.form.getValue('.query-form');
                me.userLock({properties: data, status: 1});
            });

            $('.restPwd').click(function () {
                var items = me._userList.getChecked();
                if (items.length > 0) {
                    Dolphin.confirm("确认重置所选择用户的密码?", {
                        callback: function (flg) {
                            if (flg) {
                                var pl = [];
                                for (var i = 0; i < items.length; i++) {
                                    pl.push(items[i].id);
                                }
                                Dolphin.ajax({
                                    url: '/api/c3021c10fdfc48c0842856ff7f879247',
                                    data: Dolphin.json2string({id: pl.join(',')}),
                                    type: Dolphin.requestMethod.POST,
                                    onSuccess: function (reData) {
                                        $(".btn-query").click();
                                    }
                                });
                            }
                        }
                    });
                }
            });

        },
        /**
         * 用户列表
         * @param panelId
         * @returns {*|LIST}
         */
        userList: function (panelId) {
            var me = this;
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: '用户名称',
                    width: '140px'
                }, {
                    code: 'code',
                    title: '用户编码',
                    width: '140px'
                }, {
                    code: 'status',
                    title: '状态',
                    width: '120px',
                    formatter: function (val) {
                        return val ? '正常' : '<span style="color: red;">禁用</span>';
                    }
                }, {
                    code: 'sex',
                    title: '性别',
                    width: '75px',
                    formatter: function (val, data) {
                        val = val || 0;
                        return me.sex_constant[val];
                    }
                }, {
                    code: 'mobile',
                    title: '手机',
                    width: '140px'
                }, {
                    code: 'email',
                    title: '电子邮件'
                }, {
                    code: 'customerId',
                    title: '客户编码'
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        return org.breezee.buttons.edit({
                                id: row.id
                            })
                            + '<a href="javascript:;" data-status="' + Math.abs(row.status - 1) + '" data-id="' + val + '" ' +
                            'class="btn btn-outline btn-circle btn-sm dark lock_btn">' +
                            '<i class="fa fa-' + (row.status > 0 ? 'lock' : 'unlock') + '"></i>' + (row.status > 0 ? '锁定' : '解锁') + '</a>';
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/ce3f73c0482a4c98a06429a2e64b5f63',
                pagination: true,
                onLoadSuccess: function () {
                    org.breezee.buttons.editCallback('56eaa61cc42d46f8a49a8cf5aba22d2a', 'id', function (data) {
                        $('#account_win').modal('show');
                    });

                    $('.lock_btn').click(function () {
                        var _this = $(this);
                        me.userLock(_this.data());
                    });
                }
            });
        },
        userLock: function (data) {
            var me = this;
            Dolphin.ajax({
                url: '/api/c8140dab39d14ce59c7809dff09066e1',
                data: Dolphin.json2string(data),
                type: Dolphin.requestMethod.POST,
                onSuccess: function (reData) {
                    $(".btn-query").click();
                }
            });
        }
    };
    org.breezee.page.init();
});