/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */
"use strict";
$(function () {
    let curClickId = null;
    org.breezee.page = {
        init: function () {
            var loginCustomerId = $("#_loginCustomerId").val();
            var loginAccountId = $("#_loginAccountId").val();
            var loginVendorId = $("#_loginVendorId").val();
            if((loginCustomerId != null && loginCustomerId != "")
                || (loginVendorId != null && loginVendorId != "")){

                /*var optionUrl="/api/c666378c5d3b417d8e150c456a489d23@accountId="+loginAccountId;
                var ajaxType="get"
                var optionParam='{"properties":{}}'
                $("#customer_select").attr("optionUrl",optionUrl)
                    .attr("ajaxType",ajaxType)
                    .attr("optionParam", optionParam);*/

                let _this = $("#customer_select");
                Dolphin.ajax({
                    url: '/api/c666378c5d3b417d8e150c456a489d23@accountId='+loginAccountId,
                    type: Dolphin.requestMethod.GET,
                    async : false,
                    onSuccess: function (reData) {
                        _this.append(
                            '<option value="">--'+Dolphin.i18n.get("account.pleaseSelect")+'--</option>');
                        $.each(reData.rows,function(index,obj){
                            _this.append(
                                '<option value="' + obj.id + '">'
                                + obj.name + '</option>');
                        });
                    }
                });
                $(".js-example-basic-single").select2();

            }else{
                /*var optionUrl="/api/742445c653654caaa4a1fa69eb651a9a"
                var ajaxType="post"
                var optionParam = '{"properties":{"category":"1"}}';
                $("#customer_select").attr("optionUrl",optionUrl)
                    .attr("ajaxType",ajaxType)
                    .attr("optionParam", optionParam);*/

                let _this = $("#customer_select");
                Dolphin.ajax({
                    url: '/api/742445c653654caaa4a1fa69eb651a9a',
                    type: Dolphin.requestMethod.POST,
                    async : false,
                    data: Dolphin.json2string({properties : {"category":"1"}}),
                    onSuccess: function (reData) {
                        _this.append(
                            '<option value="">--'+Dolphin.i18n.get("account.pleaseSelect")+'--</option>');
                        $.each(reData.rows,function(index,obj){
                            _this.append(
                                '<option value="' + obj.id + '">'
                                + obj.name + '</option>');
                        });
                    }
                });
                $(".js-example-basic-single").select2();
            }
            Dolphin.form.parse();
            this.initHistoryList();
            this.initEvent();
            var queryObj = {
                customerId: org.breezee.context.userData.customerId,
                _day: '1',
                _busiHourFilter : '1'
            }
            if ($("#isFilterShow").val() == '-1') {
                queryObj.status_not_equal = '2';
                // queryObj.status = '1';
                $("#status").val('1,0');
            }
            Dolphin.ajax({
                url: '/api/0ba568cd128743e4aa19c369c4b0ebd9',
                type: Dolphin.requestMethod.POST,
                data: Dolphin.json2string({properties : { "selectCustomerId" : loginCustomerId , "selectShopType" : ''}}),
                async: false,
                loading:true,
                onSuccess: function (reData) {
                    if(reData.rows){
                        var shopIdArray = $.map(reData.rows,function(n){
                            return n.id;
                        });
                        queryObj._areaId = shopIdArray.join('-');
                    }
                }
            });
            this._userList = this.userList('#userList', queryObj);

            var ssel = $("#myShopSel");
            if(loginAccountId){
                Dolphin.ajax({
                    url: '/api/3c43c627b41e4fafbfdbfb526345b248@accountId='+loginAccountId,
                    type: Dolphin.requestMethod.GET,
                    async: false,
                    onSuccess: function (reData) {
                        ssel.append(
                            '<option value="">--'+Dolphin.i18n.get("account.pleaseSelect")+'--</option>');
                        $.each(reData.rows, function (index, obj) {
                            ssel.append(
                                '<option value="' + obj.id + '">'
                                + obj.name + '</option>');
                        });
                    }
                });
            }
        $(".js-example-basic-single").select2();

        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            let me = this;
            /**
             * 绑定回车事件，触发搜索
             */
            $(document).keypress(function(e) {
                var eCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
                if (eCode == 13){
                    $(".btn-query").click();
                }
            });

            $(".newAccount").click(function () {
                Dolphin.form.empty('.edit-form');
                Dolphin.form.setValue({
                    type: 'dtk3',
                    recordPeriod: '10',
                    uploadPeriod: '30',
                    level: '5'
                }, '.edit-form');
                $('#account_win').modal('show');
            });
            /**
             * 保存按钮
             */
            $(".btn_submit").click(function () {
                let ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    let data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/b6a1808e4fcd4844836eb802249ac30c',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._userList.load();
                            $('#account_win').modal('hide');
                        }
                    });

                }
            });

            $(".memo_save").click(function () {
                if(curClickId!=null){
                    Dolphin.ajax({
                        url: '/api/0b680cc8256c47fd8c64fc847d6cf0d3',
                        type: Dolphin.requestMethod.POST,
                        data: Dolphin.json2string({id:curClickId,memo:$("#memo_text").val()}),
                        async: false,
                        loading:true,
                        onSuccess: function (reData) {
                            if(reData.extProps.flag){
                                $('#memo_win').modal('hide');
                                /*var curRows = me._userList.getRows();
                                 for(var i=0;i<curRows.length;i++){
                                 if(curRows[i].id==curClickId){
                                 curRows[i].memo = $("#memo_text").val();
                                 }
                                 }*/
                                me._userList.goPage(me._userList.opts.pageNumber);
                            }else if(reData.extProps.flag){
                                alert("error");
                            }
                        }
                    });
                }


            });

            $('.content-refresh').click(function () {
                me._userList.reload();
            });

            //导出
            $(".btn-export").click(function () {

                var shopIds = '';
                var shopAreaCondition = $("#customer_select").val()|| $("#_loginCustomerId").val();//查询条件 区域
                if( shopAreaCondition != null && shopAreaCondition.length > 0 ){
                    Dolphin.ajax({
                        url: '/api/0ba568cd128743e4aa19c369c4b0ebd9',
                        type: Dolphin.requestMethod.POST,
                        data: Dolphin.json2string({properties : { "selectCustomerId" : shopAreaCondition , "selectShopType" : ''}}),
                        async: false,
                        loading:true,
                        onSuccess: function (reData) {
                            if(reData.rows){
                                var shopIdArray = $.map(reData.rows,function(n){
                                    return n.id;
                                });
                                shopIds = shopIdArray.join('-');
                            }
                        }
                    });
                }
                var queryCondition = Dolphin.form.getValue('.query-form');
                me._userList.opts.queryParams._busiHourFilter = "1";
                me._userList.opts.queryParams.isQuery="false";//是否为设备查询
                if(!$("#customer_select").val() && $("#shopType").val()){
                    delete me._userList.opts.queryParams._areaId;
                }else{
                    queryCondition._areaId = shopIds;
                }

                if (me._userList.opts.queryParams.status_not_equal) {
                    delete me._userList.opts.queryParams.status_not_equal;
                }

                var url = (org.breezee.context.contextPath=='/'?'':org.breezee.context.contextPath) +'/api/08dd4548faa14d3ba3c32d5099b775f3';
                var form = $("<form />");
                form.attr({"style": "display: none", "target": '_blank', "method": "post", "action": url});
                $('body').append(form);
                $.each(me._userList.opts.queryParams, function(key, value){
                    var input = $("<input>");
                    input.attr({"name": key, "value": value, "type": "hidden"});
                    console.log('key : '+key+' | value : '+value);
                    form.append(input);
                });
                form.submit();
                form.remove();
            });

            //查询
            $(".btn-query").click(function () {
                var queryCondition = Dolphin.form.getValue('.query-form');
                var accountType = $("#accountType").val();
                console.log('accountType : '+accountType);
                console.log(typeof accountType);
                if(accountType != '1'){
                    if(!(queryCondition._areaId
                            || queryCondition.code_like
                            || queryCondition.customer_id_obj_ae)){
                        Dolphin.alert(Dolphin.i18n.get("passenger.pleaseChooseOne"));
                        return;
                    }
                }


                var shopIds = '';
                var shopAreaCondition = $("#customer_select").val()|| $("#_loginCustomerId").val();//查询条件 区域
                if( shopAreaCondition != null && shopAreaCondition.length > 0 ){
                    Dolphin.ajax({
                        url: '/api/0ba568cd128743e4aa19c369c4b0ebd9',
                        type: Dolphin.requestMethod.POST,
                        data: Dolphin.json2string({properties : { "selectCustomerId" : shopAreaCondition , "selectShopType" : ''}}),
                        async: false,
                        loading:true,
                        onSuccess: function (reData) {
                            if(reData.rows){
                                var shopIdArray = $.map(reData.rows,function(n){
                                    return n.id;
                                });
                                shopIds = shopIdArray.join('-');
                            }
                        }
                    });
                }

                me._userList.opts.queryParams._busiHourFilter = "1";
                if(!$("#customer_select").val() && $("#shopType").val()){
                    delete me._userList.opts.queryParams._areaId;
                }else{
                    queryCondition._areaId = shopIds;
                }

                if (me._userList.opts.queryParams.status_not_equal) {
                    delete me._userList.opts.queryParams.status_not_equal;
                }

                me._userList.load('/api/4e149d0fb488470fb3e493ade50564b6', queryCondition);
            });
        },

        initHistoryList: function () {
            let me = this;
            $('#historyList').empty();
            this._historyList = new Dolphin.LIST({
                panel: '#historyList',
                idField: 'id',
                queryParams:{
                    status : 1
                },
                columns: [{
                    code: 'sn',
                    title: Dolphin.i18n.get("framework.equipmentCode")
                },{
                    code: 'happenTime',
                    title: Dolphin.i18n.get("sales.occurrenceTime"),
                    formatter:function (val) {
                        if(val.length==19){
                             return val;
                        }else{
                            val = val+"";
                            return val.substring(0,4)+"-"+val.substring(4,6)+"-"+val.substring(6,8)+" "
                                +val.substring(8,10)+":"+val.substring(10,12)+":"+val.substring(12,14);
                        }

                    }
                }, {
                    code: 'createdDate',
                    title: Dolphin.i18n.get("equipment.dataUploadedTime")
                }, {
                    code: 'dxin',
                    title: Dolphin.i18n.get("equipment.entry")
                },{
                    code: 'dxout',
                    title: Dolphin.i18n.get("equipment.exit")
                },{
                    code: 'focus',
                    title: Dolphin.i18n.get("equipment.looseFocusOrNot"),
                    formatter: function (val, row) {
                            return val ? Dolphin.i18n.get("equipment.looseFocus") : Dolphin.i18n.get("equipment.normal");
                    }
                }, {
                    code: 'battery',
                    title: Dolphin.i18n.get("framework.powerVolumn"),
                    formatter: function (val, row) {
                       return val ? val+'%' : '-' ;
                    }
                }],
                multiple: false,
                rowIndex: true,
                checkbox: false,
                pagination: true,
                ajaxType: Dolphin.requestMethod.POST,
                onLoadSuccess: function (data) {
                }
            });
        },

        userList: function (panelId, queryParams) {
            let me = this, colors = ['yellow', 'red', 'green'];
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                queryParams: queryParams,
                idField: 'id',
                columns: [{
                    code: 'province',
                    title: Dolphin.i18n.get("framework.province")+'<br/>'+Dolphin.i18n.get("framework.city"),
                    width: '90px',
                    formatter: function (val, row) {
                        return val+'<br/>'+row.city;
                    }
                }, {
                    code: 'code',
                    title: Dolphin.i18n.get("equipment.equipmentCode")
                }, {
                    code: 'customerName',
                    title: Dolphin.i18n.get("store.name"),
                    width: '200px'
                }, {
                    code: 'statusName',
                    title: Dolphin.i18n.get("framework.net"),
                    width: '95px',
                    formatter: function (val, row) {
                        var statusVal = (row.status == 1 ? 1 : 2);
                        var statusText = (row.status == 1 ? Dolphin.i18n.get("equipment.offLine") : Dolphin.i18n.get("equipment.normal"));
                        return '<i class="fa fa-circle" aria-hidden="true" style="margin-right: 10px;color:' + colors[statusVal] + ';"></i>' + statusText;
                    }
                }, {
                    code: 'focus',
                    title: Dolphin.i18n.get("framework.focusing"),
                    width: '92px',
                    formatter: function (val, row) {
                        if (row.status != 1) {
                            let v = val ? Dolphin.i18n.get("equipment.looseFocus") : Dolphin.i18n.get("equipment.normal");
                            return '<i class="fa fa-circle" aria-hidden="true" style="margin-right: 10px;' + (row.focus == 1 ? 'color: red;' : 'color: green;') + '"></i>' + v;
                        }else{
                            return "-";
                        }
                    }
                }, {
                    code: 'voltage',
                    title: Dolphin.i18n.get("equipment.batteryCapacity"),
                    width: '115px',
                    formatter: function (val, row) {

                            if (row.status != 1) {
                                if(row.charge === 2){
                                    return '<i class="fa fa-battery-half" aria-hidden="true" style="margin-right: 10px;color: green;"></i>'+Dolphin.i18n.get("equipment.charging");
                                }else {
                                    return '<i class="fa fa-circle" aria-hidden="true" style="margin-right: 10px;' + (val < 10 ? 'color: red;' : 'color: green;') + '"></i>' + val + '%';
                                }
                            } else {
                                return '-'
                            }

                    }
                }, {
                    code: 'lastReceiveTime',
                    title: Dolphin.i18n.get("framework.latestTime"),
                    width: '224px',
                    formatter: function (val) {
                        return val ? Dolphin.longDate2string(val, 'yyyy-MM-dd hh:mm:ss') : '-';
                    }
                }, {
                    code: 'properties.dxIn',
                    title: Dolphin.i18n.get("equipment.totalIntradayEntry"),
                    width: '85px',
                    textAlign: 'right',
                    formatter: function (val) {
                        return val ? val : '0';
                    }
                }, {
                    code: 'properties.dxOut',
                    title:  Dolphin.i18n.get("equipment.toalIntradayExit"),
                    width: '85px',
                    textAlign: 'right',
                    formatter: function (val) {
                        return val ? val : '0';
                    }
                }, {
                        code: 'memo',
                        title: Dolphin.i18n.get("equipment.remarks"),
                        width: '100px',
                        textAlign: 'left',
                        formatter: function (val) {
                            if(val&&val.length>7){
                                return '<a title="'+val+'" >'+val.substring(0,7)+"..."+'</a>';
                            }else if(val==null||val==""){
                                return '<a ></a>';
                            }else{
                                return '<a >'+val+'</a>';
                            }
                        }
                    }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '165px',
                    formatter: function (val, row, index) {
                        var s = '<a class="btn btn-outline btn-circle btn-sm btn-margin dark editBtn" ';
                        s += 'data-id' + '="' + row.id + '"';
                        s += ' href="javascript:;"><i class="fa fa-edit"></i>'+Dolphin.i18n.get("account.edit")+'</a>';

                        s += '<a class="btn btn-outline btn-circle btn-sm btn-margin dark delBtn" ';
                        s += 'data-id' + '="' + row.id + '"';
                        s += ' href="javascript:void(0);"><i class="fa fa-trash-o"></i>'+Dolphin.i18n.get("role.delete")+'</a>';

                        s += '<a class="btn btn-outline btn-circle btn-sm btn-margin dark memoBtn" ';
                        s += 'data-id' + '="' + row.id + '"';
                        s += ' href="javascript:;"><i class="fa fa-book"></i>'+Dolphin.i18n.get("equipment.remarks")+'</a>';

                        s += '<a class="btn btn-outline btn-circle btn-sm btn-margin dark historyBtn" ';
                        s += 'data-id' + '="' + row.id + '"';
                        s += ' href="javascript:;"><i class="fa fa-history"></i>'+Dolphin.i18n.get("equipment.history")+'</a>';

                        return s;


                        /*return org.breezee.buttons.edit({
                                id: row.id
                            })
                            + org.breezee.buttons.del({
                                id: row.id
                            });*/
                    }
                }],
                multiple: false,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,

                pagination: true,
                onLoadSuccess: function () {
                    org.breezee.buttons.editCallback('a7b6f1f193a2446d9a5579885a62e60f', 'id', function (data) {
                        $('#account_win').modal('show');
                        $("#myShopSel").val(data.value.customerId).trigger("change");
                    });
                    org.breezee.buttons.delCallback('a7b6f1f193a2446d9a5579885a62e60f', function () {
                        me._userList.reload()
                    });
                    $(".memoBtn").click(function () {
                        $('#memo_win').modal('show');
                        curClickId = $(this).data("id");
                        var _rows = me._userList.getRows();
                        for(var i=0;i<_rows.length;i++){
                            if(_rows[i].id==curClickId){
                                $("#memo_text").val(_rows[i].memo);
                            }
                        }

                    });

                    //查看历史记录
                    $(".historyBtn").click(function () {

                        var deviceId = $(this).data('id');
                        var condition = {'device_id_obj_ae' : deviceId};
                        me._historyList.load('/api/871d375052dc47689a208e66aae04d3a', condition);
                        $('#historyModal').modal('show');

                    });

                }
            });
        }
    };
    org.breezee.page.init();
});