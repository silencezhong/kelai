/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */

$(function () {

    org.breezee.menu.topSelect('system');

    org.breezee.page = {
        init: function () {
            var defaultMenu = $('#flyto').val() || $('.menu-item:first').children('a').data('menu');
            var realMenu;
            if(defaultMenu.indexOf('-') > 0){
                realMenu = defaultMenu.split('-')[0];
            }else{
                realMenu = defaultMenu;
            }
            var menuObj = {menu: realMenu};
            if($('#flyto').val() != '' && $('#flyto').val() != null){
                if(defaultMenu.indexOf('-') > 0){
                    menuObj.isFilterShow = -1;
                }else{
                    menuObj.isFilterShow = 1;
                }
            }
            org.breezee.menu.menuClick(menuObj);
            $('.'+realMenu).addClass("selected").siblings().removeClass("selected");
        }
    };
    org.breezee.page.init();

});
