/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

$(function () {
    org.breezee.page = {
        init: function () {
            Dolphin.form.parse();
            this.initEvent();
            this._marketList = this.marketList('#marketList');

            var shopCondition = {"category":"3"};
                var _this = $("#shop_select");
                Dolphin.ajax({
                    url: '/api/742445c653654caaa4a1fa69eb651a9a',
                    type: Dolphin.requestMethod.POST,
                    async : false,
                    data: Dolphin.json2string({properties : shopCondition}),
                    onSuccess: function (reData) {
                        $.each(reData.rows,function(index,obj){
                            _this.append(
                                '<option value="' + obj.id + '">'
                                + obj.name + '</option>');
                        });
                    }
                });
            $(".js-example-basic-multiple").select2({tags: true});

        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;
            $(".newAccount").click(function () {
                Dolphin.form.empty('.edit-form');
                $("#shop_select").val([]).trigger('change');
                $('#market_win').modal('show');
            });

            /**
             * 保存按钮
             */
            $(".btn_submit").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    if(data.startDate)
                        data.startDate = data.startDate+" 01:00:00";
                    if(data.endDate)
                        data.endDate = data.endDate+" 23:00:00";
                    console.log(data);
                    Dolphin.ajax({
                        url: '/api/d03e69ddc306417e8022fa0ab213db29',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string({properties : data}),
                        onSuccess: function (reData) {
                            me._marketList.load();
                            $('#market_win').modal('hide');
                        }
                    });

                }
            });
            $('.content-refresh').click(function () {
                me._marketList.reload();
            });

            $(".btn-query").click(function () {
                me._marketList.load('', Dolphin.form.getValue('.query-form'));
            });
        },
        marketList: function (panelId) {
            var me = this;
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: Dolphin.i18n.get("marketing.activityName")
                }, {
                    code: 'code',
                    title: Dolphin.i18n.get("marketing.activityCode")
                }, {
                    code: 'customersName',
                    title: Dolphin.i18n.get("marketing.storeOfActivity")
                }, {
                    code: 'status',
                    title: Dolphin.i18n.get("marketing.status"),
                    width: '80px',
                    formatter: function (val) {
                        return val ? Dolphin.i18n.get("equipment.normal") : '<span style="color: red;">'+Dolphin.i18n.get("account.forbiddenedUse")+'</span>';
                    }
                }, {
                    code: 'startDate',
                    title: Dolphin.i18n.get("marketing.startingTime"),
                    width: '180px'
                }, {
                    code: 'endDate',
                    title: Dolphin.i18n.get("marketing.endingTime"),
                    width: '180px'
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        return '<a class="btn btn-outline btn-circle btn-sm purple editBtn" data-id="'+row.id+'" href="javascript:;">' +
                            '<i class="fa fa-edit"></i>&nbsp;'+Dolphin.i18n.get("account.edit")+'</a>'
                            +
                            '<a class="btn btn-outline btn-circle btn-sm dark delBtn" data-id="'+row.id+'" href="javascript:;">' +
                            '<i class="fa fa-trash-o"></i>&nbsp;'+Dolphin.i18n.get("role.delete")+'</a>';
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/d03e69ddc306417e8022fa0ab213db29',
                pagination: true,
                onLoadSuccess: function () {
                    org.breezee.buttons.editCallback('eedd8d43769a40828d8c6e1b68de3265', 'id', function (data) {
                        $("#shop_select").val(data.value.customerArray).trigger('change');
                        $("#startDate").val(data.value.startDate.substr(0,10));
                        $("#endDate").val(data.value.endDate.substr(0,10));
                        $('#market_win').modal('show');
                    });
                    org.breezee.buttons.delCallback('eedd8d43769a40828d8c6e1b68de3265', function (data) {
                        $(".btn-query").click();
                    });
                }
            });
        }
    };
    org.breezee.page.init();
});