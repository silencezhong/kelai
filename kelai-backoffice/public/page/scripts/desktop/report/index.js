
$(function () {
    org.breezee.menu.topSelect('report');

    org.breezee.page = {
        init: function () {

            var defaultMenu = $('#flyto').val() || $('.menu-item:first').children('a').data('menu');
            var menuObj = {menu: defaultMenu};
            if($('#flyto').val() != '' && $('#flyto').val() != null){
                menuObj.isFilterShow = 1;
            }
            org.breezee.menu.menuClick(menuObj);
            $('.'+defaultMenu).addClass("selected").siblings().removeClass("selected");
        }
    };
    org.breezee.page.init();

});
