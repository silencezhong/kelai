/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

$(function () {
    org.breezee.page = {
        init: function () {
            Dolphin.form.parse();
            this.initEvent();
            this._userList = this.userList('#userList');
            this.initSearch();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;
            $(".newData").click(function () {
                Dolphin.form.empty('.edit-form');
                $('#account_win').modal('show');
            });

            $(".importData").click(function () {
                $("#upload_win").modal('show');
            });
            /**
             * 保存按钮
             */
            $(".btn_submit").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    if(data.happenDate)
                        data.happenDate = data.happenDate + " 12:00:00";
                    Dolphin.ajax({
                        url: '/api/27c5fc9e97b34e209468be93098cce58',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._userList.load();
                            $('#account_win').modal('hide');
                        }
                    });

                }
            });
            $('.content-refresh').click(function () {
                me._userList.reload();
            });

            $(".btn-query").click(function () {
                me._userList.load('', Dolphin.form.getValue('.query-form'));
            });
        },
        userList: function (panelId) {
            var me = this;
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'id',
                columns: [{
                    code: 'customerCode',
                    title: Dolphin.i18n.get("sales.storeCode"),
                    width: '150px'
                }, {
                    code: 'customerName',
                    title: Dolphin.i18n.get("framework.storeName")
                }, {
                    code: 'happenDate',
                    title: Dolphin.i18n.get("sales.occurrenceTime"),
                    width: '160px'
                }, {
                    code: 'totalAmount',
                    title: Dolphin.i18n.get("sales.tradingVolume"),
                    textAlign: 'right',
                    width: '120px'
                }, {
                    code: 'totalQuantity',
                    title: Dolphin.i18n.get("sales.tradingQuantity"),
                    textAlign: 'right',
                    width: '120px'
                }, {
                    code: 'totalCount',
                    title: Dolphin.i18n.get("sales.tradingPenNumber"),
                    textAlign: 'right',
                    width: '120px'
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '110px',
                    formatter: function (val, row, index) {
                        return '<a class="btn btn-outline btn-circle btn-sm dark delBtn" data-id="'+row.id+'" href="javascript:;">' +
                            '<i class="fa fa-trash-o"></i>&nbsp;'+Dolphin.i18n.get("role.delete")+'</a>';
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/27c5fc9e97b34e209468be93098cce58',
                pagination: true,
                onLoadSuccess: function () {
                    org.breezee.buttons.delCallback('b29ea6b792b9498c83643fa52d14dd49', function (data) {
                        $(".btn-query").click();
                    });
                }
            });
        },

        initSearch: function () {
            var me = this;
            $("#cusCode").bsSuggest({
                delayUntilKeyup: true,
                allowNoKeyword: false,   //是否允许无关键字时请求数据。为 false 则无输入时不执行过滤请求
                multiWord: false,         //以分隔符号分割的多关键字支持
                separator: ",",          //多关键字支持时的分隔符，默认为空格
                getDataMethod: "url",    //获取数据的方式，总是从 URL 获取
                effectiveFields: ["code", "name"],
                effectiveFieldsAlias: {store: Dolphin.i18n.get("sales.codes"), name: Dolphin.i18n.get("sales.name")},
                idField: 'id',
                keyField: 'code',
                url: (org.breezee.context.contextPath == '/' ? '' : org.breezee.context.contextPath) + '/api/742445c653654caaa4a1fa69eb651a9a', //优先从url ajax 请求 json 帮助数据，注意最后一个参数为关键字请求参数
                listStyle: {
                    'padding-top': 0,
                    'max-height': '425px',
                    'max-width': '800px',
                    'overflow': 'auto',
                    'border': '1px solid',
                    'width': 'auto',
                    'transition': '0.3s',
                    '-webkit-transition': '0.3s',
                    '-moz-transition': '0.3s',
                    '-o-transition': '0.3s'
                },                              //列表的样式控制
                listAlign: 'left',              //提示列表对齐位置，left/right/auto
                listHoverStyle: 'background: #07d; color:#000', //提示框列表鼠标悬浮的样式
                fnAdjustAjaxParam: function (keyword, opt) {
                    var data = {code: keyword, category: '3'};
                    return {
                        type: 'POST',
                        data: data,
                        dataType: 'json',
                        beforeSend: function (XMLHttpRequest) {
                            var requestHeaderParam = Dolphin.defaults.ajax.requestHeader;
                            for (var key in requestHeaderParam) {
                                XMLHttpRequest.setRequestHeader(key, requestHeaderParam[key]);
                            }
                        }
                    };
                },
                fnProcessData: function (json) {    // url 获取数据时，对数据的处理，作为 fnGetData 的回调函数
                    var index, len, data = {value: []};
                    data.value = json.rows;
                    //字符串转化为 js 对象
                    return data;
                }
            }).on('onDataRequestSuccess', function (e, result) {
                $("#cusName").val('');
                $("#customerId").val('');
            }).on('onSetSelectValue', function (e, keyword, data) {
                $("#cusName").val(data.name);
                $("#customerId").val(data.id)
            }).on('onUnsetSelectValue', function () {
            });
        }
    };
    org.breezee.page.init();
});