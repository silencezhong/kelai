/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */
/**
 * 将字符串格式为yyyy-MM-dd的字符串转换成日期
 * @param fDate
 * @returns {Date}
 */
function strToDate(fDate){
    var fullDate = fDate.split("-");
    return new Date(fullDate[0], fullDate[1]-1, fullDate[2], 0, 0, 0);
}

$(function () {
    org.breezee.page = {
        init: function () {
            var loginCustomerId = $("#_loginCustomerId").val();
            Dolphin.form.parse();
            this._dataList = this.dataList('#dataList');
            this.initEvent();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            let me = this;

            //分析
            $(".btn-query").click(function () {
                var marketText = $("#market_select").find("option:selected").text();
                $("#showDataDiv").css({width: "1200px",height:"600px"});
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('showDataDiv'));
                // 指定图表的配置项和数据
                var chartOption = {
                    title: {
                        text: marketText
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data:[]
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true,
                        width:'92%'
                    },
                    toolbox: {
                        feature: {
                            saveAsImage: {}
                        }
                    },
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,
                        data: []
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [

                    ]
                };

                chartOption.xAxis.data = [];
                chartOption.series = [];

                var queryConditionObj = Dolphin.form.getValue('.query-form');

                var columns = [{
                    code: 'happenTime',
                    title: Dolphin.i18n.get("equipment.time"),
                    textAlign:'center',
                    formatter:function (val) {
                            return val.substr(0, 10);
                    }
                }];
                var compareDetailRows = [];

                Dolphin.ajax({
                    url: '/api/44c7efa59fde4b37b2d1ca8957e271ba',
                    type: Dolphin.requestMethod.POST,
                    async : false,
                    data: Dolphin.json2string({properties : queryConditionObj}),
                    onSuccess: function (reData) {

                        var filterGroup = _.groupBy(reData.rows, function(obj){ return obj.shopName; });
                        var idx = 0;
                        _.each(filterGroup, function(value, key) {
                            chartOption.legend.data[idx] = key;

                            var shopIdx = 'shop' + idx;
                            columns.push({
                                code: shopIdx,
                                title: key
                            });

                            var xArray = [];
                            var yArray = [];
                            $.each(value ,function(index,obj){
                                xArray.push(obj.happenTime.substr(5,5));
                                yArray.push(obj.dxin);

                                if(compareDetailRows[index] == undefined){
                                    compareDetailRows[index] = {happenTime : obj.happenTime };
                                }
                                compareDetailRows[index][shopIdx] = obj.dxin;

                            });

                            chartOption.xAxis.data = xArray;
                            chartOption.series[idx] = {};
                            chartOption.series[idx].name = key;
                            chartOption.series[idx].type = 'line';
                            // chartOption.series[idx].stack = '总量';
                            chartOption.series[idx].data = yArray;
                            idx++;

                        });

                    }
                });
                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(chartOption,true);

                //对比数据明细
                $("#dataList").empty();
                me._dataList = new Dolphin.LIST({
                    panel: '#dataList',
                    idField: 'id',
                    columns: columns,
                    multiple: false,
                    rowIndex: true,
                    checkbox: false,
                    pagination:false,
                    data:{rows:compareDetailRows }

                });

            });

        },
        dataList: function (panelId) {}
    };
    org.breezee.page.init();

});