/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */

$(function () {

    $("#username").focus();

    $('#submit').click(function () {
        var data = Dolphin.form.getValue('form');
        var redirectUrl = $(this).data('redirect') || '/index';
        Dolphin.ajax({
            url: '/api/login',
            type: Dolphin.requestMethod.POST,
            data: Dolphin.json2string(data),
            success: function (reData, textStatus) {
                if(!reData.success){
                    $('.error-mess').css('display','block');
                    $('#error-message').html(reData.msg || Dolphin.i18n.get("login.reqErr"));
                }else{
                    $('.error-mess').css('display','none');
                    Dolphin.goUrl(redirectUrl);
                }
                $('#submit').attr("disabled", false).val(Dolphin.i18n.get("login.log.in"));
            },
            onError: function () {
                $('#submit').attr("disabled", false).val(Dolphin.i18n.get("login.log.in"));
            }
        });
        $('#submit').attr("disabled", true).val(Dolphin.i18n.get("login.log.in")+"...");
    });

    $("input[name='password']").keydown(function (e) {
        if (e.keyCode == 13) {
            $('#submit').click();
        }
    });
});