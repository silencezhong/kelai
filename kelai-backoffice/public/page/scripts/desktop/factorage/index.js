$(function () {
    org.breezee.menu.topSelect('factorage');

    org.breezee.page = {
        init: function () {
            var defaultMenu = $('.menu-item:first').children('a').data('menu');
            org.breezee.menu.menuClick({menu: defaultMenu});
            $('.'+defaultMenu).addClass("selected").siblings().removeClass("selected");
        }
    };
    org.breezee.page.init();

});