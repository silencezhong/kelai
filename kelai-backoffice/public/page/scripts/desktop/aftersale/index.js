$(function () {
    org.breezee.menu.topSelect('aftersale');

    org.breezee.page = {
        init: function () {
            Dolphin.form.parse();
            this.initEvent();
            this._repairList = this.repairList('#repairList');
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;


            $('.content-refresh').click(function () {
                me._repairList.reload();
            });

            $(".btn-query").click(function () {
                me._repairList.load('', Dolphin.form.getValue('.query-form'));
            });

            $(".modelSave").click(function () {
                let ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    let data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/0aac1b853c024d4c9dddae858934a38d',
                        type: Dolphin.requestMethod.POST,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load();
                            $('#dataModal').modal('hide');
                        }
                    });
                }
            });
        },
        repairList: function (panelId) {
            var me = this;
            $(panelId).empty();
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'id',
                queryParams:{
                    // customer_id_obj_ae:org.breezee.context.userData.customerId || ''
                },
                columns: [{
                    code: 'deviceName',
                    title: Dolphin.i18n.get("framework.equipmentName"),
                    width:'100px'
                }, {
                    code: 'deviceCode',
                    title: Dolphin.i18n.get("framework.equipmentCode"),
                    width:'110px'
                }, {
                    code: 'issueTime',
                    title: Dolphin.i18n.get("service.timeRepairCall"),
                    width:'160px'

                },{
                    code:'address',
                    title:Dolphin.i18n.get("repair.address"),
                    width:'160px'
                }, {
                    code: 'handleResult',
                    title: Dolphin.i18n.get("repair.result")
                },{
                    code: 'statusName',
                    title: Dolphin.i18n.get("repair.currentStatus"),
                    width: '100px'
                }, {
                    code: 'handleTime',
                    title: Dolphin.i18n.get("repair.handlingTime"),
                    width: '160px',
                    formatter:function (val) {
                        return val || '-';
                    }
                }, {
                    code: 'handler',
                    title: Dolphin.i18n.get("repair.handlingPerson"),
                    width: '130px',
                    formatter:function (val) {
                        return val || '-';
                    }
                },{
                    code: 'id',
                    title: '&nbsp;',
                    width: '110px',
                    formatter: function (val, row, index) {
                        return '<a class="btn btn-outline btn-circle btn-sm purple editBtn" data-id="'+row.id+'" href="javascript:;">' +
                            '<i class="fa fa-edit"></i>&nbsp;'+Dolphin.i18n.get("account.edit")+'</a>';
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/aa81acbe39c94ec9a64fe6cf092f9dc9',
                pagination: true,
                onLoadSuccess: function () {
                    org.breezee.buttons.editCallback('14290627d39a4f7fbdf74dc8968cdedc', 'id', function (data) {
                        $('#dataModal').modal('show');
                    });
                }
            });
        },

    };
    org.breezee.page.init();

});