/**
 * Created by luffyluo on 2017/7/25.
 */
(function ($) {
    var DOLPHIN_ECHARTS = {};

    DOLPHIN_ECHARTS.bar_chart = function(chartOption_legend_data,chartOption_yAxis_data,
                                         chartOption_series,chartDivId,cnt,maxNum){
        // console.log(chartOption_series[0].data[0]);
        $("#"+chartDivId).css({width: '100%' ,//((maxNum && maxNum>460) ? maxNum+"px":"100%") ,
                               height: ((40*(cnt?cnt:1)) > 300 ? 40*(cnt?cnt:1) : 300 ) +"px"});
        // console.log()
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById(chartDivId));
        // 指定图表的配置项和数据
        var chartOption = {
            title: {
                text: ''
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                left : 'left',
                data:[]
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            xAxis: {
                position : 'top',
                type: 'value'
                // boundaryGap: [0, 0.01]
            },
            yAxis: {
                type: 'category',
                axisLabel:{
                    interval:0//横轴信息全部显示
                },
                data: []
            },
            series: [

            ]
        };
        chartOption.legend.data = chartOption_legend_data;
        chartOption.yAxis.data = chartOption_yAxis_data;
        chartOption.series = chartOption_series;
        myChart.setOption(chartOption,true);
    };

    DOLPHIN_ECHARTS.line_chart = function(chartOption_legend_data,chartOption_xAxis_data,chartOption_series,
                                          chartDivId){

        $("#"+chartDivId).css({width: "100%",height:"500px"});
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById(chartDivId));
        // 指定图表的配置项和数据
        var chartOption = {
            title: {
                text: ''
            },
            tooltip: {
                trigger: 'item'
            },
            legend: {
                data:[]
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true,
                width:'92%'
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: []
            },
            yAxis: {
                type: 'value'
            },
            series: [

            ]
        };

        chartOption.legend.data = chartOption_legend_data;
        chartOption.xAxis.data = chartOption_xAxis_data;
        chartOption.series = chartOption_series;
        myChart.setOption(chartOption,true);

    };

    window.dolphinEcharts = DOLPHIN_ECHARTS;
    $.dolphinEcharts = DOLPHIN_ECHARTS;
})(jQuery);