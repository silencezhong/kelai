/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */

/**
 * dolphin i18n message file
 * Created by Shubert.Wang on 2016/1/15.
 */
Dolphin.i18n.addMessages({
    //core
    core_modalWin_title: 'System prompt',
    core_alert_title: 'System prompt',
    core_confirm_title: 'System prompt',
    core_prompt_title: 'System prompt',
    core_jsonDate2string_error: 'Parameter({1})wrong',
    core_ajax_error: 'Request failed',
    core_login_timeout: 'Login timeout',
    core_reLogin: 'Please login again',
    core_alert_countDown: 'Close automatically after {1} seconds',
    core_confirm_yes: 'OK',
    core_confirm_no: 'Cancel',
    core_prompt_ok: 'OK',
    core_prompt_cancel: 'Cancel',

    //enum
    enum_cannot_found: "EnumData({1})Not found"
});

//core
Dolphin.defaults.modalWin.title = 'System prompt';
Dolphin.defaults.alert.title = 'System prompt';
Dolphin.defaults.confirm.title = 'System prompt';
Dolphin.defaults.prompt.title = 'System prompt';